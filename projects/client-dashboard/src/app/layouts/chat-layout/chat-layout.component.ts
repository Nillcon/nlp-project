import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';

@Component({
    selector: 'app-chat-layout',
    templateUrl: './chat-layout.component.html',
    styleUrls: ['./chat-layout.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class ChatLayoutComponent implements OnInit {

    constructor () {}

    public ngOnInit (): void {
    }

}
