import { Component, OnInit, ChangeDetectionStrategy, OnDestroy } from '@angular/core';
import { AutoUnsubscribe } from 'ngx-auto-unsubscribe';

@AutoUnsubscribe()
@Component({
    selector: 'app-index-page',
    templateUrl: './index-page.component.html',
    styleUrls: ['./index-page.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class IndexPageComponent implements OnInit, OnDestroy {

    constructor (
    ) { }

    public ngOnInit (): void {
        // this.feed = merge(
        //     this.local,
        //   ).pipe(
        //     scan((acc: Message[], x: Message) => [...acc, x], [])
        // );
    }

    public ngOnDestroy (): void {}
}
