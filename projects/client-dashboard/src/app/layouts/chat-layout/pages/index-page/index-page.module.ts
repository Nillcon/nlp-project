import { NgModule } from '@angular/core';
import { IndexPageComponent } from './index-page.component';
import { IndexPageRoutingModule } from './index-page.routing';
import { WindowChatComponent } from './window-chat/window-chat.component';
import { ChatHeaderComponent } from './window-chat/chat-header/chat-header.component';
import { SharedModule } from '@ClientDashboard/app-shared.module';

import { ChatModule } from '@Shared/modules/chat';
import { ChatApiService } from '@ClientDashboard/services';
import { CHAT_API } from '@Shared/modules/chat/config';

@NgModule({
    declarations: [
        IndexPageComponent,
        WindowChatComponent,
        ChatHeaderComponent
    ],
    imports: [
        IndexPageRoutingModule,

        SharedModule,
        ChatModule,
    ],
    providers: [
        { provide: CHAT_API, useClass: ChatApiService }
    ]
})
export class IndexPageModule { }
