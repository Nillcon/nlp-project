import { NgModule } from '@angular/core';
import { ChatLayoutComponent } from './chat-layout.component';
import { ChatLayoutRoutingModule } from './chat-layout.routing';
import { IndexPageModule } from './pages/index-page/index-page.module';
import { SharedModule } from '@ClientDashboard/app-shared.module';

@NgModule({
    declarations: [
        ChatLayoutComponent
    ],
    imports: [
        ChatLayoutRoutingModule,

        IndexPageModule,
        SharedModule
    ]
})
export class ChatLayoutModule { }
