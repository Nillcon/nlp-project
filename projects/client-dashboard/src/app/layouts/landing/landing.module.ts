import { NgModule } from '@angular/core';
import { LandingComponent } from './landing.component';
import { LandingRoutingModule } from './landing.routing';
import { FeaturesModule } from '@ClientDashboard/features/features.module';
import { SharedModule } from '@ClientDashboard/app-shared.module';

import { CHAT_API } from '@Shared/modules/chat/config';
import { ChatApiService } from '@ClientDashboard/services';
import { WindowChatModule } from '@ClientDashboard/components/window-chat';

@NgModule({
    declarations: [
        LandingComponent,
        // WindowChatComponent,
        // ChatHeaderComponent
    ],
    imports: [
        LandingRoutingModule,

        FeaturesModule,
        SharedModule,

        WindowChatModule
        // ChatModule
    ],
    providers: [
    //     ChatApiService,
    //     { provide: CHAT_API, useClass: ChatApiService }
    ]
})
export class LandingModule {}
