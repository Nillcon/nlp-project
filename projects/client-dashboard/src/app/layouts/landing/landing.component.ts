import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { User } from '@ClientDashboard/features/user';
import { Observable } from 'rxjs';
import { UserStateService } from '@ClientDashboard/features/user/services/user-state.service';

@Component({
    selector: 'app-landing',
    templateUrl: './landing.component.html',
    styleUrls: ['./landing.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class LandingComponent implements OnInit {

    public userData$: Observable<User>;

    constructor (
        private readonly userStateService: UserStateService
    ) {}

    public ngOnInit (): void {
        this.initUserData();
    }

    public initUserData (): void {
        this.userData$ = this.userStateService.data$;
    }

}
