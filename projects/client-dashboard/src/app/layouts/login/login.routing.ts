import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login.component';
import { LoginPagesEnum } from './shared/login-pages.enum';

const routes: Routes = [
    {
        path: '',
        component: LoginComponent,
        children: [
            {
                path: LoginPagesEnum.SignIn,
                loadChildren: () => import('./pages/sign-in').then(m => m.SignInModule)
            },
            {
                path: LoginPagesEnum.ResetPassword,
                loadChildren: () => import('./pages/reset-password').then(m => m.ResetPasswordModule)
            }
        ]
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(routes)
    ],
    exports: [
        RouterModule
    ]
})
export class LoginRoutingModule {}
