import { NgModule } from '@angular/core';
import { LoginComponent } from './login.component';
import { LoginRoutingModule } from './login.routing';
import { AuthorizationModule } from '@ClientDashboard/features/authorization';
import { SharedModule } from '@ClientDashboard/app-shared.module';


@NgModule({
    declarations: [
        LoginComponent,
    ],
    imports: [
        SharedModule,

        LoginRoutingModule,
        AuthorizationModule
    ]
})
export class LoginModule {}
