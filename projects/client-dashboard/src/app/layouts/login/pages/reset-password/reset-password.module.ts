import { NgModule } from '@angular/core';
import { IConfig, NgxMaskModule } from 'ngx-mask';

import { AuthorizationModule } from '@ClientDashboard/features/authorization';
import { SharedModule } from '@ClientDashboard/app-shared.module';

import { ResetPasswordRoutingModule } from './reset-password.routing';
import { ResetPasswordComponent } from './reset-password.component';
import { ConfirmationBlockComponent } from './confirmation-block';
import { CreatePasswordComponent } from './create-password';


const maskConfig: Partial<IConfig> = {
    validation: false,
};

@NgModule({
    declarations: [
        ResetPasswordComponent,
        ConfirmationBlockComponent,
        CreatePasswordComponent
    ],
    imports: [
        ResetPasswordRoutingModule,

        AuthorizationModule,

        SharedModule,
        NgxMaskModule.forRoot(maskConfig),
    ]
})
export class ResetPasswordModule { }
