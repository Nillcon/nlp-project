import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { AppLayoutEnum } from '@ClientDashboard/enums';

@Component({
  selector: 'app-reset-password-page',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.scss']
})
export class ResetPasswordComponent implements OnInit {

    public AppLayoutEnum = AppLayoutEnum;

    public phone: string;

    public codeHasConfirmed: boolean = false;

    constructor (
    ) { }

    public ngOnInit (): void {
    }

    public onPhoneSended (phone: string): void {
        this.phone = phone;
    }

    public onCodeConfirmed (isConfirmed: boolean): void {
        this.codeHasConfirmed = isConfirmed;
    }

    public onPasswordSended (password: string): void {
        console.log(password);
    }

}
