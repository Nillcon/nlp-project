import { Component, OnInit, Output, EventEmitter, ChangeDetectorRef, ElementRef, ViewChild, ChangeDetectionStrategy } from '@angular/core';
import { FormControlTypeSafe } from 'form-type-safe';
import { Validators } from '@angular/forms';
import { AuthorizationService } from '@ClientDashboard/features/authorization/services';
import { Observable } from 'rxjs';
import { ToastService } from '@Shared/features/toast';

@Component({
    selector: 'app-confirmation-block',
    templateUrl: './confirmation-block.component.html',
    styleUrls: ['./confirmation-block.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class ConfirmationBlockComponent implements OnInit {

    @Output()
    public OnPhoneSended: EventEmitter<string> = new EventEmitter();

    @Output()
    public OnCodeConfirmed: EventEmitter<boolean> = new EventEmitter();

    @ViewChild('codeInput', { static: false, read: ElementRef })
    public codeInput: ElementRef;

    public phoneControl: FormControlTypeSafe<string> = new FormControlTypeSafe('', [
        Validators.required,
        Validators.pattern("[0-9 ]{12}")
    ]);

    public codeControl: FormControlTypeSafe<string> = new FormControlTypeSafe('', [
        Validators.required
    ]);

    public phoneHasSended: boolean = false;

    constructor (
        private readonly changeDetectorRef: ChangeDetectorRef,
        private readonly authorizationService: AuthorizationService,
        private readonly toastService: ToastService
    ) { }

    public ngOnInit (): void {}

    public onSendButtonClick (): void {
        if (this.phoneControl.valid) {
            this.sendPhone(this.phoneControl.value)
                .subscribe(() => {},
                error => {
                    this.makePhoneInvalid();

                    this.toastService.error({
                        description: error.error
                    });
                });

            this.makePhoneSended();
        } else {
            this.phoneControl.markAsTouched();
        }
    }

    public onConfirmButtonClick (): void {
        if (this.codeControl.valid && this.phoneHasSended) {
            this.confirmCode(this.codeControl.value, this.phoneControl.value)
                .subscribe(isConfirmed => {
                    if (isConfirmed) {
                        this.OnCodeConfirmed.emit(isConfirmed);
                    } else {
                        this.showCodeConfirmError();
                    }
                });
        } else {
            this.codeControl.markAsTouched();
        }
    }

    private makePhoneSended (): void {
        this.OnPhoneSended.emit(this.phoneControl.value);

        this.phoneHasSended = true;

        this.changeDetectorRef.markForCheck();

        this.phoneControl.disable();

        setTimeout(() => {
            this.codeInput.nativeElement.focus();
        });
    }

    private makePhoneInvalid (): void {
        this.phoneHasSended = false;
        this.phoneControl.enable();

        this.changeDetectorRef.markForCheck();
    }

    private confirmCode (code: string, phone: string): Observable<boolean> {
        return this.authorizationService.verifyPhoneToResetCode(code, phone);
    }

    private sendPhone (phone: string): Observable<any> {
        return this.authorizationService.sendPhoneToReset(phone);
    }

    private showCodeConfirmError (): void {
        this.toastService.error({
            description: 'Code is not valid'
        });
    }

}
