import { Component, OnInit, Output, EventEmitter, ChangeDetectorRef, Input } from '@angular/core';
import { FormControlTypeSafe } from 'form-type-safe';
import { Validators } from '@angular/forms';
import { AuthorizationService } from '@ClientDashboard/features/authorization/services';
import { Observable } from 'rxjs';
import { NavigationService } from '@Shared/features/navigation/navigation.service';
import { LoginPagesEnum } from '@ClientDashboard/layouts/login/shared';
import { ToastService } from '@Shared/features/toast';

@Component({
    selector: 'app-create-password',
    templateUrl: './create-password.component.html',
    styleUrls: ['./create-password.component.scss'],
    providers: [
        NavigationService
    ]
})
export class CreatePasswordComponent implements OnInit {

    @Input()
    public phone: string;

    @Output()
    public OnPasswordSave: EventEmitter<string> = new EventEmitter();

    public isPasswordVisible: boolean = false;

    public passwordControl: FormControlTypeSafe<string> = new FormControlTypeSafe('', [
        Validators.required
    ]);

    public confirmPasswordControl: FormControlTypeSafe<string> = new FormControlTypeSafe('', [
        Validators.required,
        this.validateConfirmPassword.bind(this)
    ]);

    constructor (
        private readonly authorizationService: AuthorizationService,
        private readonly changeDetectorRef: ChangeDetectorRef,
        private readonly navigationService: NavigationService,
        private readonly toastService: ToastService
    ) { }

    public ngOnInit (): void {
    }

    public onPasswordVisibilityButtonClick (): void {
        this.isPasswordVisible = !this.isPasswordVisible;
    }

    public onConfirmButtonClick (): void {
        if (this.passwordControl.valid && this.confirmPasswordControl.valid) {
            this.OnPasswordSave.emit(this.passwordControl.value);
            this.savePassword().subscribe(() => {
                this.navigationService.navigate({
                    subUrl: LoginPagesEnum.SignIn
                });
            },
            error => {
                this.toastService.error({
                    description: error.error
                });
            });
        } else {
            this.passwordControl.markAsTouched();
            this.confirmPasswordControl.markAsTouched();

            this.changeDetectorRef.markForCheck();
        }
    }

    private validateConfirmPassword (control: FormControlTypeSafe<string>): { passwordsAreDifferent: boolean } {
        if (this.passwordControl) {
            return (control.value === this.passwordControl.value) ? null : { passwordsAreDifferent: true };
        }

        return null;
    }

    private savePassword (): Observable<any> {
        return this.authorizationService.saveNewPassword(
            this.passwordControl.value,
            this.confirmPasswordControl.value,
            this.phone
        );
    }

}
