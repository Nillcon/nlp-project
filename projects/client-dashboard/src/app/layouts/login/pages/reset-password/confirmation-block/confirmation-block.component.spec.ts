import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfirmationBlockComponent } from './confirmation-block.component';

describe('ConfirmationBlockComponent', () => {
  let component: ConfirmationBlockComponent;
  let fixture: ComponentFixture<ConfirmationBlockComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConfirmationBlockComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfirmationBlockComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
