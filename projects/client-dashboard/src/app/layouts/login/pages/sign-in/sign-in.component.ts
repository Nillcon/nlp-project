import { Component, OnInit, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { FormGroupTypeSafe } from 'form-type-safe';
import { Subscription } from 'rxjs';

import { SignIn } from '@ClientDashboard/features/authorization';
import { AuthorizationService } from '@ClientDashboard/features/authorization/services';
import { AppLayoutEnum } from '@ClientDashboard/enums';

import { ToastService } from '@Shared/features/toast';
import { LoginPagesEnum } from '../../shared';
import { environment } from '@ClientDashboard/environment/environment';

@Component({
    selector: 'app-sign-in',
    templateUrl: './sign-in.component.html',
    styleUrls: ['./sign-in.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class SignInComponent implements OnInit {

    public LoginPagesEnum = LoginPagesEnum;

    public formGroup: FormGroupTypeSafe<SignIn>;
    public signInSubscription: Subscription;

    public readonly AppLayoutEnum = AppLayoutEnum;

    constructor (
        private readonly authorizationService: AuthorizationService,
        private readonly changeDetector: ChangeDetectorRef,
        private readonly toastService: ToastService
    ) { }

    public ngOnInit (): void {
    }

    public onFormInit (formGroup: FormGroupTypeSafe<SignIn>): void {
        this.formGroup = formGroup;
    }

    public onSignInButtonClick (): void {
        if (this.formGroup.valid) {
            this.signIn();
        } else {
            this.markAllAsTouched();
        }
    }

    private signIn (): void {
        this.signInSubscription = this.authorizationService.signIn(this.formGroup.value)
            .subscribe(
                () => {
                    location.href = environment.dashboardAppPath;
                },
                error => {
                    this.loginErrorMessage(error.error);
                }
            );
    }

    private markAllAsTouched (): void {
        this.formGroup.markAllAsTouched();

        setTimeout(() => {
            this.changeDetector.markForCheck();
            this.changeDetector.detectChanges();
        });
    }

    private loginErrorMessage (error: string): void {
        this.toastService.error({
            description: error
        });
    }

}
