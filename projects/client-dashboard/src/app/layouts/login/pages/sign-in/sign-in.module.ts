import { NgModule } from '@angular/core';
import { SignInComponent } from './sign-in.component';
import { SignInRoutingModule } from './sign-in.routing';
import { SharedModule } from '@ClientDashboard/app-shared.module';
import { AuthorizationModule } from '@ClientDashboard/features/authorization';
import { NavigationService } from '@Shared/features/navigation/navigation.service';

@NgModule({
    declarations: [
        SignInComponent
    ],
    imports: [
        SharedModule,
        AuthorizationModule,
        SignInRoutingModule
    ],
    providers: [
        NavigationService
    ],
    exports: [
        SignInComponent
    ]
})
export class SignInModule { }
