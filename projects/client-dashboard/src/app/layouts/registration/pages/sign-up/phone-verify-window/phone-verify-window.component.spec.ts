import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PhoneVerifyWindowComponent } from './phone-verify-window.component';

describe('PhoneVerifyWindowComponent', () => {
  let component: PhoneVerifyWindowComponent;
  let fixture: ComponentFixture<PhoneVerifyWindowComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PhoneVerifyWindowComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PhoneVerifyWindowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
