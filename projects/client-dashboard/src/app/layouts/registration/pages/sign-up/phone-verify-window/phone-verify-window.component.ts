import { Component, OnInit } from '@angular/core';
import { AuthorizationService } from '@ClientDashboard/features/authorization/services';
import { DynamicDialogRef, DynamicDialogConfig } from 'primeng-lts/api';
import { ToastService } from '@Shared/features/toast';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';

@Component({
  selector: 'app-phone-verify-window',
  templateUrl: './phone-verify-window.component.html',
  styleUrls: ['./phone-verify-window.component.scss']
})
export class PhoneVerifyWindowComponent implements OnInit {

    constructor (
        private readonly authorizationService: AuthorizationService,
        private readonly ref: DynamicDialogRef,
        private readonly toastService: ToastService,
        public config: DynamicDialogConfig
    ) { }

    public ngOnInit (): void {
    }

    public onPhoneVerify (code: string): void {
        this.verifyCode(code, this.config.data['phone'])
            .subscribe(res => {
                if (res === true) {
                    this.ref.close(res);
                }
            });

        this.ref.close(true);
    }

    private showCodeIsFalseToast (): void {
        this.toastService.error({
            title: 'Code is incorrect'
        });
    }

    private showCodeIsTrueToast (): void {
        this.toastService.success({
            title: 'Phone has been verify'
        });
    }

    private verifyCode (code: string, phone: string): Observable<boolean> {
        return this.authorizationService.verifyCode(code, phone)
            .pipe(
                tap(res => {
                    if (res) {
                        this.showCodeIsTrueToast();
                    } else {
                        this.showCodeIsFalseToast();
                    }
                })
            );
    }

}
