import { Component, OnInit, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { SignUp } from '@ClientDashboard/features/authorization';
import { FormGroupTypeSafe } from 'form-type-safe';
import { AuthorizationService } from '@ClientDashboard/features/authorization/services';
import { Subscription, Observable } from 'rxjs';
import { AppLayoutEnum } from '@ClientDashboard/enums';
import { DialogService } from 'primeng-lts/api';
import { PhoneVerifyWindowComponent } from './phone-verify-window/phone-verify-window.component';
import { ToastService } from '@Shared/features/toast';
import { NavigationService } from '@Shared/features/navigation/navigation.service';


@Component({
    selector: 'app-sign-up',
    templateUrl: './sign-up.component.html',
    styleUrls: ['./sign-up.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class SignUpComponent implements OnInit {

    public formGroup: FormGroupTypeSafe<SignUp>;

    public AppLayoutEnum = AppLayoutEnum;

    public signUpSubscription: Subscription;

    public isPhoneVerified: boolean = false;

    constructor (
        private readonly authorizationService: AuthorizationService,
        private readonly dialogService: DialogService,
        private readonly changeDetectorRef: ChangeDetectorRef,
        private readonly toastService: ToastService,
        private readonly navigationService: NavigationService
    ) { }

    public ngOnInit (): void {
    }

    public onFormInit (formGroup: FormGroupTypeSafe<SignUp>): void {
        this.formGroup = formGroup;
    }

    public onSignUpButtonClick (): void {
        if (this.formGroup.valid) {
            if (this.isPhoneVerified) {
                this.signUpSubscription = this.signUp()
                    .subscribe(() => {
                        this.navigateToLogin();
                    });
            } else {
                this.phoneIsNotVerifiedMessage();
            }
        } else {
            this.markAllAsTouched();
        }
    }

    public onPhoneVerifyButtonClick (number: string): void {
        this.sendPhoneToVerify(number)
            .subscribe(() => {
                this.openPhoneVerifyWindow();
            },
            error => {
                this.toastService.error({
                    description: error.error,
                });
            });
    }

    private signUp (): Observable<string> {
        return this.authorizationService.signUp(this.formGroup.value);
    }

    private markAllAsTouched (): void {
        this.formGroup.markAllAsTouched();
    }

    private sendPhoneToVerify (number: string): Observable<any> {
        return this.authorizationService.sendPhoneToVerify(number);
    }

    private openPhoneVerifyWindow (): void {
        const dialog = this.dialogService.open(PhoneVerifyWindowComponent, {
            header: 'Verify the phone',
            data: {
                phone: this.formGroup.controls.phone.value
            },
        });

        dialog.onClose
            .subscribe(res => {
                this.isPhoneVerified = true;
                this.changeDetectorRef.markForCheck();
            });
    }

    private phoneIsNotVerifiedMessage (): void {
        this.toastService.error({
            description: 'Phone has not been verify'
        });
    }

    private navigateToLogin (): void {
        this.navigationService.navigate({ subUrl: AppLayoutEnum.Login });
    }
}
