import { NgModule } from '@angular/core';
import { SignUpComponent } from './sign-up.component';
import { PhoneVerifyWindowComponent } from './phone-verify-window/phone-verify-window.component';
import { SharedModule } from '@ClientDashboard/app-shared.module';
import { AuthorizationModule } from '@ClientDashboard/features/authorization';
import { NavigationService } from '@Shared/features/navigation/navigation.service';
import { SignUpRoutingModule } from './sign-up.routing';

@NgModule({
    declarations: [
        SignUpComponent,
        PhoneVerifyWindowComponent
    ],
    imports: [
        SignUpRoutingModule,

        SharedModule,
        AuthorizationModule
    ],
    providers: [
        NavigationService
    ],
    entryComponents: [
        PhoneVerifyWindowComponent
    ]
})
export class SignUpModule {}
