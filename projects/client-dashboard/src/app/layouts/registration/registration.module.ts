import { NgModule } from '@angular/core';
import { RegistrationComponent } from './registration.component';
import { RegistrationRoutingModule } from './registration.routing';
import { SharedModule } from '@ClientDashboard/app-shared.module';

@NgModule({
    declarations: [
        RegistrationComponent,
    ],
    imports: [
        SharedModule,

        RegistrationRoutingModule,
    ]
})
export class RegistrationModule {}
