import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { trigger, transition, useAnimation } from '@angular/animations';
import { bounceIn, fadeIn } from 'ng-animate';

@Component({
    selector: 'app-registration',
    templateUrl: './registration.component.html',
    styleUrls: ['./registration.component.scss'],
    animations: [
        trigger('wrapperAnimation', [
            transition(':enter', useAnimation(fadeIn, { params: { timing: 0.15 } }))
        ])
    ],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class RegistrationComponent implements OnInit {

    constructor () {}

    public ngOnInit (): void {}

}
