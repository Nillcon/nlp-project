import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RegistrationComponent } from './registration.component';
import { RegistrationPagesEnum } from './shared/registration-pages.enum';

const routes: Routes = [
    {
        path: '',
        component: RegistrationComponent,
        children: [
            {
                path: RegistrationPagesEnum.SignUp,
                loadChildren: () => import('./pages/sign-up').then(m => m.SignUpModule)
            }
        ]
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(routes)
    ],
    exports: [
        RouterModule
    ]
})
export class RegistrationRoutingModule {}
