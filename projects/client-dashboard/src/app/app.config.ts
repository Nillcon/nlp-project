import { UserService } from '@ClientDashboard/features/user';

export class AppConfig {

    constructor (
        private readonly userService: UserService
    ) {}

    public load (): any {
        return this.userService.update()
            .toPromise()
            .catch(() => {
                return true;
            });
    }

}
