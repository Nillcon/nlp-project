import { BrowserModule } from '@angular/platform-browser';
import { NgModule, APP_INITIALIZER } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppConfig } from './app.config';
import { SharedModule } from './app-shared.module';
import { AppRoutingModule } from './app.routing';
import { AppComponent } from './app.component';

import { ChatModule } from '@progress/kendo-angular-conversational-ui';
import { InputsModule } from '@progress/kendo-angular-inputs';
import { ButtonsModule } from '@progress/kendo-angular-buttons';

import { HTTP_HOST, HTTP_API_ROUTE, DEBUG_HOST_STORAGE_KEY, ACCESS_TOKEN_STORAGE_KEY } from '@Shared/features';
import { environment } from '@ClientDashboard/environment/environment';
import { FeaturesModule } from '@ClientDashboard/features/features.module';

import { StorageKey } from './enums/storage-key.enum';

import {
    ToastsContainerComponent,
 } from './components';

@NgModule({
    declarations: [
        AppComponent,
        ToastsContainerComponent,
    ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        HttpClientModule,

        FeaturesModule,
        SharedModule,
        BrowserAnimationsModule,
        ChatModule,
        InputsModule,
        ButtonsModule,
    ],
    providers: [
        AppConfig,
        { provide: HTTP_HOST, useValue: environment.host },
        { provide: HTTP_API_ROUTE, useValue: environment.apiRoute },
        { provide: DEBUG_HOST_STORAGE_KEY, useValue: StorageKey.Debug_Host },
        { provide: ACCESS_TOKEN_STORAGE_KEY, useValue: StorageKey.Access_Token },
        { provide: APP_INITIALIZER, useFactory: (config: AppConfig) => () => config.load(), deps: [AppConfig], multi: true }
    ],
    bootstrap: [
        AppComponent
    ]
})
export class AppModule {}
