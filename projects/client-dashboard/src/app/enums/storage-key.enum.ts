export enum StorageKey {
    Access_Token   = 'access_token',
    Debug_Host     = 'debug_host',
    User_Room_Id   = 'user_room_id'
}
