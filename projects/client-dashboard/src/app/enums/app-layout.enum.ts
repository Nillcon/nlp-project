export enum AppLayoutEnum {
    Landing         = 'Landing',
    Login           = 'Login',
    Registration    = 'Registration'
}
