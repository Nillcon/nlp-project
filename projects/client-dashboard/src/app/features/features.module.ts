import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { UserModule } from '@ClientDashboard/features/user';

@NgModule({
    imports: [
        UserModule
    ],
    exports: [
        UserModule
    ]
})
export class FeaturesModule {}
