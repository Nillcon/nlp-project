import { Component, OnInit, Input, ChangeDetectionStrategy, Output, EventEmitter } from '@angular/core';
import { User } from '../../interfaces';
import { ClassValidation } from '@Shared/decorators';
import { IsObject, IsNumber } from 'class-validator';

@ClassValidation()
@Component({
    selector: 'user-avatar',
    templateUrl: './avatar.component.html',
    styleUrls: ['./avatar.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class AvatarComponent implements OnInit {

    @Input()
    @IsObject()
    public readonly userData: User;

    @Input()
    @IsNumber()
    public readonly size: number = 50;

    @Output()
    public OnClick: EventEmitter<MouseEvent> = new EventEmitter();

    constructor () {}

    public ngOnInit (): void {}

}
