import { Observable, BehaviorSubject } from 'rxjs';
import { User } from '../interfaces/user.interface';
import { Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root'
})
export class UserStateService {

    private readonly _isAuthorized$ = new BehaviorSubject<boolean>(null);
    private readonly _data$         = new BehaviorSubject<User>(null);

    public get isAuthorized$ (): Observable<boolean> {
        return this._isAuthorized$.asObservable();
    }

    public get isAuthorized (): boolean {
        return this._isAuthorized$.getValue();
    }

    public get data$ (): Observable<User> {
        return this._data$.asObservable();
    }

    public get data (): User {
        return this._data$.getValue();
    }

    constructor () {}

    public setIsAuthorized (state: boolean): void {
        this._isAuthorized$.next(state);
    }

    public setData (data: User): void {
        this._data$.next(data);
    }

}
