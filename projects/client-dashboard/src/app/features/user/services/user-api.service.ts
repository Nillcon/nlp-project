import { HttpRequestService } from '@Shared/features';
import { Observable } from 'rxjs';
import { User } from '../interfaces';
import { Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root'
})
export class UserApiService {

    constructor (
        private readonly httpService: HttpRequestService
    ) {}

    public getInfo (): Observable<User> {
        return this.httpService.get(`/Users/Info`);
    }

    // public getUserById (): Observable<boolean> {
    //     return this.httpService.get(`/Users/Info`);
    // }

    public getChatRoomId (): Observable<string> {
        return this.httpService.get('/Users/ChatRoomId');
    }

}
