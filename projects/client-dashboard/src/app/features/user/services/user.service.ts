import { UserApiService } from './user-api.service';
import { Observable, throwError } from 'rxjs';
import { User } from '../interfaces';
import { Injectable } from '@angular/core';
import { catchError, tap } from 'rxjs/operators';
import { UserStateService } from './user-state.service';

@Injectable({
    providedIn: 'root'
})
export class UserService {

    constructor (
        private readonly userStateService: UserStateService,
        private readonly userApiService: UserApiService
    ) {}

    public update (): Observable<User> {
        return this.getInfo()
            .pipe(
                catchError(() => {
                    this.makeUserUnauthorized();
                    return throwError(null);
                }),
                tap((data) => {
                    if (data) {
                        this.makeUserAuthorized(data);
                    } else {
                        this.makeUserUnauthorized();
                    }
                })
            );
    }

    public getInfo (): Observable<User> {
        return this.userApiService.getInfo();
    }

    public getChatRoomId (): Observable<string> {
        return this.userApiService.getChatRoomId();
    }

    private makeUserUnauthorized (): void {
        this.userStateService.setIsAuthorized(false);
        this.userStateService.setData(null);
    }

    private makeUserAuthorized (data: User): void {
        this.userStateService.setIsAuthorized(true);
        this.userStateService.setData(data);
    }

}
