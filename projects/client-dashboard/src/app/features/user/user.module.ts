import { NgModule } from '@angular/core';
import { UserService, UserApiService } from './services';
import { SharedModule } from '@ClientDashboard/app-shared.module';
import { AvatarComponent } from './components/avatar';

@NgModule({
    declarations: [
        AvatarComponent
    ],
    imports: [
        SharedModule
    ],
    exports: [
        AvatarComponent
    ],
    providers: [
        UserApiService,
        UserService
    ]
})
export class UserModule {}
