import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';

import { LocalStorageService } from '@Shared/features';
import { StorageKey } from '@ClientDashboard/enums/storage-key.enum';

import { AuthorizationApiService } from './authorization-api.service';
import { SignIn, SignUp } from '../interfaces';

@Injectable()
export class AuthorizationService {

    constructor (
        private readonly authorizationApiService: AuthorizationApiService,
        private readonly localStorageService: LocalStorageService
    ) { }

    public signIn (signIn: SignIn): Observable<string> {
        return this.authorizationApiService.signIn(signIn)
            .pipe(
                tap(token => {
                    this.localStorageService.set(StorageKey.Access_Token, token);
                })
            );
    }

    public signUp (signUp: SignUp): Observable<string> {
        return this.authorizationApiService.signUp(signUp);
    }

    public checkLoginExist (login: string): Observable<boolean> {
        return this.authorizationApiService.checkLoginExist(login);
    }

    public checkPhoneExist (phone: string): Observable<boolean> {
        return this.authorizationApiService.checkPhoneExist(phone);
    }

    public checkEmailExist (email: string): Observable<boolean> {
        return this.authorizationApiService.checkEmailExist(email);
    }

    public sendPhoneToVerify (phone: string): Observable<any> {
        phone = this.preparePhone(phone);

        return this.authorizationApiService.sendPhoneToVerify(phone);
    }

    public verifyCode (code: string, phone: string): Observable<boolean> {
        return this.authorizationApiService.verifyCode(code, phone);
    }

    public sendPhoneToReset (phone: string): Observable<any> {
        phone = this.preparePhone(phone);

        return this.authorizationApiService.sendPhoneToReset(phone);
    }

    public verifyPhoneToResetCode (code: string, phone: string): Observable<boolean> {
        return this.authorizationApiService.verifyCode(code, phone);
    }

    public saveNewPassword (password: string, confirmPassword: string, phone: string): Observable<any> {
        return this.authorizationApiService.saveNewPassword({
            password: password,
            confirmPassword: confirmPassword,
            phone: phone
        });
    }

    private preparePhone (phone: string): string {
        if (phone[0] !== '+') {
            phone = `+${phone}`;
        }

        return phone;
    }

}
