import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { HttpRequestService } from '@Shared/features';
import { SignIn, SignUp, Sms, VerifyCode, RecoveryPassword } from '../interfaces';
import { SmsType } from '../shared';

@Injectable()
export class AuthorizationApiService {

    constructor (
        private readonly httpService: HttpRequestService
    ) { }

    public signIn (signInData: SignIn): Observable<string> {
        return this.httpService.post(`/Users/SignIn`, signInData);
    }

    public signUp (signUpData: SignUp): Observable<any> {
        return this.httpService.post(`/Users`, signUpData);
    }

    public checkLoginExist (login: string): Observable<boolean> {
        return this.httpService.get(`/Users/IsLoginExist?searchString=${login}`);
    }

    public checkPhoneExist (phone: string): Observable<boolean> {
        return this.httpService.get(`/Users/IsPhoneExist?searchString=${phone}`);
    }

    public checkEmailExist (email: string): Observable<boolean> {
        return this.httpService.get(`/Users/IsEmailExist?searchString=${email}`);
    }

    public sendPhoneToVerify (phone: string): Observable<any> {
        const sms: Sms = {
            phone: phone,
            type: SmsType.Registration
        };

        return this.httpService.post(`/Sms/Send`, sms);
    }

    public verifyCode (code: string, phone: string): Observable<boolean> {
        const verifyCode: VerifyCode = {
            code,
            phone,
            type: SmsType.Reset
        };

        return this.httpService.post(`/Sms/VerifyCode`, verifyCode);
    }

    public sendPhoneToReset (phone: string): Observable<any> {
        const sms: Sms = {
            phone: phone,
            type: SmsType.Reset
        };

        return this.httpService.post(`/Sms/Send`, sms);
    }

    public verifyPhoneToResetCode (code: string, phone: string): Observable<boolean> {
        const verifyCode: VerifyCode = {
            code,
            phone,
            type: SmsType.Reset
        };

        return this.httpService.post(`/Sms/VerifyCode`, verifyCode);
    }

    public saveNewPassword (recoveryPassword: RecoveryPassword): Observable<any> {
        return this.httpService.put(`/Users/RecoveryPassword`, recoveryPassword);
    }
}
