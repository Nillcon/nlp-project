export interface RecoveryPassword {
    password: string;
    confirmPassword: string;
    phone: string;
}
