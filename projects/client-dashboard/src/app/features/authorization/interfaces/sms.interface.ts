import { SmsType } from '../shared';

export interface Sms {
    phone: string;
    type: SmsType;
}
