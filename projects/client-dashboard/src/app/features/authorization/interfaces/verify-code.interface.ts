import { SmsType } from '../shared';

export interface VerifyCode {
    code: string;
    phone: string;
    type: SmsType;
}
