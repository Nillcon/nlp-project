export interface SignUp {
    login: string;
    password: string;
    confirmPassword?: string;
    name: string;
    email: string;
    phone: string;
}
