import { NgModule } from '@angular/core';
import { NgxMaskModule, IConfig } from 'ngx-mask';

import { SharedModule } from '@ClientDashboard/app-shared.module';

import {
    SignInFormComponent,
    SignUpFormComponent,
    VerifyPhoneComponent,
    ResetPasswordComponent
} from './components';

import {
    AuthorizationService,
    AuthorizationApiService
} from './services';



const maskConfig: Partial<IConfig> = {
    validation: false,
};

@NgModule({
    declarations: [
        SignInFormComponent,
        SignUpFormComponent,
        VerifyPhoneComponent,
        ResetPasswordComponent
    ],
    imports: [
        SharedModule,

        NgxMaskModule.forRoot(maskConfig),
    ],
    providers: [
        AuthorizationService,
        AuthorizationApiService
    ],
    exports: [
        SignInFormComponent,
        SignUpFormComponent,
        VerifyPhoneComponent,
        ResetPasswordComponent
    ]
})
export class AuthorizationModule { }
