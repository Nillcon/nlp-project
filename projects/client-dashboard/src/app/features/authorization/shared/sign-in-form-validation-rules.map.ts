import { Validators, ValidatorFn } from '@angular/forms';
import { SignIn } from '../interfaces';

export const SignInFormValidationRules = new Map<keyof SignIn, ValidatorFn[]>()
    .set(
        'login',
        [
            Validators.required,
        ]
    )
    .set(
        'password',
        [
            Validators.required,
        ]
    );

