import { Validators, ValidatorFn } from '@angular/forms';
import { SignUp } from '../interfaces';
import { EmailValidator } from '../validators';

export const SignUpFormValidationRules = new Map<keyof SignUp, ValidatorFn[]>()
    .set(
        'login',
        [
            Validators.required,
            // Validators.minLength(2),
            // Validators.maxLength(40)
        ]
    )
    .set(
        'password',
        [
            Validators.required,
            // Validators.minLength(6),
            // Validators.maxLength(40)
        ]
    ).set(
        'confirmPassword',
        [
            Validators.required,
        ]
    ).set(
        'name',
        [
            Validators.required,
            Validators.maxLength(40)
        ]
    ).set(
        'email',
        [
            Validators.required,
            EmailValidator
        ]
    )
    .set(
        'phone',
        [
            Validators.required,
            Validators.pattern("[0-9]{12}")
        ]
    );
