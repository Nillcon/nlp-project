import { Component, OnInit, EventEmitter, OnDestroy, Output } from '@angular/core';
import { BaseForm, Form } from '@Shared/features';
import { FormGroupTypeSafe, FormBuilderTypeSafe } from 'form-type-safe';
import { Validators } from '@angular/forms';

import { SignIn } from '../../interfaces';
import { SignInFormValidationRules } from './../../shared';
import { ChangeDetectionStrategy } from '@angular/compiler/src/core';

@Form()
@Component({
    selector: 'authorization-sign-in-form',
    templateUrl: './sign-in-form.component.html',
    styleUrls: ['./sign-in-form.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class SignInFormComponent implements OnInit, OnDestroy, BaseForm<SignIn> {

    @Output()
    public OnFormInit: EventEmitter<FormGroupTypeSafe<SignIn>> = new EventEmitter();

    public formGroup: FormGroupTypeSafe<SignIn>;

    public isPasswordVisible: boolean = false;

    constructor (
        private readonly fb: FormBuilderTypeSafe
    ) { }

    public ngOnInit (): void {
    }

    public ngOnDestroy (): void {}

    public formGroupInit (): FormGroupTypeSafe<SignIn> {
        return this.fb.group<SignIn>({
            login: this.fb.control('', SignInFormValidationRules.get('login')),
            password: this.fb.control('', SignInFormValidationRules.get('password'))
        });
    }

    public onPasswordVisibilityButtonClick (): void {
        this.isPasswordVisible = !this.isPasswordVisible;
    }

}
