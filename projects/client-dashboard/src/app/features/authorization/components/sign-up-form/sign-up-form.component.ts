import { Component, OnInit, EventEmitter, OnDestroy, ChangeDetectionStrategy, Output, Input } from '@angular/core';
import { Form, BaseForm } from '@Shared/features';
import { FormGroupTypeSafe, FormBuilderTypeSafe, FormControlTypeSafe } from 'form-type-safe';

import { SignUp } from '../../interfaces/sign-up.interface';
import { SignUpFormValidationRules } from '../../shared';
import { AuthorizationService } from '../../services';
import { isPhoneExist, isEmailExist, isLoginExist } from '../../validators';

@Form()
@Component({
    selector: 'authorization-sign-up-form',
    templateUrl: './sign-up-form.component.html',
    styleUrls: ['./sign-up-form.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class SignUpFormComponent implements OnInit, OnDestroy, BaseForm<SignUp> {

    @Input()
    public isPhoneVerified: boolean = false;

    @Output()
    public OnFormInit: EventEmitter<FormGroupTypeSafe<SignUp>> = new EventEmitter();

    @Output()
    public OnPhoneVerify: EventEmitter<string> = new EventEmitter();

    public formGroup: FormGroupTypeSafe<SignUp>;

    public isPasswordVisible: boolean = false;

    constructor (
        private readonly fb: FormBuilderTypeSafe,
        private readonly authorizationService: AuthorizationService
    ) { }

    public ngOnInit (): void {
    }

    public ngOnDestroy (): void {}

    public formGroupInit (): FormGroupTypeSafe<SignUp> {
        return this.fb.group<SignUp>({
            login: this.fb.control('',
                SignUpFormValidationRules.get('login'),
                isLoginExist(this.authorizationService)
            ),
            email: this.fb.control('',
                SignUpFormValidationRules.get('email'),
                isEmailExist(this.authorizationService)
            ),
            phone: this.fb.control('',
                SignUpFormValidationRules.get('phone'),
                isPhoneExist(this.authorizationService)
            ),
            confirmPassword: this.fb.control('', [
                ...SignUpFormValidationRules.get('confirmPassword'),
                this.validateConfirmPassword.bind(this)
            ]),
            password: this.fb.control('', SignUpFormValidationRules.get('password')),
            name:  this.fb.control('', SignUpFormValidationRules.get('name')),
        });
    }

    public onVerifyPhoneButtonClick (): void {
        const phone = this.formGroup.controls.phone.value;

        this.OnPhoneVerify.emit(phone);
    }

    public onPasswordVisibilityButtonClick (): void {
        this.isPasswordVisible = !this.isPasswordVisible;
    }


    private validateConfirmPassword (control: FormControlTypeSafe<string>): { passwordsAreDifferent: boolean } {
        if (this.formGroup) {
            return (control.value === this.formGroup.controls.password.value) ? null : { passwordsAreDifferent: true };
        }

        return null;
    }
}
