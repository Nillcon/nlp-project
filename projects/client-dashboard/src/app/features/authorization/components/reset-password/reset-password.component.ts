import { Component, OnInit, Output, EventEmitter, Input, ChangeDetectorRef } from '@angular/core';

@Component({
  selector: 'reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.scss']
})
export class ResetPasswordComponent implements OnInit {

    @Input()
    public codeHasConfirmed = false;

    @Output()
    public OnPhoneSended: EventEmitter<string> = new EventEmitter();

    @Output()
    public OnCodeConfirmed: EventEmitter<string> = new EventEmitter();

    @Output()
    public OnPasswordSaved: EventEmitter<string> = new EventEmitter();

    public phoneHasSended: boolean = false;

    constructor () { }

    public ngOnInit (): void {
    }

    public onPhoneSendButtonClick (phone: string): void {
        this.OnPhoneSended.emit(`+${phone}`);
    }

    public onCodeConfirmButtonClick (code: string): void {
        this.OnCodeConfirmed.emit(code);
    }

    public onPasswordSaveButtonClick (password: string): void {
        this.OnPasswordSaved.emit(password);
    }
}
