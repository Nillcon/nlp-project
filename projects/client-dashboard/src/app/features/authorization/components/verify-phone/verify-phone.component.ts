import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { FormControlTypeSafe } from 'form-type-safe';
import { Validators } from '@angular/forms';

@Component({
  selector: 'verify-phone',
  templateUrl: './verify-phone.component.html',
  styleUrls: ['./verify-phone.component.scss']
})
export class VerifyPhoneComponent implements OnInit {

    @Output()
    public OnPhoneVerify: EventEmitter<string> = new EventEmitter();

    public codeControl = new FormControlTypeSafe<string>('', [
        Validators.required
    ]);

    constructor () { }

    public ngOnInit (): void {
    }

    public onVerifyButtonClick (): void {
        if (this.codeControl.valid) {
            this.OnPhoneVerify.emit(this.codeControl.value);

            this.codeControl.markAllAsTouched();
        }
    }

}
