import { FormControlTypeSafe } from 'form-type-safe';
import { map } from 'rxjs/operators';
import { AuthorizationService } from "../services";

export const isEmailExist = (authorizationService: AuthorizationService) => {
    return (input: FormControlTypeSafe<string>) => {
        return authorizationService.checkEmailExist(input.value)
            .pipe(
                map(_isEmailExist => {
                    return (_isEmailExist) ? {
                        emailExists: _isEmailExist
                    } : null;
                })
            );
    };
};
