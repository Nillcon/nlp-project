import { AuthorizationService } from '../services';
import { FormControlTypeSafe } from 'form-type-safe';
import { map } from 'rxjs/operators';

export const isPhoneExist = (authorizationService: AuthorizationService) => {
    return (input: FormControlTypeSafe<string>) => {
        return authorizationService.checkPhoneExist(input.value)
            .pipe(
                map(_isPhoneExist => {
                    return (_isPhoneExist) ? {
                        phoneExists: _isPhoneExist
                    } : null;
                })
            );
    };
};
