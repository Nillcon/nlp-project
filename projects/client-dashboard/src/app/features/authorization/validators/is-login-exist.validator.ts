import { FormControlTypeSafe } from 'form-type-safe';
import { map } from 'rxjs/operators';
import { AuthorizationService } from '../services';

export const isLoginExist = (authorizationService: AuthorizationService) => {
    return (input: FormControlTypeSafe<string>) => {
        return authorizationService.checkLoginExist(input.value)
            .pipe(
                map(_isLoginExist => {
                    return (_isLoginExist) ? {
                        loginExists: _isLoginExist
                    } : null;
                })
            );
    };
};
