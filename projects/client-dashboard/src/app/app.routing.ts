import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AppLayoutEnum } from '@ClientDashboard/enums';

const routes: Routes = [
    {
        path: AppLayoutEnum.Landing,
        loadChildren: () => import('./layouts/landing/landing.module').then(m => m.LandingModule)
    },
    {
        path: AppLayoutEnum.Login,
        loadChildren: () => import('./layouts/login/login.module').then(m => m.LoginModule)
    },
    {
        path: AppLayoutEnum.Registration,
        loadChildren: () => import('./layouts/registration/registration.module').then(m => m.RegistrationModule)
    },
    {
        path: 'Chat',
        loadChildren: () => import('./layouts/chat-layout/chat-layout.module').then(m => m.ChatLayoutModule)
    },
    {
        path: '**',
        redirectTo: AppLayoutEnum.Landing
    }
];

@NgModule({
    imports: [
        RouterModule.forRoot(routes)
    ],
    exports: [
        RouterModule
    ]
})
export class AppRoutingModule {}
