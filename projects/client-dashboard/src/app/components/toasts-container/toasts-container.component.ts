import { Component, OnInit, ChangeDetectionStrategy, HostListener } from '@angular/core';

@Component({
    selector: 'app-toasts-container',
    templateUrl: './toasts-container.component.html',
    styleUrls: ['./toasts-container.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class ToastsContainerComponent implements OnInit {

    public position: string;

    public marginTop: number;

    constructor () {}

    public ngOnInit (): void {
        this.applyParamsBasedOnWindowSize(window.innerWidth);
    }

    @HostListener('window:resize')
    public onWindowResize (): void {
        this.applyParamsBasedOnWindowSize(window.innerWidth);
    }

    private applyParamsBasedOnWindowSize (innerWidth: number): void {
        if (innerWidth > 640) {
            this.position  = 'top-center';
            this.marginTop = 70;
        } else {
            this.position  = 'bottom-center';
            this.marginTop = 0;
        }
    }

}
