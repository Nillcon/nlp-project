import { Component, OnInit, OnDestroy, ChangeDetectionStrategy } from '@angular/core';
import { UserService } from '@ClientDashboard/features/user';
import { Observable, ReplaySubject } from 'rxjs';
import { LocalStorageService } from '@Shared/features';
import { StorageKey } from '@ClientDashboard/enums';

@Component({
    selector: 'window-chat',
    templateUrl: './window-chat.component.html',
    styleUrls: ['./window-chat.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class WindowChatComponent implements OnInit, OnDestroy {

    public chatIsClose: boolean = false;
    public chatRoomId$: ReplaySubject<string> = new ReplaySubject();

    constructor (
        private readonly userService: UserService,
        private readonly localStorageService: LocalStorageService
    ) { }

    public ngOnInit (): void {
        this.initChatRoomId();
    }

    public ngOnDestroy (): void {}

    public onChatClosed (): void {
        this.chatIsClose = true;
    }

    public onChatOpened (): void {
        this.chatIsClose = false;
    }

    private initChatRoomId (): void {
        const chat_room_id = this.getChatRoomIdFromStorage();

        if (chat_room_id) {
            this.chatRoomId$.next(chat_room_id);
        } else {
            this.getChatRoomId()
                .subscribe(_chat_room_id => {
                    this.chatRoomId$.next(_chat_room_id);

                    this.setChatRoomIdToStorage(_chat_room_id);
                });
        }
    }

    private getChatRoomId (): Observable<string> {
        return this.userService.getChatRoomId();
    }

    private getChatRoomIdFromStorage (): string {
        const user_room_id = this.localStorageService.get<string>(StorageKey.User_Room_Id);

        return user_room_id;
    }

    private setChatRoomIdToStorage (chatRoomId: string): void {
        this.localStorageService.set(StorageKey.User_Room_Id, chatRoomId);
    }

}
