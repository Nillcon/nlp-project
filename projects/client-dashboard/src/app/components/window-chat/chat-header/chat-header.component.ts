import { Component, OnInit, EventEmitter, Output, ChangeDetectionStrategy } from '@angular/core';

@Component({
    selector: 'app-chat-header',
    templateUrl: './chat-header.component.html',
    styleUrls: ['./chat-header.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class ChatHeaderComponent implements OnInit {

    @Output()
    public OnChatOpened: EventEmitter<boolean> = new EventEmitter();

    @Output()
    public OnChatClosed: EventEmitter<boolean> = new EventEmitter();

    public chatIsOpen: boolean = true;

    constructor () { }

    public ngOnInit (): void {
        this.initChatState();
    }

    public onHeaderClick (): void {
        this.OnChatOpened.emit(true);
        this.chatIsOpen = true;
    }

    public onCloseButtonClick (event: any): void {
        this.chatIsOpen = false;

        event.cancelBubble = true;
        this.OnChatClosed.emit(true);
    }

    private initChatState (): void {
        if (this.chatIsOpen) {
            this.OnChatOpened.emit(true);
        } else {
            this.OnChatClosed.emit(true);
        }
    }
}
