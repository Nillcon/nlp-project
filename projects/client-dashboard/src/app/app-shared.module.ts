import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';

import { SharedModule as GlobalSharedModule } from '@Shared/shared.module';

import { AvatarModule } from 'ngx-avatar';
import { FormBuilderTypeSafe } from 'form-type-safe';
import { ButtonModule } from 'primeng-lts/button';
import { FileUploadModule } from 'primeng-lts/fileupload';
import { MenuModule } from 'primeng-lts/menu';
import { CardModule } from 'primeng-lts/card';
import { ChipsModule } from 'primeng-lts/chips';
import { TooltipModule } from 'primeng-lts/tooltip';
import { InputTextModule } from 'primeng-lts/inputtext';
import { PasswordModule } from 'primeng-lts/password';
import { InputTextareaModule } from 'primeng-lts/inputtextarea';
import { CheckboxModule } from 'primeng-lts/checkbox';
import { RadioButtonModule } from 'primeng-lts/radiobutton';
import { MessageModule } from 'primeng-lts/message';
import { ProgressSpinnerModule } from 'primeng-lts/progressspinner';
import { TableModule } from 'primeng-lts/table';
import { DialogModule } from 'primeng-lts/dialog';
import { DynamicDialogModule } from 'primeng-lts/dynamicdialog';
import { AutoCompleteModule } from 'primeng-lts/autocomplete';
import { ScrollPanelModule } from 'primeng-lts/scrollpanel';
import { DialogService } from 'primeng-lts/api';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,

        GlobalSharedModule,

        AvatarModule,

        ButtonModule,
        FileUploadModule,
        MenuModule,
        CardModule,
        ChipsModule,
        TooltipModule,
        InputTextModule,
        PasswordModule,
        InputTextareaModule,
        CheckboxModule,
        RadioButtonModule,
        MessageModule,
        ProgressSpinnerModule,
        TableModule,
        DialogModule,
        DynamicDialogModule,
        AutoCompleteModule,
        ScrollPanelModule
    ],
    exports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        GlobalSharedModule,

        AvatarModule,

        ButtonModule,
        FileUploadModule,
        MenuModule,
        CardModule,
        ChipsModule,
        TooltipModule,
        InputTextModule,
        PasswordModule,
        InputTextareaModule,
        CheckboxModule,
        RadioButtonModule,
        MessageModule,
        ProgressSpinnerModule,
        TableModule,
        DialogModule,
        DynamicDialogModule,
        AutoCompleteModule,
        ScrollPanelModule
    ],
    providers: [
        FormBuilderTypeSafe,
        DialogService
    ]
})
export class SharedModule {}
