import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ChatHeaderComponent } from './chat-header';
import { ChatApiService } from './services/chat-api.service';
import { CHAT_API } from '@Shared/modules/chat/config';
import { WindowChatComponent } from './window-chat.component';

import { ChatModule } from '@Shared/modules/chat';

@NgModule({
    declarations: [
        ChatHeaderComponent,
        WindowChatComponent,
    ],
    imports: [
        CommonModule,
        ChatModule
    ],
    providers: [
        ChatApiService,
        { provide: CHAT_API, useClass: ChatApiService }
    ],
    exports: [
        WindowChatComponent,
    ]
})
export class WindowChatModule { }
