import { Injectable } from '@angular/core';
import { ReplaySubject, Observable, BehaviorSubject } from 'rxjs';

import { HubConnectionBuilder, HubConnection } from "@aspnet/signalr";
import { Message } from '@progress/kendo-angular-conversational-ui';

import { HttpRequestService, LocalStorageService } from '@Shared/features';
import { StorageKey } from '@ClientDashboard/enums/storage-key.enum';

import { switchMap, filter } from 'rxjs/operators';
import { EventEnum } from '@Shared/modules/chat/shared';
import { ChatOwner } from '@Shared/modules/chat/interfaces';
import { InvokesEnum } from '@Shared/modules/chat/shared/invokes.enum';

@Injectable()
export class ChatApiService {

    public get chatOwnerId$ (): Observable<number> {
        return this._chatOwnerId$.asObservable();
    }

    public get previusMessages$ (): Observable<Message[]> {
        return this._previusMessages$.asObservable();
    }

    public get newMessage$ (): Observable<Message> {
        return this._newMessage$.asObservable();
    }

    private stream: HubConnection;
    private readonly streamHasStarder: BehaviorSubject<boolean> = new BehaviorSubject(false);

    private readonly host: string = `${this.httpService.host}/chathub`;

    private readonly _chatOwnerId$: ReplaySubject<number> = new ReplaySubject();
    private readonly _newMessage$: ReplaySubject<Message> = new ReplaySubject();
    private readonly _previusMessages$: ReplaySubject<Message[]> = new ReplaySubject();

    constructor (
        private readonly httpService: HttpRequestService,
        private readonly storageService: LocalStorageService
    ) {}

    public initStream (): void {
        const token = this.storageService.get<string>(StorageKey.Access_Token);

        this.stream = new HubConnectionBuilder()
            .withUrl(this.host,
                {
                    accessTokenFactory: () => token
                }
            )
            .build();
    }

    public startListen (): void {
        this.onInitializeChat();

        this.stream.start()
            .then(() => {
                console.log('Chat connection started');
                this.streamHasStarder.next(true);

                this.onHistory();
                this.onMessage();
            });
    }

    public onInitializeChat (): void {
        this.stream.on(EventEnum.InitializeChat, ((chatOwner: ChatOwner) => {
            this._chatOwnerId$.next(chatOwner.chatOwnerId);
        }));
    }

    public onHistory (): void {
        this.stream.on(EventEnum.History, ((messages: Message[]) => {
            this._previusMessages$.next(messages);
        }));
    }

    public enterToRoom (id: string): Observable<any> {
        return this.invoke(InvokesEnum.RoomEnter, id);
    }

    public leaveRoom (id: string): Observable<any>  {
        return this.invoke(InvokesEnum.RoomExit, id);
    }

    public onMessage (): void {
        this.stream.on(EventEnum.NewMessage, ((message: Message) => {
            this._newMessage$.next(message);
        }));
    }

    public send (message: Message): Observable<any> {
        return this.invoke(InvokesEnum.Send, message);
    }

    private invoke (methodName: InvokesEnum, ...args: any[]): Observable<any> {
        return this.streamHasStarder
            .pipe(
               filter(hasStarted => !!hasStarted),
               switchMap(() => this.stream.invoke(methodName, ...args))
            );
    }
}
