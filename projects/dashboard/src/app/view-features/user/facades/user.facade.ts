import { Injectable } from "@angular/core";
import { Observable, throwError } from 'rxjs';
import { catchError, tap, delayWhen } from 'rxjs/operators';
import { LocalStorageService } from '@Shared/features';
import { StorageKey } from '@Dashboard/enums';

import {
    UserService,
    UserStateService,
    UserChangingPasswordForm,
    SignInFormData,
    User
} from '@Dashboard/features/user';

@Injectable({
    providedIn: 'root'
})
export class UserFacade {

    public get data (): User {
        return this.userStateService.data;
    }

    public get data$ (): Observable<User> {
        return this.userStateService.data$;
    }

    public get isAuthorized (): boolean {
        return this.userStateService.isAuthorized;
    }

    public get isAuthorized$ (): Observable<boolean> {
        return this.userStateService.isAuthorized$;
    }

    constructor (
        private readonly userService: UserService,
        private readonly userStateService: UserStateService,
        private readonly storageService: LocalStorageService
    ) {}

    public update (): Observable<User> {
        return this.userService.getInfo()
            .pipe(
                catchError(() => {
                    this.makeUserUnauthorized();
                    return throwError(null);
                }),
                tap((data) => {
                    if (data) {
                        this.makeUserAuthorized(data);
                    } else {
                        this.makeUserUnauthorized();
                    }
                })
            );
    }

    public editLocalUser (data: Partial<User>): Observable<any> {
        const userId: number = this.data.id;

        return this.userService.edit(userId, data)
            .pipe(
                tap(() => this.update())
            );
    }

    public editUser (id: number, data: Partial<User>): Observable<any> {
        return this.userService.edit(id, data);
    }

    public changePassword (data: UserChangingPasswordForm): Observable<any> {
        return this.userService.changePassword(data);
    }

    public signIn (data: SignInFormData): Observable<string> {
        return this.userService.signIn(data)
            .pipe(
                tap((token) => {
                    this.storageService.set(StorageKey.Access_Token, token);
                }),
                delayWhen(() => this.update())
            );
    }

    public logout (): void {
        this.storageService.remove(StorageKey.Access_Token);
        this.makeUserUnauthorized();
    }

    private makeUserUnauthorized (): void {
        this.userStateService.setIsAuthorized(false);
        this.userStateService.setData(null);
    }

    private makeUserAuthorized (data: User): void {
        this.userStateService.setIsAuthorized(true);
        this.userStateService.setData(data);
    }

}
