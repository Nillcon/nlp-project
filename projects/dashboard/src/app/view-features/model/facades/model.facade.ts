import { Injectable } from "@angular/core";
import { Observable, throwError, forkJoin, of } from 'rxjs';
import { tap, switchMap, catchError, map } from 'rxjs/operators';

import {
    DatasetPreview,
    DatasetSourceTypeEnum,
    DatasetPreviewSettings,
    DatasetImportData,
    ModelWord,
    DatasetAssociationColumn
} from '@Dashboard/features/dataset';

import {
    ModelService,
    ModelStateService,
    Model,
    ModelCreatingData,
    ModelTopic,
    ModelTopicAnalyzerAnalysisData,
    ModelSentimentAnalysisData,
    ModelNerAnalysisData,
    ModelPredictionAnalysisData,
    SentimentTrainingResult
} from '@Dashboard/features/model';

@Injectable({
    providedIn: 'root'
})
export class ModelFacade {

    constructor (
        private readonly modelStateService: ModelStateService,
        private readonly modelService: ModelService
    ) {}

    public getModels$ (): Observable<Model[]> {
        return this.modelStateService.getModels$();
    }

    public getModels (): Model[] {
        return this.modelStateService.getModels();
    }

    public getModel (id: number): Model {
        return this.modelStateService.getModel(id);
    }

    public updateModels (): void {
        this.modelService.getAll()
            .pipe(
                tap((models) => this.modelStateService.setModels(models))
            )
            .subscribe(
                () => {},
                () => this.modelStateService.setModels([])
            );
    }

    public updateModelsIfNeeded (): void {
        const models = this.getModels();

        if (!models) {
            this.updateModels();
        }
    }

    public updateModel (id: number): Observable<Model> {
        return this.modelService.get(id)
            .pipe(
                tap((model) => this.modelStateService.updateModel(model))
            );
    }

    public createModel (modelData: ModelCreatingData): Observable<number> {
        const model: Partial<Model> = { name: modelData.name };

        this.modelStateService.addModel(model);

        return this.modelService.create(modelData)
            .pipe(
                switchMap((id) => forkJoin({
                    id: of(id),
                    model: this.modelService.get(id)
                })),
                tap((data) => this.modelStateService.updateModelId(model as Model, data.model)),
                map((data) => data.id),
                catchError((error) => {
                    this.modelStateService.removeModel(model.id);

                    return throwError(error);
                })
            );
    }

    public deleteModel (model: Model): void {
        this.modelStateService.removeModel(model.id);

        this.modelService.delete(model.id)
            .subscribe(
                () => {},
                () => this.modelStateService.addModel(model)
            );
    }

    public renameModel (id: number, data: Partial<Model>): Observable<any> {
        return this.modelService.rename(id, data);
    }

    public markAsTemplate (id: number): Observable<any> {
        return this.modelService.markAsTemplate(id);
    }

    public getPreview (id: number, rowCount: number): Observable<DatasetPreview> {
        return this.modelService.getPreview(id, rowCount);
    }

    public updatePreviewSettings (id: number, settings: Partial<DatasetPreviewSettings>): Observable<any> {
        return this.modelService.updatePreviewSettings(id, settings);
    }

    public getColumnAssociation (id: number, columnIndex: number, sheetIndex: number): Observable<DatasetAssociationColumn> {
        return this.modelService.getColumnAssociation(id, columnIndex, sheetIndex);
    }

    public updateColumnAssociation (id: number, columnIndex: number, data: DatasetAssociationColumn): Observable<any> {
        return this.modelService.updateColumnAssociation(id, columnIndex, data);
    }

    public uploadDataset (id: number, datasetType: DatasetSourceTypeEnum, files: Blob[]): Observable<any> {
        return this.modelService.uploadDataset(id, datasetType, files);
    }

    public importDataset (id: number, data: DatasetImportData): Observable<any> {
        return this.modelService.importDataset(id, data);
    }

    public setStepIndex (id: number, stepIndex: number): Observable<any> {
        return this.modelService.setStepIndex(id, stepIndex);
    }

    public getWords (id: number): Observable<ModelWord[]> {
        return this.modelService.getWords(id);
    }

    public createStopWord (id: number, word: string): Observable<any> {
        return this.modelService.createStopWord(id, word);
    }

    public deleteStopWord (id: number, word: string): Observable<any> {
        return this.modelService.deleteStopWord(id, word);
    }

    public lemmatizeWord (id: number, word: string, lemma: string): Observable<any> {
        return this.modelService.lemmatizeWord(id, word, lemma);
    }

    public getTopics (id: number): Observable<ModelTopic[]> {
        return this.modelService.getTopics(id);
    }

    public renameTopic (id: number, data: Partial<ModelTopic>): Observable<any> {
        return this.modelService.renameTopic(id, data);
    }

    /** Temp method. Will be websockets instead this */
    public training (id: number): Observable<any> {
        return this.modelService.training(id);
    }

    public getSentimentTrainingResult (id: number): Observable<SentimentTrainingResult> {
        return this.modelService.getSentimentTrainingResult(id);
    }

    public analyzeTopicAnalyzer (id: number, rawText: string): Observable<ModelTopicAnalyzerAnalysisData[]> {
        return this.modelService.analyzeTopicAnalyzer(id, rawText);
    }

    public analyzeSentiment (id: number, rawText: string): Observable<ModelSentimentAnalysisData> {
        return this.modelService.analyzeSentiment(id, rawText);
    }

    public analyzeNer (id: number, rawText: string): Observable<ModelNerAnalysisData> {
        return this.modelService.analyzeNer(id, rawText);
    }

    public analyzePrediction (id: number, rawText: string): Observable<ModelPredictionAnalysisData> {
        return this.modelService.analyzePrediction(id, rawText);
    }

}
