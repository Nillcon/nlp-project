import { Model } from '@Dashboard/features/model';
import { ModelStateEnum } from '@Shared/enums';

export interface ViewModel extends Model {
    modelState: ModelStateEnum;
}
