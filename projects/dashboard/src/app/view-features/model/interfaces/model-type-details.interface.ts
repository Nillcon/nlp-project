import { ModelContainerEnum } from '@Dashboard/layouts/main/pages/model-predict/enums/model-container.enum';
import { ModelTypeSettingsStep } from './model-type-details-step.interface';

export interface ModelTypeDetails {
    settingsSteps: ModelTypeSettingsStep[];
    predictContainer: ModelContainerEnum;
}
