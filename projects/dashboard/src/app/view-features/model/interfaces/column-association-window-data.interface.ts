import { DatasetColumnSelectorOnColumnSelected } from '@Dashboard/features/dataset';

export interface ColumnAssociationWindowData extends DatasetColumnSelectorOnColumnSelected {
    modelId: number;
}
