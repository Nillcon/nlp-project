import { ModelSettingsContainerEnum } from '@Dashboard/layouts/main/pages/model-settings/enums/models-container.enum';
import { ModelSettingsQueryParamEnum } from '@Dashboard/layouts/main/pages/model-settings/enums/query-params.enum';
import { ModelSettingsActionButton } from './action-button.interface';

export interface ModelTypeSettingsStep {
    containerName: ModelSettingsContainerEnum;
    dependentQueryParams: ModelSettingsQueryParamEnum[];
    actionButtons: ModelSettingsActionButton[];
    isStepBackAvailable: boolean;
}
