import { KeysOfType } from '@Shared/types/keys-of-type.type';
import { ActionButtonsComponent } from '@Dashboard/layouts/main/pages/model-settings/components/action-buttons/action-buttons.component';
import { Observable } from 'rxjs';

export interface ModelSettingsActionButton {
    id: number;
    text: string;
    class?: string;
    iconClass?: string;
    iconPos?: 'left' | 'right';
    /** ### List of button ids, after calling the "callback" which, this button will be rendered */
    dependentButtonIds: number[];
    callbackName: KeysOfType<ActionButtonsComponent, () => Observable<boolean>>;
}
