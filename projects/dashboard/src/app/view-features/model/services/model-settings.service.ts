import { Injectable, OnDestroy } from '@angular/core';
import { Router, Params } from '@angular/router';
import { UrlService, NavigationEventsService } from '@Shared/features';
import { ModelTypeEnum } from '@Dashboard/features/model';
import { ModelSettingsContainerEnum } from '@Dashboard/layouts/main/pages/model-settings/enums/models-container.enum';
import { ModelSettingsQueryParamEnum } from '@Dashboard/layouts/main/pages/model-settings/enums/query-params.enum';
import { ModelTypeDetailsMap } from '../data';
import { ModelTypeSettingsStep } from '../interfaces';
import { Subscription } from 'rxjs';
import { AutoUnsubscribe } from 'ngx-auto-unsubscribe';
import { AppLayoutEnum } from '@Dashboard/enums';
import { MainLayoutPageEnum } from '@Dashboard/layouts/main/shared/main-layout-page.enum';

@AutoUnsubscribe()
@Injectable()
export class ModelSettingsService implements OnDestroy {

    public get containerName (): ModelSettingsContainerEnum {
        return this._containerName;
    }

    public get step (): ModelTypeSettingsStep {
        return this._step;
    }

    public get stepIndex (): number {
        return this._stepIndex;
    }

    public get urlParams (): Params {
        return this._urlParams;
    }

    private _containerName: ModelSettingsContainerEnum;
    private _modelType: ModelTypeEnum;
    private _step: ModelTypeSettingsStep;
    private _stepIndex: number;
    private _urlParams: Params;

    private _isNeedRedirectWhenErrors: boolean;
    private _redirectionWhenErrorsSubUrl: string;

    private _routerSubscription: Subscription;

    constructor (
        private readonly navigationEventsService: NavigationEventsService,
        private readonly router: Router,
        private readonly urlService: UrlService
    ) {}

    public ngOnDestroy (): void {}

    public init ({
        isNeedRedirectWhenErrors = false,
        redirectionWhenErrorsSubUrl = null
    }): void {
        this._isNeedRedirectWhenErrors      = isNeedRedirectWhenErrors;
        this._redirectionWhenErrorsSubUrl   = redirectionWhenErrorsSubUrl;

        this._routerSubscription = this.navigationEventsService.navigationEnd$
            .subscribe(() => this.onNavigationEnd());
    }

    public back (): void {
        this.validateOpportunityGoBack();

        const targetStep = this.getStepByIndex(this._stepIndex - 1);

        this.navigateToContainer(targetStep.containerName);
    }

    public continue (options = {
        queryParams: <Params>{}
    }): void {
        const targetStep = this.getStepByIndex(this._stepIndex + 1);

        if (targetStep) {
            this.navigateToContainer(targetStep.containerName, options.queryParams);
        } else {
            this.redirectByErrorReason();
        }
    }

    public onNavigationEnd (): void {
        this._containerName  = this.getCurrentContainerName();
        this.validateContainerName();
        this._modelType      = this.getModelType();
        this.validateModelType();
        this._step           = this.getCurrentStep();
        this._stepIndex      = this.getCurrentStepIndex();
        this._urlParams      = this.getUrlParams();
        this.validateStepQueryParams();
    }

    public getModelType (): ModelTypeEnum {
        return +this.urlService.getParameter(ModelSettingsQueryParamEnum.modelType) as ModelTypeEnum;
    }

    public getCurrentContainerName (): ModelSettingsContainerEnum {
        const parsedUrl   = this.router.parseUrl(this.router.url);
        const urlSegments = parsedUrl.root.children.primary.segments;

        return urlSegments[urlSegments.length - 1].path as ModelSettingsContainerEnum;
    }

    public getCurrentStep (): ModelTypeSettingsStep {
        return this.getModelSettingsSteps(this._modelType)
            .find((currStep) => currStep.containerName === this._containerName);
    }

    public getCurrentStepIndex (): number {
        return this.getModelSettingsSteps(this._modelType)
            .findIndex((currStep) => currStep.containerName === this._containerName);
    }

    public getStepByIndex (_stepIndex: number): ModelTypeSettingsStep {
        return this.getModelSettingsSteps(this._modelType)[_stepIndex];
    }

    public getUrlParams (): Params {
        return this.urlService.getParameters();
    }

    private navigateToContainer (containerName: ModelSettingsContainerEnum, queryParams: object = {}): void {
        this.router.navigate(
            [`/${AppLayoutEnum.Main}/${MainLayoutPageEnum.ModelSettings}/${containerName}`],
            { queryParams: queryParams, queryParamsHandling: 'merge' }
        );
    }

    private redirectByErrorReason (): void {
        this.router.navigate(
            [this._redirectionWhenErrorsSubUrl],
            { queryParams: {}, queryParamsHandling: 'preserve' }
        );
    }

    private redirectByErrorReasonIfNeeded (): void {
        if (this._isNeedRedirectWhenErrors) {
            this.redirectByErrorReason();
        }
    }

    private getModelSettingsSteps (modelType: ModelTypeEnum): ModelTypeSettingsStep[] {
        const details = ModelTypeDetailsMap.get(modelType);

        return details?.settingsSteps;
    }

    private validateContainerName (): void {
        const containers = Object.values(ModelSettingsContainerEnum)
            .filter((key) => Number.isNaN(+key));

        if (!containers.includes(this._containerName)) {
            this.redirectByErrorReasonIfNeeded();

            throw new Error(`Incorrect container!`);
        }
    }

    private validateModelType (): void {
        const modelTypes = Object.values(ModelTypeEnum)
            .filter((key) => !Number.isNaN(+key));

        if (!modelTypes.includes(this._modelType)) {
            this.redirectByErrorReasonIfNeeded();

            throw new Error(`Incorrect modelType!`);
        }
    }

    private validateStepQueryParams (): void {
        const params        = this.urlService.getParameters();
        const stepParams    = this._step.dependentQueryParams;

        stepParams.forEach((currParam) => {
            if (!params[currParam]) {
                this.redirectByErrorReasonIfNeeded();

                throw new Error(`Container doesn't contains some query params!`);
            }
        });
    }

    private validateOpportunityGoBack (): void {
        if (!this._step.isStepBackAvailable || this._stepIndex <= 0) {
            throw new Error(`Can't go back from current step!`);
        }
    }

}
