import { Injectable } from "@angular/core";
import { ModelTypeSettingsStep } from '../interfaces';
import { ModelTypeDetailsMap } from '../data';
import { AppLayoutEnum } from '@Dashboard/enums';
import { MainLayoutPageEnum } from '@Dashboard/layouts/main/shared/main-layout-page.enum';
import { Params, Router } from '@angular/router';
import { ModelContainerEnum } from '@Dashboard/layouts/main/pages/model-predict/enums/model-container.enum';
import { ModelSettingsQueryParamEnum } from '@Dashboard/layouts/main/pages/model-settings/enums/query-params.enum';
import { ModelSettingsContainerEnum } from '@Dashboard/layouts/main/pages/model-settings/enums/models-container.enum';
import { Model } from '@Dashboard/features/model';

@Injectable({
    providedIn: 'root'
})
export class ModelStepIdentifierService {

    constructor (
        private readonly router: Router
    ) {}

    /** Opens model on last settingsStep or on predictStep */
    public openModel (model: Model): void {
        const modelSteps = this.getModelSettingsSteps(model);

        if (model.stepIndex >= (modelSteps.length - 1)) {
            this.openModelPredictStep(model);
        } else {
            this.openModelSettingsOnStepIndex(model);
        }
    }

    public openModelSettingsOnLastStep (model: Model): void {
        const modelSteps = this.getModelSettingsSteps(model);

        this.navigateToModelStep(model, modelSteps[modelSteps.length - 1]);
    }

    public openModelSettingsOnStepIndex (model: Model): void {
        const modelSteps = this.getModelSettingsSteps(model);

        this.navigateToModelStep(model, modelSteps[model.stepIndex]);
    }

    public openModelPredictStep (model: Model): void {
        const predictStepContainer = this.getModelPredictStepContainer(model);

        this.router.navigate(
            [`/${AppLayoutEnum.Main}/${MainLayoutPageEnum.ModelPredict}/${predictStepContainer}`],
            {
                queryParams: {
                    [ModelSettingsQueryParamEnum.modelId]: model.id,
                    [ModelSettingsQueryParamEnum.isTemplate]: +model.isTemplate
                },
                queryParamsHandling: 'merge'
            }
        );
    }

    public navigateToModelStep (model: Model, step: ModelTypeSettingsStep): void {
        this.validateModelStep(model, step);

        const modelQueryParams = this.extractQueryParamsFromModel(step.dependentQueryParams, model);

        this.validateContainerQueryParamsFilling(modelQueryParams, step.dependentQueryParams);
        this.navigateToSettingsContainer(step.containerName, modelQueryParams);
    }

    private navigateToSettingsContainer (containerName: ModelSettingsContainerEnum, queryParams: object): void {
        this.router.navigate(
            [`/${AppLayoutEnum.Main}/${MainLayoutPageEnum.ModelSettings}/${containerName}`],
            { queryParams: queryParams, queryParamsHandling: 'merge' }
        );
    }

    private extractQueryParamsFromModel (params: string[], model: Model): Params {
        const result = {};

        for (const param of params) {
            if (model[param] !== undefined) {
                result[param] = model[param];
            }
        }

        return result;
    }

    private getModelSettingsSteps (model: Model): ModelTypeSettingsStep[] {
        const details = ModelTypeDetailsMap.get(model.modelType);

        return details.settingsSteps;
    }

    private getModelPredictStepContainer (model: Model): ModelContainerEnum {
        const details = ModelTypeDetailsMap.get(model.modelType);

        return details.predictContainer;
    }

    private validateContainerQueryParamsFilling (modelQueryParams: Params, containerDependentParams: string[]): void {
        const modelParams = Object.keys(modelQueryParams);
        const isAllModelParamsFilled = containerDependentParams
            .every((currDependentParam) => modelParams.includes(currDependentParam));

        if (!isAllModelParamsFilled) {
            throw new Error(`Model doesn't contains some information for target container!`);
        }
    }

    private validateModelStep (model: Model, step: ModelTypeSettingsStep): void {
        if (!step) {
            throw new Error(`Model with id ${model.id} doesn't have step with index ${model.stepIndex}!`);
        }
    }

}
