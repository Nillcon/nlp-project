import { ModelTypeSettingsStep } from '../../interfaces';
import { ModelSettingsContainerEnum as ContainerEnum } from '@Dashboard/layouts/main/pages/model-settings/enums/models-container.enum';
import { ModelSettingsQueryParamEnum as QueryParamEnum } from '@Dashboard/layouts/main/pages/model-settings/enums/query-params.enum';

export const PredictionStepsArray: ModelTypeSettingsStep[] = [
    {
        containerName: ContainerEnum.TypeSelection,
        dependentQueryParams: [
            QueryParamEnum.modelId,
            QueryParamEnum.modelType
        ],
        isStepBackAvailable: false,
        actionButtons: []
    },
    {
        containerName: ContainerEnum.WordsExtractor,
        dependentQueryParams: [
            QueryParamEnum.modelId,
            QueryParamEnum.modelType,
            QueryParamEnum.datasetSourceType
        ],
        isStepBackAvailable: false,
        actionButtons: [
            {
                id: 1,
                text: 'That\'ts all for now',
                class: 'ui-button-raised ui-button-primary font-weight-600',
                iconClass: 'fas fa-check',
                iconPos: 'right',
                dependentButtonIds: [],
                callbackName: 'continueButtonCallback'
            }
        ]
    }
];
