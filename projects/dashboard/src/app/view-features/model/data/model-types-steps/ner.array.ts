import { ModelTypeSettingsStep } from '../../interfaces';
import { ModelSettingsContainerEnum as ContainerEnum } from '@Dashboard/layouts/main/pages/model-settings/enums/models-container.enum';
import { ModelSettingsQueryParamEnum as QueryParamEnum } from '@Dashboard/layouts/main/pages/model-settings/enums/query-params.enum';

export const NerStepsArray: ModelTypeSettingsStep[] = [
    {
        containerName: ContainerEnum.TypeSelection,
        dependentQueryParams: [
            QueryParamEnum.modelId,
            QueryParamEnum.modelType
        ],
        isStepBackAvailable: false,
        actionButtons: []
    },
    {
        containerName: ContainerEnum.DefiningTags,
        dependentQueryParams: [
            QueryParamEnum.modelId,
            QueryParamEnum.modelType,
            QueryParamEnum.datasetSourceType
        ],
        isStepBackAvailable: true,
        actionButtons: [
            {
                id: 1,
                text: 'Continue',
                class: 'ui-button-raised ui-button-primary font-weight-600',
                iconClass: 'fas fa-arrow-right',
                iconPos: 'right',
                dependentButtonIds: [],
                callbackName: 'continueButtonCallback'
            }
        ]
    },
    {
        containerName: ContainerEnum.WordsExtractor,
        dependentQueryParams: [
            QueryParamEnum.modelId,
            QueryParamEnum.modelType,
            QueryParamEnum.datasetSourceType
        ],
        isStepBackAvailable: true,
        actionButtons: [
            {
                id: 1,
                text: 'Finish',
                class: 'ui-button-raised ui-button-primary font-weight-600',
                iconClass: 'fas fa-check',
                iconPos: 'right',
                dependentButtonIds: [],
                callbackName: 'continueButtonCallback'
            }
        ]
    }
];
