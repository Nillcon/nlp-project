import { ModelTypeSettingsStep } from '../../interfaces';
import { ModelSettingsContainerEnum as ContainerEnum } from '@Dashboard/layouts/main/pages/model-settings/enums/models-container.enum';
import { ModelSettingsQueryParamEnum as QueryParamEnum } from '@Dashboard/layouts/main/pages/model-settings/enums/query-params.enum';

export const TopicAnalyzerStepsArray: ModelTypeSettingsStep[] = [
    {
        containerName: ContainerEnum.TypeSelection,
        dependentQueryParams: [
            QueryParamEnum.modelId,
            QueryParamEnum.modelType
        ],
        isStepBackAvailable: false,
        actionButtons: []
    },
    {
        containerName: ContainerEnum.DatasetUploading,
        dependentQueryParams: [
            QueryParamEnum.modelId,
            QueryParamEnum.modelType,
            QueryParamEnum.datasetSourceType
        ],
        isStepBackAvailable: true,
        actionButtons: []
    },
    {
        containerName: ContainerEnum.DatasetColumnSelection,
        dependentQueryParams: [
            QueryParamEnum.modelId,
            QueryParamEnum.modelType,
            QueryParamEnum.datasetSourceType
        ],
        isStepBackAvailable: false,
        actionButtons: [
            {
                id: 1,
                text: 'Continue',
                class: 'ui-button-raised ui-button-primary font-weight-600',
                iconClass: 'fas fa-arrow-right',
                iconPos: 'right',
                dependentButtonIds: [],
                callbackName: 'continueButtonCallback'
            }
        ]
    },
    {
        containerName: ContainerEnum.Lemmatization,
        dependentQueryParams: [
            QueryParamEnum.modelId,
            QueryParamEnum.modelType
        ],
        isStepBackAvailable: false,
        actionButtons: [
            {
                id: 1,
                text: 'Training',
                class: 'ui-button-raised ui-button-success font-weight-600',
                iconClass: 'fas fa-graduation-cap',
                iconPos: 'right',
                dependentButtonIds: [],
                callbackName: 'trainingButtonCallback'
            },
            {
                id: 2,
                text: 'Continue',
                class: 'ui-button-raised ui-button-primary font-weight-600',
                iconClass: 'fas fa-arrow-right',
                iconPos: 'right',
                dependentButtonIds: [1],
                callbackName: 'continueButtonCallback'
            }
        ]
    },
    {
        containerName: ContainerEnum.ModelTopics,
        dependentQueryParams: [
            QueryParamEnum.modelId,
            QueryParamEnum.modelType
        ],
        isStepBackAvailable: true,
        actionButtons: [
            {
                id: 1,
                text: 'Finish',
                class: 'ui-button-raised ui-button-primary font-weight-600',
                iconClass: 'fas fa-check',
                iconPos: 'right',
                dependentButtonIds: [],
                callbackName: 'continueButtonCallback'
            }
        ]
    }
];
