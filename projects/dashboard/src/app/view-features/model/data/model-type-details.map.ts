import { ModelTypeEnum } from '@Dashboard/features/model';
import { ModelTypeDetails } from '../interfaces';

import {
    SentimentStepsArray,
    TopicAnalyzerStepsArray,
    NerStepsArray,
    PredictionStepsArray
} from './model-types-steps';
import { ModelContainerEnum } from '@Dashboard/layouts/main/pages/model-predict/enums/model-container.enum';

export const ModelTypeDetailsMap = new Map<ModelTypeEnum, ModelTypeDetails>()
    .set(
        ModelTypeEnum.TopicAnalyzer,
        {
            settingsSteps: TopicAnalyzerStepsArray,
            predictContainer: ModelContainerEnum.TopicAnalyzer
        }
    )
    .set(
        ModelTypeEnum.Sentiment,
        {
            settingsSteps: SentimentStepsArray,
            predictContainer: ModelContainerEnum.Sentiment
        }
    )
    .set(
        ModelTypeEnum.NER,
        {
            settingsSteps: NerStepsArray,
            predictContainer: ModelContainerEnum.NER
        }
    )
    .set(
        ModelTypeEnum.Prediction,
        {
            settingsSteps: PredictionStepsArray,
            predictContainer: ModelContainerEnum.Prediction
        }
    );
