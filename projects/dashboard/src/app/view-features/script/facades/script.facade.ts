import { Injectable } from '@angular/core';
import { Observable, forkJoin, of, throwError } from 'rxjs';
import { ScriptService, ScriptStateService } from '@Dashboard/features/script/services';
import { map, tap, switchMap, catchError, filter } from 'rxjs/operators';
import { Script, Node, Tree, Link } from '@Dashboard/features/script/interfaces';
import { ScriptView } from '../interfaces';
import { ScriptStateEnum } from '../enums';
import { Variable } from '@Dashboard/features/variable';

@Injectable({
    providedIn: 'root'
})
export class ScriptFacade {

    constructor (
        private readonly scriptService: ScriptService,
        private readonly scriptStateService: ScriptStateService
    ) {}

    public get scripts$ (): Observable<Script[]> {
        return this.scriptStateService.scripts$;
    }

    public getScript$ (id: number): Observable<Script> {
        return this.scriptStateService.scripts$
            .pipe(
                filter(scripts => !!scripts),
                map(scripts => {
                    const resultScript = scripts.filter(script => script.id === id)[0] || null;
                    return resultScript;
                })
            );
    }

    public getNodeOfScript (scriptId: number, nodeId: number): Observable<Node> {
        return this.scriptService.getNode(scriptId, nodeId);
    }

    public updateScripts (): void {
        this.scriptService.getAll()
            .pipe(
                tap(scripts => this.scriptStateService.setScripts(scripts)),
            )
            .subscribe(
                () => {},
                error => {
                    this.scriptStateService.setScripts([]);
                    return throwError(error);
                }
            );
    }

    public createScript (script: ScriptView): Observable<number> {
        script.modelState = ScriptStateEnum.Emulated;
        this.scriptStateService.addScript(script);

        return this.scriptService.create(script)
            .pipe(
                switchMap((id) => forkJoin({
                    id: of(id),
                    script: this.scriptService.get(id)
                })),
                map(data => {
                    data.script  = { ...data.script, modelState: ScriptStateEnum.Created } as ScriptView;
                    return data;
                }),
                tap((data) => this.scriptStateService.updateScriptId(script, data.script)),
                map((data) => data.id),
                catchError((error) => {
                    this.scriptStateService.removeScript(script.id);

                    return throwError(error);
                })
            );
    }

    public editScript (script: Script): Observable<any> {
        this.scriptStateService.updateScript(script);

        return this.scriptService.edit(script)
            .pipe(
                catchError(error => {
                    this.updateScripts();

                    return throwError(error);
                })
            );
    }

    public deleteScript (id: number): Observable<any> {
        this.scriptStateService.removeScript(id);

        return this.scriptService.delete(id)
            .pipe(
                catchError((error) => {
                    this.updateScripts();

                    return throwError(error);
                })
            );
    }

    public updateScript (id: number): Observable<Script> {
        return this.scriptService.get(id)
            .pipe(
                tap(script => {
                    this.scriptStateService.addScript(script);
                })
            );
    }

    public createNode (scriptId: number, node: Node): Observable<number> {
        return this.scriptService.createNode(scriptId, node);
    }

    public editNode (scriptId: number, node: Node): Observable<any> {
        return this.scriptService.editNode(scriptId, node);
    }

    public deleteNode (scriptId: number, nodeId: number): Observable<any> {
        return this.scriptService.deleteNode(scriptId, nodeId);
    }

    public getTree (): Observable<Tree> {
        return this.scriptService.getTree();
    }

    public updateVariables (id: number): Observable<Variable[]> {
        return this.scriptService.getVariables(id)
            .pipe(
                tap(variables => {
                    this.scriptStateService.updateVariablesOfScript(id, variables);
                })
            );
    }

    public createVariable (scriptId: number, variable: Variable): Observable<number> {
        this.addVariable(scriptId, variable);

        return this.scriptService.createVariable(scriptId, variable)
            .pipe(
                switchMap((id) => forkJoin({
                    id: of(id),
                    variable: this.scriptService.getVariableOfScript(scriptId, id)
                })),
                tap((data) => this.scriptStateService.updateVariableOfScript(scriptId, variable, data.variable)),
                map((data) => data.id),
                catchError((error) => {
                    this.scriptStateService.deleteVariableOfScript(scriptId, variable.id);

                    return throwError(error);
                })
            );
    }

    public editVariable (scriptId: number, variable: Variable): Observable<any> {
        const recentVariable: Variable = this.scriptStateService.getVariableOfScript(scriptId, variable.id);

        this.scriptStateService.updateVariableOfScript(scriptId, recentVariable, variable);
        return this.scriptService.editVariable(scriptId, variable)
            .pipe(
                catchError((error) => {
                    this.scriptStateService.updateVariableOfScript(scriptId, recentVariable, recentVariable);

                    return throwError(error);
                })
            );
    }

    public deleteVariable (scriptId: number, variableId: number): Observable<any> {
        const removedVariable = this.scriptStateService.deleteVariableOfScript(scriptId, variableId);

        return this.scriptService.deleteVariable(scriptId, variableId)
            .pipe(
                catchError(error => {
                    this.addVariable(scriptId, removedVariable);
                    return throwError(error);
                })
            );
    }

    public createLink (scriptId: number, link: Link): Observable<any> {
        return this.scriptService.createLink(scriptId, link);
    }

    private addVariable (scriptId: number, variable: Variable): void {
        this.scriptStateService.addVariableToScript(scriptId, variable);
    }
}
