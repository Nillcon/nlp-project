import { ScriptStateEnum } from '../enums';
import { Script } from '@Dashboard/features/script/interfaces';


export interface ScriptView extends Script {
    modelState: ScriptStateEnum;
}
