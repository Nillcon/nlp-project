import { DatasetColumnTypeEnum } from '../../../features/dataset/enums';
import { SentimentMap } from '@Dashboard/features/model/data/sentiment.map';
import { SelectItem } from 'primeng-lts/api';

export const DatasetColumnTypeAssociationDataMap = new Map<DatasetColumnTypeEnum, SelectItem[]>()
    .set(
        DatasetColumnTypeEnum.SentimentLabel,
        (() => {
            const sentimentArray = [...SentimentMap.values()];

            return sentimentArray.map((sentiment) => <SelectItem>{
                label: sentiment.name,
                value: sentiment.id
            });
        })()
    );
