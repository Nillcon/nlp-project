import { Injectable } from "@angular/core";
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { TemplateStateService, TemplateService, Template } from '@Dashboard/features/template';
import { ModelTopicAnalyzerAnalysisData, ModelSentimentAnalysisData, ModelNerAnalysisData, ModelPredictionAnalysisData } from '@Dashboard/features/model';

@Injectable({
    providedIn: 'root'
})
export class TemplateFacade {

    constructor (
        private readonly templateStateService: TemplateStateService,
        private readonly templateService: TemplateService
    ) {}

    public getTemplates$ (): Observable<Template[]> {
        return this.templateStateService.getTemplates$();
    }

    public getTemplates (): Template[] {
        return this.templateStateService.getTemplates();
    }

    public getTemplate (id: number): Template {
        return this.templateStateService.getTemplate(id);
    }

    public updateTemplates (): void {
        this.templateService.getAll()
            .pipe(
                tap((templates) => this.templateStateService.setTemplates(templates))
            )
            .subscribe(
                () => {},
                () => this.templateStateService.setTemplates([])
            );
    }

    public updateTemplate (id: number): Observable<Template> {
        return this.templateService.get(id)
            .pipe(
                tap((template) => this.templateStateService.updateTemplate(template))
            );
    }

    public updateTemplatesIfNeeded (): void {
        const templates = this.getTemplates();

        if (!templates) {
            this.updateTemplates();
        }
    }

    public deleteTemplate (template: Template): void {
        this.templateStateService.removeTemplate(template.id);

        this.templateService.delete(template.id)
            .subscribe(
                () => {},
                () => this.templateStateService.addTemplate(template)
            );
    }

    public markAsModel (id: number): Observable<any> {
        return this.templateService.markAsModel(id);
    }

    public analyzeTopicAnalyzer (id: number, rawText: string): Observable<ModelTopicAnalyzerAnalysisData[]> {
        return this.templateService.analyzeTopicAnalyzer(id, rawText);
    }

    public analyzeSentiment (id: number, rawText: string): Observable<ModelSentimentAnalysisData> {
        return this.templateService.analyzeSentiment(id, rawText);
    }

    public analyzeNer (id: number, rawText: string): Observable<ModelNerAnalysisData> {
        return this.templateService.analyzeNer(id, rawText);
    }

    public analyzePrediction (id: number, rawText: string): Observable<ModelPredictionAnalysisData> {
        return this.templateService.analyzePrediction(id, rawText);
    }

}
