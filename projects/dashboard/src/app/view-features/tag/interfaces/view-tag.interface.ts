import { Tag } from '@Dashboard/features/tag';
import { ModelStateEnum } from '@Shared/enums';

export interface ViewTag extends Tag {
    modelState: ModelStateEnum;
}
