import { Injectable } from "@angular/core";
import { Observable } from 'rxjs';
import { Tag, TagService, TagStateService } from '@Dashboard/features/tag';
import { tap, switchMap } from 'rxjs/operators';

@Injectable({
    providedIn: 'root'
})
export class TagFacade {

    constructor (
        private readonly tagStateService: TagStateService,
        private readonly tagService: TagService
    ) {}

    public getTags$ (): Observable<Tag[]> {
        return this.tagStateService.getTags$();
    }

    public getTags (): Tag[] {
        return this.tagStateService.getTags();
    }

    public getTag (id: number): Tag {
        const tags = this.getTags();

        return tags.find((currTag) => currTag.id === id);
    }

    public getTagByIndex (index: number): Tag {
        const tags = this.getTags();

        return tags[index];
    }

    public updateTags (): void {
        this.tagService.getAll()
            .pipe(
                tap((tags) => this.tagStateService.setTags(tags))
            )
            .subscribe();
    }

    public updateTagsIfNeeded (): void {
        const tags = this.getTags();

        if (!tags) {
            this.updateTags();
        }
    }

    public createTag (tagName: string = 'New Tag'): void {
        const tag: Partial<Tag> = { name: tagName };

        this.tagStateService.addTag(tag);

        this.tagService.create(tag.name)
            .pipe(
                switchMap((tagId) => this.tagService.get(tagId))
            )
            .subscribe(
                (addedTag) => this.tagStateService.updateTagId(tag as Tag, addedTag),
                () => this.tagStateService.removeTag(tag.id)
            );
    }

    public changeTagName (tagId: number, newTagName: string): void {
        const targetTag = this.getTag(tagId);
        const newTag    = { ...targetTag };

        newTag.name = newTagName;

        this.tagStateService.editTag(newTag);

        this.tagService.edit(tagId, newTag)
            .subscribe(
                () => {},
                () => this.tagStateService.editTag(targetTag)
            );
    }

    public deleteTag (tag: Tag): void {
        this.tagStateService.removeTag(tag.id);

        this.tagService.delete(tag.id)
            .subscribe(
                () => {},
                () => this.tagStateService.addTag(tag)
            );
    }

}
