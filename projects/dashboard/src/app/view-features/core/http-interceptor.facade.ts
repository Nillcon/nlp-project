import { Injectable, OnDestroy } from "@angular/core";
import { AutoUnsubscribe } from 'ngx-auto-unsubscribe';
import { ToastService } from '@Shared/features/toast';
import { HttpInterceptorService } from '@Shared/features';
import { Subscription } from 'rxjs';
import { HttpErrorResponse } from '@angular/common/http';
import { tap } from 'rxjs/operators';

@AutoUnsubscribe()
@Injectable({
    providedIn: 'root'
})
export class HttpInterceptorFacade implements OnDestroy {

    private readonly _httpErrorToastMap = new Map<number, (error: HttpErrorResponse) => void>()
        .set(
            0,
            (error: HttpErrorResponse) => {
                this.toastService.error({
                    title: 'Unknown Error',
                    description: error.message,
                    duration: 7000
                });
            }
        )
        .set(
            200,
            (error: HttpErrorResponse) => {
                this.toastService.error({
                    title: 'Something went wrong',
                    description: error.message,
                    duration: 7000
                });
            }
        )
        .set(
            422,
            (error: HttpErrorResponse) => {
                this.toastService.warn({
                    title: 'Warning',
                    description: error.error || error.message,
                    duration: 12000
                });
            }
        )
        .set(
            403,
            (error: HttpErrorResponse) => {
                this.toastService.error({
                    title: 'Forbidden action',
                    description: error.error || error.message,
                    duration: 7000
                });
            }
        )
        .set(
            500,
            (error: HttpErrorResponse) => {
                this.toastService.error({
                    title: 'Error',
                    description: error.error || error.message,
                    duration: 7000
                });
            }
        );

    private _httpErrorSubscription: Subscription;

    constructor (
        private readonly httpInterceptorService: HttpInterceptorService,
        private readonly toastService: ToastService
    ) {}

    public ngOnDestroy (): void {}

    public subscribe (): void {
        this.httpErrorObserver();
    }

    public unsubscribe (): void {
        if (this._httpErrorSubscription) {
            this._httpErrorSubscription.unsubscribe();
        }
    }

    private httpErrorObserver (): void {
        this._httpErrorSubscription = this.httpInterceptorService.OnError$
            .pipe(
                tap((error) => {
                    console.log(error);
                    const errorMessageFunction = this._httpErrorToastMap.get(error.status);

                    if (errorMessageFunction) {
                        errorMessageFunction(error);
                    }
                })
            )
            .subscribe();
    }

}
