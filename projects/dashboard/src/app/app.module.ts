import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { NgModule, APP_INITIALIZER } from '@angular/core';

import { AppRoutingModule } from './app.routing';
import { AppComponent } from './app.component';
import { SharedModule } from './app-shared.module';
import { HTTP_HOST, HTTP_API_ROUTE, HttpInterceptorService, DEBUG_HOST_STORAGE_KEY, ACCESS_TOKEN_STORAGE_KEY } from '@Shared/features';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { environment } from '@Dashboard/environment/environment';
import { ToastsContainerComponent } from './components/toasts-container/toasts-container.component';
import { StorageKey } from '@Dashboard/enums/storage-key.enum';
import { AppConfig } from './app.config';
import { ChatModule } from '@progress/kendo-angular-conversational-ui';


@NgModule({
    declarations: [
        AppComponent,
        ToastsContainerComponent
    ],
    imports: [
        BrowserModule,
        BrowserAnimationsModule,
        HttpClientModule,

        AppRoutingModule,

        SharedModule.forRoot(),

        ChatModule
    ],
    providers: [
        AppConfig,
        HttpInterceptorService,
        { provide: HTTP_HOST, useValue: environment.host },
        { provide: HTTP_API_ROUTE, useValue: environment.apiRoute },
        { provide: DEBUG_HOST_STORAGE_KEY, useValue: StorageKey.Debug_Host },
        { provide: ACCESS_TOKEN_STORAGE_KEY, useValue: StorageKey.Access_Token },
        { provide: HTTP_INTERCEPTORS, useExisting: HttpInterceptorService, multi: true },
        { provide: APP_INITIALIZER, useFactory: (config: AppConfig) => () => config.load(), deps: [AppConfig], multi: true }
    ],
    bootstrap: [
        AppComponent
    ]
})
export class AppModule {

    constructor () {}

}
