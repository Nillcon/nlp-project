import { NgModule } from '@angular/core';
import { SharedModule } from '@Dashboard/app-shared.module';
import { FeaturesModule } from '@Dashboard/features/features.module';
import { HeaderComponent } from './header.component';
import { HeaderLogoComponent } from './header-logo/header-logo.component';
import { HeaderUserComponent } from './header-user/header-user.component';
import { HeaderUserMenuComponent } from './header-user-menu/header-user-menu.component';

@NgModule({
    declarations: [
        HeaderComponent,
        HeaderLogoComponent,
        HeaderUserComponent,
        HeaderUserMenuComponent
    ],
    imports: [
        SharedModule,
        FeaturesModule
    ],
    exports: [
        HeaderComponent
    ]
})
export class HeaderModule {}
