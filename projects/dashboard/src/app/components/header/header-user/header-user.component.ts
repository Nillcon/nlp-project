import { Component, OnInit, Output, EventEmitter, ChangeDetectionStrategy } from '@angular/core';
import { Observable } from 'rxjs';
import { User } from '@Dashboard/features/user/interfaces/user.interface';
import { UserFacade } from '@Dashboard/view-features/user/facades/user.facade';

@Component({
    selector: 'app-header-user',
    templateUrl: './header-user.component.html',
    styleUrls: ['./header-user.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class HeaderUserComponent implements OnInit {

    public user$: Observable<User>;

    @Output()
    public OnClick: EventEmitter<MouseEvent> = new EventEmitter<MouseEvent>();

    constructor (
        private readonly userFacade: UserFacade
    ) {}

    public ngOnInit (): void {
        this.initUserObservable();
    }

    private initUserObservable (): void {
        this.user$ = this.userFacade.data$;
    }

}
