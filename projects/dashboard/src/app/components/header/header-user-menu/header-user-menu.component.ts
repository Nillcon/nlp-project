import { Component, OnInit, ViewChild, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { Menu } from 'primeng-lts/menu';
import { MenuItem } from 'primeng-lts/api';
import { environment } from '@Dashboard/environment/environment';
import { NavigationService } from '@Shared/features/navigation/navigation.service';
import { MainLayoutPageEnum } from '@Dashboard/layouts/main/shared/main-layout-page.enum';
import { AppLayoutEnum } from '@Dashboard/enums/app-layout.enum';
import { UserFacade } from '@Dashboard/view-features/user/facades/user.facade';

@Component({
    selector: 'app-header-user-menu',
    templateUrl: './header-user-menu.component.html',
    styleUrls: ['./header-user-menu.component.scss'],
    providers: [NavigationService, UserFacade],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class HeaderUserMenuComponent implements OnInit {

    public items: MenuItem[];

    @ViewChild(Menu, { static: true })
    private readonly _menuComponent: Menu;

    constructor (
        private readonly navigationService: NavigationService,
        private readonly userFacade: UserFacade,
        private readonly changeDetector: ChangeDetectorRef
    ) {}

    public ngOnInit (): void {
        this.items = [
            {
                label: 'My account',
                icon: 'fas fa-user',
                routerLink: MainLayoutPageEnum.User
            },
            { separator: true },
            {
                label: 'Go to landing',
                icon: 'fas fa-certificate',
                command: () => location.href = environment.landingAppPath
            },
            {
                label: 'Logout',
                icon: 'fas fa-sign-out-alt',
                command: () => this.onLogoutButtonClick()
            },
        ];
    }

    public toggle (mouseEvent: MouseEvent): void {
        this._menuComponent.toggle(mouseEvent);
        this.changeDetector.markForCheck();
    }

    public open (mouseEvent: MouseEvent): void {
        this._menuComponent.show(mouseEvent);
        this.changeDetector.markForCheck();
    }

    public close (): void {
        this._menuComponent.hide();
        this.changeDetector.markForCheck();
    }

    private onLogoutButtonClick (): void {
        this.userFacade.logout();

        if (environment.production) {
            location.href = environment.landingAppPath;
        } else {
            this.navigationService.navigate({
                subUrl: `${AppLayoutEnum.SignIn}`,
                relativeToCurrentPath: false
            });
        }
    }

}
