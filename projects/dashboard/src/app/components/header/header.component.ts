import { Component, OnInit, ViewChild, ChangeDetectionStrategy } from '@angular/core';
import { HeaderUserMenuComponent } from './header-user-menu/header-user-menu.component';
import { AppLayoutEnum } from '@Dashboard/enums/app-layout.enum';
import { environment } from '@Dashboard/environment/environment';

@Component({
    selector: 'global-header',
    templateUrl: './header.component.html',
    styleUrls: ['./header.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class HeaderComponent implements OnInit {

    public readonly mainLayoutLink = `/${AppLayoutEnum.Main}`;

    public readonly menuItems = [
        {
            icon: '🏠',
            name: 'Home',
            link: `/${AppLayoutEnum.Main}`
        },
        {
            icon: '🤖',
            name: 'Bot builder',
            link: `/${AppLayoutEnum.BotBuilder}`
        }
    ];

    @ViewChild(HeaderUserMenuComponent, { static: true })
    private readonly _userMenuComponent: HeaderUserMenuComponent;

    constructor () {}

    public ngOnInit (): void {}

    public onUserAvatarClick (event: MouseEvent): void {
        this._userMenuComponent.toggle(event);
    }

}
