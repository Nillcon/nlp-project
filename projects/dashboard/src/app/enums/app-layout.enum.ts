export enum AppLayoutEnum {
    Main        = 'Main',
    SignIn      = 'SignIn',
    BotBuilder  = 'Script',
    NotFound    = '404',
}
