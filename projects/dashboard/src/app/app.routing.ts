import { NgModule } from '@angular/core';
import { Routes, RouterModule, PreloadAllModules } from '@angular/router';
import { AppLayoutEnum } from './enums/app-layout.enum';

const routes: Routes = [
    {
        path: AppLayoutEnum.Main,
        loadChildren: () => import('./layouts/main/main-layout.module').then(m => m.MainLayoutModule)
    },
    {
        path: AppLayoutEnum.BotBuilder,
        loadChildren: () => import('./layouts/bot-builder-layout/bot-builder-layout.module').then(m => m.BotBuilderLayoutModule)
    },
    {
        path: AppLayoutEnum.SignIn,
        loadChildren: () => import('./layouts/sign-in/sign-in.module').then(m => m.SignInModule)
    },
    {
        path: AppLayoutEnum.NotFound,
        loadChildren: () => import('./layouts/not-found/not-found-layout.module').then(m => m.NotFoundLayoutModule)
    },
    {
        path: '',
        redirectTo: AppLayoutEnum.Main,
        pathMatch: 'full'
    },
    {
        path: '**',
        redirectTo: AppLayoutEnum.NotFound
    }
];

@NgModule({
    imports: [
        RouterModule.forRoot(routes, {
            preloadingStrategy: PreloadAllModules
        })
    ],
    exports: [
        RouterModule
    ]
})
export class AppRoutingModule {}
