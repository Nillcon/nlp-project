import { ElementRef, TemplateRef, ViewContainerRef } from "@angular/core";

export class ElementRenderableFeatures {

    constructor (
        private readonly _elem: ElementRef,
        private readonly _templateRef: TemplateRef<any>,
        private readonly _viewContainer: ViewContainerRef,
    ) {}

    protected hideElement (): void {
        this._viewContainer.clear();
    }

    protected showElement (): void {
        this.hideElement();
        this._viewContainer.createEmbeddedView(this._templateRef);
    }

}
