import { NgModule } from '@angular/core';
import { SharedModule } from '@Dashboard/app-shared.module';

@NgModule({
    imports: [
        SharedModule
    ],
    exports: [
        SharedModule
    ]
})
export class MainLayoutSharedModule {}
