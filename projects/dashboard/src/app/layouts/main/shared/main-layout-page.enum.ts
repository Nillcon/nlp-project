export enum MainLayoutPageEnum {
    Index           = 'Index',
    ModelPredict    = 'ModelPredict',
    ModelCreation   = 'ModelCreation',
    ModelSettings   = 'ModelSettings',
    User            = 'User',
    Script          = 'Script'
}
