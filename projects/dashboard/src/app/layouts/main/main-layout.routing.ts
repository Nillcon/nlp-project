import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MainLayoutComponent } from './main-layout.component';
import { MainLayoutPageEnum } from './shared/main-layout-page.enum';
import { MainLayoutGuard } from './guards/main-layout.guard';

const routes: Routes = [
    {
        path: '',
        component: MainLayoutComponent,
        canActivate: [MainLayoutGuard],
        children: [
            {
                path: MainLayoutPageEnum.Index,
                loadChildren: () => import('./pages/index/index.module').then(m => m.IndexPageModule)
            },
            {
                path: MainLayoutPageEnum.ModelPredict,
                loadChildren: () => import('./pages/model-predict/model-predict.module').then(m => m.ModelPredictModule)
            },
            {
                path: MainLayoutPageEnum.ModelCreation,
                loadChildren: () => import('./pages/model-creation/model-creation.module').then(m => m.ModelCreationPageModule)
            },
            {
                path: MainLayoutPageEnum.ModelSettings,
                loadChildren: () => import('./pages/model-settings/model-settings.module').then(m => m.ModelSettingsPageModule)
            },
            {
                path: MainLayoutPageEnum.User,
                loadChildren: () => import('./pages/user/user.module').then(m => m.UserPageModule)
            },
            {
                path: MainLayoutPageEnum.Script,
                loadChildren: () => import('./pages/script').then(m => m.ScriptModule)
            },
            {
                path: '**',
                redirectTo: MainLayoutPageEnum.Index,
                pathMatch: 'full'
            }
        ]
    },
    {
        path: '**',
        redirectTo: '',
        pathMatch: 'full'
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(routes)
    ],
    exports: [
        RouterModule
    ]
})
export class MainLayoutRoutingModule {}
