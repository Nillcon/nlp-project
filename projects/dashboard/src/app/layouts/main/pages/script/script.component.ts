import { Component, OnInit, ChangeDetectorRef, ChangeDetectionStrategy } from '@angular/core';
import { ScriptFacade } from '@Dashboard/view-features/script';
import { ActivatedRoute } from '@angular/router';
import { Script } from '@Dashboard/features/script/interfaces';
import { UrlService } from '@Shared/features';
import { QueryParametrEnum } from './shared';
import { trigger, transition, useAnimation } from '@angular/animations';
import { fadeIn } from 'ng-animate';

@Component({
    selector: 'app-script',
    templateUrl: './script.component.html',
    styleUrls: ['./script.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
    animations: [
        trigger('wrapperAnimation', [
            transition(':enter', useAnimation(fadeIn, { params: { timing: 0.3 } }))
        ])
    ],
})
export class ScriptComponent implements OnInit {

    public scriptId: number;
    public script: Script;

    constructor (
        private readonly scriptFacade: ScriptFacade,
        private readonly changeDetectorRef: ChangeDetectorRef,
        private readonly urlService: UrlService
    ) { }

    public ngOnInit (): void {
        this.scriptId = +this.urlService.getParameter(QueryParametrEnum.Id);

        this.scriptFacade.updateScript(this.scriptId)
            .subscribe(script => {
                this.script = script;

                this.changeDetectorRef.markForCheck();
            });
    }

}
