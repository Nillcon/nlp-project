import { Component, OnInit, ChangeDetectionStrategy, Input } from '@angular/core';
import { Script } from '@Dashboard/features/script/interfaces';

@Component({
    selector: 'app-script-title',
    templateUrl: './title.component.html',
    styleUrls: ['./title.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class ScriptTitleComponent implements OnInit {

    @Input()
    public readonly script: Script;

    public readonly scriptIconLink = 'assets/icons/script-icon.png';

    constructor () {}

    public ngOnInit (): void {}

}
