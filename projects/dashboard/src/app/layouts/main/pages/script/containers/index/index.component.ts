import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { trigger, transition, useAnimation } from '@angular/animations';
import { fadeIn } from 'ng-animate';
import { UrlService } from '@Shared/features';
import { QueryParametrEnum } from '../../shared';

@Component({
    selector: 'app-index',
    templateUrl: './index.component.html',
    styleUrls: ['./index.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
    animations: [
        trigger('wrapperAnimation', [
            transition(':enter', useAnimation(fadeIn, { params: { timing: 0.2 } }))
        ])
    ]
})
export class IndexComponent implements OnInit {

    public scriptId: number;

    constructor (
        private readonly urlService: UrlService,
    ) { }

    public ngOnInit (): void {
        this.scriptId = +this.urlService.getParameter(QueryParametrEnum.Id);
    }

}
