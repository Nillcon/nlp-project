import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VariableCreationWindowComponent } from './variable-creation-window.component';

describe('VariableCreationWindowComponent', () => {
  let component: VariableCreationWindowComponent;
  let fixture: ComponentFixture<VariableCreationWindowComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VariableCreationWindowComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VariableCreationWindowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
