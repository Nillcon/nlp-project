import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VariableEditingWindowComponent } from './variable-editing-window.component';

describe('VariableEditingWindowComponent', () => {
  let component: VariableEditingWindowComponent;
  let fixture: ComponentFixture<VariableEditingWindowComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VariableEditingWindowComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VariableEditingWindowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
