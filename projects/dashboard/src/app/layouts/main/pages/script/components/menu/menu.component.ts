import { Component, OnInit } from '@angular/core';
import { MenuItem } from 'primeng-lts/api';

import { ScriptContainerEnum } from '../../shared';
import { NavigationService } from '@Shared/features';

@Component({
    selector: 'app-menu',
    templateUrl: './menu.component.html',
    styleUrls: ['./menu.component.scss'],
    providers: [
        NavigationService
    ]
})
export class MenuComponent implements OnInit {

    public readonly menuItems: MenuItem[] = [
        {
            label: 'Info',
            icon: 'pi pi-fw pi-plus',
            routerLink: ScriptContainerEnum.Index,
            command: () => {
                this.navigationService.navigate({
                    subUrl: ScriptContainerEnum.Index,
                    mergeQueryParams: true
                });
            }
        },
        {
            label: 'Variables',
            icon: 'fas fa-superscript',
            routerLink: ScriptContainerEnum.Variables,
            command: () => {
                this.navigationService.navigate({
                    subUrl: ScriptContainerEnum.Variables,
                    mergeQueryParams: true
                });
            }
        }
    ];

    constructor (
        private readonly navigationService: NavigationService
    ) { }

    public ngOnInit (): void {
    }

}
