import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ScriptComponent } from './script.component';
import { ScriptContainerEnum } from './shared';

const routes: Routes = [
    {
        path: '',
        component: ScriptComponent,
        children: [
            {
                path: ScriptContainerEnum.Index,
                loadChildren: () => import('./containers/index').then(m => m.IndexModule)
            },
            {
                path: ScriptContainerEnum.Variables,
                loadChildren: () => import('./containers/variables').then(m => m.VariablesModule)
            },
            {
                path: '',
                redirectTo: ScriptContainerEnum.Variables
            }
        ]
    },
    {
        path: '**',
        redirectTo: ''
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(routes)
    ],
    exports: [
        RouterModule
    ]
})
export class ScriptRoutingModule {}
