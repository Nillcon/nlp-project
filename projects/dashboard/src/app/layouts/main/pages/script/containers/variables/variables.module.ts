import { NgModule } from '@angular/core';
import { VariablesRoutingModule } from './variables.routing';
import { FeaturesModule } from '@Dashboard/features/features.module';

import { VariablesComponent } from './variables.component';
import { ToolbarComponent } from './toolbar';
import { VariablesListComponent } from './variables-list';
import { VariableCreationWindowComponent } from './variable-creation-window';
import { VariableEditingWindowComponent } from './variable-editing-window';
import { MainLayoutSharedModule } from '@Dashboard/layouts/main/main-layout-shared.module';
import { VariableFilterPipe } from './variable-filter.pipe';



@NgModule({
    declarations: [
        VariablesComponent,
        ToolbarComponent,
        VariablesListComponent,
        VariableCreationWindowComponent,
        VariableEditingWindowComponent,
        VariableFilterPipe
    ],
    imports: [
        FeaturesModule,
        MainLayoutSharedModule,

        VariablesRoutingModule
    ],
    entryComponents: [
        VariableCreationWindowComponent,
        VariableEditingWindowComponent
    ]
})
export class VariablesModule { }
