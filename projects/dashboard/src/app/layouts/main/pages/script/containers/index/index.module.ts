import { NgModule } from '@angular/core';

import { IndexComponent } from './index.component';
import { IndexRoutingModule } from './index.routing';



@NgModule({
    declarations: [
        IndexComponent
    ],
    imports: [
        IndexRoutingModule,
        // SharedModule,

    ]
})
export class IndexModule { }
