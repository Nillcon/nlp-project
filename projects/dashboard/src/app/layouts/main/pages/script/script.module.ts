import { NgModule } from '@angular/core';
import { SharedModule } from '@Dashboard/app-shared.module';
import { ScriptRoutingModule } from './script.routing';

import { ScriptComponent } from './script.component';
import { MenuComponent } from './components';
import { BackButtonComponent } from './components/back-button/back-button.component';
import { ScriptTitleComponent } from './components/title/title.component';
import { MainLayoutSharedModule } from '../../main-layout-shared.module';

@NgModule({
    declarations: [
        ScriptComponent,
        MenuComponent,
        BackButtonComponent,
        ScriptTitleComponent,
    ],
    imports: [
        SharedModule,
        MainLayoutSharedModule,
        ScriptRoutingModule
    ]
})
export class ScriptModule {}
