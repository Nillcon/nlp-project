import { MenuItem } from 'primeng-lts/api';
import { ScriptContainerEnum } from '../shared';

export const MenuItems: MenuItem[] = [
    {
        label: 'Info',
        icon: 'pi pi-fw pi-plus',
        routerLink: ScriptContainerEnum.Index,
    },
    {
        label: 'Variables',
        icon: 'fas fa-superscript',
        routerLink: ScriptContainerEnum.Variables,
    }
];
