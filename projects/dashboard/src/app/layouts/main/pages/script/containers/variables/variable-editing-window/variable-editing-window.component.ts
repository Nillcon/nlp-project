import { Component, OnInit, ChangeDetectorRef, ChangeDetectionStrategy } from '@angular/core';
import { DynamicDialogRef, DynamicDialogConfig } from 'primeng-lts/api';
import { Variable } from '@Dashboard/features/variable';
import { FormGroupTypeSafe } from 'form-type-safe';

@Component({
    selector: 'app-variable-editing-window',
    templateUrl: './variable-editing-window.component.html',
    styleUrls: ['./variable-editing-window.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class VariableEditingWindowComponent implements OnInit {

    public formGroup: FormGroupTypeSafe<Variable>;
    public inputData: Variable;

    constructor (
        private readonly config: DynamicDialogConfig,
        private readonly changeDetectorRef: ChangeDetectorRef,
        public ref: DynamicDialogRef
    ) {
        this.inputData = this.config.data;
    }

    public ngOnInit (): void {
    }

    public onFormInit (formGroup: FormGroupTypeSafe<Variable>): void {
        this.formGroup = formGroup;
    }

    public onSaveButtonClick (): void {
        if (this.formGroup.valid) {
            this.ref.close(this.formGroup.value);
        } else {
            this.formGroup.markAllAsTouched();
            this.changeDetectorRef.detectChanges();
        }
    }

    public onCloseButtonClick (): void {
        this.ref.close(false);
    }
}
