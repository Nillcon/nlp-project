import { Component, OnInit, Output, EventEmitter, ChangeDetectionStrategy } from '@angular/core';

@Component({
    selector: 'app-toolbar',
    templateUrl: './toolbar.component.html',
    styleUrls: ['./toolbar.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class ToolbarComponent implements OnInit {

    @Output()
    public OnCreateButtonClick: EventEmitter<MouseEvent> = new EventEmitter();

    @Output()
    public OnUploadButtonClick: EventEmitter<MouseEvent> = new EventEmitter();

    @Output()
    public OnSearchTextChanged: EventEmitter<string> = new EventEmitter();

    public searchText: string;

    constructor () { }

    public ngOnInit (): void {
    }

    public onCreateButtonClick (mouseEvent: MouseEvent): void {
        this.OnCreateButtonClick.emit(mouseEvent);
    }

    public onUploadButtonClick (mouseEvent: MouseEvent): void {
        this.OnCreateButtonClick.emit(mouseEvent);
    }

    public onSearchChange (): void {
        this.OnSearchTextChanged.emit(this.searchText);
    }
}
