import { Component, OnInit, Input, Output, EventEmitter, ChangeDetectionStrategy } from '@angular/core';
import { Script } from '@Dashboard/features/script/interfaces';
import { Variable } from '@Dashboard/features/variable';

@Component({
    selector: 'app-variables-list',
    templateUrl: './variables-list.component.html',
    styleUrls: ['./variables-list.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class VariablesListComponent implements OnInit {

    @Input()
    public variables: Variable[];

    @Output()
    public OnEditButtonClick: EventEmitter<Variable> = new EventEmitter();

    @Output()
    public OnDeleteButtonClick: EventEmitter<Variable> = new EventEmitter();

    constructor () { }

    public ngOnInit (): void {
    }

    public onEditButtonClick (variable: Variable): void {
        this.OnEditButtonClick.emit(variable);
    }

    public onDeleteButtonClick (variable: Variable): void {
        this.OnDeleteButtonClick.emit(variable);
    }

}
