import { Component, OnInit, ChangeDetectorRef, Injector, ChangeDetectionStrategy } from '@angular/core';
import { ScriptFacade } from '@Dashboard/view-features/script';
import { ActivatedRoute } from '@angular/router';
import { Script } from '@Dashboard/features/script/interfaces';
import { Observable } from 'rxjs';
import { UrlService } from '@Shared/features';
import { QueryParametrEnum } from '../../shared';
import { tap, switchMap, filter, map } from 'rxjs/operators';
import { Variable } from '@Dashboard/features/variable';
import { NeedsConfirmation } from '@Shared/modules/confirmation-window';
import { DialogService } from 'primeng-lts/api';
import { VariableCreationWindowComponent } from './variable-creation-window';
import { VariableEditingWindowComponent } from './variable-editing-window';
import { trigger, transition, useAnimation } from '@angular/animations';
import { fadeIn } from 'ng-animate';

@Component({
    selector: 'app-variables',
    templateUrl: './variables.component.html',
    styleUrls: ['./variables.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
    animations: [
        trigger('wrapperAnimation', [
            transition(':enter', useAnimation(fadeIn, { params: { timing: 0.2 } }))
        ])
    ]
})
export class VariablesComponent implements OnInit {

    public scriptId: number;
    public script$: Observable<Script>;

    public searchText: string;

    constructor (
        public injector: Injector,
        private readonly scriptFacade: ScriptFacade,
        private readonly urlService: UrlService,
        private readonly dialog: DialogService,
    ) { }

    public ngOnInit (): void {
        this.scriptId = +this.urlService.getParameter(QueryParametrEnum.Id);

        this.updateVariables();
        this.script$ = this.scriptFacade.getScript$(this.scriptId);
    }

    private updateVariables (): void {
        // this.scriptFacade.updateScript(this.scriptId)
        //     .pipe(
        //         switchMap(() => this.scriptFacade.updateVariables(this.scriptId))
        //     )
        //     .subscribe();

        this.scriptFacade.updateVariables(this.scriptId).subscribe();
    }

    public onCreateVariableButtonClick (): void {
        const dialog = this.dialog.open(VariableCreationWindowComponent, {
            showHeader: false,
            dismissableMask: true
        });

        dialog.onClose
            .pipe(
                filter(res => !!res),
                switchMap(variable => this.scriptFacade.createVariable(this.scriptId, variable))
            )
            .subscribe();
    }

    public onEditVariableButtonClick (variable: Variable): void {
        const dialog = this.dialog.open(VariableEditingWindowComponent, {
            data: variable,
            showHeader: false,
            dismissableMask: true
        });

        dialog.onClose
            .pipe(
                filter(res => !!res),
                map(new_variable => {
                    return {...new_variable, id: variable.id};
                }),
                switchMap(new_variable => this.scriptFacade.editVariable(this.scriptId, new_variable))
            )
            .subscribe();
    }

    public onSearchTextChanged (searchText: string): void {
        this.searchText = searchText;
    }

    @NeedsConfirmation('Are you sure, you want to delete variable?')
    public onDeleteVariableButtonClick (variable: Variable): void {
        this.scriptFacade.deleteVariable(this.scriptId, variable.id)
            .subscribe();
    }

}
