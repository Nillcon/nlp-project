import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { VariablesComponent } from './variables.component';

const routes: Routes = [
    {
        path: '',
        component: VariablesComponent
    },
    {
        path: '**',
        redirectTo: ''
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(routes)
    ],
    exports: [
        RouterModule
    ]
})
export class VariablesRoutingModule {}
