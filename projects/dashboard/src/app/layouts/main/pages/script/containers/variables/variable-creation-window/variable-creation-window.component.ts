import { Component, OnInit, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng-lts/api';
import { FormGroupTypeSafe } from 'form-type-safe';
import { Variable } from '@Dashboard/features/variable';

@Component({
    selector: 'app-variable-creation-window',
    templateUrl: './variable-creation-window.component.html',
    styleUrls: ['./variable-creation-window.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class VariableCreationWindowComponent implements OnInit {

    public formGroup: FormGroupTypeSafe<Variable>;

    constructor (
        private readonly changeDetectorRef: ChangeDetectorRef,
        public ref: DynamicDialogRef
    ) { }

    public ngOnInit (): void {
    }

    public onFormInit (formGroup: FormGroupTypeSafe<Variable>): void {
        this.formGroup = formGroup;
    }

    public onCreateButtonClick (): void {
        if (this.formGroup.valid) {
            this.ref.close(this.formGroup.value);
        } else {
            this.formGroup.markAllAsTouched();
            this.changeDetectorRef.detectChanges();
        }
    }

    public onCloseButtonClick (): void {
        this.ref.close(false);
    }

}
