import { Pipe, PipeTransform } from '@angular/core';
import { Variable } from '@Dashboard/features/variable';

@Pipe({
    name: 'variableFilter'
})
export class VariableFilterPipe implements PipeTransform {
    public transform (variables: Variable[], args: string = ''): Variable[] {
        const keyword = args.toLowerCase();

        if (args) {
            variables = variables
                .filter(variable => {
                    const variableName = variable.name.toLowerCase();
                    return variableName.includes(keyword);
                });
        }

        return variables || [];
    }
}
