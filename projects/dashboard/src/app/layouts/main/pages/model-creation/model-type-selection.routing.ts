import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ModelCreationComponent } from './model-creation.component';

const routes: Routes = [
    {
        path: '',
        component: ModelCreationComponent
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(routes)
    ],
    exports: [
        RouterModule
    ]
})
export class ModelCreationRoutingModule {}
