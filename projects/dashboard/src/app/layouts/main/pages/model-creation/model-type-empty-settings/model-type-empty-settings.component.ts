import { Component, OnInit, ChangeDetectionStrategy, EventEmitter, Output } from '@angular/core';

@Component({
    selector: 'app-model-type-empty-settings',
    templateUrl: './model-type-empty-settings.component.html',
    styleUrls: ['./model-type-empty-settings.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class ModelTypeEmptySettingsComponent implements OnInit {

    @Output()
    public OnEmptySettingsInit = new EventEmitter();

    constructor () {}

    public ngOnInit (): void {
        this.OnEmptySettingsInit.emit(null);
    }

}
