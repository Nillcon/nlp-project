import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { FormGroupTypeSafe } from 'form-type-safe';
import { Observable, Subscription } from 'rxjs';
import { Model, ModelType, ModelTypeEnum, ModelCreatingData } from '@Dashboard/features/model';
import { ToastService } from '@Shared/features/toast';
import { ModelFacade, ModelStepIdentifierService } from '@Dashboard/view-features';

@Component({
    selector: 'app-model-creation',
    templateUrl: './model-creation.component.html',
    styleUrls: ['./model-creation.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class ModelCreationComponent implements OnInit {

    public selectedModelType: ModelType;
    public modelTypeEnum: typeof ModelTypeEnum = ModelTypeEnum;

    public modelCreationSubscription: Subscription;

    private modelFormGroup: FormGroupTypeSafe<Partial<Model>>;
    private modelTypeSettingsFormGroup: FormGroupTypeSafe<any>;

    constructor (
        private readonly stepIdentifierService: ModelStepIdentifierService,
        private readonly toastService: ToastService,
        private readonly modelFacade: ModelFacade
    ) {}

    public ngOnInit (): void {}

    public onModelFormGroupInit (formGroup: FormGroupTypeSafe<Partial<Model>>): void {
        this.modelFormGroup = formGroup;
    }

    public onModelTypeSettingsFormGroupInit (formGroup: FormGroupTypeSafe<any>): void {
        this.modelTypeSettingsFormGroup = formGroup;
    }

    public onModelTypeSelected (modelType: ModelType): void {
        this.selectedModelType = modelType;
    }

    public onCreateButtonClick (): void {
        this.validateModelFormGroup();
        this.validateModelTypeSettingsFormGroup();

        this.modelCreationSubscription = this.createModel()
            .subscribe((modelId) => this.openModelSettings(modelId));
    }

    public openModelSettings (createdModelId: number): void {
        const model = this.modelFacade.getModel(createdModelId);

        this.stepIdentifierService.openModelSettingsOnLastStep(model);
    }

    private createModel (): Observable<number> {
        const modelCreationFormData = this.modelFormGroup.value;
        const data = <ModelCreatingData>{
            ...modelCreationFormData,
            modelType: this.selectedModelType.id,
            data: this.modelTypeSettingsFormGroup?.value || null
        };

        return this.modelFacade.createModel(data);
    }

    private validateModelFormGroup (): void {
        if (!this.modelFormGroup.valid) {
            this.toastService.error({
                title: `Ooops...`,
                description: `Please enter model name before creating!`
            });

            throw new Error(`Model form isn't valid!`);
        }
    }

    private validateModelTypeSettingsFormGroup (): void {
        if (this.modelTypeSettingsFormGroup && !this.modelTypeSettingsFormGroup.valid) {
            this.toastService.error({
                title: `Ooops...`,
                description: `Model settings isn't valid!`
            });

            throw new Error(`Model settings form isn't valid!`);
        }
    }

}
