import { Component, OnInit, ChangeDetectionStrategy, Output, EventEmitter } from '@angular/core';
import { ModelType, ModelTypeMap } from '@Dashboard/features/model';

@Component({
    selector: 'app-model-type-list',
    templateUrl: './model-type-list.component.html',
    styleUrls: ['./model-type-list.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class ModelTypeListComponent implements OnInit {

    @Output()
    public OnModelTypeSelected = new EventEmitter<ModelType>();

    public modelTypes: ModelType[];

    public selectedModelTypeId: number;

    constructor () {}

    public ngOnInit (): void {
        this.initTypeList();
        this.initSelectionForFirstModelType();
    }

    public onModelTypeCardClick (modelType: ModelType): void {
        this.selectedModelTypeId = modelType.id;

        this.OnModelTypeSelected.emit(modelType);
    }

    private initTypeList (): void {
        this.modelTypes = [...ModelTypeMap.values()];
    }

    private initSelectionForFirstModelType (): void {
        this.onModelTypeCardClick(this.modelTypes[0]);
    }

}
