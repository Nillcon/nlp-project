import { NgModule } from '@angular/core';
import { ModelCreationComponent } from './model-creation.component';
import { ModelCreationRoutingModule } from './model-type-selection.routing';
import { SharedModule } from '@Dashboard/app-shared.module';
import { FeaturesModule } from '@Dashboard/features/features.module';
import { ModelTypeListComponent } from './model-type-list/model-type-list.component';
import { ModelTypeEmptySettingsComponent } from './model-type-empty-settings/model-type-empty-settings.component';

@NgModule({
    declarations: [
        ModelCreationComponent,
        ModelTypeListComponent,
        ModelTypeEmptySettingsComponent
    ],
    imports: [
        ModelCreationRoutingModule,

        SharedModule,
        FeaturesModule
    ]
})
export class ModelCreationPageModule {}
