import { NgModule } from '@angular/core';
import { ModelsComponent } from './models.component';
import { CreateModelButtonComponent } from './create-model-button/create-model-button.component';
import { ModelsListComponent } from './models-list/models-list.component';
import { ModelCardFooterComponent } from './model-card-footer/model-card-footer.component';
import { ModelCardHeaderComponent } from './model-card-header/model-card-header.component';
import { FeaturesModule } from '@Dashboard/features/features.module';
import { SharedModule } from '@Dashboard/app-shared.module';
import { ModelsContainerRoutingModule } from './models.routing';
import { MainLayoutSharedModule } from '@Dashboard/layouts/main/main-layout-shared.module';
import { ModelRenamingWindowComponent } from './model-renaming-window/model-renaming-window.component';
import { SearchFieldComponent } from './search-field/search-field.component';
import { FilterMultiselectComponent } from './filter-multiselect/filter-multiselect.component';

@NgModule({
    declarations: [
        ModelsComponent,
        CreateModelButtonComponent,
        ModelsListComponent,
        ModelCardFooterComponent,
        ModelCardHeaderComponent,
        ModelRenamingWindowComponent,
        SearchFieldComponent,
        FilterMultiselectComponent
    ],
    imports: [
        ModelsContainerRoutingModule,

        FeaturesModule,
        MainLayoutSharedModule,
        SharedModule
    ],
    entryComponents: [
        ModelRenamingWindowComponent
    ]
})
export class ModelsModule {}
