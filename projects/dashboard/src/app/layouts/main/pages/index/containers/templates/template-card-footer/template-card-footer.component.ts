import { Component, OnInit, ChangeDetectionStrategy, Input } from '@angular/core';
import { Template } from '@Dashboard/features/template';

@Component({
    selector: 'app-template-card-footer',
    templateUrl: './template-card-footer.component.html',
    styleUrls: ['./template-card-footer.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class TemplateCardFooterComponent implements OnInit {

    @Input()
    public readonly template: Template;

    constructor () {}

    public ngOnInit (): void {}

}
