import { Component, OnInit, ChangeDetectionStrategy, Output, EventEmitter } from '@angular/core';
import { MenuItem } from 'primeng-lts/api';
import { Router } from '@angular/router';
import { IndexContainerMap } from '../../data/index-container.map';

@Component({
    selector: 'app-tabs',
    templateUrl: './tabs.component.html',
    styleUrls: ['./tabs.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class TabsComponent implements OnInit {

    public tabs: MenuItem[];

    public activeTab: MenuItem;

    @Output()
    public OnTabClick = new EventEmitter<string>();

    constructor (
        private readonly router: Router
    ) {}

    public ngOnInit (): void {
        this.initTabItems();
        this.initActiveTabItem();
    }

    public initTabItems (): void {
        this.tabs = [...IndexContainerMap.values()]
            .map((currContainer) => <MenuItem>{
                label: currContainer.name,
                icon: currContainer.iconUrl,
                command: () => this.OnTabClick.emit(currContainer.id)
            });
    }

    public initActiveTabItem (): void {
        this.activeTab = this.getActiveTabItem();
    }

    private getActiveTabItem (): MenuItem {
        const parsedUrl   = this.router.parseUrl(this.router.url);
        const urlSegments = parsedUrl.root.children.primary.segments;
        const targetUrlSegment = urlSegments[urlSegments.length - 1];

        for (const currTab of this.tabs) {
            if (targetUrlSegment.path === currTab.label) {
                return currTab;
            }
        }
    }

}
