import { Script } from '@Dashboard/features/script/interfaces';
import { ScriptStateEnum } from '../enums/script-state.enum';

export interface ScriptView extends Script {
    state: ScriptStateEnum;
}
