import { Component, OnInit, ChangeDetectionStrategy, Input, ViewChild, Output, EventEmitter } from '@angular/core';
import { MenuItem } from 'primeng-lts/api';
import { ModelStateEnum } from '@Shared/enums';
import { IsObject } from 'class-validator';
import { ClassValidation } from '@Shared/decorators';
import { Menu } from 'primeng-lts/menu';
import { UserFacade, ViewModel } from '@Dashboard/view-features';
import { UserRoleEnum } from '@Dashboard/features/user';

@ClassValidation()
@Component({
    selector: 'app-model-card-header',
    templateUrl: './model-card-header.component.html',
    styleUrls: ['./model-card-header.component.scss'],
    providers: [UserFacade],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class ModelCardHeaderComponent implements OnInit {

    @Input()
    @IsObject()
    public modelData: ViewModel;

    @Output()
    public OnMoveToTemplatesButtonClick = new EventEmitter<MouseEvent>();

    @Output()
    public OnRenameButtonClick = new EventEmitter<MouseEvent>();

    @Output()
    public OnDeleteButtonClick = new EventEmitter<MouseEvent>();

    public actionsMenuItems: MenuItem[];

    public modelStateEnum: typeof ModelStateEnum = ModelStateEnum;

    @ViewChild('menu', { static: false })
    private readonly menuComponent: Menu;

    constructor (
        private readonly userFacade: UserFacade
    ) {}

    public ngOnInit (): void {
        this.initMenuItems();
    }

    public onMenuButtonClick (event: MouseEvent): void {
        this.menuComponent.toggle(event);
    }

    private initMenuItems (): void {
        this.actionsMenuItems = [
            {
                label: 'To templates',
                icon: 'fas fa-chevron-right',
                visible: this.isMenuItemAvailable([UserRoleEnum.SuperUser]),
                command: (event) => this.OnMoveToTemplatesButtonClick.emit(event.originalEvent)
            },
            {
                label: 'Rename',
                icon: 'fas fa-pencil-alt',
                command: (event) => this.OnRenameButtonClick.emit(event.originalEvent)
            },
            {
                label: 'Delete',
                icon: 'far fa-trash-alt text-danger',
                command: (event) => this.OnDeleteButtonClick.emit(event.originalEvent)
            }
        ];
    }

    private isMenuItemAvailable (availableRoles: UserRoleEnum[]): boolean {
        const { role: userRole, id: userId } = this.userFacade.data;

        return availableRoles.includes(userRole) && this.modelData.userId === userId;
    }

}
