import { Component, OnInit, ChangeDetectionStrategy, Input, ViewChild, Output, EventEmitter } from '@angular/core';
import { MenuItem } from 'primeng-lts/api';
import { ModelStateEnum } from '@Shared/enums';
import { ClassValidation } from '@Shared/decorators';
import { Menu } from 'primeng-lts/menu';
import { Template } from '@Dashboard/features/template';
import { UserRoleEnum } from '@Dashboard/features/user';
import { UserFacade } from '@Dashboard/view-features';
import { IsObject } from 'class-validator';

@ClassValidation()
@Component({
    selector: 'app-template-card-header',
    templateUrl: './template-card-header.component.html',
    styleUrls: ['./template-card-header.component.scss'],
    providers: [UserFacade],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class TemplateCardHeaderComponent implements OnInit {

    @Input()
    @IsObject()
    public templateData: Template;

    @Output()
    public OnMoveToModelsButtonClick = new EventEmitter<MouseEvent>();

    @Output()
    public OnDeleteButtonClick = new EventEmitter<MouseEvent>();

    public actionsMenuItems: MenuItem[];

    public modelStateEnum: typeof ModelStateEnum = ModelStateEnum;

    @ViewChild('menu', { static: false })
    private readonly menuComponent: Menu;

    constructor (
        private readonly userFacade: UserFacade
    ) {}

    public ngOnInit (): void {
        this.initMenuItems();
    }

    public onMenuButtonClick (event: MouseEvent): void {
        this.menuComponent.toggle(event);
    }

    private initMenuItems (): void {
        this.actionsMenuItems = [
            {
                label: 'To models',
                icon: 'fas fa-chevron-left',
                visible: this.isMenuItemAvailable([UserRoleEnum.SuperUser]),
                command: (event) => this.OnMoveToModelsButtonClick.emit(event.originalEvent)
            },
            {
                label: 'Delete',
                icon: 'far fa-trash-alt text-danger',
                command: (event) => this.OnDeleteButtonClick.emit(event.originalEvent)
            }
        ];
    }

    private isMenuItemAvailable (availableRoles: UserRoleEnum[]): boolean {
        const { role: userRole, id: userId } = this.userFacade.data;

        return availableRoles.includes(userRole) && this.templateData.userId === userId;
    }

}
