import { IndexContainerEnum } from '../enums/index-container.enum';

export interface IndexContainer {
    id: IndexContainerEnum;
    name: string;
    iconUrl: string;
}
