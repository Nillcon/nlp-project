import { Component, OnInit, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { Observable } from 'rxjs';
import { DialogService } from 'primeng-lts/api';
import { ScriptCreationWindowComponent } from './script-creation-window/script-creation-window.component';
import { switchMap } from 'rxjs/operators';
import { Script } from '@Dashboard/features/script/interfaces';
import { ScriptFacade } from '@Dashboard/view-features/script';
import { NavigationService } from '@Shared/features';
import { AppLayoutEnum } from '@Dashboard/enums';
import { ScriptEditingWindowComponent } from './script-editing-window/script-editing-window.component';
import { MainLayoutPageEnum } from '@Dashboard/layouts/main/shared/main-layout-page.enum';

@Component({
    selector: 'app-scripts',
    templateUrl: './scripts.component.html',
    styleUrls: ['./scripts.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class ScriptsComponent implements OnInit {

    public scripts$: Observable<Script[]>;

    constructor (
        private readonly dialog: DialogService,
        private readonly scriptFacade: ScriptFacade,
        private readonly navigationService: NavigationService,
    ) { }

    public ngOnInit (): void {
        this.scripts$ = this.scriptFacade.scripts$;
        this.scriptFacade.updateScripts();
    }

    public onCreateButtonClick (): void {
        const dialog = this.dialog.open(ScriptCreationWindowComponent, {
            showHeader: false,
            dismissableMask: true
        });

        dialog.onClose
            .pipe(
                switchMap(script => this.scriptFacade.createScript(script)),
            )
            .subscribe(() => {});
    }

    public onEditButtonClick (script: Script): void {
        const dialog = this.dialog.open(ScriptEditingWindowComponent, {
            showHeader: false,
            dismissableMask: true,
            data: script
        });

        dialog.onClose
            .pipe(
                switchMap((editedScript: Script) => this.scriptFacade.editScript(editedScript))
            )
            .subscribe(() => {});
    }

    public onScriptCardClick (script: Script): void {
        this.navigationService.navigate({
            subUrl: `/${AppLayoutEnum.Main}/${MainLayoutPageEnum.Script}`,
            params: {
                id: script.id
            },
            mergeQueryParams: true
        });
    }

    public onScriptDeleteButtonClick (script: Script): void {
        this.scriptFacade.deleteScript(script.id)
            .subscribe();
    }
}
