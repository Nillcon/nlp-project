import { Component, OnInit, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { Model } from '@Dashboard/features/model';
import { ModelFacade } from '@Dashboard/view-features';
import { IsNumber } from 'class-validator';
import { ClassValidation } from '@Shared/decorators';
import { Subscription } from 'rxjs';
import { FormGroupTypeSafe } from 'form-type-safe';
import { DynamicDialogRef, DynamicDialogConfig } from 'primeng-lts/api';
import { ToastService } from '@Shared/features/toast';

@ClassValidation()
@Component({
    selector: 'app-model-renaming-window',
    templateUrl: './model-renaming-window.component.html',
    styleUrls: ['./model-renaming-window.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class ModelRenamingWindowComponent implements OnInit {

    public modelData: Model;

    public savingSubscription: Subscription;

    @IsNumber()
    private readonly modelId: number;

    private formGroup: FormGroupTypeSafe<Partial<Model>>;

    constructor (
        private readonly changeDetector: ChangeDetectorRef,
        private readonly toastService: ToastService,
        private readonly modelFacade: ModelFacade,
        private readonly config: DynamicDialogConfig,
        private readonly dialogRef: DynamicDialogRef
    ) {
        this.modelId = this.config.data;
    }

    public ngOnInit (): void {
        this.updateModel();
    }

    public updateModel (): void {
        this.modelFacade.updateModel(this.modelId)
            .subscribe((data) => {
                this.modelData = data;

                this.changeDetector.markForCheck();
            });
    }

    public onFormGroupInit (formGroup: FormGroupTypeSafe<Partial<Model>>): void {
        this.formGroup = formGroup;
    }

    public onSaveButtonClick (): void {
        if (this.formGroup.valid) {
            this.savingSubscription = this.modelFacade.renameModel(this.modelId, this.formGroup.value)
                .subscribe(() => {
                    this.dialogRef.close(true);
                });
        } else {
            this.toastService.error({
                title: `Ooops...`,
                description: `Form isn't valid!`
            });
        }
    }

    public onCancelButtonClick (): void {
        this.dialogRef.close(false);
    }

}
