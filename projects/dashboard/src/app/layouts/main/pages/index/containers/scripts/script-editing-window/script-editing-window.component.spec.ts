import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ScriptEditingWindowComponent } from './script-editing-window.component';

describe('ScriptEditingWindowComponent', () => {
  let component: ScriptEditingWindowComponent;
  let fixture: ComponentFixture<ScriptEditingWindowComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ScriptEditingWindowComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ScriptEditingWindowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
