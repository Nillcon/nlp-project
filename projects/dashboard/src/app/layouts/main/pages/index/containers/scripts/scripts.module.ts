import { NgModule } from '@angular/core';
import { ScriptsContainerRoutingModule } from './scripts.routing';
import { MainLayoutSharedModule } from '@Dashboard/layouts/main/main-layout-shared.module';
import { SharedModule } from '@Dashboard/app-shared.module';
import { FeaturesModule } from '@Dashboard/features/features.module';

import { ScriptsComponent } from './scripts.component';
import { ScriptCreationWindowComponent } from './script-creation-window';
import { ScriptListComponent } from './script-list';
import { CreateScriptButtonComponent } from './create-script-button';
import { ScriptCardHeaderComponent } from './script-card-header';
import { ScriptEditingWindowComponent } from './script-editing-window/script-editing-window.component';

@NgModule({
    declarations: [
        ScriptsComponent,
        CreateScriptButtonComponent,
        ScriptListComponent,
        ScriptCreationWindowComponent,
        ScriptCardHeaderComponent,
        ScriptEditingWindowComponent
    ],
    imports: [
        FeaturesModule,
        MainLayoutSharedModule,
        SharedModule,

        ScriptsContainerRoutingModule
    ],
    entryComponents: [
        ScriptCreationWindowComponent,
        ScriptEditingWindowComponent
    ]
})
export class ScriptsModule {}
