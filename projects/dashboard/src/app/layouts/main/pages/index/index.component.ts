import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { trigger, transition, useAnimation } from '@angular/animations';
import { fadeIn } from 'ng-animate';
import { NavigationService } from '@Shared/features';

@Component({
    selector: 'app-index',
    templateUrl: './index.component.html',
    styleUrls: ['./index.component.scss'],
    animations: [
        trigger('wrapperAnimation', [
            transition(':enter', useAnimation(fadeIn, { params: { timing: 0.3 } }))
        ])
    ],
    providers: [NavigationService],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class IndexPageComponent implements OnInit {

    constructor (
        private readonly navigationService: NavigationService
    ) {}

    public ngOnInit (): void {}

    public onTabClick (containerName: string): void {
        this.navigationService.navigate({ subUrl: containerName });
    }

}
