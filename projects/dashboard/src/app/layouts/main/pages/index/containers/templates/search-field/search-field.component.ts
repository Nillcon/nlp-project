import { Component, OnInit, ChangeDetectionStrategy, EventEmitter, Output, AfterViewInit, ViewChild, ElementRef } from '@angular/core';

@Component({
    selector: 'app-search-field',
    templateUrl: './search-field.component.html',
    styleUrls: ['./search-field.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class SearchFieldComponent implements OnInit, AfterViewInit {

    @Output()
    public OnKeyUp = new EventEmitter<string>();

    @ViewChild('Field', { static: false })
    private readonly searchFieldElement: ElementRef<HTMLInputElement>;

    constructor () {}

    public ngOnInit (): void {}

    public ngAfterViewInit (): void {
        this.searchFieldElement?.nativeElement?.focus();
    }

}
