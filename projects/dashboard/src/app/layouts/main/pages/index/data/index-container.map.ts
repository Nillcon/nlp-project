import { IndexContainerEnum } from '../enums/index-container.enum';
import { IndexContainer } from '../interfaces/index-container.interface';

export const IndexContainerMap = new Map<IndexContainerEnum, IndexContainer>()
    .set(
        IndexContainerEnum.Models,
        {
            id: IndexContainerEnum.Models,
            name: 'Models',
            iconUrl: 'assets/icons/model-icon.png'
        }
    )
    .set(
        IndexContainerEnum.Templates,
        {
            id: IndexContainerEnum.Templates,
            name: 'Templates',
            iconUrl: 'assets/icons/template-icon.png'
        }
    ).set(
        IndexContainerEnum.Scripts,
        {
            id: IndexContainerEnum.Scripts,
            name: 'Scripts',
            iconUrl: 'assets/icons/script-icon.png'
        }
    );
