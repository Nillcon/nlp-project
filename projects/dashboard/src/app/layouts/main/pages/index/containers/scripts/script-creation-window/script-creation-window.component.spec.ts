import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ScriptCreationWindowComponent } from './script-creation-window.component';

describe('ScriptCreationWindowComponent', () => {
  let component: ScriptCreationWindowComponent;
  let fixture: ComponentFixture<ScriptCreationWindowComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ScriptCreationWindowComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ScriptCreationWindowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
