import { Component, OnInit, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { DynamicDialogRef } from 'primeng-lts/api';
import { FormGroupTypeSafe } from 'form-type-safe';
import { Script } from '@Dashboard/features/script/interfaces';

@Component({
    selector: 'app-script-creation-window',
    templateUrl: './script-creation-window.component.html',
    styleUrls: ['./script-creation-window.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class ScriptCreationWindowComponent implements OnInit {

    public formGroup: FormGroupTypeSafe<Script>;

    constructor (
        private readonly changeDetectorRef: ChangeDetectorRef,
        public ref: DynamicDialogRef
    ) { }

    public ngOnInit (): void {
    }

    public onFormGroupInit (formGroup: FormGroupTypeSafe<Script>): void {
        this.formGroup = formGroup;
    }

    public onCreateButtonClick (): void {
        if (this.formGroup.valid) {
            this.ref.close(this.formGroup.value);
        } else {
            this.formGroup.markAllAsTouched();
            this.changeDetectorRef.detectChanges();
        }
    }

    public onCloseButtonClick (): void {
        this.ref.close(false);
    }
}
