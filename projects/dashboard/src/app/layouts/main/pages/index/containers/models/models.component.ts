import { Component, OnInit, ChangeDetectionStrategy, OnDestroy } from '@angular/core';
import { NavigationService } from '@Shared/features';
import { MainLayoutPageEnum } from '@Dashboard/layouts/main/shared/main-layout-page.enum';
import { ViewModel, ModelStepIdentifierService, ModelFacade } from '@Dashboard/view-features';
import { AppLayoutEnum } from '@Dashboard/enums';
import { Observable, BehaviorSubject, Subscription } from 'rxjs';
import { filter, map, tap } from 'rxjs/operators';
import { ModelStateEnum } from '@Shared/enums';
import { ModelTypeEnum, Model } from '@Dashboard/features/model';
import { AutoUnsubscribe } from 'ngx-auto-unsubscribe';

@AutoUnsubscribe()
@Component({
    selector: 'app-models',
    templateUrl: './models.component.html',
    styleUrls: ['./models.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class ModelsComponent implements OnInit, OnDestroy {

    public get filteredModels$ (): Observable<ViewModel[]> {
        return this._filteredModels$.asObservable();
    }

    public modelsSubscription: Subscription;

    private readonly _filteredModels$ = new BehaviorSubject<ViewModel[]>(null);

    private modelArray: ViewModel[];

    private searchString: string = '';
    private filteredModelTypes: ModelTypeEnum[] = [];

    constructor (
        private readonly modelFacade: ModelFacade,
        private readonly navigationService: NavigationService,
        private readonly modelStepIdentifierService: ModelStepIdentifierService
    ) {}

    public ngOnInit (): void {
        this.initModels();
        this.modelFacade.updateModels();
    }

    public ngOnDestroy (): void {}

    public initModels (): void {
        this.modelsSubscription = this.modelFacade.getModels$()
            .pipe(
                filter((models) => models !== null),
                map((models) => models.map((currModel) => <ViewModel>{
                    ...currModel,
                    modelState: (currModel.id) ? ModelStateEnum.Created : ModelStateEnum.Emulated
                })),
                tap((viewModels) => {
                    this.modelArray = viewModels;

                    this.filterModels();
                })
            )
            .subscribe();
    }

    public onFilterSelectionChanged (filteredModelTypes: ModelTypeEnum[]): void {
        this.filteredModelTypes = filteredModelTypes;

        this.filterModels();
    }

    public onSearchFieldKeyUp (searchString: string = ''): void {
        this.searchString = searchString;

        this.filterModels();
    }

    public onCreateModelButtonClick (): void {
        this.navigationService.navigate({
            subUrl: `${AppLayoutEnum.Main}/${MainLayoutPageEnum.ModelCreation}`,
            relativeToCurrentPath: false
        });
    }

    public onModelCardClick (model: ViewModel): void {
        this.modelStepIdentifierService.openModel(model);
    }

    private filterModels (): void {
        const lowerSearchString = this.searchString.toLocaleLowerCase();
        const filteredModels    = this.modelArray
            .filter((currModel) => this.filterModelByType(currModel))
            .filter((currModel) => this.filterModelBySearchString(currModel, lowerSearchString));

        this._filteredModels$.next(filteredModels);
    }

    private filterModelByType (model: Model): boolean {
        if (this.filteredModelTypes.length) {
            return this.filteredModelTypes.includes(model.modelType);
        }

        return true;
    }

    private filterModelBySearchString (model: Model, searchString: string): boolean {
        return `${model.name}${model.description}`
            .toLocaleLowerCase()
            .includes(searchString);
    }

}
