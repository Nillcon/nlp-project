import { NgModule } from '@angular/core';
import { IndexRoutingModule } from './index.routing';
import { FeaturesModule } from '@Dashboard/features/features.module';
import { MainLayoutSharedModule } from '../../main-layout-shared.module';
import { SharedModule } from '@Dashboard/app-shared.module';
import { IndexPageComponent } from './index.component';
import { TabsComponent } from './components/tabs/tabs.component';

@NgModule({
    declarations: [
        IndexPageComponent,
        TabsComponent
    ],
    imports: [
        IndexRoutingModule,

        FeaturesModule,
        MainLayoutSharedModule,
    ]
})
export class IndexPageModule {}
