import { NgModule } from '@angular/core';
import { TemplatesComponent } from './templates.component';
import { TemplatesListComponent } from './templates-list/templates-list.component';
import { SharedModule } from '@Dashboard/app-shared.module';
import { FeaturesModule } from '@Dashboard/features/features.module';
import { TemplatesContainerRoutingModule } from './templates.routing';
import { MainLayoutSharedModule } from '@Dashboard/layouts/main/main-layout-shared.module';
import { SearchFieldComponent } from './search-field/search-field.component';
import { TemplateCardFooterComponent } from './template-card-footer/template-card-footer.component';
import { TemplateCardHeaderComponent } from './template-card-header/template-card-header.component';
import { FilterMultiselectComponent } from './filter-multiselect/filter-multiselect.component';

@NgModule({
    declarations: [
        TemplatesComponent,
        TemplatesListComponent,
        SearchFieldComponent,
        TemplateCardHeaderComponent,
        TemplateCardFooterComponent,
        FilterMultiselectComponent
    ],
    imports: [
        TemplatesContainerRoutingModule,

        FeaturesModule,
        MainLayoutSharedModule,
        SharedModule
    ]
})
export class TemplatesModule {}
