import { Component, OnInit, ChangeDetectionStrategy, EventEmitter, Output, Input, Injector } from '@angular/core';
import { Template } from '@Dashboard/features/template';
import { IsArray } from 'class-validator';
import { ClassValidation } from '@Shared/decorators';
import { TemplateFacade } from '@Dashboard/view-features/template';
import { NeedsConfirmation } from '@Shared/modules/confirmation-window';
import { ForbiddenClickService } from '@Shared/features';

@ClassValidation()
@Component({
    selector: 'app-templates-list',
    templateUrl: './templates-list.component.html',
    styleUrls: ['./templates-list.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class TemplatesListComponent implements OnInit {

    @Input()
    @IsArray()
    public templateArray: Template[];

    @Output()
    public OnTemplateCardClick = new EventEmitter<Template>();

    constructor (
        public readonly injector: Injector,
        private readonly templateFacade: TemplateFacade,
        private readonly forbiddenClickService: ForbiddenClickService
    ) {}

    public ngOnInit (): void {}

    public onTemplateCardClick (event: MouseEvent, template: Template): void {
        const isForbiddenClick = this.forbiddenClickService.checkIsMarkAsForbiddenClick(event);

        if (!isForbiddenClick) {
            this.OnTemplateCardClick.emit(template);
        }
    }

    public onMoveToModelsButtonClick (template: Template): void {
        this.templateFacade.markAsModel(template.id)
            .subscribe();
    }

    @NeedsConfirmation(
        `<strong class="text-danger">
            ⚠️Do you really want to delete this template?<br/>
            <i>You cannot restore this template in future</i>⚠️
        </strong>`
    )
    public onDeleteButtonClick (template: Template): void {
        this.templateFacade.deleteTemplate(template);
    }

}
