import { Component, OnInit, Output, EventEmitter, ChangeDetectionStrategy } from '@angular/core';

@Component({
    selector: 'app-create-model-button',
    templateUrl: './create-model-button.component.html',
    styleUrls: ['./create-model-button.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class CreateModelButtonComponent implements OnInit {

    @Output()
    public OnClick: EventEmitter<MouseEvent> = new EventEmitter();

    constructor () {}

    public ngOnInit (): void {}

}
