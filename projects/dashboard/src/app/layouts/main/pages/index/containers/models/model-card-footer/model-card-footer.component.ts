import { Component, OnInit, Input, ChangeDetectionStrategy } from '@angular/core';
import { IsObject } from 'class-validator';
import { ClassValidation } from '@Shared/decorators';
import { Model } from '@Dashboard/features/model';
import { ModelTypeDetailsMap } from '@Dashboard/view-features';

@ClassValidation()
@Component({
    selector: 'app-model-card-footer',
    templateUrl: './model-card-footer.component.html',
    styleUrls: ['./model-card-footer.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class ModelCardFooterComponent implements OnInit {

    @Input()
    @IsObject()
    public model: Model;

    constructor () {}

    public ngOnInit (): void {}

    public getFillingProgressPercent (): number {
        const modelDetails = ModelTypeDetailsMap.get(this.model.modelType);
        const percent = ((this.model.stepIndex + 1) / modelDetails.settingsSteps.length) * 100;

        return +percent.toFixed(0);
    }

}
