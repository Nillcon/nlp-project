import { Component, OnInit, Output, EventEmitter, ChangeDetectionStrategy } from '@angular/core';
import { ScriptService } from '@Dashboard/features/script/services';
import { Observable } from 'rxjs';

@Component({
    selector: 'app-create-script-button',
    templateUrl: './create-script-button.component.html',
    styleUrls: ['./create-script-button.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class CreateScriptButtonComponent implements OnInit {

    @Output()
    public OnClick: EventEmitter<MouseEvent> = new EventEmitter();

    constructor () {}

    public ngOnInit (): void {}

    public buttonOnClick (event: MouseEvent): void {
        this.OnClick.emit(event);
    }

}
