import { Component, OnInit, ChangeDetectionStrategy, Output, EventEmitter } from '@angular/core';
import { SelectItem } from 'primeng-lts/api';
import { ModelTypeMap, ModelTypeEnum, ModelType } from '@Dashboard/features/model';

@Component({
    selector: 'app-filter-multiselect',
    templateUrl: './filter-multiselect.component.html',
    styleUrls: ['./filter-multiselect.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class FilterMultiselectComponent implements OnInit {

    @Output()
    public OnSelectionChanged = new EventEmitter<ModelTypeEnum[]>();

    public filteringItems: SelectItem[];
    public selectedItems: ModelTypeEnum[] = [];

    constructor () {}

    public ngOnInit (): void {
        this.initFilteringItems();
    }

    public getModelType (modelTypeId: ModelTypeEnum): ModelType {
        return ModelTypeMap.get(modelTypeId);
    }

    private initFilteringItems (): void {
        let items: SelectItem[] = [];
        const modelTypes = [...ModelTypeMap.values()];

        items = modelTypes.map((modelType) => <SelectItem>{
            label: modelType.name,
            value: modelType.id,
            icon: modelType.iconUrl,
        });

        this.filteringItems = items;
    }

}
