export enum IndexContainerEnum {
    Models      = 'Models',
    Templates   = 'Templates',
    Scripts     = 'Scripts'
}
