import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { IndexContainerEnum } from './enums/index-container.enum';
import { IndexPageComponent } from './index.component';

const routes: Routes = [
    {
        path: '',
        component: IndexPageComponent,
        children: [
            {
                path: IndexContainerEnum.Models,
                loadChildren: () => import('./containers/models/models.module').then((m) => m.ModelsModule)
            },
            {
                path: IndexContainerEnum.Templates,
                loadChildren: () => import('./containers/templates/templates.module').then((m) => m.TemplatesModule)
            },
            {
                path: IndexContainerEnum.Scripts,
                loadChildren: () => import('./containers/scripts').then((m) => m.ScriptsModule)
            },
            {
                path: '**',
                redirectTo: IndexContainerEnum.Models
            }
        ]
    },
    {
        path: '**',
        redirectTo: ''
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(routes)
    ],
    exports: [
        RouterModule
    ]
})
export class IndexRoutingModule {}
