import { Component, OnInit, ChangeDetectionStrategy, OnDestroy } from '@angular/core';
import { TemplateFacade } from '@Dashboard/view-features/template';
import { BehaviorSubject, Observable, Subscription } from 'rxjs';
import { Template } from '@Dashboard/features/template';
import { tap, filter } from 'rxjs/operators';
import { ModelTypeEnum } from '@Dashboard/features/model';
import { ModelStepIdentifierService } from '@Dashboard/view-features';
import { AutoUnsubscribe } from 'ngx-auto-unsubscribe';

@AutoUnsubscribe()
@Component({
    selector: 'app-templates',
    templateUrl: './templates.component.html',
    styleUrls: ['./templates.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class TemplatesComponent implements OnInit, OnDestroy {

    public get filteredTemplates$ (): Observable<Template[]> {
        return this._filteredTemplates$.asObservable();
    }

    public templatesSubscription: Subscription;

    private readonly _filteredTemplates$ = new BehaviorSubject<Template[]>(null);

    private searchString: string = '';
    private filteredTemplateTypes: ModelTypeEnum[] = [];

    constructor (
        private readonly templateFacade: TemplateFacade,
        private readonly modelStepIdentifierService: ModelStepIdentifierService
    ) {}

    public ngOnInit (): void {
        this.templateFacade.updateTemplatesIfNeeded();
        this.initTemplates();
    }

    public ngOnDestroy (): void {}

    public initTemplates (): void {
        this.templatesSubscription = this.templateFacade.getTemplates$()
            .pipe(
                filter((templates) => templates !== null),
                tap(() => this.onSearchFieldKeyUp())
            )
            .subscribe();
    }

    public onFilterSelectionChanged (filteredTemplateTypes: ModelTypeEnum[]): void {
        this.filteredTemplateTypes = filteredTemplateTypes;

        this.filterTemplates();
    }

    public onSearchFieldKeyUp (searchString: string = ''): void {
        this.searchString = searchString;

        this.filterTemplates();
    }

    public onTemplateCardClick (template: Template): void {
        this.modelStepIdentifierService.openModelPredictStep(template);
    }

    private filterTemplates (): void {
        const lowerSearchString    = this.searchString.toLocaleLowerCase();
        const filteredTemplates    = this.templateFacade.getTemplates()
            .filter((currTemplate) => this.filterTemplateByType(currTemplate))
            .filter((currTemplate) => this.filterTemplateBySearchString(currTemplate, lowerSearchString));

        this._filteredTemplates$.next(filteredTemplates);
    }

    private filterTemplateByType (template: Template): boolean {
        if (this.filteredTemplateTypes.length) {
            return this.filteredTemplateTypes.includes(template.modelType);
        }

        return true;
    }

    private filterTemplateBySearchString (template: Template, searchString: string): boolean {
        return `${template.name}${template.description}`
            .toLocaleLowerCase()
            .includes(searchString);
    }

}
