import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ScriptCardHeaderComponent } from './script-card-header.component';

describe('ScriptCardHeaderComponent', () => {
  let component: ScriptCardHeaderComponent;
  let fixture: ComponentFixture<ScriptCardHeaderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ScriptCardHeaderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ScriptCardHeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
