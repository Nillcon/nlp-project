import { Component, OnInit, Output, EventEmitter, Input, ViewChild, ChangeDetectionStrategy } from '@angular/core';
import { MenuItem } from 'primeng-lts/api';
import { IsEnum } from 'class-validator';
import { ScriptStateEnum } from '../../../enums/script-state.enum';
import { Menu } from 'primeng-lts/menu';

@Component({
    selector: 'app-script-card-header',
    templateUrl: './script-card-header.component.html',
    styleUrls: ['./script-card-header.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class ScriptCardHeaderComponent implements OnInit {

    @Input()
    @IsEnum(ScriptStateEnum)
    public scriptState: ScriptStateEnum = ScriptStateEnum.Created;

    @Output()
    public OnEditButtonClick = new EventEmitter<MouseEvent>();

    @Output()
    public OnDeleteButtonClick = new EventEmitter<MouseEvent>();

    public actionsMenuItems: MenuItem[];

    public ScriptStateEnum = ScriptStateEnum;

    @ViewChild('menu', { static: false })
    private readonly menuComponent: Menu;

    constructor () { }

    public ngOnInit (): void {
        this.initMenuItems();
    }

    public onMenuButtonClick (event: MouseEvent): void {
        this.menuComponent.toggle(event);
    }

    private initMenuItems (): void {
        this.actionsMenuItems = [
            {
                label: 'Edit',
                icon: 'fas fa-edit',
                command: (event) => this.OnEditButtonClick.emit(event.originalEvent)
            },
            {
                label: 'Delete',
                icon: 'far fa-trash-alt text-danger',
                command: (event) => this.OnDeleteButtonClick.emit(event.originalEvent)
            }
        ];
    }

}
