import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng-lts/api';
import { FormGroupTypeSafe } from 'form-type-safe';
import { Script } from '@Dashboard/features/script/interfaces';

@Component({
  selector: 'app-script-editing-window',
  templateUrl: './script-editing-window.component.html',
  styleUrls: ['./script-editing-window.component.scss']
})
export class ScriptEditingWindowComponent implements OnInit {

    public inputData: Script;

    public formGroup: FormGroupTypeSafe<Script>;

    constructor (
        public ref: DynamicDialogRef,
        private readonly config: DynamicDialogConfig,
        private readonly changeDetectorRef: ChangeDetectorRef,
    ) {
        this.inputData = this.config.data;
    }

    public ngOnInit (): void {
    }

    public onFormGroupInit (formGroup: FormGroupTypeSafe<Script>): void {
        this.formGroup = formGroup;
    }

    public onSaveButtonClick (): void {
        if (this.formGroup.valid) {
            this.ref.close({
                ...this.formGroup.value,
                id: this.inputData.id
            });
        } else {
            this.formGroup.markAllAsTouched();
            this.changeDetectorRef.detectChanges();
        }
    }

    public onCloseButtonClick (): void {
        this.ref.close(false);
    }

}
