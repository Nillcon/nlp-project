import { Component, OnInit, ChangeDetectionStrategy, EventEmitter, Output, Injector, Input } from '@angular/core';
import { Model } from '@Dashboard/features/model';
import { ModelFacade, ViewModel } from '@Dashboard/view-features';
import { Observable } from 'rxjs';
import { filter, tap } from 'rxjs/operators';
import { NeedsConfirmation } from '@Shared/modules/confirmation-window';
import { ModelRenamingWindowComponent } from '../model-renaming-window/model-renaming-window.component';
import { DialogService } from 'primeng-lts/api';
import { IsArray } from 'class-validator';
import { ClassValidation } from '@Shared/decorators';
import { ForbiddenClickService } from '@Shared/features';

@ClassValidation()
@Component({
    selector: 'app-models-list',
    templateUrl: './models-list.component.html',
    styleUrls: ['./models-list.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class ModelsListComponent implements OnInit {

    @Input()
    @IsArray()
    public modelArray: ViewModel[];

    @Output()
    public OnModelCardClick = new EventEmitter<ViewModel>();

    constructor (
        public readonly injector: Injector,
        private readonly modelFacade: ModelFacade,
        private readonly dialog: DialogService,
        private readonly forbiddenClickService: ForbiddenClickService
    ) {}

    public ngOnInit (): void {}

    public onModelCardClick (event: MouseEvent, model: ViewModel): void {
        const isClickForbidden = this.forbiddenClickService.checkIsMarkAsForbiddenClick(event);

        if (!isClickForbidden) {
            this.OnModelCardClick.emit(model);
        }
    }

    public onMoveToTemplatesButtonClick (model: Model): void {
        this.modelFacade.markAsTemplate(model.id)
            .pipe(
                tap(() => this.modelFacade.updateModels())
            )
            .subscribe();
    }

    public onModelRenameButtonClick (model: Model): void {
        this.openModelRenamingWindow(model.id)
            .pipe(
                filter((result) => result === true),
                tap(() => this.modelFacade.updateModels())
            )
            .subscribe();
    }

    @NeedsConfirmation(
        `<strong class="text-danger">
            ⚠️Do you really want to delete this model?<br/>
            <i>You cannot restore this model in future</i>⚠️
        </strong>`
    )
    public deleteModel (model: Model): void {
        this.modelFacade.deleteModel(model);
    }

    public openModelRenamingWindow (modelId: number): Observable<boolean> {
        return this.dialog.open(ModelRenamingWindowComponent, {
            width: '300px',
            header: 'Rename model',
            data: modelId
        }).onClose;
    }

    public trackByFunction (index: number, item: Model): number {
        return (item) ? item.id : null;
    }

}
