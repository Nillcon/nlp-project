import { Component, OnInit, Input, ChangeDetectionStrategy, Output, EventEmitter } from '@angular/core';
import { ScriptStateEnum, ScriptView } from '@Dashboard/view-features/script';
import { Script } from '@Dashboard/features/script/interfaces';
import { ForbiddenClickService } from '@Shared/features';

@Component({
    selector: 'app-script-list',
    templateUrl: './script-list.component.html',
    styleUrls: ['./script-list.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class ScriptListComponent implements OnInit {

    @Input()
    public scripts: ScriptView[];

    @Output()
    public readonly OnScriptCardClick: EventEmitter<Script> = new EventEmitter();

    @Output()
    public readonly OnScriptEditButtonClick: EventEmitter<Script> = new EventEmitter();

    @Output()
    public readonly OnScriptDeleteButtonClick: EventEmitter<Script> = new EventEmitter();

    public ScriptStateEnum = ScriptStateEnum;

    constructor (
        private readonly forbiddenClickService: ForbiddenClickService
    ) {}

    public ngOnInit (): void {}

    public onCardClick (event: MouseEvent, script: Script): void {
        const isForbiddenClick = this.forbiddenClickService.checkIsMarkAsForbiddenClick(event);

        if (!isForbiddenClick) {
            this.OnScriptCardClick.emit(script);
        }
    }

    public onEditButtonClick (script: Script): void {
        this.OnScriptEditButtonClick.emit(script);
    }

    public onDeleteButtonClick (script: Script): void {
        this.OnScriptDeleteButtonClick.emit(script);
    }

}
