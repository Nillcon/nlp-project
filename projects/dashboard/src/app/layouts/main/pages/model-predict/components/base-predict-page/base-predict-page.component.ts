import { Component, OnInit, ChangeDetectionStrategy, Input, ChangeDetectorRef } from '@angular/core';
import { BaseModelPredictPage } from '../../classes/base-model-predict-page';
import { Subscription } from 'rxjs';

@Component({
    selector: 'app-base-predict-page',
    templateUrl: './base-predict-page.component.html',
    styleUrls: ['./base-predict-page.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class BasePredictPageComponent implements OnInit {

    @Input()
    public data: BaseModelPredictPage<any, any>;

    public analyzeSubscription: Subscription;

    constructor (
        private readonly changeDetector: ChangeDetectorRef
    ) {}

    public ngOnInit (): void {}

    public onAnalyzeButtonClick (): void {
        if (this.data.analyzeCallback) {
            this.analyzeSubscription = this.data.analyzeCallback()
                .subscribe(() => this.changeDetector.markForCheck());
        }
    }

    public onEditButtonClick (): void {
        if (this.data.editCallback) {
            this.data.editCallback();
        }
    }

}
