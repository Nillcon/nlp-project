import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';

@Component({
    selector: 'app-ner',
    templateUrl: './ner.component.html',
    styleUrls: ['./ner.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class NerComponent implements OnInit {

    constructor () {}

    public ngOnInit (): void {}

}
