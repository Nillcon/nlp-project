import { OnInit, Injector, Type, ChangeDetectorRef } from '@angular/core';
import { UrlService } from '@Shared/features';
import { ModelSettingsQueryParamEnum as QueryParamEnum } from '../../model-settings/enums/query-params.enum';
import { Observable, of } from 'rxjs';
import { Model } from '@Dashboard/features/model';
import { ModelFacade, TemplateFacade } from '@Dashboard/view-features';

export class BaseModelPredictPage<I, O> implements OnInit {

    public modelId: number;
    public isTemplate: boolean;

    public inputData: I;
    public outputData: O;

    public openedModel$: Observable<Model>;

    public isPageInitWithError: boolean = false;

    protected readonly urlService: UrlService;
    protected readonly modelFacade: ModelFacade;
    protected readonly templateFacade: TemplateFacade;
    protected readonly changeDetector: ChangeDetectorRef;

    constructor (
        readonly injector: Injector
    ) {
        this.urlService     = injector.get(UrlService as Type<UrlService>);
        this.modelFacade    = injector.get(ModelFacade as Type<ModelFacade>);
        this.templateFacade = injector.get(TemplateFacade as Type<TemplateFacade>);
        this.changeDetector = injector.get(ChangeDetectorRef as Type<ChangeDetectorRef>);
    }

    public ngOnInit (): void {
        this.initModelId();
        this.initIsTemplate();

        this.validateDependQueryParam(QueryParamEnum.modelId, this.modelId);
        this.validateDependQueryParam(QueryParamEnum.isTemplate, this.isTemplate);

        this.initOpenedModel();
    }

    public analyzeCallback = (): Observable<any> => {
        return of(null);
    }

    public editCallback = (): any => {
        this.outputData = null;
    }

    private initOpenedModel (): void {
        this.openedModel$ = this.getModelInfo();
    }

    private getModelInfo (): Observable<Model> {
        if (this.isTemplate) {
            return this.templateFacade.updateTemplate(this.modelId);
        } else {
            return this.modelFacade.updateModel(this.modelId);
        }
    }

    private initModelId (): void {
        const modelIdQueryParam = this.urlService.getParameter(QueryParamEnum.modelId);

        if (modelIdQueryParam !== undefined) {
            this.modelId = +modelIdQueryParam;
        }
    }

    private initIsTemplate (): void {
        const isTemplateQueryParam = this.urlService.getParameter(QueryParamEnum.isTemplate);

        if (isTemplateQueryParam !== undefined) {
            this.isTemplate = !!+isTemplateQueryParam;
        }
    }

    private validateDependQueryParam (paramName: QueryParamEnum, paramValue: any): void {
        if (paramValue === undefined) {
            this.isPageInitWithError = true;

            throw new Error(`Page component could not init ${paramName}!`);
        }
    }

}
