import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';

@Component({
    selector: 'app-prediction',
    templateUrl: './prediction.component.html',
    styleUrls: ['./prediction.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class PredictionComponent implements OnInit {

    constructor () {}

    public ngOnInit (): void {}

}
