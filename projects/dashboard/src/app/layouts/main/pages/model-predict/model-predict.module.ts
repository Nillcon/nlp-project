import { NgModule } from '@angular/core';
import { ModelPredictComponent } from './model-predict.component';
import { ModelPredictRoutingModule } from './model-predict.routing';
import { ModelSharedModule } from './model-predict-shared.module';
import { MainLayoutSharedModule } from '../../main-layout-shared.module';

@NgModule({
    declarations: [
        ModelPredictComponent
    ],
    imports: [
        ModelPredictRoutingModule,
        ModelSharedModule,
        MainLayoutSharedModule
    ]
})
export class ModelPredictModule {}
