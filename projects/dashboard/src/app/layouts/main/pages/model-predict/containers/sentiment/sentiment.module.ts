import { NgModule } from '@angular/core';
import { SentimentComponent } from './sentiment.component';
import { SentimentRoutingModule } from './sentiment.routing';
import { ModelSharedModule } from '../../model-predict-shared.module';

@NgModule({
    declarations: [
        SentimentComponent
    ],
    imports: [
        SentimentRoutingModule,
        ModelSharedModule
    ]
})
export class SentimentModule {}
