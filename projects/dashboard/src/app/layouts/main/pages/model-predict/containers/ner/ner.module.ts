import { NgModule } from '@angular/core';
import { NerComponent } from './ner.component';
import { NerRoutingModule } from './ner.routing';
import { ModelSharedModule } from '../../model-predict-shared.module';

@NgModule({
    declarations: [
        NerComponent
    ],
    imports: [
        NerRoutingModule,
        ModelSharedModule
    ]
})
export class NerModule {}
