import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TopicAnalyzerComponent } from './topic-analyzer.component';

const routes: Routes = [
    {
        path: '',
        component: TopicAnalyzerComponent
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(routes)
    ],
    exports: [
        RouterModule
    ]
})
export class TopicAnalyzerRoutingModule {}
