import { Component, OnInit, ChangeDetectionStrategy, Input } from '@angular/core';
import { Model } from '@Dashboard/features/model';
import { ModelStepIdentifierService } from '@Dashboard/view-features';

@Component({
    selector: 'app-model-title',
    templateUrl: './title.component.html',
    styleUrls: ['./title.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class ModelTitleComponent implements OnInit {

    @Input()
    public readonly model: Model;

    public readonly modelIconLink    = 'assets/icons/model-icon.png';
    public readonly templateIconLink = 'assets/icons/template-icon.png';

    constructor (
        private readonly modelStepIdentifierService: ModelStepIdentifierService
    ) {}

    public ngOnInit (): void {}

    public onSettingsButtonClick (): void {
        this.modelStepIdentifierService.openModelSettingsOnLastStep(this.model);
    }

}
