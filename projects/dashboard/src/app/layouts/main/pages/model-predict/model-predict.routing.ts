import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ModelPredictComponent } from './model-predict.component';
import { ModelContainerEnum } from './enums/model-container.enum';

const routes: Routes = [
    {
        path: '',
        component: ModelPredictComponent,
        children: [
            {
                path: ModelContainerEnum.TopicAnalyzer,
                loadChildren: () => import('./containers/topic-analyzer/topic-analyzer.module').then((m) => m.TopicAnalyzerModule)
            },
            {
                path: ModelContainerEnum.Sentiment,
                loadChildren: () => import('./containers/sentiment/sentiment.module').then((m) => m.SentimentModule)
            },
            {
                path: ModelContainerEnum.NER,
                loadChildren: () => import('./containers/ner/ner.module').then((m) => m.NerModule)
            },
            {
                path: ModelContainerEnum.Prediction,
                loadChildren: () => import('./containers/prediction/prediction.module').then((m) => m.PredictionModule)
            },
            {
                path: '**',
                redirectTo: ModelContainerEnum.TopicAnalyzer
            }
        ]
    },
    {
        path: '**',
        redirectTo: ''
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(routes)
    ],
    exports: [
        RouterModule
    ]
})
export class ModelPredictRoutingModule {}
