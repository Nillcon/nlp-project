import { NgModule } from '@angular/core';
import { TopicAnalyzerComponent } from './topic-analyzer.component';
import { TopicAnalyzerRoutingModule } from './topic-analyzer.routing';
import { ModelSharedModule } from '../../model-predict-shared.module';

@NgModule({
    declarations: [
        TopicAnalyzerComponent
    ],
    imports: [
        TopicAnalyzerRoutingModule,
        ModelSharedModule
    ]
})
export class TopicAnalyzerModule {}
