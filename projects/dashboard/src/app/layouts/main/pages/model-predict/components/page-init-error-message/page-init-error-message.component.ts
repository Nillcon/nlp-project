import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';

@Component({
    selector: 'app-page-init-error-message',
    templateUrl: './page-init-error-message.component.html',
    styleUrls: ['./page-init-error-message.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class PageInitErrorMessageComponent implements OnInit {

    constructor () {}

    public ngOnInit (): void {}

}
