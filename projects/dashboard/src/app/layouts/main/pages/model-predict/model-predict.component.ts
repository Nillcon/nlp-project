import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { trigger, transition, useAnimation } from '@angular/animations';
import { fadeIn } from 'ng-animate';

@Component({
    selector: 'app-model',
    templateUrl: './model-predict.component.html',
    styleUrls: ['./model-predict.component.scss'],
    animations: [
        trigger('wrapperAnimations', [
            transition(':enter', useAnimation(fadeIn, { params: { timing: 0.15 } }))
        ])
    ],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class ModelPredictComponent implements OnInit {

    constructor () {}

    public ngOnInit (): void {}

}
