export enum ModelContainerEnum {
    TopicAnalyzer   = 'TopicAnalyzer',
    Sentiment       = 'Sentiment',
    NER             = 'NER',
    Prediction      = 'Prediction'
}
