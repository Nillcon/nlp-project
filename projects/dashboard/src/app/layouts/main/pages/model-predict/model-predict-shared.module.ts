import { NgModule } from '@angular/core';
import { SharedModule } from '@Dashboard/app-shared.module';
import { ModelTitleComponent } from './components/title/title.component';
import { MainLayoutSharedModule } from '../../main-layout-shared.module';
import { ActionButtonsComponent } from './components/action-buttons/action-buttons.component';
import { FeaturesModule } from '@Dashboard/features/features.module';
import { NoDataBlockComponent } from './components/no-data-block/no-data-block.component';
import { BasePredictPageComponent } from './components/base-predict-page/base-predict-page.component';
import { PageInitErrorMessageComponent } from './components/page-init-error-message/page-init-error-message.component';

@NgModule({
    declarations: [
        BasePredictPageComponent,
        ModelTitleComponent,
        ActionButtonsComponent,
        NoDataBlockComponent,
        PageInitErrorMessageComponent
    ],
    imports: [
        SharedModule,
        FeaturesModule,
        MainLayoutSharedModule
    ],
    exports: [
        SharedModule,
        FeaturesModule,

        BasePredictPageComponent,
        ModelTitleComponent,
        ActionButtonsComponent,
        NoDataBlockComponent,
        PageInitErrorMessageComponent
    ]
})
export class ModelSharedModule {}
