import { Component, OnInit, ChangeDetectionStrategy, ViewChild, ElementRef, AfterViewInit, Injector } from '@angular/core';
import { Observable } from 'rxjs';
import { ModelSentimentAnalysisData } from '@Dashboard/features/model';
import { BaseModelPredictPage } from '../../classes/base-model-predict-page';
import { tap } from 'rxjs/operators';

@Component({
    selector: 'app-sentiment',
    templateUrl: './sentiment.component.html',
    styleUrls: ['./sentiment.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class SentimentComponent extends BaseModelPredictPage<string, ModelSentimentAnalysisData> implements OnInit, AfterViewInit {

    @ViewChild('RawTextInput', { static: false })
    private readonly _rawTextInputElementRef: ElementRef<HTMLInputElement>;

    constructor (
        readonly injector: Injector
    ) {
        super(injector);
    }

    public ngOnInit (): void {
        super.ngOnInit();
    }

    public ngAfterViewInit (): void {
        this._rawTextInputElementRef?.nativeElement?.focus();
    }

    public analyzeCallback = (): Observable<any> => {
        const enteredRawText = this._rawTextInputElementRef.nativeElement.value;

        return this.analyzeData(enteredRawText)
            .pipe(
                tap((data) => {
                    this.outputData = data;

                    this.changeDetector.markForCheck();
                })
            );
    }

    private analyzeData (text: string): Observable<ModelSentimentAnalysisData> {
        if (this.isTemplate) {
            return this.templateFacade.analyzeSentiment(this.modelId, text);
        } else {
            return this.modelFacade.analyzeSentiment(this.modelId, text);
        }
    }

}
