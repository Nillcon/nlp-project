import { Component, OnInit, ChangeDetectionStrategy, Output, EventEmitter, Input } from '@angular/core';
import { Subscription } from 'rxjs';

@Component({
    selector: 'app-action-buttons',
    templateUrl: './action-buttons.component.html',
    styleUrls: ['./action-buttons.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class ActionButtonsComponent implements OnInit {

    @Input()
    public analyzeButtonSubscription: Subscription;

    @Input()
    public isAnalyzeButtonVisible: boolean = true;

    @Input()
    public isEditButtonVisible: boolean = true;

    @Output()
    public OnAnalyzeButtonClick = new EventEmitter<MouseEvent>();

    @Output()
    public OnEditButtonClick = new EventEmitter<MouseEvent>();

    constructor () {}

    public ngOnInit (): void {}

}
