import { Component, OnInit, ChangeDetectionStrategy, ViewChild, ElementRef, AfterViewInit, Injector } from '@angular/core';
import { Observable } from 'rxjs';
import { ModelTopicAnalyzerAnalysisData } from '@Dashboard/features/model';
import { BaseModelPredictPage } from '../../classes/base-model-predict-page';
import { tap } from 'rxjs/operators';

@Component({
    selector: 'app-topic-analyzer',
    templateUrl: './topic-analyzer.component.html',
    styleUrls: ['./topic-analyzer.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class TopicAnalyzerComponent extends BaseModelPredictPage<string, ModelTopicAnalyzerAnalysisData[]> implements OnInit, AfterViewInit {

    @ViewChild('RawTextInput', { static: false })
    private readonly _rawTextInputElementRef: ElementRef<HTMLInputElement>;

    constructor (
        readonly injector: Injector
    ) {
        super(injector);
    }

    public ngOnInit (): void {
        super.ngOnInit();
    }

    public ngAfterViewInit (): void {
        this._rawTextInputElementRef?.nativeElement?.focus();
    }

    public analyzeCallback = (): Observable<any> => {
        const enteredRawText: string = this._rawTextInputElementRef.nativeElement.value;

        return this.analyzeData(enteredRawText)
            .pipe(
                tap((data) => {
                    this.outputData = data;

                    this.changeDetector.markForCheck();
                })
            );
    }

    private analyzeData (text: string): Observable<ModelTopicAnalyzerAnalysisData[]> {
        if (this.isTemplate) {
            return this.templateFacade.analyzeTopicAnalyzer(this.modelId, text);
        } else {
            return this.modelFacade.analyzeTopicAnalyzer(this.modelId, text);
        }
    }

}
