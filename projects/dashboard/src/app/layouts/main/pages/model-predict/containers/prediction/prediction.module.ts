import { NgModule } from '@angular/core';
import { PredictionComponent } from './prediction.component';
import { PredictionRoutingModule } from './prediction.routing';
import { ModelSharedModule } from '../../model-predict-shared.module';

@NgModule({
    declarations: [
        PredictionComponent
    ],
    imports: [
        PredictionRoutingModule,
        ModelSharedModule
    ]
})
export class PredictionModule {}
