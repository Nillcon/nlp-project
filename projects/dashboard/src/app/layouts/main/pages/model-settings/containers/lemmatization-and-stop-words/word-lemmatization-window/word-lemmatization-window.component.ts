import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { DynamicDialogRef, DynamicDialogConfig } from 'primeng-lts/api';
import { IsString, IsArray } from 'class-validator';
import { ClassValidation } from '@Shared/decorators';

@ClassValidation()
@Component({
    selector: 'app-word-lemmatization-window',
    templateUrl: './word-lemmatization-window.component.html',
    styleUrls: ['./word-lemmatization-window.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class WordLemmatizationWindowComponent implements OnInit {

    @IsString()
    public enteredLemma: string = '';

    @IsArray()
    public targetWord: string;

    constructor (
        private readonly dialogRef: DynamicDialogRef,
        private readonly config: DynamicDialogConfig
    ) {
        this.targetWord = this.config.data;
    }

    public ngOnInit (): void {}

    public onInputValueChanged (value: string): void {
        this.enteredLemma = value;
    }

    public onSaveButtonClick (): void {
        this.dialogRef.close(this.enteredLemma);
    }

    public onCancelButtonClick (): void {
        this.dialogRef.close(null);
    }

}
