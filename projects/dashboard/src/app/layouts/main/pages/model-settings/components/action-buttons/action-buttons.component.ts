import { Component, OnInit, ChangeDetectionStrategy, Input, ChangeDetectorRef } from '@angular/core';
import { IsArray } from 'class-validator';
import { ClassValidation } from '@Shared/decorators';
import { ModelSettingsActionButton } from '@Dashboard/view-features';
import { Observable, throwError, Subscription } from 'rxjs';
import { tap, catchError } from 'rxjs/operators';

@ClassValidation()
@Component({
    selector: 'app-action-buttons',
    templateUrl: './action-buttons.component.html',
    styleUrls: ['./action-buttons.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class ActionButtonsComponent implements OnInit {

    @Input()
    @IsArray()
    public actionButtons: ModelSettingsActionButton[];

    @Input()
    public trainingButtonCallback: () => Observable<boolean>;

    @Input()
    public continueButtonCallback: () => Observable<boolean>;

    private _buttonClickStateMap: Map<number, boolean>;
    private _buttonLoadingStateMap: Map<number, Subscription>;

    constructor (
        private readonly changeDetector: ChangeDetectorRef
    ) {}

    public ngOnInit (): void {
        this.initButtonClickStateMap();
        this.initButtonLoadingStateMap();
    }

    public onButtonClick (event: MouseEvent, actionButton: ModelSettingsActionButton): void {
        this.validateIsButtonCallbackIsset(actionButton);

        const loadingState = this[actionButton.callbackName]()
            .pipe(
                tap((callbackResult) => {
                    this._buttonClickStateMap.set(actionButton.id, !!callbackResult);

                    this.changeDetector.markForCheck();
                }),
                catchError((error) => {
                    this._buttonClickStateMap.set(actionButton.id, false);

                    this.changeDetector.markForCheck();

                    return throwError(error);
                })
            )
            .subscribe();

        this._buttonLoadingStateMap.set(actionButton.id, loadingState);

        this.changeDetector.markForCheck();
    }

    public getActionButtonRenderState (actionButton: ModelSettingsActionButton): boolean {
        return actionButton.dependentButtonIds
            .every((buttonId) => this._buttonClickStateMap.get(buttonId));
    }

    public getActionButtonLoadingState (actionButton: ModelSettingsActionButton): Subscription {
        return this._buttonLoadingStateMap.get(actionButton.id);
    }

    private initButtonClickStateMap (): void {
        this._buttonClickStateMap = new Map();

        this.actionButtons
            .forEach((button) => this._buttonClickStateMap.set(button.id, false));
    }

    private initButtonLoadingStateMap (): void {
        this._buttonLoadingStateMap = new Map();

        this.actionButtons
            .forEach((button) => this._buttonLoadingStateMap.set(button.id, null));
    }

    private validateIsButtonCallbackIsset (actionButton: ModelSettingsActionButton): void {
        if (!this[actionButton.callbackName]) {
            throw new Error(`Action button can't get the callback function of current step!`);
        }
    }

}
