import { Component, OnInit, ChangeDetectionStrategy, ChangeDetectorRef, ViewChild } from '@angular/core';
import { DynamicDialogRef, DynamicDialogConfig, MenuItem } from 'primeng-lts/api';
import { IsObject } from 'class-validator';
import { ModelWord, DatasetExtractorToken, PartOfSpeechMap, PartOfSpeechEnum } from '@Dashboard/features/dataset';
import { timer } from 'rxjs';
import { mapTo } from 'rxjs/operators';
import { Tag } from '@Dashboard/features/tag';
import { Menu } from 'primeng-lts/menu';

@Component({
    selector: 'app-word-part-of-speech-window',
    templateUrl: './word-part-of-speech-window.component.html',
    styleUrls: ['./word-part-of-speech-window.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class WordPartOfSpeechWindowComponent implements OnInit {

    @IsObject()
    public readonly word: ModelWord;

    public tags: Tag[];

    public partOfSpeechMenuItems: MenuItem[];

    public contexts: DatasetExtractorToken[][];

    private _selectedContextIndex: number;
    private _selectedTokenIndex: number;

    @ViewChild(Menu, { static: true })
    private readonly _menuComponent: Menu;

    constructor (
        private readonly changeDetector: ChangeDetectorRef,
        private readonly dialogRef: DynamicDialogRef,
        private readonly config: DynamicDialogConfig
    ) {
        this.word = this.config.data;
    }

    public ngOnInit (): void {
        this.initPartOfSpeechTags();
        this.initPartOfSpeechMenuItems();
        this.updateContexts();
    }

    public updateContexts (): void {
        timer(500)
            .pipe(
                mapTo(<DatasetExtractorToken[][]>[
                    [
                        { value: 'Sam ', tagId: null },
                        { value: 'said', tagId: PartOfSpeechEnum.Adjective },
                        { value: 'the', tagId: null },
                        { value: 'car', tagId: null },
                        { value: 'had', tagId: null },
                        { value: 'some', tagId: null },
                        { value: 'sentimental', tagId: null },
                        { value: 'value,', tagId: null },
                        { value: 'said', tagId: PartOfSpeechEnum.Verb },
                        { value: 'it', tagId: null },
                        { value: 'belonged', tagId: null },
                        { value: 'to', tagId: null },
                        { value: 'your', tagId: null },
                        { value: 'dad.', tagId: null }
                    ],
                    [
                        { value: 'You', tagId: null },
                        { value: 'said', tagId: PartOfSpeechEnum.Noun },
                        { value: 'she', tagId: null },
                        { value: 'really', tagId: null },
                        { value: 'helped', tagId: null },
                        { value: 'last', tagId: null },
                        { value: 'time.', tagId: null },
                    ]
                ])
            )
            .subscribe((data) => {
                this.contexts = data;

                this.changeDetector.markForCheck();
            });
    }

    public initPartOfSpeechTags (): void {
        this.tags = [...PartOfSpeechMap.values()].map((partOfSpeech) => {
            return <Tag>{
                id: partOfSpeech.id,
                name: partOfSpeech.name,
                color: partOfSpeech.color
            };
        });
    }

    public initPartOfSpeechMenuItems (): void {
        this.partOfSpeechMenuItems = [...PartOfSpeechMap.values()].map((partOfSpeech) => {
            return <MenuItem>{
                label: partOfSpeech.name,
                style: { fontWeight: `bold` },
                command: () => {
                    this.onPartOfSpeechMenuItemClick(
                        this._selectedContextIndex,
                        this._selectedTokenIndex,
                        partOfSpeech.id
                    );
                }
            };
        });
    }

    public openPartOfSpeechMenu (event: MouseEvent): void {
        this._menuComponent.toggle(event);
    }

    public changeTokenTagId (contextIndex: number, tokenIndex: number, tagId: number): void {
        const targetContext = [...this.contexts[contextIndex]];
        targetContext[tokenIndex].tagId = tagId;

        this.contexts[contextIndex] = targetContext;
    }

    public onTagClick (event: MouseEvent, contextIndex: number, tokenIndex: number): void {
        this._selectedContextIndex = contextIndex;
        this._selectedTokenIndex   = tokenIndex;

        this.openPartOfSpeechMenu(event);
    }

    public onPartOfSpeechMenuHide (): void {
        this._selectedContextIndex = null;
        this._selectedTokenIndex   = null;
    }

    public onPartOfSpeechMenuItemClick (contextIndex: number, tokenIndex: number, tagId: number): void {
        this.changeTokenTagId(contextIndex, tokenIndex, tagId);
    }

    public onSaveButtonClick (): void {
        this.dialogRef.close(true);
    }

    public onCancelButtonClick (): void {
        this.dialogRef.close(false);
    }

}
