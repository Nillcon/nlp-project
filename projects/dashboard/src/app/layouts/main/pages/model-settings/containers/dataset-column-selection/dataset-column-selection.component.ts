import { Component, OnInit, ChangeDetectionStrategy, ViewChild, Injector } from '@angular/core';

import {
    DatasetSourceTypeEnum,
    DatasetPreview,
    DatasetImportData,
    DatasetColumnTypeEnum,
    DatasetSelectedColumn,
    DatasetColumnTypeMap,
    DatasetColumnSelectorOnColumnSelected as OnColumnSelectedData,
    ColumnsSelectorComponent
} from '@Dashboard/features/dataset';

import { ColumnAssociationWindowData } from '@Dashboard/view-features';

import { ToastService } from '@Shared/features/toast';
import { DialogService } from 'primeng-lts/api';
import { ColumnAssociationWindowComponent } from './column-association-window/column-association-window.component';
import { filter, mapTo, tap } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { BaseModelStep } from '../../classes/base-model-step.class';

@Component({
    selector: 'app-dataset-column-selection',
    templateUrl: './dataset-column-selection.component.html',
    styleUrls: ['./dataset-column-selection.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class DatasetColumnSelectionComponent extends BaseModelStep implements OnInit {

    public previewData: DatasetPreview;

    public readonly rowCountToShow: number = 10;
    public sourceTypeEnum: typeof DatasetSourceTypeEnum = DatasetSourceTypeEnum;

    private columnSelectionData: Map<number, DatasetColumnTypeEnum>;
    private selectedSheetIndex: number = 0;

    @ViewChild(ColumnsSelectorComponent, { static: false })
    private readonly columnsSelectorComponent: ColumnsSelectorComponent;

    constructor (
        readonly injector: Injector,
        private readonly toastService: ToastService,
        private readonly dialogService: DialogService
    ) {
        super(injector);
    }

    public ngOnInit (): void {
        super.ngOnInit();

        this.updatePreview();
    }

    public updatePreview (): void {
        this.previewData = null;

        this.modelFacade.getPreview(this.modelId, this.rowCountToShow)
            .subscribe((data) => {
                this.previewData = data;
                this.changeDetector.markForCheck();
            });
    }

    public continueButtonCallback = (): Observable<boolean> => {
        const data = this.getImportData();
        this.validateImportData(data);

        return this.modelFacade.importDataset(this.modelId, data)
            .pipe(
                mapTo(true),
                tap(() => this.modelSettingsService.continue()),
            );
    }

    public onColumnSelected (data: OnColumnSelectedData): void {
        const columnType = DatasetColumnTypeMap.get(data.columnTypeId);

        if (columnType.isNeedAssociation) {
            this.openAssociationWindow({
                modelId: this.modelId,
                ...data
            })
                .pipe(
                    filter((result) => !result)
                )
                .subscribe(() => {
                    this.columnsSelectorComponent.clearColumnsSelectionWithType(
                        data.sheetIndex,
                        data.columnTypeId
                    );
                });
        }
    }

    public onSelection (data: Map<number, DatasetColumnTypeEnum>): void {
        this.columnSelectionData = data;
    }

    public onSheetSelected (sheetIndex: number): void {
        this.selectedSheetIndex = sheetIndex;
    }

    public onSettingsUpdated (): void {
        this.updatePreview();
    }

    private openAssociationWindow (data: ColumnAssociationWindowData): Observable<boolean> {
        return this.dialogService.open(ColumnAssociationWindowComponent, {
            width: '95%',
            header: 'Associate column',
            data
        }).onClose;
    }

    private validateImportData (data: DatasetImportData): void {
        if (data.selectedColumns.length === 0) {
            this.toastService.error({
                title: `Ooops...`,
                description: `Please select some columns before continue!`
            });

            throw new Error(`Import data isn't valid!`);
        }
    }

    private getImportData (): DatasetImportData {
        return {
            worksheetIndex: this.selectedSheetIndex,
            selectedColumns: this.getSelectedColumns()
        };
    }

    private getSelectedColumns (): DatasetSelectedColumn[] {
        const result: DatasetSelectedColumn[] = [];

        for (const [columnIndex, columnSelection] of this.columnSelectionData.entries()) {
            if (columnSelection !== DatasetColumnTypeEnum.None) {
                result.push({
                    index: columnIndex,
                    type: columnSelection
                });
            }
        }

        return result;
    }

}
