import { Component, OnInit, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { DatasetAssociationColumn, DatasetColumnType, DatasetColumnTypeMap } from '@Dashboard/features/dataset';
import { DynamicDialogRef, DynamicDialogConfig, SelectItem } from 'primeng-lts/api';
import { ToastService } from '@Shared/features/toast';
import { ModelFacade, ColumnAssociationWindowData, DatasetColumnTypeAssociationDataMap } from '@Dashboard/view-features';
import { IsObject } from 'class-validator';
import { ClassValidation } from '@Shared/decorators';
import { Subscription } from 'rxjs';

@ClassValidation()
@Component({
    selector: 'app-column-association-window',
    templateUrl: './column-association-window.component.html',
    styleUrls: ['./column-association-window.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class ColumnAssociationWindowComponent implements OnInit {

    @IsObject()
    public data: ColumnAssociationWindowData;

    public savingSubscription: Subscription;

    public associationColumn: DatasetAssociationColumn;
    public associationDropdownItems: SelectItem[];

    public columnType: DatasetColumnType;

    constructor (
        private readonly dialogRef: DynamicDialogRef,
        private readonly config: DynamicDialogConfig,
        private readonly modelFacade: ModelFacade,
        private readonly toastService: ToastService,
        private readonly changeDetector: ChangeDetectorRef
    ) {
        this.data = this.config.data;
    }

    public ngOnInit (): void {
        this.updateAssociationColumnData();
    }

    public updateAssociationColumnData (): void {
        this.modelFacade.getColumnAssociation(
            this.data.modelId,
            this.data.columnIndex,
            this.data.sheetIndex
        )
            .subscribe(
                (associationColumnData) => {
                    this.associationColumn = associationColumnData;

                    this.initColumnTypeData();
                    this.initAssociationDropboxItems();

                    this.changeDetector.markForCheck();
                },
                () => this.dialogRef.close(false)
            );
    }

    public onSaveButtonClick (): void {
        const isDataValid = this.validateData();

        if (isDataValid) {
            this.savingSubscription = this.modelFacade.updateColumnAssociation(
                this.data.modelId,
                this.data.columnIndex,
                this.associationColumn
            )
                .subscribe(() => {
                    this.dialogRef.close(true);
                });
        }
    }

    private validateData (): boolean {
        const isAllRowsAssociated = this.associationColumn.uniqueRows
            .every((row) => row.association !== null);

        if (!isAllRowsAssociated) {
            this.toastService.error({
                title: `Ooops...`,
                description: `Please associate all rows!`
            });
        }

        return isAllRowsAssociated;
    }

    private initColumnTypeData (): void {
        this.columnType = DatasetColumnTypeMap.get(this.associationColumn.type);
    }

    private initAssociationDropboxItems (): void {
        const items = [...DatasetColumnTypeAssociationDataMap.get(this.associationColumn.type)];

        items.unshift({
            label: '-Unselected-',
            value: null
        });

        this.associationDropdownItems = items;
    }

}
