export enum ModelSettingsContainerEnum {
    TypeSelection           = 'TypeSelection',
    DatasetUploading        = 'DatasetUploading',
    DatasetColumnSelection  = 'DatasetColumnSelection',
    DefiningTags            = 'DefiningTags',
    WordsExtractor          = 'WordsExtractor',
    Lemmatization           = 'Lemmatization',
    ModelTopics             = 'ModelTopics',
    SentimentAccuracy       = 'SentimentAccuracy'
}
