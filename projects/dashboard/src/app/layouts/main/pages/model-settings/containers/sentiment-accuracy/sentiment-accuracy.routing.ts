import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SentimentAccuracyComponent } from './sentiment-accuracy.component';

const routes: Routes = [
    {
        path: '',
        component: SentimentAccuracyComponent
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(routes)
    ],
    exports: [
        RouterModule
    ]
})
export class SentimentAccuracyRoutingModule {}
