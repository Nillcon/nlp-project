import { NgModule } from '@angular/core';
import { DatasetColumnSelectionComponent } from './dataset-column-selection.component';
import { DatasetColumnSelectionRoutingModule } from './dataset-column-selection.routing';
import { ModelSettingsPageSharedModule } from '../../model-settings-shared.module';
import { FeaturesModule } from '@Dashboard/features/features.module';
import { SharedModule } from '@Dashboard/app-shared.module';
import { SeparatorSelectorWindowComponent } from './separator-selector-window/separator-selector-window.component';
import { ToolbarComponent } from './toolbar/toolbar.component';
import { ColumnAssociationWindowComponent } from './column-association-window/column-association-window.component';

@NgModule({
    declarations: [
        DatasetColumnSelectionComponent,
        SeparatorSelectorWindowComponent,
        ToolbarComponent,
        ColumnAssociationWindowComponent
    ],
    imports: [
        DatasetColumnSelectionRoutingModule,

        SharedModule,
        FeaturesModule,
        ModelSettingsPageSharedModule
    ],
    exports: [
        DatasetColumnSelectionComponent
    ],
    entryComponents: [
        SeparatorSelectorWindowComponent,
        ColumnAssociationWindowComponent
    ]
})
export class DatasetColumnSelectionContainerModule {}
