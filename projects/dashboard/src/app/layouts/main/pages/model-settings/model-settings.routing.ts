import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ModelSettingsContainerEnum } from './enums/models-container.enum';
import { ModelSettingsComponent } from './model-settings.component';

const routes: Routes = [
    {
        path: '',
        component: ModelSettingsComponent,
        children: [
            {
                path: ModelSettingsContainerEnum.TypeSelection,
                loadChildren: () => import('./containers/model-type-selection/model-type-selection.module').then(m => m.ModelTypeSelectionContainerModule)
            },
            {
                path: ModelSettingsContainerEnum.DatasetUploading,
                loadChildren: () => import('./containers/dataset-uploading/dataset-uploading.module').then(m => m.DatasetUploadingContainerModule)
            },
            {
                path: ModelSettingsContainerEnum.DatasetColumnSelection,
                loadChildren: () => import('./containers/dataset-column-selection/dataset-column-selection.module').then(m => m.DatasetColumnSelectionContainerModule)
            },
            {
                path: ModelSettingsContainerEnum.DefiningTags,
                loadChildren: () => import('./containers/defining-tags/defining-tags.module').then(m => m.DefiningTagsContainerModule)
            },
            {
                path: ModelSettingsContainerEnum.Lemmatization,
                loadChildren: () => import('./containers/lemmatization-and-stop-words/lemmatization-and-stop-words.module').then(m => m.LemmatizationAndStopWordsContainerModule)
            },
            {
                path: ModelSettingsContainerEnum.WordsExtractor,
                loadChildren: () => import('./containers/words-extractor/words-extractor.module').then(m => m.WordsExtractorContainerModule)
            },
            {
                path: ModelSettingsContainerEnum.ModelTopics,
                loadChildren: () => import('./containers/model-topics/model-topics.module').then(m => m.ModelTopicsModule)
            },
            {
                path: ModelSettingsContainerEnum.SentimentAccuracy,
                loadChildren: () => import('./containers/sentiment-accuracy/sentiment-accuracy.module').then(m => m.SentimentAccuracyModule)
            }
        ]
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(routes)
    ],
    exports: [
        RouterModule
    ]
})
export class ModelSettingsRoutingModule {}
