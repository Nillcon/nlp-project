import { NgModule } from '@angular/core';
import { ModelTypeSelectionComponent } from './model-type-selection.component';
import { DatasetSourceTypeBlockListComponent } from './dataset-source-type-block-list/dataset-source-type-block-list.component';
import { ModelTypeSelectionRoutingModule } from './model-type-selection.routing';
import { ModelSettingsPageSharedModule } from '../../model-settings-shared.module';
import { FeaturesModule } from '@Dashboard/features/features.module';

@NgModule({
    declarations: [
        ModelTypeSelectionComponent,
        DatasetSourceTypeBlockListComponent
    ],
    imports: [
        ModelTypeSelectionRoutingModule,

        FeaturesModule,
        ModelSettingsPageSharedModule
    ],
    exports: [
        ModelTypeSelectionComponent
    ]
})
export class ModelTypeSelectionContainerModule {}
