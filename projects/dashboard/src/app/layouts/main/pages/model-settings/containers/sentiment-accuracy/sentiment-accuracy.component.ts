import { Component, OnInit, ChangeDetectionStrategy, Injector } from '@angular/core';
import { SentimentTrainingResult } from '@Dashboard/features/model';
import { BaseModelStep } from '../../classes/base-model-step.class';

@Component({
    selector: 'app-sentiment-accuracy',
    templateUrl: './sentiment-accuracy.component.html',
    styleUrls: ['./sentiment-accuracy.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class SentimentAccuracyComponent extends BaseModelStep implements OnInit {

    public trainingResult: SentimentTrainingResult;

    constructor (
        readonly injector: Injector
    ) {
        super(injector);
    }

    public ngOnInit (): void {
        super.ngOnInit();

        this.updateTrainingResult();
    }

    public updateTrainingResult (): void {
        this.trainingResult = null;

        this.modelFacade.getSentimentTrainingResult(this.modelId)
            .subscribe((result) => {
                this.trainingResult = result;

                this.changeDetector.markForCheck();
            });
    }

}
