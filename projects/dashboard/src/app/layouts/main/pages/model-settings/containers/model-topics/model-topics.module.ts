import { NgModule } from '@angular/core';
import { ModelTopicsComponent } from './model-topics.component';
import { ModelTopicsRoutingModule } from './model-topics.routing';
import { SharedModule } from '@Dashboard/app-shared.module';
import { FeaturesModule } from '@Dashboard/features/features.module';
import { ModelSettingsPageSharedModule } from '../../model-settings-shared.module';
import { TopicListComponent } from './topic-list/topic-list.component';

@NgModule({
	declarations: [
		ModelTopicsComponent,
		TopicListComponent
	],
	imports: [
		ModelTopicsRoutingModule,

		ModelSettingsPageSharedModule,
		SharedModule,
		FeaturesModule
	]
})
export class ModelTopicsModule {}
