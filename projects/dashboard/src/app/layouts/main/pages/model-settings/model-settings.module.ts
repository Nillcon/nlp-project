import { NgModule } from '@angular/core';
import { ModelSettingsRoutingModule } from './model-settings.routing';
import { ModelSettingsComponent } from './model-settings.component';
import { ModelSettingsPageSharedModule } from './model-settings-shared.module';
import { MainLayoutSharedModule } from '../../main-layout-shared.module';

@NgModule({
    declarations: [
        ModelSettingsComponent
    ],
    imports: [
        ModelSettingsRoutingModule,

        ModelSettingsPageSharedModule,
        MainLayoutSharedModule
    ]
})
export class ModelSettingsPageModule {}
