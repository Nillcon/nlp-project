import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DatasetUploadingComponent } from './dataset-uploading.component';

const routes: Routes = [
    {
        path: '',
        component: DatasetUploadingComponent
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(routes)
    ],
    exports: [
        RouterModule
    ]
})
export class DatasetUploadingRoutingModule {}
