import { Component, OnInit, ChangeDetectionStrategy, Output, EventEmitter, OnDestroy, ChangeDetectorRef } from '@angular/core';
import { TagFacade, ViewTag } from '@Dashboard/view-features';
import { Tag } from '@Dashboard/features/tag';
import { tap, map, filter } from 'rxjs/operators';
import { ModelStateEnum } from '@Shared/enums';
import { Subscription } from 'rxjs';
import { AutoUnsubscribe } from "ngx-auto-unsubscribe";

@AutoUnsubscribe()
@Component({
    selector: 'app-tags-card',
    templateUrl: './tags-card.component.html',
    styleUrls: ['./tags-card.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class TagsCardComponent implements OnInit, OnDestroy {

    @Output()
    public OnTagSelected: EventEmitter<Tag> = new EventEmitter();

    @Output()
    public OnTagDeleted: EventEmitter<Tag> = new EventEmitter();

    public tags: ViewTag[];

    public isEditMode: boolean = false;

    private _tagsSubscription: Subscription;

    constructor (
        private readonly tagFacade: TagFacade,
        private readonly changeDetector: ChangeDetectorRef
    ) {}

    public ngOnInit (): void {
        this.initTagsObservable();
    }

    public ngOnDestroy (): void {}

    public initTagsObservable (): void {
        this._tagsSubscription = this.tagFacade.getTags$()
            .pipe(
                filter((tags) => !!tags),
                map((tags) => tags.map((currTag) => <ViewTag>{
                    ...currTag,
                    modelState: (currTag.id) ? ModelStateEnum.Created : ModelStateEnum.Emulated
                })),
                tap((tags) => {
                    this.tags = tags;

                    this.changeDetector.markForCheck();
                })
            )
            .subscribe();
    }

    public onAddTagButtonClick (): void {
        this.isEditMode = true;

        this.tagFacade.createTag();
    }

    public onEditTagButtonClick (): void {
        this.isEditMode = !this.isEditMode;
    }

    public onConfirmButtonClick (): void {
        this.isEditMode = false;
    }

}
