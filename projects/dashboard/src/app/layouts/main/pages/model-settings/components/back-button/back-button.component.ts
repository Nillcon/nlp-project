import { Component, OnInit, ChangeDetectionStrategy, Output, EventEmitter } from '@angular/core';

@Component({
    selector: 'app-back-button',
    templateUrl: './back-button.component.html',
    styleUrls: ['./back-button.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class BackButtonComponent implements OnInit {

    @Output()
    public OnClick: EventEmitter<MouseEvent> = new EventEmitter();

    constructor () {}

    public ngOnInit (): void {}

}
