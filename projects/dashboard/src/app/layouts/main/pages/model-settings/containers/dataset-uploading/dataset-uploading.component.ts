import { Component, OnInit, ChangeDetectionStrategy, Injector } from '@angular/core';
import { BaseModelStep } from '../../classes/base-model-step.class';

@Component({
    selector: 'app-dataset-uploading',
    templateUrl: './dataset-uploading.component.html',
    styleUrls: ['./dataset-uploading.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class DatasetUploadingComponent extends BaseModelStep implements OnInit {

    public isDatasetUploading: boolean = false;

    constructor (
        readonly injector: Injector
    ) {
        super(injector);
    }

    public ngOnInit (): void {
        super.ngOnInit();
    }

    public onFilesSelected (files: Blob[]): void {
        this.isDatasetUploading = true;

        this.modelFacade.uploadDataset(this.modelId, this.sourceType.id, files)
            .subscribe(
                () => {
                    this.isDatasetUploading = false;

                    this.changeDetector.markForCheck();
                    this.onFileUploaded();
                },
                () => {
                    this.isDatasetUploading = false;

                    this.changeDetector.markForCheck();
                }
            );
    }

    public onFileUploaded (): void {
        this.modelSettingsService.continue();
    }

}
