import { Component, OnInit, ChangeDetectionStrategy, Input } from '@angular/core';

@Component({
    selector: 'app-container-title',
    templateUrl: './container-title.component.html',
    styleUrls: ['./container-title.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class ContainerTitleComponent implements OnInit {

    @Input()
    public header: string = '';

    @Input()
    public subheader: string = '';

    constructor () {}

    public ngOnInit (): void {}

}
