import { Component, OnInit, ChangeDetectionStrategy, Input, Output, EventEmitter, Injector } from '@angular/core';
import { IsArray, IsBoolean } from 'class-validator';
import { ClassValidation } from '@Shared/decorators';
import { ViewTag, TagFacade } from '@Dashboard/view-features';
import { Tag } from '@Dashboard/features/tag';
import { ModelStateEnum } from '@Shared/enums';
import { NeedsConfirmation } from '@Shared/modules/confirmation-window';

@ClassValidation()
@Component({
    selector: 'app-tag-list',
    templateUrl: './tag-list.component.html',
    styleUrls: ['./tag-list.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class TagListComponent implements OnInit {

    @Input()
    @IsBoolean()
    public isEditMode: boolean;

    @Input()
    public set tags (tags: ViewTag[]) {
        this._tags = tags;

        this.processValidityOfSelectedTagIndex();
    }

    public get tags (): ViewTag[] {
        return this._tags;
    }

    @Output()
    public OnAddButtonClick: EventEmitter<MouseEvent> = new EventEmitter();

    @Output()
    public OnTagSelected: EventEmitter<Tag> = new EventEmitter();

    @Output()
    public OnTagDeleted: EventEmitter<Tag> = new EventEmitter();

    public selectedTagIndex: number;

    public readonly modelStateEnum: typeof ModelStateEnum = ModelStateEnum;

    @IsArray()
    private _tags: ViewTag[];

    constructor (
        private readonly tagFacade: TagFacade,
        private readonly injector: Injector
    ) {}

    public ngOnInit (): void {}

    public updateTags (): void {
        this.tagFacade.updateTagsIfNeeded();
    }

    public onTagSelected (tagIndex: number): void {
        const selectedTag = this.tagFacade.getTagByIndex(tagIndex);

        this.OnTagSelected.emit(selectedTag);
    }

    public onTagNameEdited (tag: Tag, enteredTagName: string): void {
        this.tagFacade.changeTagName(tag.id, enteredTagName);
    }

    @NeedsConfirmation('Do you really want to delete this tag?')
    public onTagCloseButtonClick (tag: Tag): void {
        const currentSelectedTag = this.tagFacade.getTagByIndex(this.selectedTagIndex);

        this.tagFacade.deleteTag(tag);
        this.OnTagDeleted.emit(tag);

        if (currentSelectedTag.id === tag.id) {
            const newSelectedTag = this.tagFacade.getTagByIndex(this.selectedTagIndex);
            this.selectedTagIndex = (newSelectedTag) ? this.selectedTagIndex : null;

            this.OnTagSelected.emit(newSelectedTag);
        }
    }

    private processValidityOfSelectedTagIndex (): void {
        const isSelectedTagByIndexExists = !!this.tagFacade.getTagByIndex(this.selectedTagIndex)?.id;

        if (!isSelectedTagByIndexExists) {
            this.selectFirstValidTagAsDefault();
        }
    }

    private selectFirstValidTagAsDefault (): void {
        const validTags = this.tags.filter((currTag) => !!currTag.id);

        if (validTags.length >= 1) {
            const firstValidTagIndex = this.tags.findIndex((currTag) => currTag.id === validTags[0].id);
            this.selectedTagIndex    = firstValidTagIndex;

            this.onTagSelected(this.selectedTagIndex);
        }
    }

}
