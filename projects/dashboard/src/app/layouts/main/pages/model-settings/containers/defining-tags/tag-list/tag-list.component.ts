import { Component, OnInit, ChangeDetectionStrategy, Injector, Output, EventEmitter } from '@angular/core';
import { Tag } from '@Dashboard/features/tag';
import { Observable } from 'rxjs';
import { map, tap } from 'rxjs/operators';
import { ModelStateEnum } from '@Shared/enums';
import { TagFacade, ViewTag } from '@Dashboard/view-features';
import { NeedsConfirmation } from '@Shared/modules/confirmation-window';

@Component({
    selector: 'app-tag-list',
    templateUrl: './tag-list.component.html',
    styleUrls: ['./tag-list.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class TagListComponent implements OnInit {

    @Output()
    public OnTagsUpdated = new EventEmitter<ViewTag[]>();

    public tags$: Observable<ViewTag[]>;

    constructor (
        public readonly injector: Injector,
        private readonly tagFacade: TagFacade
    ) {}

    public ngOnInit (): void {
        this.initTagsObservable();

        this.updateTags();
    }

    public updateTags (): void {
        this.tagFacade.updateTagsIfNeeded();
    }

    public createTag (tag: Tag): void {
        this.tagFacade.createTag(tag.name);
    }

    @NeedsConfirmation('Do you really want to delete this tag?')
    public deleteTag (tag: Tag): void {
        this.tagFacade.deleteTag(tag);
    }

    private initTagsObservable (): void {
        this.tags$ = this.tagFacade.getTags$()
            .pipe(
                map((tags) => tags.map((currTag) => <ViewTag>{
                    ...currTag,
                    modelState: (currTag.id) ? ModelStateEnum.Created : ModelStateEnum.Emulated
                })),
                tap((tags) => this.OnTagsUpdated.emit(tags))
            );
    }

}
