import { Component, OnInit, ChangeDetectionStrategy, Input } from '@angular/core';
import { BaseModelStep } from '../../classes/base-model-step.class';

@Component({
    selector: 'app-base-step',
    templateUrl: './base-step.component.html',
    styleUrls: ['./base-step.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class BaseStepComponent implements OnInit {

    @Input()
    public headerText: string = '';

    @Input()
    public subheaderText: string = '';

    @Input()
    public data: BaseModelStep;

    constructor () {}

    public ngOnInit (): void {}

    public onBackButtonClick (): void {
        if (this.data.backButtonCallback) {
            this.data.backButtonCallback();
        }
    }

}
