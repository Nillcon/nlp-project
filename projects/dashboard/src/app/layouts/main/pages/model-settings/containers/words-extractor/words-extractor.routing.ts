import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { WordsExtractorComponent } from './words-extractor.component';

const routes: Routes = [
    {
        path: '',
        component: WordsExtractorComponent
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(routes)
    ],
    exports: [
        RouterModule
    ]
})
export class WordsExtractorRoutingModule {}
