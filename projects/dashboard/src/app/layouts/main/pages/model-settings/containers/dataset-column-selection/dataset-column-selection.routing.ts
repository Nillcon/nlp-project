import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DatasetColumnSelectionComponent } from './dataset-column-selection.component';

const routes: Routes = [
    {
        path: '',
        component: DatasetColumnSelectionComponent
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(routes)
    ],
    exports: [
        RouterModule
    ]
})
export class DatasetColumnSelectionRoutingModule {}
