import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DefiningTagsComponent } from './defining-tags.component';

const routes: Routes = [
    {
        path: '',
        component: DefiningTagsComponent
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(routes)
    ],
    exports: [
        RouterModule
    ]
})
export class DefiningTagsRoutingModule {}
