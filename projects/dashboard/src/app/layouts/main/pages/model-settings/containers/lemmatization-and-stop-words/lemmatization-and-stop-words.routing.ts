import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LemmatizationAndStopWordsComponent } from './lemmatization-and-stop-words.component';

const routes: Routes = [
    {
        path: '',
        component: LemmatizationAndStopWordsComponent
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(routes)
    ],
    exports: [
        RouterModule
    ]
})
export class LemmatizationAndStopWordsRoutingModule {}
