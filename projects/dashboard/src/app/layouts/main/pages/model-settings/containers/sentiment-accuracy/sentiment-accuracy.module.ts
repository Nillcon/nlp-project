import { NgModule } from '@angular/core';
import { SentimentAccuracyComponent } from './sentiment-accuracy.component';
import { SentimentAccuracyRoutingModule } from './sentiment-accuracy.routing';
import { SharedModule } from '@Dashboard/app-shared.module';
import { ModelSettingsPageSharedModule } from '../../model-settings-shared.module';
import { MainLayoutSharedModule } from '@Dashboard/layouts/main/main-layout-shared.module';

@NgModule({
    declarations: [
        SentimentAccuracyComponent
    ],
    imports: [
        SharedModule,
        ModelSettingsPageSharedModule,
        MainLayoutSharedModule,

        SentimentAccuracyRoutingModule
    ]
})
export class SentimentAccuracyModule {}
