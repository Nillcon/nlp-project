import { OnInit, ChangeDetectorRef, Injector, Type } from '@angular/core';
import { ModelSettingsService, ModelTypeSettingsStep, ModelFacade } from '@Dashboard/view-features';
import { UrlService } from '@Shared/features';
import { ModelSettingsQueryParamEnum as QueryParamEnum } from '../enums/query-params.enum';
import { DatasetSourceType, DatasetSourceTypeMap } from '@Dashboard/features/dataset';
import { ModelType, ModelTypeMap } from '@Dashboard/features/model';
import { Observable, of, throwError } from 'rxjs';
import { mapTo, tap, catchError } from 'rxjs/operators';
import { LoadingWindowService } from '@Shared/modules/loading-window/services/loading-window.service';

export class BaseModelStep implements OnInit {

    public stepData: ModelTypeSettingsStep;
    public modelId: number;
    public modelType: ModelType;
    public sourceType: DatasetSourceType;

    public isStepInitWithError: boolean = false;

    protected readonly urlService: UrlService;
    protected readonly modelSettingsService: ModelSettingsService;
    protected readonly loadingWindowService: LoadingWindowService;
    protected readonly modelFacade: ModelFacade;
    protected readonly changeDetector: ChangeDetectorRef;

    constructor (
        public readonly injector: Injector
    ) {
        this.urlService             = injector.get(UrlService as Type<UrlService>);
        this.modelSettingsService   = injector.get(ModelSettingsService as Type<ModelSettingsService>);
        this.loadingWindowService   = injector.get(LoadingWindowService as Type<LoadingWindowService>);
        this.modelFacade            = injector.get(ModelFacade as Type<ModelFacade>);
        this.changeDetector         = injector.get(ChangeDetectorRef as Type<ChangeDetectorRef>);
    }

    public ngOnInit (): void {
        this.initStepData();
        this.initModelId();
        this.initModelType();
        this.initSourceType();

        this.validateStepData();
        this.validateDependQueryParam(QueryParamEnum.modelId, this.modelId);
        this.validateDependQueryParam(QueryParamEnum.modelType, this.modelType);
        this.validateDependQueryParam(QueryParamEnum.datasetSourceType, this.sourceType);
    }

    public continueButtonCallback = (): Observable<boolean> => {
        return of(null)
            .pipe(
                tap(() => this.modelSettingsService.continue()),
                mapTo(true)
            );
    }

    public trainingButtonCallback = (): Observable<boolean> => {
        const loadingWindow = this.loadingWindowService.show();

        return this.modelFacade.training(this.modelId)
            .pipe(
                tap(() => loadingWindow.close()),
                mapTo(true),
                catchError((error) => {
                    loadingWindow.close();

                    return throwError(error);
                })
            );
    }

    public backButtonCallback = (): void => {
        this.modelSettingsService.back();
    }

    private initModelId (): void {
        const modelIdQueryParam = this.urlService.getParameter(QueryParamEnum.modelId);

        if (modelIdQueryParam !== undefined) {
            this.modelId = +modelIdQueryParam;
        }
    }

    private initModelType (): void {
        const modelTypeIdQueryParam = this.urlService.getParameter(QueryParamEnum.modelType);

        if (modelTypeIdQueryParam !== undefined) {
            this.modelType = ModelTypeMap.get(+modelTypeIdQueryParam);
        }
    }

    private initSourceType (): void {
        const sourceTypeIdQueryParam = this.urlService.getParameter(QueryParamEnum.datasetSourceType);

        if (sourceTypeIdQueryParam !== undefined) {
            this.sourceType = DatasetSourceTypeMap.get(+sourceTypeIdQueryParam);
        }
    }

    private initStepData (): void {
        this.stepData = this.modelSettingsService.step;
    }

    private validateStepData (): void {
        if (this.stepData === undefined) {
            this.isStepInitWithError = true;

            throw new Error(`Step component could not init stepData!`);
        }
    }

    private validateDependQueryParam (paramName: QueryParamEnum, paramValue: any): void {
        const isDependsOn = this.stepData.dependentQueryParams.includes(paramName);

        if (isDependsOn && paramValue === undefined) {
            this.isStepInitWithError = true;

            throw new Error(`Step component could not init ${paramName}!`);
        }
    }

}
