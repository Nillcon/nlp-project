import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng-lts/api';

@Component({
    selector: 'app-separator-selector-window',
    templateUrl: './separator-selector-window.component.html',
    styleUrls: ['./separator-selector-window.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class SeparatorSelectorWindowComponent implements OnInit {

    public separator: string;

    constructor (
        private readonly config: DynamicDialogConfig,
        public ref: DynamicDialogRef
    ) {
        this.separator = this.config.data;
    }

    public ngOnInit (): void {}

    public onOkButtonClick (): void {
        this.ref.close(this.separator);
    }

}
