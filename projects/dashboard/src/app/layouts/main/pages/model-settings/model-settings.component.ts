import { Component, OnInit, ChangeDetectionStrategy, OnDestroy } from '@angular/core';
import { transition, useAnimation, trigger } from '@angular/animations';
import { fadeIn } from 'ng-animate';
import { ModelSettingsService, ModelFacade } from '@Dashboard/view-features';
import { AppLayoutEnum } from '@Dashboard/enums';
import { NavigationEventsService } from '@Shared/features';
import { Subscription, Observable, of } from 'rxjs';
import { delayWhen, switchMap } from 'rxjs/operators';
import { ModelSettingsQueryParamEnum } from './enums/query-params.enum';
import { AutoUnsubscribe } from 'ngx-auto-unsubscribe';

@AutoUnsubscribe()
@Component({
    selector: 'app-model-settings',
    templateUrl: './model-settings.component.html',
    styleUrls: ['model-settings.component.scss'],
    providers: [ModelSettingsService],
    animations: [
        trigger('routeAnimations', [
            transition(':enter', useAnimation(fadeIn, { params: { timing: 0.15 } }))
        ])
    ],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class ModelSettingsComponent implements OnInit, OnDestroy {

    public stepSubscription: Subscription;

    constructor (
        private readonly modelSettingsService: ModelSettingsService,
        private readonly navigationEventsService: NavigationEventsService,
        private readonly modelFacade: ModelFacade
    ) {}

    public ngOnInit (): void {
        this.modelSettingsService.init({
            isNeedRedirectWhenErrors: true,
            redirectionWhenErrorsSubUrl: `/${AppLayoutEnum.Main}`
        });

        this.initOnStepChangedObserver();
    }

    public ngOnDestroy (): void {}

    private initOnStepChangedObserver (): void {
        this.stepSubscription = this.navigationEventsService.navigationEnd$
            .pipe(
                switchMap(() => of(this.modelSettingsService.stepIndex)),
                delayWhen(() => this.trySetStepIndexForModel())
            )
            .subscribe();
    }

    private trySetStepIndexForModel (): Observable<any> {
        const urlParams = this.modelSettingsService.urlParams;
        const modelId   = +urlParams[ModelSettingsQueryParamEnum.modelId];
        const stepIndex = this.modelSettingsService.stepIndex;

        return (!modelId) ? of(null) : this.modelFacade.setStepIndex(modelId, stepIndex);
    }

}
