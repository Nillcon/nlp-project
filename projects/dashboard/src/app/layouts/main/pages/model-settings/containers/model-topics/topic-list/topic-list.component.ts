import { Component, OnInit, ChangeDetectionStrategy, Input, Output, EventEmitter } from '@angular/core';
import { IsArray } from 'class-validator';
import { ClassValidation } from '@Shared/decorators';
import { ModelTopic } from '@Dashboard/features/model';

@ClassValidation()
@Component({
    selector: 'app-topic-list',
    templateUrl: './topic-list.component.html',
    styleUrls: ['./topic-list.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class TopicListComponent implements OnInit {

    @Input()
    @IsArray()
    public topics: ModelTopic[];

    @Output()
    public OnTopicNameChange = new EventEmitter<Partial<ModelTopic>>();

    constructor () {}

    public ngOnInit (): void {}

    public onModelTopicNameChanged (newTopicName: string, topic: ModelTopic): void {
        this.OnTopicNameChange.emit({
            index: topic.index,
            name: newTopicName
        });
    }

}
