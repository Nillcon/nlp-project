import { Component, OnInit, ChangeDetectionStrategy, Output, EventEmitter } from '@angular/core';
import { FormGroupTypeSafe } from 'form-type-safe';
import { Tag } from '@Dashboard/features/tag';
import { ToastService } from '@Shared/features/toast';

@Component({
    selector: 'app-tag-creation-field',
    templateUrl: './tag-creation-field.component.html',
    styleUrls: ['./tag-creation-field.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class TagCreationFieldComponent implements OnInit {

    @Output()
    public OnCreation = new EventEmitter<Partial<Tag>>();

    private formGroup: FormGroupTypeSafe<Partial<Tag>>;

    constructor (
        private readonly toastService: ToastService
    ) {}

    public ngOnInit (): void {}

    public onFormGroupInit (formGroup: FormGroupTypeSafe<Partial<Tag>>): void {
        this.formGroup = formGroup;
    }

    public onTagCreation (): void {
        if (this.formGroup.valid) {
            this.OnCreation.emit(this.formGroup.value);

            this.formGroup.reset();
        } else {
            this.toastService.error({
                title: `Ooops...`,
                description: `Please enter tag name before creating!`
            });

            this.formGroup.markAllAsTouched();
        }
    }

}
