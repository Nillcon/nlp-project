import { NgModule } from '@angular/core';
import { DefiningTagsComponent } from './defining-tags.component';
import { DefiningTagsRoutingModule } from './defining-tags.routing';
import { ModelSettingsPageSharedModule } from '../../model-settings-shared.module';
import { FeaturesModule } from '@Dashboard/features/features.module';
import { SharedModule } from '@Dashboard/app-shared.module';
import { TagListComponent } from './tag-list/tag-list.component';
import { TagCreationFieldComponent } from './tag-creation-field/tag-creation-field.component';

@NgModule({
    declarations: [
        DefiningTagsComponent,
        TagListComponent,
        TagCreationFieldComponent
    ],
    imports: [
        DefiningTagsRoutingModule,

        SharedModule,
        FeaturesModule,
        ModelSettingsPageSharedModule
    ],
    exports: [
        DefiningTagsComponent
    ]
})
export class DefiningTagsContainerModule {}
