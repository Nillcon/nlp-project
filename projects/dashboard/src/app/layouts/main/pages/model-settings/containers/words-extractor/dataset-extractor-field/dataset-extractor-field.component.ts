import { Component, OnInit, ChangeDetectionStrategy, Input, ViewChild } from '@angular/core';
import { Tag } from '@Dashboard/features/tag';
import { ModelStateEnum } from '@Shared/enums';
import { IsArray } from 'class-validator';
import { ClassValidation } from '@Shared/decorators';
import { ExtractorFieldComponent } from '@Dashboard/features/dataset';

@ClassValidation()
@Component({
    selector: 'app-dataset-extractor-field',
    templateUrl: './dataset-extractor-field.component.html',
    styleUrls: ['./dataset-extractor-field.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class DatasetExtractorFieldComponent implements OnInit {

    @Input()
    @IsArray()
    public readonly tags: Tag[];

    @Input()
    public targetTagId: number;

    public readonly sentence: string = 'How do I turn on line numbers by default in TextWrangler on the Mac? How do I turn on line numbers by default in TextWrangler on the Mac? How do I turn on line numbers by default in TextWrangler on the Mac? How do I turn on line numbers by default in TextWrangler on the Mac? How do I turn on line numbers by default in TextWrangler on the Mac? How do I turn on line numbers by default in TextWrangler on the Mac? How do I turn on line numbers by default in TextWrangler on the Mac? How do I turn on line numbers by default in TextWrangler on the Mac?';

    public modelStateEnum: typeof ModelStateEnum = ModelStateEnum;

    @ViewChild(ExtractorFieldComponent, { static: true })
    private readonly _extractorFieldComponent: ExtractorFieldComponent;

    constructor () {}

    public ngOnInit (): void {}

    public removeTagFromTokens (tag: Tag): void {
        this._extractorFieldComponent.removeTagFromTokens(tag);
    }

}
