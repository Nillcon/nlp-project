import { Component, OnInit, ChangeDetectionStrategy, Injector } from '@angular/core';
import { DatasetSourceType } from '@Dashboard/features/dataset/interfaces/dataset-source-type.interface';
import { ModelSettingsQueryParamEnum } from '../../enums/query-params.enum';
import { BaseModelStep } from '../../classes/base-model-step.class';

@Component({
    selector: 'app-model-type-selection',
    templateUrl: './model-type-selection.component.html',
    styleUrls: ['./model-type-selection.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class ModelTypeSelectionComponent extends BaseModelStep implements OnInit {

    constructor (
        readonly injector: Injector
    ) {
        super(injector);
    }

    public ngOnInit (): void {
        super.ngOnInit();
    }

    public onSourceTypeCardClick (sourceType: DatasetSourceType): void {
        this.modelSettingsService.continue({
            queryParams: {
                [ModelSettingsQueryParamEnum.datasetSourceType]: sourceType.id
            }
        });
    }

}
