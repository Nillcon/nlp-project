import { Component, OnInit, ChangeDetectionStrategy, Input, Output, EventEmitter } from '@angular/core';
import { ClassValidation } from '@Shared/decorators';
import { IsBoolean, IsObject, IsNumber } from 'class-validator';
import { DatasetSourceType, DatasetPreviewSettings } from '@Dashboard/features/dataset';
import { ModelFacade } from '@Dashboard/view-features';
import { SeparatorSelectorWindowComponent } from '../separator-selector-window/separator-selector-window.component';
import { DialogService } from 'primeng-lts/api';
import { Observable } from 'rxjs';
import { filter, tap } from 'rxjs/operators';

@ClassValidation()
@Component({
    selector: 'app-toolbar',
    templateUrl: './toolbar.component.html',
    styleUrls: ['./toolbar.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class ToolbarComponent implements OnInit {

    @Input()
    @IsBoolean()
    public isDiscardFirstRow: boolean;

    @Input()
    public delimiter: string;

    @Input()
    @IsNumber()
    public modelId: number;

    @Input()
    @IsObject()
    public sourceType: DatasetSourceType;

    @Output()
    public OnSettingsUpdating = new EventEmitter<Partial<DatasetPreviewSettings>>();

    @Output()
    public OnSettingsUpdated = new EventEmitter<Partial<DatasetPreviewSettings>>();

    constructor (
        private readonly modelFacade: ModelFacade,
        private readonly dialog: DialogService
    ) {}

    public ngOnInit (): void {}

    public updateSettings (): void {
        const data = this.getSettings();

        this.OnSettingsUpdating.emit(data);

        this.modelFacade.updatePreviewSettings(this.modelId, data)
            .subscribe(() => {
                this.OnSettingsUpdated.emit(data);
            });
    }

    public onSeparatorEditButtonClick (): void {
        this.openSeparatorEditorWindow()
            .pipe(
                filter((delimiter) => delimiter !== undefined && this.delimiter !== delimiter),
                tap((delimiter) => this.delimiter = delimiter),
                tap(() => this.updateSettings())
            )
            .subscribe();
    }

    public openSeparatorEditorWindow (): Observable<string> {
        return this.dialog.open(SeparatorSelectorWindowComponent, {
            width: '300px',
            header: 'Edit column separators',
            data: this.delimiter
        }).onClose;
    }

    public onDiscardFirstRowCheckboxToggle (state: boolean): void {
        this.updateSettings();
    }

    public getSettings (): Partial<DatasetPreviewSettings> {
        return {
            delimiter: this.delimiter,
            hasHeader: this.isDiscardFirstRow
        };
    }

}
