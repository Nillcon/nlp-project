import { NgModule } from '@angular/core';
import { DatasetUploadingComponent } from './dataset-uploading.component';
import { DatasetUploadingRoutingModule } from './dataset-uploading.routing';
import { ModelSettingsPageSharedModule } from '../../model-settings-shared.module';
import { FeaturesModule } from '@Dashboard/features/features.module';
import { SharedModule } from '@Dashboard/app-shared.module';
import { DatasetUploaderComponent } from './dataset-uploader/dataset-uploader.component';

@NgModule({
    declarations: [
        DatasetUploadingComponent,
        DatasetUploaderComponent
    ],
    imports: [
        DatasetUploadingRoutingModule,

        SharedModule,
        FeaturesModule,
        ModelSettingsPageSharedModule
    ],
    exports: [
        DatasetUploadingComponent
    ]
})
export class DatasetUploadingContainerModule {}
