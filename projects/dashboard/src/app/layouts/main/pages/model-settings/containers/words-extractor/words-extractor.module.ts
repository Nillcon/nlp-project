import { NgModule } from '@angular/core';
import { WordsExtractorComponent } from './words-extractor.component';
import { WordsExtractorRoutingModule } from './words-extractor.routing';
import { ModelSettingsPageSharedModule } from '../../model-settings-shared.module';
import { FeaturesModule } from '@Dashboard/features/features.module';
import { DatasetExtractorFieldComponent } from './dataset-extractor-field/dataset-extractor-field.component';
import { TagsCardComponent } from './tags-card/tags-card.component';
import { ExtractorFooterComponent } from './dataset-extractor-field/extractor-footer/extractor-footer.component';
import { TagListComponent } from './tags-card/tag-list/tag-list.component';

@NgModule({
    declarations: [
        WordsExtractorComponent,
        DatasetExtractorFieldComponent,
        TagsCardComponent,
        ExtractorFooterComponent,
        TagListComponent
    ],
    imports: [
        WordsExtractorRoutingModule,

        FeaturesModule,
        ModelSettingsPageSharedModule
    ],
    exports: [
        WordsExtractorComponent
    ]
})
export class WordsExtractorContainerModule {}
