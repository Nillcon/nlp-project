import { Component, OnInit, ChangeDetectionStrategy, Injector } from '@angular/core';
import { ModelTopic } from '@Dashboard/features/model';
import { BaseModelStep } from '../../classes/base-model-step.class';

@Component({
    selector: 'app-model-topics',
    templateUrl: './model-topics.component.html',
    styleUrls: ['./model-topics.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class ModelTopicsComponent extends BaseModelStep implements OnInit {

    public topics: ModelTopic[];

    constructor (
        readonly injector: Injector
    ) {
        super(injector);
    }

    public ngOnInit (): void {
        super.ngOnInit();

        this.updateTopics();
    }

    public updateTopics (): void {
        this.modelFacade.getTopics(this.modelId)
            .subscribe((data) => {
                this.topics = data;

                this.changeDetector.markForCheck();
            });
    }

    public onTopicNameChanged (data: Partial<ModelTopic>): void {
        this.modelFacade.renameTopic(this.modelId, data)
            .subscribe();
    }

}
