import { Component, OnInit, ChangeDetectionStrategy, ViewChild, Injector } from '@angular/core';
import { Tag } from '@Dashboard/features/tag';
import { TagFacade } from '@Dashboard/view-features';
import { Observable } from 'rxjs';
import { DatasetExtractorFieldComponent } from './dataset-extractor-field/dataset-extractor-field.component';
import { BaseModelStep } from '../../classes/base-model-step.class';

@Component({
    selector: 'app-words-extractor',
    templateUrl: './words-extractor.component.html',
    styleUrls: ['./words-extractor.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class WordsExtractorComponent extends BaseModelStep implements OnInit {

    public tags$: Observable<Tag[]>;

    public selectedTag: Tag;

    @ViewChild(DatasetExtractorFieldComponent, { static: false })
    private readonly _extractorFieldComponent: DatasetExtractorFieldComponent;

    constructor (
        readonly injector: Injector,
        private readonly tagFacade: TagFacade
    ) {
        super(injector);
    }

    public ngOnInit (): void {
        super.ngOnInit();

        this.initTagsObservable();
        this.updateTagsIfNeeded();
    }

    public updateTagsIfNeeded (): void {
        this.tagFacade.updateTagsIfNeeded();
    }

    public initTagsObservable (): void {
        this.tags$ = this.tagFacade.getTags$();
    }

    public onTagSelected (tag: Tag): void {
        this.selectedTag = tag;
    }

    public onTagDeleted (tag: Tag): void {
        this._extractorFieldComponent.removeTagFromTokens(tag);
    }

}
