import { NgModule } from '@angular/core';
import { BackButtonComponent } from './components/back-button/back-button.component';
import { ContainerTitleComponent } from './components/container-title/container-title.component';
import { SharedModule } from '@Dashboard/app-shared.module';
import { ActionButtonsComponent } from './components/action-buttons/action-buttons.component';
import { TrainingProgressWindowComponent } from './components/training-progress-window/training-progress-window.component';
import { TrainingWindowControllerComponent } from './components/training-window-controller/training-window-controller.component';
import { StepInitErrorMessageComponent } from './components/step-init-error-message/step-init-error-message.component';
import { BaseStepComponent } from './components/base-step/base-step.component';

@NgModule({
    declarations: [
        BackButtonComponent,
        ContainerTitleComponent,
        ActionButtonsComponent,
        TrainingProgressWindowComponent,
        TrainingWindowControllerComponent,
        StepInitErrorMessageComponent,
        BaseStepComponent
    ],
    imports: [
        SharedModule
    ],
    exports: [
        BaseStepComponent,
        BackButtonComponent,
        ContainerTitleComponent,
        ActionButtonsComponent,
        TrainingProgressWindowComponent,
        TrainingWindowControllerComponent,
        StepInitErrorMessageComponent,

        SharedModule
    ],
    entryComponents: [
        TrainingProgressWindowComponent
    ]
})
export class ModelSettingsPageSharedModule {}
