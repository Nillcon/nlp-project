import { Component, OnInit, ChangeDetectionStrategy, OnDestroy } from '@angular/core';
import { ModelTrainingWsService } from '@Dashboard/features/model';
import { DialogService, DynamicDialogRef } from 'primeng-lts/api';
import { AutoUnsubscribe } from 'ngx-auto-unsubscribe';
import { Subscription } from 'rxjs';
import { filter } from 'rxjs/operators';
import { TrainingProgressWindowComponent } from '../training-progress-window/training-progress-window.component';
import { Router, NavigationEnd } from '@angular/router';
import { ModelSettingsService } from '@Dashboard/view-features';
import { ModelSettingsQueryParamEnum } from '../../enums/query-params.enum';

@AutoUnsubscribe()
@Component({
    selector: 'app-training-window-controller',
    templateUrl: './training-window-controller.component.html',
    styleUrls: ['./training-window-controller.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class TrainingWindowControllerComponent implements OnInit, OnDestroy {

    public dialogRef: DynamicDialogRef;

    private trainingProcessSubscription: Subscription;
    private routerSubscription: Subscription;

    constructor (
        private readonly modelTrainingService: ModelTrainingWsService,
        private readonly modelSettingsService: ModelSettingsService,
        private readonly dialogService: DialogService,
        private readonly router: Router
    ) {}

    public ngOnInit (): void {
        this.initTrainingProcessObserver();
        this.initRouterSubscription();
    }

    public ngOnDestroy (): void {}

    public openTrainingWindow (): void {
        if (!this.dialogRef) {
            this.dialogRef = this.dialogService.open(TrainingProgressWindowComponent, {
                width: '350px',
                header: `Training model`,
                closable: false
            });

            this.dialogRef.onClose
                .subscribe(() => this.dialogRef = null);
        }
    }

    public onNavigationEnd (): void {
        const urlParams = this.modelSettingsService.getUrlParams();
        const modelId = +urlParams[ModelSettingsQueryParamEnum.modelId];

        if (modelId && !this.modelTrainingService.isActivated) {
            this.modelTrainingService.activate({
                modelId
            });
        }
    }

    private initTrainingProcessObserver (): void {
        this.trainingProcessSubscription = this.modelTrainingService.onTrainingProcess$
            .pipe(
                filter((isNowTraining) => isNowTraining === true)
            )
            .subscribe(() => {
                this.openTrainingWindow();
            });
    }

    private initRouterSubscription (): void {
        this.routerSubscription = this.router.events
            .pipe(
                filter((event) => event instanceof NavigationEnd)
            )
            .subscribe(() => this.onNavigationEnd());
    }

}
