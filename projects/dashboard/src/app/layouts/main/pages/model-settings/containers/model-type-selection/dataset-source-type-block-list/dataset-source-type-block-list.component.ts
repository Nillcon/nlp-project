import { Component, OnInit, ChangeDetectionStrategy, Output, EventEmitter } from '@angular/core';
import { DatasetSourceType, DatasetSourceTypeMap } from '@Dashboard/features/dataset';

@Component({
    selector: 'app-dataset-source-type-block-list',
    templateUrl: './dataset-source-type-block-list.component.html',
    styleUrls: ['./dataset-source-type-block-list.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class DatasetSourceTypeBlockListComponent implements OnInit {

    public sourceTypeArray: DatasetSourceType[];

    @Output()
    public OnSourceTypeCardClick: EventEmitter<DatasetSourceType> = new EventEmitter();

    constructor () {}

    public ngOnInit (): void {
        this.initDatasetSourceTypes();
    }

    public initDatasetSourceTypes (): void {
        this.sourceTypeArray = [...DatasetSourceTypeMap.values()];
    }

}
