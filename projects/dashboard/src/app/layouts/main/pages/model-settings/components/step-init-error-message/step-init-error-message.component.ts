import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';

@Component({
    selector: 'app-step-init-error-message',
    templateUrl: './step-init-error-message.component.html',
    styleUrls: ['./step-init-error-message.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class StepInitErrorMessageComponent implements OnInit {

    constructor () {}

    public ngOnInit (): void {}

}
