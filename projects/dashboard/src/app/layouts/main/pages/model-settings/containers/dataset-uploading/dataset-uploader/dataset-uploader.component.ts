import { Component, OnInit, ChangeDetectionStrategy, Input, Output, EventEmitter } from '@angular/core';
import { DatasetSourceType } from '@Dashboard/features/dataset';
import { IsObject } from 'class-validator';
import { ClassValidation } from '@Shared/decorators';

@ClassValidation()
@Component({
    selector: 'app-dataset-uploader',
    templateUrl: './dataset-uploader.component.html',
    styleUrls: ['./dataset-uploader.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class DatasetUploaderComponent implements OnInit {

    @Input()
    @IsObject()
    public sourceType: DatasetSourceType;

    @Output()
    public OnFilesSelected = new EventEmitter<Blob[]>();

    constructor () {}

    public ngOnInit (): void {}

}
