import { Component, OnInit, ChangeDetectionStrategy, ViewChild, Injector } from '@angular/core';
import { TagListComponent } from './tag-list/tag-list.component';
import { Tag } from '@Dashboard/features/tag';
import { ViewTag } from '@Dashboard/view-features';
import { BaseModelStep } from '../../classes/base-model-step.class';

@Component({
    selector: 'app-defining-tags',
    templateUrl: './defining-tags.component.html',
    styleUrls: ['./defining-tags.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class DefiningTagsComponent extends BaseModelStep implements OnInit {

    public tags: ViewTag[];

    @ViewChild(TagListComponent, { static: true })
    private readonly _tagListComponent: TagListComponent;

    constructor (
        readonly injector: Injector
    ) {
        super(injector);
    }

    public ngOnInit (): void {
        super.ngOnInit();
    }

    public onTagCreation (tag: Tag): void {
        this._tagListComponent.createTag(tag);
    }

    public onTagsUpdated (tags: ViewTag[]): void {
        this.tags = tags;

        this.changeDetector.detectChanges();
    }

}
