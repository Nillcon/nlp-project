import { Component, OnInit, ChangeDetectionStrategy, Injector } from '@angular/core';
import { ModelWord } from '@Dashboard/features/dataset';
import { DialogService } from 'primeng-lts/api';
import { WordLemmatizationWindowComponent } from './word-lemmatization-window/word-lemmatization-window.component';
import { NeedsConfirmation } from '@Shared/modules/confirmation-window';
import { WordPartOfSpeechWindowComponent } from './word-part-of-speech-window/word-part-of-speech-window.component';
import { Observable } from 'rxjs';
import { BaseModelStep } from '../../classes/base-model-step.class';

@Component({
    selector: 'app-lemmatization-and-stop-words',
    templateUrl: './lemmatization-and-stop-words.component.html',
    styleUrls: ['./lemmatization-and-stop-words.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class LemmatizationAndStopWordsComponent extends BaseModelStep implements OnInit {

    public words: ModelWord[];

    constructor (
        readonly injector: Injector,
        private readonly dialogService: DialogService
    ) {
        super(injector);
    }

    public ngOnInit (): void {
        super.ngOnInit();

        this.updateWords();
    }

    public updateWords (): void {
        this.modelFacade.getWords(this.modelId)
            .subscribe((words) => {
                this.words = words;

                this.changeDetector.markForCheck();
            });
    }

    public onLemmatizeButtonClick (word: ModelWord): void {
        this.openLemmatizationWindow(word);
    }

    public onPartOfSpeechButtonClick (word: ModelWord): void {
        this.openPartOfSpeechWindow(word);
    }

    @NeedsConfirmation('Do you really want to mark this word as "stop-word"?', 'Mark as stop word')
    public onStopWordButtonClick (word: ModelWord): void {
        if (word.isStopWord) {
            this.modelFacade.deleteStopWord(this.modelId, word.value)
                .subscribe();
        } else {
            this.modelFacade.deleteStopWord(this.modelId, word.value)
                .subscribe();
        }
    }

    private openLemmatizationWindow (word: ModelWord): Observable<any> {
        return this.dialogService.open(WordLemmatizationWindowComponent, {
            width: '350px',
            header: 'Lemmatize words',
            data: word.value
        }).onClose;
    }

    private openPartOfSpeechWindow (word: ModelWord): Observable<any> {
        return this.dialogService.open(WordPartOfSpeechWindowComponent, {
            width: '80%',
            header: `Set Part of Speech for word "${word.value}"`,
            data: word
        }).onClose;
    }

}
