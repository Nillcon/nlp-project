import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ModelTopicsComponent } from './model-topics.component';

const routes: Routes = [
    {
        path: '',
        component: ModelTopicsComponent
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(routes)
    ],
    exports: [
        RouterModule
    ]
})
export class ModelTopicsRoutingModule {}
