import { Component, OnInit, ChangeDetectionStrategy, ChangeDetectorRef, OnDestroy } from '@angular/core';
import { ModelTrainingWsService } from '@Dashboard/features/model';
import { Observable, Subscription } from 'rxjs';
import { DynamicDialogRef } from 'primeng-lts/api';
import { AutoUnsubscribe } from 'ngx-auto-unsubscribe';
import { delay, tap } from 'rxjs/operators';
import { trigger, transition, useAnimation } from '@angular/animations';
import { rotateIn } from 'ng-animate';

@AutoUnsubscribe()
@Component({
    selector: 'app-training-progress-window',
    templateUrl: './training-progress-window.component.html',
    styleUrls: ['./training-progress-window.component.scss'],
    animations: [
        trigger('trainingCompleteAnimation', [
            transition(':enter', useAnimation(rotateIn, { params: { timing: 0.35, } }))
        ])
    ],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class TrainingProgressWindowComponent implements OnInit, OnDestroy {

    public progressValue$: Observable<number>;
    public progressValue: number = 0;

    public isTrainingComplete = false;

    private trainingCompleteSubscription: Subscription;

    constructor (
        private readonly changeDetector: ChangeDetectorRef,
        private readonly modelTrainingWsService: ModelTrainingWsService,
        private readonly dialogRef: DynamicDialogRef
    ) {}

    public ngOnInit (): void {
        this.initProgressValueObservable();
        this.initOnTrainingCompleteObserver();
    }

    public ngOnDestroy (): void {}

    private initProgressValueObservable (): void {
        this.progressValue$ = this.modelTrainingWsService.progress$;
    }

    private initOnTrainingCompleteObserver (): void {
        this.trainingCompleteSubscription = this.modelTrainingWsService.onTrainingEnded$
            .pipe(
                delay(600),
                tap(() => {
                    this.isTrainingComplete = true;

                    this.changeDetector.markForCheck();
                }),
                delay(1500)
            )
            .subscribe((trainingResult) => {
                this.dialogRef.close(trainingResult);
            });
    }

}
