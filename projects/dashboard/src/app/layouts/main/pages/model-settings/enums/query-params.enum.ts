export enum ModelSettingsQueryParamEnum {
    modelId             = 'id',
    modelType           = 'modelType',
    datasetSourceType   = 'datasetSourceType',
    isTemplate          = 'isTemplate'
}
