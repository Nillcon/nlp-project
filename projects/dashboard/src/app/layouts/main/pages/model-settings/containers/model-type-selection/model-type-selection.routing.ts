import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ModelTypeSelectionComponent } from './model-type-selection.component';

const routes: Routes = [
    {
        path: '',
        component: ModelTypeSelectionComponent
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(routes)
    ],
    exports: [
        RouterModule
    ]
})
export class ModelTypeSelectionRoutingModule {}
