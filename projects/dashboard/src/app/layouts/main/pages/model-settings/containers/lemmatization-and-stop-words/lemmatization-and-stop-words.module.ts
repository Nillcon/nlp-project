import { NgModule } from '@angular/core';
import { LemmatizationAndStopWordsComponent } from './lemmatization-and-stop-words.component';
import { SharedModule } from '@Dashboard/app-shared.module';
import { FeaturesModule } from '@Dashboard/features/features.module';
import { ModelSettingsPageSharedModule } from '../../model-settings-shared.module';
import { LemmatizationAndStopWordsRoutingModule } from './lemmatization-and-stop-words.routing';
import { WordLemmatizationWindowComponent } from './word-lemmatization-window/word-lemmatization-window.component';
import { WordPartOfSpeechWindowComponent } from './word-part-of-speech-window/word-part-of-speech-window.component';

@NgModule({
    declarations: [
        LemmatizationAndStopWordsComponent,
        WordLemmatizationWindowComponent,
        WordPartOfSpeechWindowComponent
    ],
    imports: [
        LemmatizationAndStopWordsRoutingModule,

        SharedModule,
        FeaturesModule,
        ModelSettingsPageSharedModule
    ],
    entryComponents: [
        WordLemmatizationWindowComponent,
        WordPartOfSpeechWindowComponent
    ]
})
export class LemmatizationAndStopWordsContainerModule {}
