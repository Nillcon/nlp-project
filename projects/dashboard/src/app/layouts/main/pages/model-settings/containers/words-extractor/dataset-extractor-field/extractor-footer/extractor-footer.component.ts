import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';

@Component({
    selector: 'app-extractor-footer',
    templateUrl: './extractor-footer.component.html',
    styleUrls: ['./extractor-footer.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class ExtractorFooterComponent implements OnInit {

    constructor () {}

    public ngOnInit (): void {}

}
