import { NgModule } from '@angular/core';
import { SecurityComponent } from './security.component';
import { SecurityContainerRoutingModule } from './security.routing';
import { SharedModule } from '@Dashboard/app-shared.module';
import { FeaturesModule } from '@Dashboard/features/features.module';

@NgModule({
    declarations: [
        SecurityComponent
    ],
    imports: [
        SharedModule,
        FeaturesModule,
        SecurityContainerRoutingModule
    ]
})
export class SecurityModule {}
