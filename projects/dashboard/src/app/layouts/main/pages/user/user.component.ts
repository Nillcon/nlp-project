import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { NavigationService } from '@Shared/features/navigation/navigation.service';

@Component({
    selector: 'app-user',
    templateUrl: './user.component.html',
    styleUrls: ['./user.component.scss'],
    providers: [NavigationService],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class UserPageComponent implements OnInit {

    constructor (
        private readonly navigationService: NavigationService
    ) {}

    public ngOnInit (): void {}

    public onTabClick (containerName: string): void {
        this.navigationService.navigate({ subUrl: containerName });
    }

}
