import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UserPageComponent } from './user.component';
import { UserContainerEnum } from './enums/user-container.enum';

const routes: Routes = [
    {
        path: '',
        component: UserPageComponent,
        children: [
            {
                path: UserContainerEnum.Info,
                loadChildren: () => import('./containers/info/info.module').then((m) => m.InfoModule)
            },
            {
                path: UserContainerEnum.Security,
                loadChildren: () => import('./containers/security/security.module').then((m) => m.SecurityModule)
            },
            {
                path: '**',
                redirectTo: UserContainerEnum.Info
            }
        ]
    },
    {
        path: '**',
        redirectTo: ''
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(routes)
    ],
    exports: [
        RouterModule
    ]
})
export class UserRoutingModule {}
