import { Component, OnInit, ChangeDetectionStrategy, Output, EventEmitter } from '@angular/core';
import { MenuItem } from 'primeng-lts/api';
import { UserContainerEnum } from '../../enums/user-container.enum';
import { Router } from '@angular/router';

@Component({
    selector: 'app-tabs',
    templateUrl: './tabs.component.html',
    styleUrls: ['./tabs.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class TabsComponent implements OnInit {

    public tabs: MenuItem[];

    public activeTab: MenuItem;

    @Output()
    public OnTabClick = new EventEmitter<string>();

    constructor (
        private readonly router: Router
    ) {}

    public ngOnInit (): void {
        this.initTabItems();
        this.initActiveTabItem();
    }

    public initTabItems (): void {
        this.tabs = Object.values(UserContainerEnum)
            .map((currContainer) => <MenuItem>{
                label: currContainer,
                command: () => this.OnTabClick.emit(currContainer)
            });
    }

    public initActiveTabItem (): void {
        this.activeTab = this.getActiveTabItem();
    }

    private getActiveTabItem (): MenuItem {
        const parsedUrl   = this.router.parseUrl(this.router.url);
        const urlSegments = parsedUrl.root.children.primary.segments;
        const targetUrlSegment = urlSegments[urlSegments.length - 1];

        for (const currTab of this.tabs) {
            if (targetUrlSegment.path === currTab.label) {
                return currTab;
            }
        }
    }

}
