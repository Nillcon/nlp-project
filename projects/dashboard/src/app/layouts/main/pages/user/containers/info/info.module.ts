import { NgModule } from '@angular/core';
import { InfoComponent } from './info.component';
import { InfoContainerRoutingModule } from './info.routing';
import { SharedModule } from '@Dashboard/app-shared.module';
import { FeaturesModule } from '@Dashboard/features/features.module';

@NgModule({
    declarations: [
        InfoComponent
    ],
    imports: [
        SharedModule,
        FeaturesModule,
        InfoContainerRoutingModule
    ]
})
export class InfoModule {}
