import { NgModule } from '@angular/core';
import { UserPageComponent } from './user.component';
import { UserRoutingModule } from './user.routing';
import { FeaturesModule } from '@Dashboard/features/features.module';
import { TabsComponent } from './components/tabs/tabs.component';
import { MainLayoutSharedModule } from '../../main-layout-shared.module';

@NgModule({
    declarations: [
        UserPageComponent,
        TabsComponent
    ],
    imports: [
        UserRoutingModule,
        FeaturesModule,
        MainLayoutSharedModule
    ]
})
export class UserPageModule {}
