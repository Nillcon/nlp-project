import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { UserEditingForm, User } from '@Dashboard/features/user';
import { UserFacade } from '@Dashboard/view-features/user';
import { tap } from 'rxjs/operators';
import { Subscription } from 'rxjs';
import { FormGroupTypeSafe } from 'form-type-safe';
import { ToastService } from '@Shared/features/toast';

@Component({
    selector: 'app-info',
    templateUrl: './info.component.html',
    styleUrls: ['./info.component.scss'],
    providers: [UserFacade],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class InfoComponent implements OnInit {

    public formData: UserEditingForm;

    public formGroup: FormGroupTypeSafe<UserEditingForm>;

    public saveSubscription: Subscription;

    constructor (
        private readonly toastService: ToastService,
        private readonly userFacade: UserFacade
    ) {}

    public ngOnInit (): void {
        this.initUserEditingFormObservable();
    }

    public initUserEditingFormObservable (): void {
        this.userFacade.data$
            .pipe(
                tap((userData) => {
                    this.formData = {
                        login: userData.login,
                        name: userData.name,
                        email: userData.email,
                        phone: userData.phone
                    };
                })
            )
            .subscribe();
    }

    public onFormGroupInit (formGroup: FormGroupTypeSafe<UserEditingForm>): void {
        this.formGroup = formGroup;
    }

    public onSaveButtonClick (): void {
        if (this.formGroup.valid) {
            this.saveSubscription = this.userFacade.editLocalUser(this.formGroup.value as Partial<User>)
                .subscribe();
        } else {
            this.toastService.error({
                description: `Form isn't valid!`
            });
        }
    }

}
