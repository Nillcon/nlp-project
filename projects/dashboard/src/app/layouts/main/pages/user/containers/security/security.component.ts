import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { FormGroupTypeSafe } from 'form-type-safe';
import { UserChangingPasswordForm } from '@Dashboard/features/user';
import { Subscription } from 'rxjs';
import { ToastService } from '@Shared/features/toast';
import { UserFacade } from '@Dashboard/view-features/user';

@Component({
    selector: 'app-security',
    templateUrl: './security.component.html',
    styleUrls: ['./security.component.scss'],
    providers: [UserFacade],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class SecurityComponent implements OnInit {

    public formGroup: FormGroupTypeSafe<UserChangingPasswordForm>;

    public saveSubscription: Subscription;

    constructor (
        private readonly toastService: ToastService,
        private readonly userFacade: UserFacade
    ) {}

    public ngOnInit (): void {}

    public onFormGroupInit (formGroup: FormGroupTypeSafe<UserChangingPasswordForm>): void {
        this.formGroup = formGroup;
    }

    public onSaveButtonClick (): void {
        if (this.formGroup.valid) {
            this.saveSubscription = this.userFacade.changePassword(this.formGroup.value)
                .subscribe();
        } else {
            this.toastService.error({
                description: `Form isn't valid!`
            });
        }
    }

}
