import { MainLayoutPageEnum } from '../shared/main-layout-page.enum';

export interface MainLayoutPage {
    id: MainLayoutPageEnum;
    name: string;
    iconClass: string;
}
