import { NgModule } from '@angular/core';
import { MainLayoutComponent } from './main-layout.component';
import { MainLayoutRoutingModule } from './main-layout.routing';
import { ContainerComponent } from './components/container/container.component';
import { SharedModule } from '@Dashboard/app-shared.module';
import { FeaturesModule } from '@Dashboard/features/features.module';
import { MainLayoutSharedModule } from './main-layout-shared.module';
import { ModelStateService } from '@Dashboard/features/model/services/model-state.service';
import { TemplateStateService } from '@Dashboard/features/template';
import { UserFacade } from '@Dashboard/view-features/user';
import { MainLayoutGuard } from './guards/main-layout.guard';
import { NavigationService } from '@Shared/features/navigation/navigation.service';
import { HeaderModule } from '@Dashboard/components/header/header.module';

@NgModule({
    declarations: [
        MainLayoutComponent,
        ContainerComponent
    ],
    imports: [
        MainLayoutRoutingModule,

        HeaderModule,
        FeaturesModule,
        SharedModule,
        MainLayoutSharedModule
    ],
    providers: [
        MainLayoutGuard,
        NavigationService,
        ModelStateService,
        TemplateStateService,
        UserFacade
    ]
})
export class MainLayoutModule {}
