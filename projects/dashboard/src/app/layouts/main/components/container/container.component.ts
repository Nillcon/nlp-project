import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';

@Component({
    selector: 'section-container',
    templateUrl: './container.component.html',
    styleUrls: ['./container.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class ContainerComponent implements OnInit {

    constructor () {}

    public ngOnInit (): void {}

}
