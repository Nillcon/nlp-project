import { Observable } from 'rxjs';
import { CanActivate } from '@angular/router';
import { ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { tap, mapTo } from 'rxjs/operators';
import { UserFacade } from '@Dashboard/view-features/user';
import { NavigationService } from '@Shared/features/navigation/navigation.service';
import { environment } from '@Dashboard/environment/environment';
import { AppLayoutEnum } from '@Dashboard/enums';

export class SignInLayoutGuard implements CanActivate {

    constructor (
        private readonly userFacade: UserFacade,
        private readonly navigationService: NavigationService
    ) {}

    public canActivate (route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
        return this.userFacade.isAuthorized$
            .pipe(
                tap((authState) => {
                    if (authState) {
                        this.navigationService.navigate({
                            subUrl: `/${AppLayoutEnum.Main}`
                        });
                    }
                }),
                mapTo(true)
            );
    }

}
