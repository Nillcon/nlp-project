import { NgModule } from '@angular/core';
import { SignInLayoutRoutingModule } from './sign-in.routing';
import { SignInLayoutComponent } from './sign-in.component';
import { SignInLayoutGuard } from './guards/sign-in-layout.guard';
import { NavigationService } from '@Shared/features/navigation/navigation.service';
import { SharedModule } from '@Dashboard/app-shared.module';

@NgModule({
    declarations: [
        SignInLayoutComponent
    ],
    imports: [
        SignInLayoutRoutingModule,

        SharedModule
    ],
    providers: [
        SignInLayoutGuard,
        NavigationService
    ]
})
export class SignInModule {}
