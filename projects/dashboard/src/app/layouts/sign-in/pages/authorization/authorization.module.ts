import { NgModule } from '@angular/core';
import { SharedModule } from '@Dashboard/app-shared.module';
import { FeaturesModule } from '@Dashboard/features/features.module';
import { AuthorizationPageComponent } from './authorization.component';
import { AuthorizationPageRoutingModule } from './authorization.routing';
import { UserFacade } from '@Dashboard/view-features/user';

@NgModule({
    declarations: [
        AuthorizationPageComponent
    ],
    imports: [
        AuthorizationPageRoutingModule,

        SharedModule,
        FeaturesModule
    ],
    providers: [
        UserFacade
    ]
})
export class AuthorizationModule {}
