import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthorizationPageComponent } from './authorization.component';

const routes: Routes = [
    {
        path: '',
        component: AuthorizationPageComponent
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(routes)
    ],
    exports: [
        RouterModule
    ]
})
export class AuthorizationPageRoutingModule {}
