import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { FormGroupTypeSafe } from 'form-type-safe';
import { SignInFormData } from '@Dashboard/features/user';
import { ToastService } from '@Shared/features/toast';
import { Subscription } from 'rxjs';
import { NavigationService } from '@Shared/features/navigation/navigation.service';
import { AppLayoutEnum } from '@Dashboard/enums/app-layout.enum';
import { UserFacade } from '@Dashboard/view-features/user';

@Component({
    selector: 'app-authorization',
    templateUrl: './authorization.component.html',
    styleUrls: ['./authorization.component.scss'],
    providers: [NavigationService],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class AuthorizationPageComponent implements OnInit {

    public authFormGroup: FormGroupTypeSafe<SignInFormData>;

    public authSubscription: Subscription;

    constructor (
        private readonly toastService: ToastService,
        private readonly navigationService: NavigationService,
        private readonly userFacade: UserFacade
    ) {}

    public ngOnInit (): void {}

    public onAuthFormGroupInit (formGroup: FormGroupTypeSafe<SignInFormData>): void {
        this.authFormGroup = formGroup;
    }

    public onSignInButtonClick (): void {
        if (this.authFormGroup.valid) {
            this.authSubscription = this.userFacade.signIn(this.authFormGroup.value)
                .subscribe(() => {
                    this.navigateToMainLayout();
                });
        } else {
            this.toastService.error({
                title: `Ooops...`,
                description: `Form isn't valid!`
            });
        }
    }

    private navigateToMainLayout (): void {
        this.navigationService.navigate({
            subUrl: `/${AppLayoutEnum.Main}`,
            relativeToCurrentPath: false
        });
    }

}
