import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SignInLayoutComponent } from './sign-in.component';
import { SignInLayoutGuard } from './guards/sign-in-layout.guard';

const routes: Routes = [
    {
        path: '',
        component: SignInLayoutComponent,
        loadChildren: () => import('./pages/authorization/authorization.module').then((m) => m.AuthorizationModule),
        canActivate: [SignInLayoutGuard]
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(routes)
    ],
    exports: [
        RouterModule
    ]
})
export class SignInLayoutRoutingModule {}
