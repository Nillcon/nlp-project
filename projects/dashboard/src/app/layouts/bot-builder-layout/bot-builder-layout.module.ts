import { NgModule } from '@angular/core';
import { BotBuilderLayoutRoutingModule } from './bot-builder-layout.routing';

import { BotBuilderLayoutComponent } from './bot-builder-layout.component';
import { SharedModule } from '@Dashboard/app-shared.module';
import { FeaturesModule } from '@Dashboard/features/features.module';
import { HeaderModule } from '@Dashboard/components/header/header.module';
import {
    HeaderComponent,
    HeaderLogoComponent,
    HeaderUserComponent,
    HeaderUserMenuComponent
} from './components/header';

@NgModule({
    declarations: [
        BotBuilderLayoutComponent,
        HeaderComponent,
        HeaderLogoComponent,
        HeaderUserComponent,
        HeaderUserMenuComponent
    ],
    imports: [
        HeaderModule,
        SharedModule,
        FeaturesModule,

        BotBuilderLayoutRoutingModule
    ]
})
export class BotBuilderLayoutModule {}
