import { Component, OnInit, ChangeDetectorRef, Input, ChangeDetectionStrategy, Injector, Output, EventEmitter } from '@angular/core';
import { FormGroupTypeSafe } from 'form-type-safe';

import { NeedsConfirmation } from '@Shared/modules/confirmation-window';
import { Node } from '@Dashboard/features/script/interfaces';
import { NodeTypeEnum } from '@Dashboard/features/script/enums';
import { ScriptFacade } from '@Dashboard/view-features/script';

@Component({
    selector: 'app-node-form',
    templateUrl: './node-form.component.html',
    styleUrls: ['./node-form.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class NodeFormComponent implements OnInit {

    public isOpen: boolean = true;

    @Input()
    public scriptId: number;

    @Input()
    public node: Node;

    @Output()
    public OnHiddenButtonClick: EventEmitter<boolean> = new EventEmitter();

    @Output()
    public OnActionComplete: EventEmitter<boolean> = new EventEmitter();

    public formGroup: FormGroupTypeSafe<Node>;

    public NodeTypeEnum = NodeTypeEnum;

    constructor (
        public readonly injector: Injector,
        private readonly changeDetectorRef: ChangeDetectorRef,
        private readonly scriptFacade: ScriptFacade
    ) { }

    public ngOnInit (): void {
    }

    public onFormInit (form: FormGroupTypeSafe<Node>): void {
        this.formGroup = form;

        this.changeDetectorRef.markForCheck();
    }

    public onCreateNodeButtonClick (): void {
        this.scriptFacade.createNode(this.scriptId, {
            ...this.formGroup.value,
            id: this.node.id,
            type: this.node.type
        })
            .subscribe(() => {
                this.OnActionComplete.emit(true);
            });
    }

    public onEditNodeButtonClick (): void {
        this.scriptFacade.editNode(this.scriptId, {
            ...this.formGroup.value,
            type: this.node.type
        })
            .subscribe(() => {
                this.OnActionComplete.emit(true);
            });
    }

    @NeedsConfirmation()
    public onDeleteNodeButtonClick (): void {
        console.log(this.node.id);
        this.scriptFacade.deleteNode(this.scriptId, +this.node.id)
            .subscribe(() => {
                this.OnActionComplete.emit(true);
            });
    }

    public onHiddenButtonClick (): void {
        this.OnHiddenButtonClick.emit(true);
        this.changeDetectorRef.markForCheck();
    }

}
