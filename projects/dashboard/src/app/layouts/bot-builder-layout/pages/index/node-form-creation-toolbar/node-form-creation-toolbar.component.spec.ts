import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NodeFormCreationToolbarComponent } from './node-form-creation-toolbar.component';

describe('NodeFormCreationToolbarComponent', () => {
  let component: NodeFormCreationToolbarComponent;
  let fixture: ComponentFixture<NodeFormCreationToolbarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NodeFormCreationToolbarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NodeFormCreationToolbarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
