import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateLinkWindowComponent } from './create-link-window.component';

describe('CreateLinkWindowComponent', () => {
  let component: CreateLinkWindowComponent;
  let fixture: ComponentFixture<CreateLinkWindowComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateLinkWindowComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateLinkWindowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
