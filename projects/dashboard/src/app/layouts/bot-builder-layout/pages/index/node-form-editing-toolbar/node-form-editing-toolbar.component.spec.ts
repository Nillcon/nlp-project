import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NodeFormEditingToolbarComponent } from './node-form-editing-toolbar.component';

describe('NodeFormEditingToolbarComponent', () => {
  let component: NodeFormEditingToolbarComponent;
  let fixture: ComponentFixture<NodeFormEditingToolbarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NodeFormEditingToolbarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NodeFormEditingToolbarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
