import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-node-form-creation-toolbar',
  templateUrl: './node-form-creation-toolbar.component.html',
  styleUrls: ['./node-form-creation-toolbar.component.scss']
})
export class NodeFormCreationToolbarComponent implements OnInit {

    @Output()
    public OnCreateButtonClick: EventEmitter<boolean> = new EventEmitter();

    constructor () { }

    public ngOnInit (): void {
    }

    public onCreateButtonClick (): void {
        this.OnCreateButtonClick.emit();
    }
}
