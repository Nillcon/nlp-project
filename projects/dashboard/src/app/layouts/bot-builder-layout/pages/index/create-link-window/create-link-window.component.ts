import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { DynamicDialogRef, DynamicDialogConfig } from 'primeng-lts/api';
import { GraphNode } from '@Dashboard/features/script/interfaces';

@Component({
    selector: 'app-create-link-window',
    templateUrl: './create-link-window.component.html',
    styleUrls: ['./create-link-window.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class CreateLinkWindowComponent implements OnInit {

    public nodes: GraphNode[];

    public selectedNode: GraphNode;

    constructor (
        public ref: DynamicDialogRef,
        private readonly config: DynamicDialogConfig,
    ) {
        this.nodes = config.data;
        this.selectedNode = config.data[0] || null;
    }

    public ngOnInit (): void {
    }

    public onCreateButtonClick (): void {
        this.ref.close(this.selectedNode);
    }

    public onCancelButtonClick (): void {
        this.ref.close(false);
    }

}
