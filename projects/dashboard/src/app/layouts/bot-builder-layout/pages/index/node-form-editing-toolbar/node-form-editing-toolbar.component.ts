import { Component, OnInit, EventEmitter, Output } from '@angular/core';

@Component({
    selector: 'app-node-form-editing-toolbar',
    templateUrl: './node-form-editing-toolbar.component.html',
    styleUrls: ['./node-form-editing-toolbar.component.scss']
})
export class NodeFormEditingToolbarComponent implements OnInit {

    @Output()
    public OnSaveButtonClick: EventEmitter<boolean> = new EventEmitter();

    @Output()
    public OnDeleteButtonClick: EventEmitter<boolean> = new EventEmitter();

    constructor () { }

    public ngOnInit (): void {
    }

    public onSaveButtonClick (): void {
		this.OnSaveButtonClick.emit();
	}

    public onDeleteButtonClick (): void {
		this.OnDeleteButtonClick.emit();
    }

}
