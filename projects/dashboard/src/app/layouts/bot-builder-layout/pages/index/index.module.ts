import { NgModule } from '@angular/core';
import { MainLayoutSharedModule } from '@Dashboard/layouts/main/main-layout-shared.module';
import { FeaturesModule } from '@Dashboard/features/features.module';
import { WindowChatModule } from '@Dashboard/modules/window-chat';

import { IndexRoutingModule } from './index.routing';

import { IndexComponent } from '../index/index.component';
import { AlgorithmSchemaComponent } from './algorithm-schema';
import { NodeFormCreationToolbarComponent } from './node-form-creation-toolbar';
import { NodeFormEditingToolbarComponent } from './node-form-editing-toolbar';
import { NodeFormComponent } from './node-form';
import { LoaderComponent } from './loader';
import { ToolbarComponent } from './toolbar';
import { CreateLinkWindowComponent } from './create-link-window';

@NgModule({
    declarations: [
        IndexComponent,
        AlgorithmSchemaComponent,
        NodeFormComponent,
        LoaderComponent,
        NodeFormCreationToolbarComponent,
        NodeFormEditingToolbarComponent,
        ToolbarComponent,
        CreateLinkWindowComponent
    ],
    imports: [
        MainLayoutSharedModule,
        FeaturesModule,
        IndexRoutingModule,

        WindowChatModule
    ],
    entryComponents: [
        CreateLinkWindowComponent
    ]
})
export class IndexModule {}
