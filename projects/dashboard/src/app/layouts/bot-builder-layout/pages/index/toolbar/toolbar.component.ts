import { Component, OnInit, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'app-toolbar',
  templateUrl: './toolbar.component.html',
  styleUrls: ['./toolbar.component.scss']
})
export class ToolbarComponent implements OnInit {

    @Output()
    public OnCreateActionNodeButtonClick: EventEmitter<boolean> = new EventEmitter();

    @Output()
    public OnCreateConditionNodeButtonClick: EventEmitter<boolean> = new EventEmitter();

    @Output()
    public OnCreateModelNodeButtonClick: EventEmitter<boolean> = new EventEmitter();

    constructor () { }

    public ngOnInit (): void {
    }

    public onCreateActionNodeButtonClick (): void {
        this.OnCreateActionNodeButtonClick.emit(true);
    }

    public onCreateConditionNodeButtonClick (): void {
        this.OnCreateConditionNodeButtonClick.emit(true);
    }

    public onCreateModelNodeButtonClick (): void {
        this.OnCreateModelNodeButtonClick.emit();
    }

}
