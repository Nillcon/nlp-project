import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AlgorithmSchemaComponent } from './algorithm-schema.component';

describe('AlgorithmSchemaComponent', () => {
  let component: AlgorithmSchemaComponent;
  let fixture: ComponentFixture<AlgorithmSchemaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AlgorithmSchemaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AlgorithmSchemaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
