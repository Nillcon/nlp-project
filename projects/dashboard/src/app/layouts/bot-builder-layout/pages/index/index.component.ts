import { Component, OnInit, ChangeDetectorRef, ChangeDetectionStrategy, Injector } from '@angular/core';
import { ScriptFacade } from '@Dashboard/view-features/script';
import { UrlService } from '@Shared/features';
import { Node, Tree } from '@Dashboard/features/script/interfaces';
import { NodeTypeEnum } from '@Dashboard/features/script/enums';
import { FormGroupTypeSafe } from 'form-type-safe';
import { trigger, transition, useAnimation } from '@angular/animations';
import { bounceInLeft, bounceOutRight } from 'ng-animate';
import { DialogService } from 'primeng-lts/api';

@Component({
    selector: 'app-index',
    templateUrl: './index.component.html',
    styleUrls: ['./index.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
    animations: [
        trigger('nodeFormAnimation', [
            transition(':enter', useAnimation(bounceInLeft, { params: { timing: 0.4 } })),
            transition(':leave', useAnimation(bounceOutRight, { params: { timing: 0.2 } }))
        ])
    ],
})
export class IndexComponent implements OnInit {

    public scriptId: number;
    public selectedNode: Node;

    public tree: Tree;

    // public isSelectedNodeExisted: boolean;
    // public isNodeFormHidden: boolean = true;
    public isLoading: boolean = false;

    public readonly chatIsOpen = false;

    public formGroup: FormGroupTypeSafe<Node>;

    constructor (
        public readonly injector: Injector,
        private readonly changeDetectorRef: ChangeDetectorRef,
        private readonly scriptFacade: ScriptFacade,
        private readonly urlService: UrlService
    ) { }

    public ngOnInit (): void {
        this.scriptId = +this.urlService.getParameter('id');

        this.updateTree();
    }

    public onHiddenFormButtonClick (): void {
        this.closeNodeForm();
    }

    public onNodeSelected (selectedNode: Node): void {
        this.turnOnLoader();
        this.closeNodeForm();

        this.scriptFacade.getNodeOfScript(this.scriptId, +selectedNode.id)
            .subscribe(node => {
                this.openNodeForm(node);

                this.changeDetectorRef.markForCheck();
            },
            () => {},
            () => {
               this.turnOffLoader();
            });
    }

    public onCreateActionNodeButtonClick (): void {
        // this.isSelectedNodeExisted = false;
        this.closeNodeForm();

        const node = {
            type: NodeTypeEnum.Action
        };

        this.openNodeForm(node);
    }

    public onCreateConditionNodeButtonClick (): void {
        // this.isSelectedNodeExisted = false;

        const node = {
            type: NodeTypeEnum.Condition,
        };

        this.openNodeForm(node);
    }

    public onCreateModelNodeButtonClick (): void {
        // this.isSelectedNodeExisted = false;
        this.closeNodeForm();

        const node = {
            type: NodeTypeEnum.Model
        };

        this.openNodeForm(node);
    }

    // public onNodeFormInit (formGroup: FormGroupTypeSafe<Node>): void {
    //     this.formGroup = formGroup;
    // }

    // public onCreateNodeButtonClick (): void {
    //     this.scriptFacade.createNode(this.scriptId, {
    //         ...this.formGroup.value,
    //         type: this.selectedNode.type
    //     })
    //         .subscribe(() => {
    //             this.closeNodeForm();
    //             this.updateTree();
    //         });
    // }

    // public onEditNodeButtonClick (): void {
    //     this.scriptFacade.editNode(this.scriptId, {
    //         ...this.formGroup.value,
    //         id: this.selectedNode.id,
    //         type: this.selectedNode.type
    //     })
    //         .subscribe(() => {
    //             this.closeNodeForm();
    //             this.updateTree();
    //         });
    // }

    // @NeedsConfirmation()
    // public onDeleteNodeButtonClick (): void {
    //     this.turnOnLoader();
    //     this.scriptFacade.deleteNode(this.scriptId, +this.selectedNode.id)
    //         .subscribe(() => {
    //             this.turnOffLoader();
    //             this.updateTree();
    //             this.closeNodeForm();
    //         });
    // }

    public onActionComplete (): void {
        this.updateTree();
        this.closeNodeForm();
    }

    private updateTree (): void {
        this.turnOnLoader();
        this.scriptFacade.getTree()
            .subscribe(tree => {
                this.tree = tree;
                this.changeDetectorRef.markForCheck();
            },
            () => {},
            () => {
               this.turnOffLoader();
            });
    }

    private openNodeForm (node: Node): void {
        this.selectedNode = node;
    }

    private closeNodeForm (): void {
        this.selectedNode = null;

        this.changeDetectorRef.markForCheck();
    }

    private turnOnLoader (): void {
        this.isLoading = true;
    }

    private turnOffLoader (): void {
        this.isLoading = false;
    }
}
