import { Component, OnInit, ChangeDetectionStrategy, EventEmitter, Output, Input } from '@angular/core';

import { Node, Edge } from '@swimlane/ngx-graph';
import { Tree, Link, GraphNode } from '@Dashboard/features/script/interfaces';
import { DialogService } from 'primeng-lts/api';
import { CreateLinkWindowComponent } from '../create-link-window';
import { filter, switchMap, map, tap } from 'rxjs/operators';
import { ScriptFacade } from '@Dashboard/view-features/script';
import { ToastService } from '@Shared/features/toast';

@Component({
    selector: 'app-algorithm-schema',
    templateUrl: './algorithm-schema.component.html',
    styleUrls: ['./algorithm-schema.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class AlgorithmSchemaComponent implements OnInit {

    @Input()
    public set tree (tree: Tree) {
        if (tree) {
            this.nodes = tree.nodes;
            this.edges = tree.edges;
        }
    }

    @Input()
    public selectedNode: Node;

    @Input()
    public scriptId: number;

    @Output()
    public OnNodeSelected: EventEmitter<Node> = new EventEmitter();

    public nodes: Node[];
    public edges: Edge[];

    constructor (
        private readonly dialog: DialogService,
        private readonly scriptFacade: ScriptFacade,
        private readonly toastService: ToastService
    ) { }

    public ngOnInit (): void {
    }

    public onNodeSelected (treeNode: Node): void {
        this.OnNodeSelected.emit(treeNode);
    }

    public onCreateLinkButtonClick (link: Link): void {
        const availableNodes: Node[] = this.getNodesWithoutOneById(link.sourceNodeId);

        const dialog = this.dialog.open(CreateLinkWindowComponent, {
            showHeader: false,
            dismissableMask: true,
            data: availableNodes
        });

        dialog.onClose
            .pipe(
                filter(selectedNode => !!selectedNode),
                tap(selectedNode => {
                    link.targetNodeId = selectedNode.id;
                }),
                switchMap(() => this.scriptFacade.createLink(this.scriptId, link))
            )
            .subscribe(() => {
                this.toastService.success({
                    description: 'Link successfuly created'
                });
            });
    }

    private getNodesWithoutOneById (id: string): Node[] {
        return this.nodes
            .filter(node => node.id !== id);
    }

}
