import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BotBuilderLayoutComponent } from './bot-builder-layout.component';
import { BotBuilderLayoutPageEnum } from './shared';

const routes: Routes = [
    {
        path: `${BotBuilderLayoutPageEnum.Index}`,
        component: BotBuilderLayoutComponent,
        loadChildren: () => import('./pages/index').then(m => m.IndexModule),
    },
    {
        path: '**',
        redirectTo: BotBuilderLayoutPageEnum.Index,
        pathMatch: 'full'
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(routes)
    ],
    exports: [
        RouterModule
    ]
})
export class BotBuilderLayoutRoutingModule {}
