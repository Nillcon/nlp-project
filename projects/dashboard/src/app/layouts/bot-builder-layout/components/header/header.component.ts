import { Component, OnInit, ViewChild, ChangeDetectionStrategy } from '@angular/core';
import { HeaderUserMenuComponent } from './header-user-menu/header-user-menu.component';
import { AppLayoutEnum } from '@Dashboard/enums/app-layout.enum';
import { MainLayoutPageEnum } from '@Dashboard/layouts/main/shared/main-layout-page.enum';
import { ActivatedRoute } from '@angular/router';
import { UrlService } from '@Shared/features';

@Component({
    selector: 'header',
    templateUrl: './header.component.html',
    styleUrls: ['./header.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class HeaderComponent implements OnInit {

    public scriptId: number;

    public readonly backLink: string = `/${AppLayoutEnum.Main}/${MainLayoutPageEnum.Script}`;

    @ViewChild(HeaderUserMenuComponent, { static: true })
    private readonly _userMenuComponent: HeaderUserMenuComponent;

    constructor (
        private readonly route: ActivatedRoute,
        private readonly urlService: UrlService
    ) {}

    public ngOnInit (): void {
        this.scriptId = +this.urlService.getParameter('id');
    }

    public onUserAvatarClick (event: MouseEvent): void {
        this._userMenuComponent.toggle(event);
    }

}
