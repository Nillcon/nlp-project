import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
    selector: 'app-bot-builder-layout',
    templateUrl: './bot-builder-layout.component.html',
    styleUrls: ['./bot-builder-layout.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class BotBuilderLayoutComponent implements OnInit {

    public scriptId: number;

    constructor (
        private readonly route: ActivatedRoute
    ) { }

    public ngOnInit (): void {
        this.scriptId = this.route.snapshot.params.id;
    }

}
