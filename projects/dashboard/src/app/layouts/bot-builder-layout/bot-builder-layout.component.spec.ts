import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BotBuilderLayoutComponent } from './bot-builder-layout.component';

describe('BotBuilderLayoutComponent', () => {
  let component: BotBuilderLayoutComponent;
  let fixture: ComponentFixture<BotBuilderLayoutComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BotBuilderLayoutComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BotBuilderLayoutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
