import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { AppLayoutEnum } from '@Dashboard/enums/app-layout.enum';

@Component({
    selector: 'app-not-found-layout',
    templateUrl: './not-found-layout.component.html',
    styleUrls: ['./not-found-layout.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class NotFoundLayoutComponent implements OnInit {

    public appLayoutEnum: typeof AppLayoutEnum = AppLayoutEnum;

    constructor () {}

    public ngOnInit (): void {}

}
