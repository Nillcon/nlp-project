import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { NotFoundLayoutComponent } from './not-found-layout.component';

const routes: Routes = [
    {
        path: '',
        component: NotFoundLayoutComponent
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(routes)
    ],
    exports: [
        RouterModule
    ]
})
export class NotFoundLayoutRoutingModule {}
