import { NgModule } from '@angular/core';
import { NotFoundLayoutComponent } from './not-found-layout.component';
import { NotFoundLayoutRoutingModule } from './not-found-layout.routing';
import { SharedModule } from '@Dashboard/app-shared.module';

@NgModule({
    declarations: [
        NotFoundLayoutComponent
    ],
    imports: [
        NotFoundLayoutRoutingModule,

        SharedModule
    ]
})
export class NotFoundLayoutModule {}
