import { UserFacade } from '@Dashboard/view-features/user';

export class AppConfig {

    constructor (
        private readonly userFacade: UserFacade
    ) {}

    public load (): any {
        return this.userFacade.update()
            .toPromise()
            .catch(() => {
                return true;
            });
    }

}
