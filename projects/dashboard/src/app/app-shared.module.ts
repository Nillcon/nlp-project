import { NgModule, ModuleWithProviders } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';

import { TitleComponent } from './components/title/title.component';

import { SharedModule as GlobalSharedModule } from '@Shared/shared.module';

import { AvatarModule } from 'ngx-avatar';

import { ButtonModule } from 'primeng-lts/button';
import { FileUploadModule } from 'primeng-lts/fileupload';
import { MenuModule } from 'primeng-lts/menu';
import { CardModule } from 'primeng-lts/card';
import { ChipsModule } from 'primeng-lts/chips';
import { TooltipModule } from 'primeng-lts/tooltip';
import { InputTextModule } from 'primeng-lts/inputtext';
import { PasswordModule } from 'primeng-lts/password';
import { InputTextareaModule } from 'primeng-lts/inputtextarea';
import { CheckboxModule } from 'primeng-lts/checkbox';
import { RadioButtonModule } from 'primeng-lts/radiobutton';
import { MessageModule } from 'primeng-lts/message';
import { ProgressSpinnerModule } from 'primeng-lts/progressspinner';
import { TableModule } from 'primeng-lts/table';
import { DialogModule } from 'primeng-lts/dialog';
import { DynamicDialogModule } from 'primeng-lts/dynamicdialog';
import { DialogService } from 'primeng-lts/api';
import { AutoCompleteModule } from 'primeng-lts/autocomplete';
import { ScrollPanelModule } from 'primeng-lts/scrollpanel';
import { TabMenuModule } from 'primeng-lts/tabmenu';
import { SelectButtonModule } from 'primeng-lts/selectbutton';
import { DropdownModule } from 'primeng-lts/dropdown';
import { ProgressBarModule } from 'primeng-lts/progressbar';
import { MultiSelectModule } from 'primeng-lts/multiselect';
import { ToolbarModule } from 'primeng-lts/toolbar';

@NgModule({
    declarations: [
        TitleComponent
    ],
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,

        AvatarModule,

        ButtonModule,
        FileUploadModule,
        MenuModule,
        CardModule,
        ChipsModule,
        TooltipModule,
        InputTextModule,
        PasswordModule,
        InputTextareaModule,
        CheckboxModule,
        RadioButtonModule,
        MessageModule,
        ProgressSpinnerModule,
        TableModule,
        DialogModule,
        DynamicDialogModule,
        AutoCompleteModule,
        ScrollPanelModule,
        TabMenuModule,
        SelectButtonModule,
        DropdownModule,
        MultiSelectModule,
        ProgressBarModule,
        ToolbarModule,

        GlobalSharedModule
    ],
    exports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,

        TitleComponent,

        AvatarModule,

        ButtonModule,
        FileUploadModule,
        MenuModule,
        CardModule,
        ChipsModule,
        TooltipModule,
        InputTextModule,
        PasswordModule,
        InputTextareaModule,
        CheckboxModule,
        RadioButtonModule,
        MessageModule,
        ProgressSpinnerModule,
        TableModule,
        DialogModule,
        DynamicDialogModule,
        AutoCompleteModule,
        ScrollPanelModule,
        TabMenuModule,
        SelectButtonModule,
        MultiSelectModule,
        DropdownModule,
        ToolbarModule,

        GlobalSharedModule
    ]
})
export class SharedModule {

    constructor () {}

    public static forRoot (): ModuleWithProviders {
        return {
            ngModule: SharedModule,
            providers: [
                DialogService
            ]
        };
    }

}
