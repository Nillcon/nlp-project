import { NgModule } from '@angular/core';
import { DatasetModule } from './dataset';
import { TagModule } from './tag';
import { UserModule } from './user';
import { ModelModule } from './model';
import { TemplateModule } from './template';
import { ScriptModule } from './script';
import { VariableModule } from './variable';

@NgModule({
    imports: [
        UserModule,
        DatasetModule,
        TagModule,
        ModelModule,
        TemplateModule,
        ScriptModule,
        VariableModule
    ],
    exports: [
        UserModule,
        DatasetModule,
        TagModule,
        ModelModule,
        TemplateModule,
        ScriptModule,
        VariableModule
    ]
})
export class FeaturesModule {}
