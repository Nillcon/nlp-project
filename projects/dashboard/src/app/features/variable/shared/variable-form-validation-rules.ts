import { Validators, ValidatorFn } from '@angular/forms';
import { Variable } from '../interfaces';

export const VariableFormValidationRules = new Map<keyof Variable, ValidatorFn[]>()
    .set(
        'name',
        [
            Validators.required,
        ]
    );

