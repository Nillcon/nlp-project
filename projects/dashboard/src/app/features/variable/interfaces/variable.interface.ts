export interface Variable {
    id?: number;
    name: string;
}
