import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { Variable } from '../../interfaces';

@Component({
  selector: 'variable-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.scss']
})
export class CardComponent implements OnInit {

    @Input()
    public variable: Variable;

    @Output()
    public OnEditButtonClick: EventEmitter<Variable> = new EventEmitter();

    @Output()
    public OnDeleteButtonClick: EventEmitter<Variable> = new EventEmitter();

    constructor () { }

    public ngOnInit (): void {
    }

    public onEditButtonClick (variable: Variable): void {
        this.OnEditButtonClick.emit(variable);
    }

    public onDeleteButtonClick (variable: Variable): void {
        this.OnDeleteButtonClick.emit(variable);
    }

}
