import { Component, OnInit, ChangeDetectionStrategy, OnDestroy, EventEmitter, Output, ViewChild, ElementRef } from '@angular/core';
import { Form, BaseForm } from '@Shared/features';
import { Variable } from '../../interfaces';
import { FormGroupTypeSafe, FormBuilderTypeSafe } from 'form-type-safe';
import { VariableFormValidationRules } from '../../shared';

@Form()
@Component({
    selector: 'variable-form-creation',
    templateUrl: './form-creation.component.html',
    styleUrls: ['./form-creation.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class FormCreationComponent implements OnInit, OnDestroy, BaseForm<Variable> {

    @Output()
    public OnFormInit: EventEmitter<FormGroupTypeSafe<Variable>> = new EventEmitter();

    @ViewChild('nameInput', {static: true})
    public nameInput: ElementRef;

    public formGroup: FormGroupTypeSafe<Variable>;

    constructor (
        private readonly fb: FormBuilderTypeSafe
    ) { }

    public ngOnInit (): void {
        this.nameInput.nativeElement.focus();
    }

    public ngOnDestroy (): void {}

    public formGroupInit (): FormGroupTypeSafe<Variable> {
        return this.fb.group<Variable>({
            name: this.fb.control('', VariableFormValidationRules.get('name'))
        });
    }

}
