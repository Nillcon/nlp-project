import { Component, OnInit, OnDestroy, EventEmitter, Output, Input, ElementRef, ViewChild } from '@angular/core';
import { Form, BaseForm } from '@Shared/features';
import { Variable } from '../../interfaces';
import { FormGroupTypeSafe, FormBuilderTypeSafe } from 'form-type-safe';
import { VariableFormValidationRules } from '../../shared';

@Form()
@Component({
  selector: 'variable-form-editing',
  templateUrl: './form-editing.component.html',
  styleUrls: ['./form-editing.component.scss']
})
export class FormEditingComponent implements OnInit, OnDestroy, BaseForm<Variable> {

    @Input()
    public inputData: Variable;

    @Output()
    public OnFormInit: EventEmitter<FormGroupTypeSafe<Variable>> = new EventEmitter();

    @ViewChild('nameInput', {static: true})
    public nameInput: ElementRef;

    public formGroup: FormGroupTypeSafe<Variable>;

    constructor (
        private readonly fb: FormBuilderTypeSafe
    ) { }

    public ngOnInit (): void {
        this.nameInput.nativeElement.focus();
    }

    public ngOnDestroy (): void {}

    public formGroupInit (inputData: Variable): FormGroupTypeSafe<Variable> {
        return this.fb.group<Variable>({
            name: this.fb.control(inputData?.name, VariableFormValidationRules.get('name'))
        });
    }

}
