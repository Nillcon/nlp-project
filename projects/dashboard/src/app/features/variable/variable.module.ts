import { NgModule } from '@angular/core';
import { SharedModule } from '@Dashboard/app-shared.module';

import {
    CardComponent,
    FormCreationComponent,
    FormEditingComponent
 } from './components';

@NgModule({
    declarations: [
        CardComponent,
        FormCreationComponent,
        FormEditingComponent
    ],
    imports: [
        SharedModule
    ],
    exports: [
        CardComponent,
        FormCreationComponent,
        FormEditingComponent
    ]
})
export class VariableModule {}
