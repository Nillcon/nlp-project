import { Injectable } from "@angular/core";
import { HttpRequestService } from '@Shared/features';
import { Observable } from 'rxjs';
import { Template } from '../interfaces/template.interface';

import {
    ModelTopicAnalyzerAnalysisData,
    ModelSentimentAnalysisData,
    ModelNerAnalysisData,
    ModelPredictionAnalysisData
} from '@Dashboard/features/model/interfaces';

@Injectable({
    providedIn: 'root'
})
export class TemplateApiService {

    constructor (
        private readonly httpService: HttpRequestService
    ) {}

    public getAll (): Observable<Template[]> {
        return this.httpService.get(`/Templates`);
    }

    public get (id: number): Observable<Template> {
        return this.httpService.get(`/Templates/${id}`);
    }

    public delete (id: number): Observable<any> {
        return this.httpService.delete(`/Templates/${id}`);
    }

    public markAsModel (id: number): Observable<any> {
        return this.httpService.put(`/Templates/${id}/MarkAsModel`);
    }

    public analyzeTopicAnalyzer (id: number, rawText: string): Observable<ModelTopicAnalyzerAnalysisData[]> {
        return this.httpService.post(`/Templates/${id}/TopicAnalyzer/Predict`, { value: rawText });
   }

   public analyzeSentiment (id: number, rawText: string): Observable<ModelSentimentAnalysisData> {
        return this.httpService.post(`/Templates/${id}/Sentiment/Predict`, { value: rawText });
   }

   public analyzeNer (id: number, rawText: string): Observable<ModelNerAnalysisData> {
       return this.httpService.post(`/Templates/${id}/NER/Predict`, { value: rawText });
   }

   public analyzePrediction (id: number, rawText: string): Observable<ModelPredictionAnalysisData> {
       return this.httpService.post(`/Templates/${id}/Prediction/Predict`, { value: rawText });
   }

}
