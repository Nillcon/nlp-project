import { Injectable } from "@angular/core";
import { TemplateApiService } from './template-api.service';
import { Observable } from 'rxjs';
import { Template } from '../interfaces/template.interface';

import {
    ModelTopicAnalyzerAnalysisData,
    ModelSentimentAnalysisData,
    ModelNerAnalysisData,
    ModelPredictionAnalysisData
} from '@Dashboard/features/model/interfaces';

@Injectable({
    providedIn: 'root'
})
export class TemplateService {

    constructor (
        private readonly templateApiService: TemplateApiService
    ) {}

    public getAll (): Observable<Template[]> {
        return this.templateApiService.getAll();
    }

    public get (id: number): Observable<Template> {
        return this.templateApiService.get(id);
    }

    public delete (id: number): Observable<any> {
        return this.templateApiService.delete(id);
    }

    public markAsModel (id: number): Observable<any> {
        return this.templateApiService.markAsModel(id);
    }

    public analyzeTopicAnalyzer (id: number, rawText: string): Observable<ModelTopicAnalyzerAnalysisData[]> {
        return this.templateApiService.analyzeTopicAnalyzer(id, rawText);
    }

    public analyzeSentiment (id: number, rawText: string): Observable<ModelSentimentAnalysisData> {
        return this.templateApiService.analyzeSentiment(id, rawText);
    }

    public analyzeNer (id: number, rawText: string): Observable<ModelNerAnalysisData> {
        return this.templateApiService.analyzeNer(id, rawText);
    }

    public analyzePrediction (id: number, rawText: string): Observable<ModelPredictionAnalysisData> {
        return this.templateApiService.analyzePrediction(id, rawText);
    }

}
