import { Injectable } from "@angular/core";
import { Observable, BehaviorSubject } from 'rxjs';
import { Template } from '../interfaces';

@Injectable({
    providedIn: 'root'
})
export class TemplateStateService {

    private readonly _templates$: BehaviorSubject<Template[]> = new BehaviorSubject([]);

    constructor () {}

    public getTemplates$ (): Observable<Template[]> {
        return this._templates$.asObservable();
    }

    public getTemplates (): Template[] {
        return this._templates$.getValue();
    }

    public getTemplate (id: number): Template {
        return this.getTemplates()
            .filter((template) => template.id === id)[0];
    }

    public setTemplates (templates: Template[]): void {
        this._templates$.next(templates);
    }

    public updateTemplate (template: Template): void {
        const templates = this.getTemplates();
        const targetTemplateId = templates
            .findIndex((currTemplate) => currTemplate.id === template.id);

        templates[targetTemplateId] = template;

        this._templates$.next([...templates]);
    }

    public updateTemplateId (templateToReplace: Template, addedTemplateWithId: Template): void {
        const templates = this.getTemplates();
        const targetTemplateId = templates
            .findIndex((currTemplate) => currTemplate === templateToReplace);

        templates[targetTemplateId] = addedTemplateWithId;

        this._templates$.next([...templates]);
    }

    public addTemplate (template: Partial<Template>): void {
        const templates = this.getTemplates() || [];

        this._templates$.next([...templates, template as Template]);
    }

    public editTemplate (template: Partial<Template>): void {
        const templates = this.getTemplates();
        const targetTemplateIndex = templates.findIndex((currTemplate) => currTemplate.id === template.id);

        templates[targetTemplateIndex] = { ...templates[targetTemplateIndex], ...template };

        this._templates$.next([...templates]);
    }

    public removeTemplate (templateId: number): void {
        const templates = this.getTemplates();
        const filteredTemplates = templates
            .filter((currTemplate) => currTemplate.id !== templateId);

        this._templates$.next(filteredTemplates);
    }

}
