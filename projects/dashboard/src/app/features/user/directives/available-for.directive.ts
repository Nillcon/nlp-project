import { ElementRef, Directive, TemplateRef, ViewContainerRef, OnDestroy, Input, OnInit } from "@angular/core";
import { Subscription } from "rxjs";
import { AutoUnsubscribe } from "ngx-auto-unsubscribe";
import { ElementRenderableFeatures } from "@Dashboard/helpers";
import { UserStateService } from '../services';
import { UserRoleEnum } from '../enums';
import { User } from '../interfaces';

@AutoUnsubscribe()
@Directive({
    selector: "[AvailableFor]"
})
export class AvailableForDirective extends ElementRenderableFeatures implements OnInit, OnDestroy {

    private userData: User;
    private userInfoSubscription: Subscription;

    @Input('AvailableFor')
    private readonly rolesList: UserRoleEnum[];

    constructor (
        public readonly element: ElementRef,
        public readonly templateRef: TemplateRef<any>,
        public readonly viewContainer: ViewContainerRef,
        private readonly userStateService: UserStateService
    ) {
        super(element, templateRef, viewContainer);
    }

    public ngOnInit (): void {
        this.userInfoSubscription = this.userStateService.data$
            .subscribe((data) => {
                this.userData = data;

                this.processElement();
            });
    }

    public ngOnDestroy (): void {}

    private processElement (): void {
        let isElementAvailable: boolean = false;

        if (this.rolesList) {
            isElementAvailable = this.rolesList.includes(this.userData.role);
        } else {
            isElementAvailable = true;
        }

        (isElementAvailable) ? this.showElement() : this.hideElement();
    }

}
