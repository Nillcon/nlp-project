import { NgModule } from '@angular/core';
import { SharedModule } from '@Dashboard/app-shared.module';
import { NgxMaskModule } from 'ngx-mask';

import {
    AvatarComponent,
    AuthorizationFormComponent,
    EditingFormComponent,
    ChangingPasswordFormComponent
} from './components';

import {
    AvailableForDirective,
    ForbiddenForDirective
} from './directives';

@NgModule({
    declarations: [
        AvailableForDirective,
        ForbiddenForDirective,

        AvatarComponent,
        AuthorizationFormComponent,
        EditingFormComponent,
        ChangingPasswordFormComponent
    ],
    imports: [
        SharedModule,
        NgxMaskModule.forRoot({
            validation: false,
        })
    ],
    exports: [
        AvailableForDirective,
        ForbiddenForDirective,

        AvatarComponent,
        AuthorizationFormComponent,
        EditingFormComponent,
        ChangingPasswordFormComponent
    ]
})
export class UserModule {}
