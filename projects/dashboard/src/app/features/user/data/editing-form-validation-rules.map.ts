import { Validators, ValidatorFn } from '@angular/forms';
import { UserEditingForm } from '../interfaces/editing-form.interface';

export const UserEditingFormValidationRules = new Map<keyof UserEditingForm, ValidatorFn[]>()
    .set(
        'login',
        [
            Validators.required,
            // Validators.minLength(2),
            // Validators.maxLength(40)
        ]
    )
    .set(
        'name',
        [
            Validators.required,
            Validators.maxLength(40)
        ]
    )
    .set(
        'email',
        [
            Validators.required,
            Validators.email
        ]
    )
    .set(
        'phone',
        [
            Validators.required,
            Validators.pattern("[0-9 ]{10}")
        ]
    );
