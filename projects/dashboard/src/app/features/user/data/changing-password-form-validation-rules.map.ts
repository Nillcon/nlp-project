import { Validators, ValidatorFn } from '@angular/forms';
import { UserChangingPasswordForm } from '../interfaces/changing-password-form.interface';

export const UserChangingPasswordFormValidationRules = new Map<keyof UserChangingPasswordForm, ValidatorFn[]>()
    .set(
        'password',
        [
            Validators.required
        ]
    );
