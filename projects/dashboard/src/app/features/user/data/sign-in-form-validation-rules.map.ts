import { SignInFormData } from '../interfaces';
import { ValidatorFn, Validators } from '@angular/forms';

export const SignInFormValidationRules = new Map<keyof SignInFormData, ValidatorFn[]>()
    .set(
        'login',
        [
            Validators.required
        ]
    )
    .set(
        'password',
        [
            Validators.required
        ]
    );
