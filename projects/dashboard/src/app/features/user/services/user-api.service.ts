import { Injectable } from '@angular/core';
import { HttpRequestService } from '@Shared/features';
import { Observable } from 'rxjs';
import { User, UserChangingPasswordForm, SignInFormData } from '../interfaces';

@Injectable({
    providedIn: 'root'
})
export class UserApiService  {

    constructor (
        private readonly httpService: HttpRequestService
    ) {}

    public getInfo (): Observable<User> {
        return this.httpService.get(`/Users/Info`);
    }

    public getAll (): Observable<User[]> {
        return this.httpService.get(`/Users`);
    }

    public get (id: number): Observable<User> {
        return this.httpService.get(`/Users/${id}`);
    }

    public edit (id: number, data: Partial<User>): Observable<any> {
        return this.httpService.put(`/Users/${id}`, data);
    }

    public changePassword (data: UserChangingPasswordForm): Observable<any> {
        return this.httpService.put(`/Users/ChangePassword`, data);
    }

    public signIn (data: SignInFormData): Observable<string> {
        return this.httpService.post(`/Users/SignIn`, data);
    }

}
