import { Injectable } from '@angular/core';
import { UserApiService } from './user-api.service';
import { Observable } from 'rxjs';
import { User, UserChangingPasswordForm, SignInFormData } from '../interfaces';

@Injectable({
    providedIn: 'root'
})
export class UserService {

    constructor (
        private readonly userApiService: UserApiService
    ) {}

    public getInfo (): Observable<User> {
        return this.userApiService.getInfo();
    }

    public getAll (): Observable<User[]> {
        return this.userApiService.getAll();
    }

    public get (id: number): Observable<User> {
        return this.userApiService.get(id);
    }

    public edit (id: number, data: Partial<User>): Observable<any> {
        return this.userApiService.edit(id, data);
    }

    public changePassword (data: UserChangingPasswordForm): Observable<any> {
        return this.userApiService.changePassword(data);
    }

    public signIn (data: SignInFormData): Observable<string> {
        return this.userApiService.signIn(data);
    }

}
