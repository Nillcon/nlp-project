import { UserRoleEnum } from '../enums/user-role.enum';

export interface User {
    id: number;
    name: string;
    login: string;
    email: string;
    phone: string;
    role: UserRoleEnum;
}
