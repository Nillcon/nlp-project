export interface UserChangingPasswordForm {
    password: string;
    confirmPassword: string;
}
