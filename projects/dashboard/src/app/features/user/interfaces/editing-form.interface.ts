export interface UserEditingForm {
    login: string;
    name: string;
    email: string;
    phone: string;
}
