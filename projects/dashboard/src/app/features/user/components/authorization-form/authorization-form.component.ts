import { Component, OnInit, ChangeDetectionStrategy, Output, EventEmitter, OnDestroy } from '@angular/core';
import { Form, BaseForm } from '@Shared/features';
import { SignInFormData } from '../../interfaces/sign-in-form-data.interface';
import { FormGroupTypeSafe, FormBuilderTypeSafe, FormControlTypeSafe } from 'form-type-safe';
import { SignInFormValidationRules } from '../../data/sign-in-form-validation-rules.map';

@Form()
@Component({
    selector: 'user-authorization-form',
    templateUrl: './authorization-form.component.html',
    styleUrls: ['./authorization-form.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class AuthorizationFormComponent implements OnInit, OnDestroy, BaseForm<SignInFormData> {

    @Output()
    public OnFormInit = new EventEmitter<FormGroupTypeSafe<SignInFormData>>();

    @Output()
    public OnEnterKeyDown = new EventEmitter<boolean>();

    public formGroup: FormGroupTypeSafe<SignInFormData>;

    public isPasswordVisible: boolean = false;

    constructor (
        private readonly formBuilder: FormBuilderTypeSafe
    ) {}

    public ngOnInit (): void {}

    public ngOnDestroy (): void {}

    public formGroupInit (): FormGroupTypeSafe<SignInFormData> {
        return this.formBuilder.group<SignInFormData>({
            login:    new FormControlTypeSafe('', SignInFormValidationRules.get('login')),
            password: new FormControlTypeSafe('', SignInFormValidationRules.get('password'))
        });
    }

    public onPasswordVisibilityButtonClick (): void {
        this.isPasswordVisible = !this.isPasswordVisible;
    }

    public onKeyDown (event: KeyboardEvent): void {
        if (event.code === 'Enter') {
            this.OnEnterKeyDown.emit(true);
        }
    }

}
