import { Component, OnInit, ChangeDetectionStrategy, OnDestroy, EventEmitter, Output } from '@angular/core';
import { Form, BaseForm } from '@Shared/features';
import { FormGroupTypeSafe, FormBuilderTypeSafe, FormControlTypeSafe } from 'form-type-safe';
import { UserChangingPasswordForm } from '../../interfaces/changing-password-form.interface';
import { UserChangingPasswordFormValidationRules } from '../../data/changing-password-form-validation-rules.map';

@Form()
@Component({
    selector: 'user-changing-password-form',
    templateUrl: './changing-password-form.component.html',
    styleUrls: ['./changing-password-form.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class ChangingPasswordFormComponent implements OnInit, OnDestroy, BaseForm<UserChangingPasswordForm> {

    public formGroup: FormGroupTypeSafe<UserChangingPasswordForm>;

    @Output()
    public OnFormInit = new EventEmitter<FormGroupTypeSafe<UserChangingPasswordForm>>();

    constructor (
        private readonly formBuilder: FormBuilderTypeSafe
    ) {}

    public ngOnInit (): void {}

    public ngOnDestroy (): void {}

    public formGroupInit (): FormGroupTypeSafe<UserChangingPasswordForm> {
        return this.formBuilder.group<UserChangingPasswordForm>({
            password: this.formBuilder.control('', UserChangingPasswordFormValidationRules.get('password')),
            confirmPassword: this.formBuilder.control('', [
                ...UserChangingPasswordFormValidationRules.get('password'),
                this.validateConfirmPassword.bind(this)
            ]),
        });
    }

    public validateConfirmPassword (control: FormControlTypeSafe<string>): { passwordsAreDifferent: boolean } {
        if (this.formGroup) {
            return (control.value === this.formGroup.controls.password.value) ? null : { passwordsAreDifferent: true };
        }

        return null;
    }

}
