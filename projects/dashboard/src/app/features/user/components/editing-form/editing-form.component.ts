import { Component, OnInit, EventEmitter, OnDestroy, ChangeDetectionStrategy, Output, Input } from '@angular/core';
import { Form, BaseForm } from '@Shared/features';
import { FormGroupTypeSafe, FormBuilderTypeSafe } from 'form-type-safe';
import { UserEditingForm } from '../../interfaces';
import { UserEditingFormValidationRules } from '../../data/editing-form-validation-rules.map';
import { IsObject } from 'class-validator';

@Form()
@Component({
    selector: 'user-editing-form',
    templateUrl: './editing-form.component.html',
    styleUrls: ['./editing-form.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class EditingFormComponent implements OnInit, OnDestroy, BaseForm<UserEditingForm> {

    @Input()
    @IsObject()
    public inputData: UserEditingForm;

    @Output()
    public OnFormInit: EventEmitter<FormGroupTypeSafe<UserEditingForm>> = new EventEmitter();

    public formGroup: FormGroupTypeSafe<UserEditingForm>;

    public readonly phonePrefix: string = '+1';

    constructor (
        private readonly formBuilder: FormBuilderTypeSafe
    ) {}

    public ngOnInit (): void {}

    public ngOnDestroy (): void {}

    public formGroupInit (data: UserEditingForm): FormGroupTypeSafe<UserEditingForm> {
        return this.formBuilder.group<UserEditingForm>({
            login: this.formBuilder.control(data.login, UserEditingFormValidationRules.get('login')),
            email: this.formBuilder.control(data.email, UserEditingFormValidationRules.get('email')),
            phone: this.formBuilder.control(data.phone, UserEditingFormValidationRules.get('phone')),
            name:  this.formBuilder.control(data.name, UserEditingFormValidationRules.get('name'))
        });
    }

}
