import { NgModule } from '@angular/core';
import { SharedModule } from '@Dashboard/app-shared.module';

import {
    TypeCardComponent,
    TopicAnalyzerSettingsFormComponent,
    CreationFormComponent,
    EditingFormComponent,
    CardComponent,
    TopicNameCardComponent,
    MultiselectorComponent,
    SentimentAnalysisBlockComponent,
    TopicAnalyzerAnalysisTableComponent
} from './components';

@NgModule({
    declarations: [
        CardComponent,
        CreationFormComponent,
        EditingFormComponent,
        TypeCardComponent,
        TopicAnalyzerSettingsFormComponent,
        TopicNameCardComponent,
        MultiselectorComponent,
        SentimentAnalysisBlockComponent,
        TopicAnalyzerAnalysisTableComponent
    ],
    imports: [
        SharedModule
    ],
    exports: [
        CardComponent,
        CreationFormComponent,
        EditingFormComponent,
        TypeCardComponent,
        TopicAnalyzerSettingsFormComponent,
        TopicNameCardComponent,
        MultiselectorComponent,
        SentimentAnalysisBlockComponent,
        TopicAnalyzerAnalysisTableComponent
    ]
})
export class ModelModule {}
