import { ModelTypeEnum } from '../enums/model-type.enum';
import { ModelType } from '../interfaces';
import { DatasetColumnTypeEnum } from '@Dashboard/features/dataset';

export const ModelTypeMap = new Map<ModelTypeEnum, ModelType>()
    .set(
        ModelTypeEnum.TopicAnalyzer,
        {
            id: ModelTypeEnum.TopicAnalyzer,
            name: 'Topic Analyzer',
            description: 'Some description text of model type',
            iconUrl: 'assets/icons/topic_analyzer-icon.png',
            datasetColumnTypes: [
                DatasetColumnTypeEnum.Value
            ]
        }
    )
    .set(
        ModelTypeEnum.Sentiment,
        {
            id: ModelTypeEnum.Sentiment,
            name: 'Sentiment',
            description: 'Some description text of model type',
            iconUrl: 'assets/icons/sentiment-icon.png',
            datasetColumnTypes: [
                DatasetColumnTypeEnum.Value,
                DatasetColumnTypeEnum.SentimentLabel
            ]
        }
    )
    .set(
        ModelTypeEnum.NER,
        {
            id: ModelTypeEnum.NER,
            name: 'Named-Entity Recognition',
            description: 'Some description text of model type',
            iconUrl: 'assets/icons/ner-icon.png',
            datasetColumnTypes: [
                DatasetColumnTypeEnum.Value
            ]
        }
    )
    .set(
        ModelTypeEnum.Prediction,
        {
            id: ModelTypeEnum.Prediction,
            name: 'Prediction',
            description: 'Some description text of model type',
            iconUrl: 'assets/icons/prediction-icon.png',
            datasetColumnTypes: [
                DatasetColumnTypeEnum.Value
            ]
        }
    );
