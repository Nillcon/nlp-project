import { ModelToolEnum } from '../enums';
import { TopicAnalyzerTool } from '../interfaces';

export const TopicAnalyzerToolMap = new Map<ModelToolEnum, TopicAnalyzerTool>()
    .set(
        ModelToolEnum.Gensim,
        {
            id: ModelToolEnum.Gensim,
            name: 'Gensim',
            description: 'Gensim is a Python library for topic modelling, document indexing and similarity retrieval with large corpora. Target audience is the natural language processing (NLP) and information retrieval (IR) community.',
            iconUrl: 'assets/icons/gensim-icon.png',
            isNumberOfTopicsAvailable: true
        }
    )
    .set(
        ModelToolEnum.BigARTM,
        {
            id: ModelToolEnum.BigARTM,
            name: 'BigARTM',
            description: 'BigARTM is a powerful tool for topic modeling based on a novel technique called Additive Regularization of Topic Models. This technique effectively builds multi-objective models by adding the weighted sums of regularizers to the optimization criterion.',
            iconUrl: 'assets/icons/bigARTM-icon.png',
            isNumberOfTopicsAvailable: true
        }
    );
