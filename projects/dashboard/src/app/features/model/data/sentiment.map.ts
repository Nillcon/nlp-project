import { SentimentEnum } from '../enums/sentiment.enum';
import { Sentiment } from '../interfaces/sentiment.interface';

export const SentimentMap = new Map<SentimentEnum, Sentiment>()
    .set(
        SentimentEnum.Negative,
        {
            id: SentimentEnum.Negative,
            name: 'Negative',
            color: '#c51c24',
            emojis: ['😡', '🤬', '😠']
        }
    )
    .set(
        SentimentEnum.Neutral,
        {
            id: SentimentEnum.Neutral,
            name: 'Neutral',
            color: '#ffcc00',
            emojis: ['😐', '😑', '😶']
        }
    )
    .set(
        SentimentEnum.Positive,
        {
            id: SentimentEnum.Positive,
            name: 'Positive',
            color: '#158f44',
            emojis: ['😊', '🙂', '😇', '😍', '😉']
        }
    );
