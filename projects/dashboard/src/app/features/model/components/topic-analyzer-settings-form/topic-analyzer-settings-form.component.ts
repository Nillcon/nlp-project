import { Component, OnInit, ChangeDetectionStrategy, OnDestroy, Output, EventEmitter } from '@angular/core';
import { Validators } from '@angular/forms';
import { BaseForm, Form } from '@Shared/features';
import { SelectItem } from 'primeng-lts/api';
import { FormGroupTypeSafe, FormBuilderTypeSafe } from 'form-type-safe';
import { TopicAnalyzerSettings, TopicAnalyzerTool } from '../../interfaces';
import { ModelToolEnum } from '../../enums';
import { TopicAnalyzerToolMap } from '../../data';

@Form()
@Component({
    selector: 'model-topic-analyzer-settings-form',
    templateUrl: './topic-analyzer-settings-form.component.html',
    styleUrls: ['./topic-analyzer-settings-form.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class TopicAnalyzerSettingsFormComponent implements OnInit, OnDestroy, BaseForm<TopicAnalyzerSettings> {

    @Output()
    public OnFormInit = new EventEmitter<FormGroupTypeSafe<TopicAnalyzerSettings>>();

    public formGroup: FormGroupTypeSafe<TopicAnalyzerSettings>;

    public modelLibrariesAsSelectItems: SelectItem[];

    constructor (
        private readonly formBuilder: FormBuilderTypeSafe
    ) {}

    public ngOnInit (): void {
        this.initModelLibraries();
    }

    public ngOnDestroy (): void {}

    public formGroupInit (): FormGroupTypeSafe<TopicAnalyzerSettings> {
        return this.formBuilder.group<TopicAnalyzerSettings>({
            toolId: this.formBuilder.control(ModelToolEnum.Gensim, [Validators.required]),
            numberTopics: this.formBuilder.control(0, [Validators.min(0)])
        });
    }

    public initModelLibraries (): void {
        const libraries = [...TopicAnalyzerToolMap.values()];
        const librariesForSelectButton: SelectItem[] = [];

        for (const currLibrary of libraries) {
            const data = <SelectItem>{
                label: currLibrary.name,
                title: currLibrary.name,
                value: currLibrary.id
            };

            librariesForSelectButton.push(data);
        }

        this.modelLibrariesAsSelectItems = librariesForSelectButton;
    }

    public getSelectedLibrary (): TopicAnalyzerTool {
        return TopicAnalyzerToolMap.get(
            this.formGroup.controls.toolId.value
        );
    }

}
