import { Component, OnInit, ChangeDetectionStrategy, OnDestroy, Output, EventEmitter, Input } from '@angular/core';
import { Validators } from '@angular/forms';
import { FormGroupTypeSafe, FormBuilderTypeSafe, FormControlTypeSafe } from 'form-type-safe';
import { Form, BaseForm } from '@Shared/features';
import { Model } from '../../interfaces';

@Form()
@Component({
    selector: 'model-editing-form',
    templateUrl: './editing-form.component.html',
    styleUrls: ['./editing-form.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class EditingFormComponent implements OnInit, OnDestroy, BaseForm<Partial<Model>> {

    @Input()
    public readonly inputData: Partial<Model>;

    @Output()
    public OnFormInit = new EventEmitter<FormGroupTypeSafe<Partial<Model>>>();

    public formGroup: FormGroupTypeSafe<Partial<Model>>;

    constructor (
        private readonly formBuilder: FormBuilderTypeSafe
    ) {}

    public ngOnInit (): void {}

    public ngOnDestroy (): void {}

    public formGroupInit (modelData: Partial<Model>): FormGroupTypeSafe<Partial<Model>> {
        const { name, description } = modelData;

        return this.formBuilder.group<Partial<Model>>({
            name: new FormControlTypeSafe(name, [Validators.required]),
            description: new FormControlTypeSafe(description)
        });
    }

}
