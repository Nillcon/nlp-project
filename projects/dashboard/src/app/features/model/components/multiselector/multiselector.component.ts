import { Component, OnInit, Input, Output, EventEmitter, ViewChild, ElementRef, ChangeDetectorRef, ChangeDetectionStrategy } from '@angular/core';
import { Model } from '../../interfaces';

@Component({
    selector: 'model-multiselector',
    templateUrl: './multiselector.component.html',
    styleUrls: ['./multiselector.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class MultiselectorComponent implements OnInit {

    @Input()
    public models: Model[];

    @Output()
    public OnSelectedModelsChanged: EventEmitter<Model[]> = new EventEmitter();

    @ViewChild('modelCardsWrapper', {static: true})
    private readonly scrollableBlock: ElementRef;

    public selectedModels: Model[] = [];

    private readonly scrollStep: number = 200;

    constructor (
        private readonly changeDetectorRef: ChangeDetectorRef
    ) { }

    public ngOnInit (): void {
    }

    public onArrowLeftClick (): void {
        this.scrollableBlock.nativeElement.scrollLeft -= this.scrollStep;
    }

    public onArrowRightClick (): void {
        this.scrollableBlock.nativeElement.scrollLeft += this.scrollStep;
        // this.changeDetectorRef.markForCheck();
    }

    public onModelSelect (model: Model): void {
        this.selectedModels.push(model);

        this.OnSelectedModelsChanged.emit(this.selectedModels);
    }

    public onModelUnselect (model: Model): void {
        this.selectedModels = this.selectedModels
            .filter(_model => model.id !== _model.id);

        this.OnSelectedModelsChanged.emit(this.selectedModels);
    }

}
