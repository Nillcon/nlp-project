import { Component, OnInit, ChangeDetectionStrategy, Input, Output, EventEmitter } from '@angular/core';
import { ModelType } from '../../interfaces';
import { IsObject, IsBoolean } from 'class-validator';
import { ClassValidation } from '@Shared/decorators';

@ClassValidation()
@Component({
    selector: 'model-type-card',
    templateUrl: './type-card.component.html',
    styleUrls: ['./type-card.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class TypeCardComponent implements OnInit {

    @Input()
    @IsBoolean()
    public isSelectable: boolean = false;

    @Input()
    @IsBoolean()
    public isSelected: boolean = false;

    @Input()
    @IsObject()
    public data: ModelType;

    @Output()
    public OnCardClick = new EventEmitter<MouseEvent>();

    constructor () {}

    public ngOnInit (): void {}

}
