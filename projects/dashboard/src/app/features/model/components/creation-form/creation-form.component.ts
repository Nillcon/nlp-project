import { Component, OnInit, ChangeDetectionStrategy, OnDestroy, Output, EventEmitter } from '@angular/core';
import { Validators } from '@angular/forms';
import { FormGroupTypeSafe, FormBuilderTypeSafe, FormControlTypeSafe } from 'form-type-safe';
import { Form, BaseForm } from '@Shared/features';
import { Model } from '../../interfaces';

@Form()
@Component({
    selector: 'model-creation-form',
    templateUrl: './creation-form.component.html',
    styleUrls: ['./creation-form.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class CreationFormComponent implements OnInit, OnDestroy, BaseForm<Partial<Model>> {

    @Output()
    public OnFormInit = new EventEmitter<FormGroupTypeSafe<Partial<Model>>>();

    public formGroup: FormGroupTypeSafe<Partial<Model>>;

    constructor (
        private readonly formBuilder: FormBuilderTypeSafe
    ) {}

    public ngOnInit (): void {}

    public ngOnDestroy (): void {}

    public formGroupInit (): FormGroupTypeSafe<Partial<Model>> {
        return this.formBuilder.group<Partial<Model>>({
            name: new FormControlTypeSafe('', [Validators.required]),
            description: new FormControlTypeSafe('')
        });
    }

}
