import { Component, OnInit, ChangeDetectionStrategy, Output, EventEmitter, Input } from '@angular/core';
import { IsObject, IsBoolean } from 'class-validator';
import { ClassValidation } from '@Shared/decorators';
import { ModelTopic } from '../../interfaces';

@ClassValidation()
@Component({
    selector: 'model-topic-name-card',
    templateUrl: './topic-name-card.component.html',
    styleUrls: ['./topic-name-card.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class TopicNameCardComponent implements OnInit {

    @Input()
    @IsObject()
    public data: ModelTopic;

    @Input()
    @IsBoolean()
    public isNeedToEmitDataOnInit = false;

    @Output()
    public OnTopicNameChange = new EventEmitter<string>();

    constructor () {}

    public ngOnInit (): void {
        if (this.isNeedToEmitDataOnInit) {
            this.OnTopicNameChange.emit(this.data.name);
        }
    }

}
