import { Component, OnInit, ChangeDetectionStrategy, Input } from '@angular/core';
import { IsObject } from 'class-validator';
import { ClassValidation } from '@Shared/decorators';
import { ModelSentimentAnalysisData } from '../../interfaces/sentiment-analysis-data.interface';
import { SentimentEnum } from '../../enums';
import { Sentiment } from '../../interfaces';
import { SentimentMap } from '../../data';

@ClassValidation()
@Component({
    selector: 'model-sentiment-analysis-block',
    templateUrl: './sentiment-analysis-block.component.html',
    styleUrls: ['./sentiment-analysis-block.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class SentimentAnalysisBlockComponent implements OnInit {

    @Input()
    public set analysisData (data: ModelSentimentAnalysisData) {
        this._analysisData = data;

        this.initSentimentData(data.sentimentTypeId);
    }

    public get analysisData (): ModelSentimentAnalysisData {
        return this._analysisData;
    }

    public sentimentData: Sentiment;

    @IsObject()
    private _analysisData: ModelSentimentAnalysisData;

    constructor () {}

    public ngOnInit (): void {}

    private initSentimentData (sentimentTypeId: SentimentEnum): void {
        this.sentimentData = SentimentMap.get(sentimentTypeId);
    }

}
