import { Component, OnInit, ChangeDetectionStrategy, Input, Output, EventEmitter } from '@angular/core';
import { Model, ModelType } from '../../interfaces';
import { IsObject, IsBoolean } from 'class-validator';
import { ClassValidation } from '@Shared/decorators';
import { ModelTypeMap } from '../../data/model-type.map';
import { ModelTypeEnum } from '../../enums/model-type.enum';

@ClassValidation()
@Component({
    selector: 'model-card',
    templateUrl: './card.component.html',
    styleUrls: ['./card.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class CardComponent implements OnInit {

    @Input()
    @IsObject()
    public set modelData (value: Model) {
        this._modelData = value;
        this.initModelType(value.modelType);
    }

    @Input()
    @IsBoolean()
    public isSelectable: boolean = false;

    @Input()
    @IsBoolean()
    public isClickable: boolean = false;

    @Input()
    @IsBoolean()
    public isSelected: boolean = false;

    @Output()
    public OnCardClick: EventEmitter<MouseEvent> = new EventEmitter();

    @Output()
    public OnCardSelect: EventEmitter<boolean> = new EventEmitter();

    @Output()
    public OnCardUnselect: EventEmitter<boolean> = new EventEmitter();

    public get modelData (): Model {
        return this._modelData;
    }

    public modelType: ModelType;

    private _modelData: Model;

    constructor () {}

    public ngOnInit (): void {}

    public onCardClick (event: MouseEvent): void {
        if (this.isSelectable) {
            this.isSelected = !this.isSelected;

            (this.isSelected) ? this.OnCardSelect.emit(true) : this.OnCardUnselect.emit(true);
        }

        this.OnCardClick.emit(event);
    }

    private initModelType (modelType: ModelTypeEnum): void {
        this.modelType = ModelTypeMap.get(modelType);
    }

}
