import { Component, OnInit, ChangeDetectionStrategy, Input } from '@angular/core';
import { IsArray } from 'class-validator';
import { ClassValidation } from '@Shared/decorators';
import { ModelTopicAnalyzerAnalysisData } from '../../interfaces';

@ClassValidation()
@Component({
    selector: 'model-topic-analyzer-analysis-table',
    templateUrl: './topic-analyzer-analysis-table.component.html',
    styleUrls: ['./topic-analyzer-analysis-table.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class TopicAnalyzerAnalysisTableComponent implements OnInit {

    @Input()
    @IsArray()
    public readonly analysisData: ModelTopicAnalyzerAnalysisData;

    constructor () {}

    public ngOnInit (): void {}

}
