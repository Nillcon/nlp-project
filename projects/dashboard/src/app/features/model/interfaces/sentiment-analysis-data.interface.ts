import { SentimentEnum } from '../enums';

export interface ModelSentimentAnalysisData {
    sentimentTypeId: SentimentEnum;
    confidencePercent: number;
}
