import { ModelTypeEnum } from '../enums/model-type.enum';
import { TopicAnalyzerSettings, SentimentSettings, NamedEntityRecognitionSettings } from './';
import { DatasetSourceTypeEnum } from '@Dashboard/features/dataset';

export interface Model {
    id: number;
    name: string;
    description: string;
    modelType: ModelTypeEnum;
    userId: number;
    datasetType: DatasetSourceTypeEnum;
    stepIndex: number;
    isTemplate: boolean;
    data: TopicAnalyzerSettings | SentimentSettings | NamedEntityRecognitionSettings;
}
