import { ModelToolEnum } from '../enums';

export interface TopicAnalyzerSettings {
    toolId: ModelToolEnum;
    numberTopics: number;
}
