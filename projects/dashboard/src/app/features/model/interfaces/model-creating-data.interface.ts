import { Model, TopicAnalyzerSettings, SentimentSettings, NamedEntityRecognitionSettings } from './';

export interface ModelCreatingData extends Partial<Model> {
    data: TopicAnalyzerSettings | SentimentSettings | NamedEntityRecognitionSettings;
}
