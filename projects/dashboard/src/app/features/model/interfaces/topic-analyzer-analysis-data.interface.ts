export interface ModelTopicAnalyzerAnalysisData {
    topicName: string;
    confidencePercent: number;
}
