import { ModelTypeEnum } from '../enums/model-type.enum';
import { DatasetColumnTypeEnum } from '@Dashboard/features/dataset';

export interface ModelType {
    id: ModelTypeEnum;
    name: string;
    description: string;
    iconUrl: string;
    datasetColumnTypes: DatasetColumnTypeEnum[];
}
