export interface ModelTopic {
    index: number;
    name: string;
    words: string[];
}
