import { SentimentEnum } from '../enums/sentiment.enum';

export interface Sentiment {
    id: SentimentEnum;
    name: string;
    color: string;
    emojis: string[];
}
