export interface SentimentTrainingResult {
    accuracyPercent: string;
}
