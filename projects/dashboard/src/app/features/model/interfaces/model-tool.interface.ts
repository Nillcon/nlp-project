export interface ModelTool {
    id: number;
    name: string;
    description: string;
    iconUrl: string;
}
