import { ModelTool } from '.';

export interface TopicAnalyzerTool extends ModelTool {
    isNumberOfTopicsAvailable: boolean;
}
