export enum SentimentEnum {
    Negative,
    Neutral,
    Positive
}
