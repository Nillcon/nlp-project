export enum ModelTypeEnum {
    TopicAnalyzer,
    Sentiment,
    NER,
    Prediction
}
