export enum ModelTrainingEnum {
    TrainingProcess = 'TrainingProcess',
    Progress = 'Progress',
    TrainingEnded = 'TrainingEnded'
}
