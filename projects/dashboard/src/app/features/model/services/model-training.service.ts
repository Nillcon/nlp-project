import { Injectable, OnDestroy } from '@angular/core';
import { HubConnectionBuilder, HubConnection } from "@aspnet/signalr";
import { HttpRequestService, LocalStorageService } from '@Shared/features';
import { StorageKey } from '@Dashboard/enums';
import { ModelTrainingEnum } from '../enums';
import { Observable, BehaviorSubject, Subject } from 'rxjs';
import { AutoUnsubscribe } from 'ngx-auto-unsubscribe';

@AutoUnsubscribe()
@Injectable({
    providedIn: 'root'
})
export class ModelTrainingWsService implements OnDestroy {

    public get isActivated (): boolean {
        return this._isActivated;
    }

    public get progress$ (): Observable<number> {
        return this._progress$.asObservable();
    }

    public get progress (): number {
        return this._progress$.getValue();
    }

    public get onTrainingProcess$ (): Observable<boolean> {
        return this._onTrainingProcess$.asObservable();
    }

    public get onTrainingEnded$ (): Observable<boolean> {
        return this._onTrainingEnded$.asObservable();
    }

    private stream: HubConnection;

    private _isActivated: boolean = false;

    private readonly _progress$: BehaviorSubject<number> = new BehaviorSubject(0);
    private readonly _onTrainingProcess$ = new BehaviorSubject<boolean>(false);
    private readonly _onTrainingEnded$ = new Subject<boolean>();

    private readonly host: string = `${this.httpService.host}/modelTraining`;

    private devProgressInterval: NodeJS.Timer;

    constructor (
        private readonly httpService: HttpRequestService,
        private readonly storageService: LocalStorageService
    ) {}

    public ngOnDestroy (): void {
        if (this.devProgressInterval) {
            clearInterval(this.devProgressInterval);
        }
    }

    public activate ({
        modelId = <number>null
    }): void {
        this.initStream();

        this._isActivated = true;
    }

    public deactivate (): void {
        this._progress$.next(0);
        this._onTrainingProcess$.next(false);
        this._onTrainingEnded$.next(true);

        this.stream.stop();
        this._isActivated = false;
    }

    public startTraining (): void {
        console.log('Training starting...');

        // FIXME: Remove dev code
        this._onTrainingProcess$.next(true);
        this.devRunProgressIncrease();
    }

    public startListen (): void {
        this.stream.start()
            .then(() => {
                console.log('Model training connection started!');

                this.initTrainingProcessObserver();
                this.initTrainingEndedObserver();
                this.initProgressObserver();
            });
    }

    private initTrainingProcessObserver (): void {
        this.stream.on(ModelTrainingEnum.TrainingProcess, (() => {
            this._onTrainingProcess$.next(true);
            this._onTrainingEnded$.next(false);
        }));
    }

    private initTrainingEndedObserver (): void {
        this.stream.on(ModelTrainingEnum.TrainingEnded, (() => {
            this._onTrainingProcess$.next(false);
            this._onTrainingEnded$.next(true);
        }));
    }

    private initProgressObserver (): void {
        this.stream.on(ModelTrainingEnum.Progress, ((progressValue: number) => {
            this._progress$.next(progressValue);
        }));
    }

    private initStream (): void {
        const token = this.storageService.get<string>(StorageKey.Access_Token);

        this.stream = new HubConnectionBuilder()
            .withUrl(this.host,
                {
                    accessTokenFactory: () => token
                }
            )
            .build();
    }

    // FIXME: Remove dev method
    private devRunProgressIncrease (): void {
        const intervalMs = 250;

        this._progress$.next(0);

        this.devProgressInterval = setInterval(() => {
            this._progress$.next(this.progress + 10);

            if (this.progress >= 100) {
                clearInterval(this.devProgressInterval);
                this._onTrainingProcess$.next(false);
                this._onTrainingEnded$.next(true);
            }
        }, intervalMs);
    }

}
