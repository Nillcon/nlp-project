import { Injectable } from "@angular/core";
import { HttpRequestService, HttpFileService } from '@Shared/features';
import { Observable, of } from 'rxjs';

import {
    Model,
    ModelCreatingData,
    ModelTopic,
    ModelTopicAnalyzerAnalysisData,
    ModelSentimentAnalysisData,
    ModelNerAnalysisData,
    ModelPredictionAnalysisData,
    SentimentTrainingResult
} from '../interfaces';

import {
    DatasetPreview,
    DatasetPreviewSettings,
    DatasetImportData,
    DatasetSourceTypeEnum,
    ModelWord,
    DatasetAssociationColumn
} from '@Dashboard/features/dataset';

@Injectable({
    providedIn: 'root'
})
export class ModelApiService {

    constructor (
        private readonly httpService: HttpRequestService,
        private readonly httpFileService: HttpFileService
    ) {}

    public getAll (): Observable<Model[]> {
        return this.httpService.get(`/NlpModels`);
    }

    public get (id: number): Observable<Model> {
        return this.httpService.get(`/NlpModels/${id}`);
    }

    public create (data: ModelCreatingData): Observable<number> {
        return this.httpService.post(`/NlpModels`, data);
    }

    public delete (id: number): Observable<any> {
        return this.httpService.delete(`/NlpModels/${id}`);
    }

    public rename (id: number, data: Partial<Model>): Observable<any> {
        return this.httpService.put(`/NlpModels/${id}/Rename`, data);
    }

    public markAsTemplate (id: number): Observable<any> {
        return this.httpService.put(`/NlpModels/${id}/MarkAsTemplate`);
    }

    public setStepIndex (id: number, stepIndex: number): Observable<any> {
        return this.httpService.put(`/NlpModels/${id}/SetStepIndex`, { stepIndex });
    }

    public getPreview (id: number, rowCount: number): Observable<DatasetPreview> {
        return this.httpService.get(`/NlpModels/${id}/Preview?rowCount=${rowCount}`);
    }

    public getColumnAssociation (id: number, columnIndex: number, sheetIndex: number): Observable<DatasetAssociationColumn> {
        return this.httpService.get(`/NlpModels/${id}/ColumnAssociation/${columnIndex}?sheetIndex=${sheetIndex}`);
    }

    public updateColumnAssociation (id: number, columnIndex: number, data: DatasetAssociationColumn): Observable<any> {
        return this.httpService.put(`/NlpModels/${id}/ColumnAssociation/${columnIndex}`, data);
    }

    public updatePreviewSettings (id: number, settingsData: Partial<DatasetPreviewSettings>): Observable<any> {
        return this.httpService.put(`/NlpModels/${id}/UpdateSettings`, settingsData);
    }

    public importDataset (id: number, data: DatasetImportData): Observable<any> {
        return this.httpService.post(`/NlpModels/${id}/Import`, data);
    }

    public uploadDataset (id: number, datasetType: DatasetSourceTypeEnum, files: Blob[]): Observable<any> {
        return this.httpFileService.uploadFile(`/NlpModels/${id}/UploadDataset/${datasetType}`, files);
    }

    public getWords (id: number): Observable<ModelWord[]> {
        return of([
            { id: 1, value: 'Coronaviruses', lemma: null, isStopWord: false },
            { id: 2, value: 'Said', lemma: 'Say', isStopWord: false },
            { id: 3, value: 'Nails', lemma: 'Nail', isStopWord: true }
        ]);
        // return this.httpService.get(`/NlpModels/${id}/Words`);
    }

    public createStopWord (id: number, word: string): Observable<any> {
        return this.httpService.post(`/NlpModels/${id}/StopWord/${word}`);
    }

    public deleteStopWord (id: number, word: string): Observable<any> {
        return this.httpService.delete(`/NlpModels/${id}/StopWord/${word}`);
    }

    public lemmatizeWord (id: number, word: string, lemma: string): Observable<any> {
        return this.httpService.put(`/NlpModels/${id}/Lemmatization`, { word, lemma });
    }

    public getTopics (id: number): Observable<ModelTopic[]> {
        return this.httpService.get(`/NlpModels/${id}/Topics`);
    }

    public renameTopic (id: number, data: Partial<ModelTopic>): Observable<any> {
        return this.httpService.put(`/NlpModels/${id}/Topic`, data);
    }

    /** Temp method. Will be websockets instead this */
    public training (id: number): Observable<any> {
        return this.httpService.post(`/NlpModels/${id}/Train`);
    }

    public getSentimentTrainingResult (id: number): Observable<SentimentTrainingResult> {
        return this.httpService.get(`/NlpModels/${id}/Sentiment/TrainingResult`);
    }

    public analyzeTopicAnalyzer (id: number, rawText: string): Observable<ModelTopicAnalyzerAnalysisData[]> {
         return this.httpService.post(`/NlpModels/${id}/TopicAnalyzer/Predict`, { value: rawText });
    }

    public analyzeSentiment (id: number, rawText: string): Observable<ModelSentimentAnalysisData> {
         return this.httpService.post(`/NlpModels/${id}/Sentiment/Predict`, { value: rawText });
    }

    public analyzeNer (id: number, rawText: string): Observable<ModelNerAnalysisData> {
        return this.httpService.post(`/NlpModels/${id}/NER/Predict`, { value: rawText });
    }

    public analyzePrediction (id: number, rawText: string): Observable<ModelPredictionAnalysisData> {
        return this.httpService.post(`/NlpModels/${id}/Prediction/Predict`, { value: rawText });
    }

}
