import { Injectable } from "@angular/core";
import { Observable, BehaviorSubject } from 'rxjs';
import { Model } from '../interfaces/model.interface';

@Injectable({
    providedIn: 'root'
})
export class ModelStateService {

    private readonly _models$: BehaviorSubject<Model[]> = new BehaviorSubject([]);

    constructor () {}

    public getModels$ (): Observable<Model[]> {
        return this._models$.asObservable();
    }

    public getModels (): Model[] {
        return this._models$.getValue();
    }

    public getModel (id: number): Model {
        return this.getModels()
            .filter((model) => model.id === id)[0];
    }

    public setModels (models: Model[]): void {
        this._models$.next(models);
    }

    public updateModel (model: Model): void {
        const models = this.getModels();
        const targetModelId = models
            .findIndex((currModel) => currModel.id === model.id);

        models[targetModelId] = model;

        this._models$.next([...models]);
    }

    public updateModelId (modelToReplace: Model, addedModelWithId: Model): void {
        const models = this.getModels();
        const targetModelId = models
            .findIndex((currModel) => currModel === modelToReplace);

        models[targetModelId] = addedModelWithId;

        this._models$.next([...models]);
    }

    public addModel (model: Partial<Model>): void {
        const models = this.getModels() || [];

        this._models$.next([...models, model as Model]);
    }

    public editModel (model: Partial<Model>): void {
        const models = this.getModels();
        const targetModelIndex = models.findIndex((currModel) => currModel.id === model.id);

        models[targetModelIndex] = { ...models[targetModelIndex], ...model };

        this._models$.next([...models]);
    }

    public removeModel (modelId: number): void {
        const models = this.getModels();
        const filteredModels = models
            .filter((currModel) => currModel.id !== modelId);

        this._models$.next(filteredModels);
    }

}
