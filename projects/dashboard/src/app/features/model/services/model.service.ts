import { Injectable } from "@angular/core";
import { ModelApiService } from './model-api.service';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import {
    Model,
    ModelCreatingData,
    ModelTopic,
    ModelTopicAnalyzerAnalysisData,
    ModelSentimentAnalysisData,
    ModelNerAnalysisData,
    ModelPredictionAnalysisData,
    SentimentTrainingResult
} from '../interfaces';

import {
    DatasetPreview,
    DatasetPreviewSettings,
    DatasetImportData,
    DatasetSourceTypeEnum,
    ModelWord,
    DatasetSheetData,
    DatasetAssociationColumn
} from '@Dashboard/features/dataset';

@Injectable({
    providedIn: 'root'
})
export class ModelService {

    constructor (
        private readonly modelApiService: ModelApiService
    ) {}

    public getAll (): Observable<Model[]> {
        return this.modelApiService.getAll();
    }

    public get (id: number): Observable<Model> {
        return this.modelApiService.get(id);
    }

    public create (data: ModelCreatingData): Observable<number> {
        return this.modelApiService.create(data);
    }

    public delete (id: number): Observable<any> {
        return this.modelApiService.delete(id);
    }

    public rename (id: number, data: Partial<Model>): Observable<any> {
        return this.modelApiService.rename(id, data);
    }

    public markAsTemplate (id: number): Observable<any> {
        return this.modelApiService.markAsTemplate(id);
    }

    public getPreview (id: number, rowCount: number): Observable<DatasetPreview> {
        return this.modelApiService.getPreview(id, rowCount)
            .pipe(
                map((previewModel) => {
                    const sheetDataMap = new Map<number, DatasetSheetData>();

                    Object.keys(previewModel.data)
                        .forEach((sheetIndex) => {
                            sheetDataMap.set(+sheetIndex, previewModel.data[sheetIndex]);
                        });

                    previewModel.data = sheetDataMap;

                    return previewModel;
                })
            );
    }

    public updatePreviewSettings (id: number, settingsData: Partial<DatasetPreviewSettings>): Observable<any> {
        return this.modelApiService.updatePreviewSettings(id, settingsData);
    }

    public getColumnAssociation (id: number, columnIndex: number, sheetIndex: number): Observable<DatasetAssociationColumn> {
        return this.modelApiService.getColumnAssociation(id, columnIndex, sheetIndex);
    }

    public updateColumnAssociation (id: number, columnIndex: number, data: DatasetAssociationColumn): Observable<any> {
        return this.modelApiService.updateColumnAssociation(id, columnIndex, data);
    }

    public uploadDataset (id: number, datasetType: DatasetSourceTypeEnum, files: Blob[]): Observable<any> {
        return this.modelApiService.uploadDataset(id, datasetType, files);
    }

    public importDataset (id: number, data: DatasetImportData): Observable<any> {
        return this.modelApiService.importDataset(id, data);
    }

    public setStepIndex (id: number, stepIndex: number): Observable<any> {
        return this.modelApiService.setStepIndex(id, stepIndex);
    }

    public getWords (id: number): Observable<ModelWord[]> {
        return this.modelApiService.getWords(id);
    }

    public createStopWord (id: number, word: string): Observable<any> {
        return this.modelApiService.createStopWord(id, word);
    }

    public deleteStopWord (id: number, word: string): Observable<any> {
        return this.modelApiService.deleteStopWord(id, word);
    }

    public lemmatizeWord (id: number, word: string, lemma: string): Observable<any> {
        return this.modelApiService.lemmatizeWord(id, word, lemma);
    }

    public getTopics (id: number): Observable<ModelTopic[]> {
        return this.modelApiService.getTopics(id);
    }

    public renameTopic (id: number, data: Partial<ModelTopic>): Observable<any> {
        return this.modelApiService.renameTopic(id, data);
    }

    /** Temp method. Will be websockets instead this */
    public training (id: number): Observable<any> {
        return this.modelApiService.training(id);
    }

    public getSentimentTrainingResult (id: number): Observable<SentimentTrainingResult> {
        return this.modelApiService.getSentimentTrainingResult(id);
    }

    public analyzeTopicAnalyzer (id: number, rawText: string): Observable<ModelTopicAnalyzerAnalysisData[]> {
        return this.modelApiService.analyzeTopicAnalyzer(id, rawText);
    }

    public analyzeSentiment (id: number, rawText: string): Observable<ModelSentimentAnalysisData> {
        return this.modelApiService.analyzeSentiment(id, rawText);
    }

    public analyzeNer (id: number, rawText: string): Observable<ModelNerAnalysisData> {
        return this.modelApiService.analyzeNer(id, rawText);
    }

    public analyzePrediction (id: number, rawText: string): Observable<ModelPredictionAnalysisData> {
        return this.modelApiService.analyzePrediction(id, rawText);
    }

}
