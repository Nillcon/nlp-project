import { Component, OnInit, OnDestroy, ChangeDetectorRef, forwardRef } from '@angular/core';
import { Form } from '@Shared/features';
import { FormGroupTypeSafe, FormBuilderTypeSafe } from 'form-type-safe';
import { ConditionNodeData } from '../../interfaces';
import { NG_VALUE_ACCESSOR, ControlValueAccessor } from '@angular/forms';


@Component({
    selector: 'script-condition-form',
    templateUrl: './condition-form.component.html',
    styleUrls: ['./condition-form.component.scss'],
    providers: [
        {
            provide: NG_VALUE_ACCESSOR,
            useExisting: forwardRef(() => ConditionFormComponent),
            multi: true
        }
    ]
})
export class ConditionFormComponent implements OnInit, OnDestroy, ControlValueAccessor {

    public inputData: Partial<ConditionNodeData>;

    public formGroup: FormGroupTypeSafe<Partial<ConditionNodeData>>;

    constructor (
        private readonly fb: FormBuilderTypeSafe,
        private readonly changeDetectorRef: ChangeDetectorRef
    ) { }

    public ngOnInit (): void {
    }

    public ngOnDestroy (): void {}

    public formGroupInit (inputData?: Partial<ConditionNodeData>): void {
        this.formGroup = this.fb.group<Partial<ConditionNodeData>>({
            logicExpression: this.fb.control(inputData?.logicExpression)
        });

        this.formChangesObserver();

        this.changeDetectorRef.markForCheck();
    }

    public registerOnChange (fn: Function): void {
        this.onChange = (value) => { fn(value); };
        this.formGroupInit(this.inputData);
    }

    public registerOnTouched (): void {}

    public writeValue (value: ConditionNodeData): void {
        this.inputData = value;
    }

    public onChange = (_: any) => {};

    private formChangesObserver (): void {
        this.formGroup.valueChanges
            .subscribe((value) => {
                this.onChange(value);
            });
    }
}
