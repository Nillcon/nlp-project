import { Component, OnInit, Output, ViewChild, EventEmitter, ElementRef, OnDestroy, Input } from '@angular/core';
import { FormGroupTypeSafe, FormBuilderTypeSafe } from 'form-type-safe';
import { BaseForm, Form } from '@Shared/features';
import { ScriptFormValidationRules } from '../../data';
import { Script } from '../../interfaces';

@Form()
@Component({
    selector: 'script-editing-form',
    templateUrl: './editing-form.component.html',
    styleUrls: ['./editing-form.component.scss']
})
export class EditingFormComponent implements OnInit, OnDestroy, BaseForm<Script> {

    @Input()
    public inputData: Script;

    @Output()
    public OnFormInit: EventEmitter<FormGroupTypeSafe<Script>> = new EventEmitter();

    @ViewChild('nameInput', {static: true})
    public nameInput: ElementRef;

    public formGroup: FormGroupTypeSafe<Script>;

    constructor (
        private readonly fb: FormBuilderTypeSafe
    ) { }

    public ngOnInit (): void {
    }

    public ngOnDestroy (): void {}

    public formGroupInit (inputData: Script): FormGroupTypeSafe<Script> {
        return this.fb.group<Script>({
            name: this.fb.control(inputData?.name, ScriptFormValidationRules.get('name'))
        });
    }

}
