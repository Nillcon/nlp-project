import { Component, OnInit, ChangeDetectionStrategy, OnDestroy, Input, Output, EventEmitter, forwardRef, ChangeDetectorRef } from '@angular/core';

import { FormGroupTypeSafe, FormBuilderTypeSafe } from 'form-type-safe';
import { ActionMap } from '../../data';
import { ActionEnum } from '../../enums';
import { ActionNodeData } from '../../interfaces';
import { NG_VALUE_ACCESSOR, ControlValueAccessor } from '@angular/forms';


@Component({
    selector: 'script-action-form',
    templateUrl: './action-form.component.html',
    styleUrls: ['./action-form.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
    providers: [
        {
            provide: NG_VALUE_ACCESSOR,
            useExisting: forwardRef(() => ActionFormComponent),
            multi: true
        }
    ]
})
export class ActionFormComponent implements OnInit, OnDestroy, ControlValueAccessor {

    public inputData: Partial<ActionNodeData>;

    public formGroup: FormGroupTypeSafe<Partial<ActionNodeData>>;

    public actions = [...ActionMap.values()];
    public ActionEnum = ActionEnum;

    constructor (
        private readonly fb: FormBuilderTypeSafe,
        private readonly changeDetectorRef: ChangeDetectorRef
    ) { }

    public ngOnInit (): void {
    }

    public ngOnDestroy (): void {}

    public formGroupInit (inputData?: Partial<ActionNodeData>): void {
        this.formGroup = this.fb.group<Partial<ActionNodeData>>({
            type: this.fb.control(inputData?.type || ActionEnum.Text),
            text: this.fb.control(inputData?.text || ''),
            data: this.fb.control(inputData?.data || null)
        });

        this.formChangesObserver();

        this.changeDetectorRef.markForCheck();
    }

    public registerOnChange (fn: Function): void {
        this.onChange = (value) => { fn(value); };
        this.formGroupInit(this.inputData);
    }

    public registerOnTouched (): void {}

    public writeValue (value: ActionNodeData): void {
        this.inputData = value;
    }

    public onChange = (_: any) => {};

    private formChangesObserver (): void {
        this.formGroup.valueChanges
            .subscribe((value) => {
                this.onChange(value);
            });
    }
}
