import { Component, OnInit, OnDestroy, ChangeDetectionStrategy, forwardRef } from '@angular/core';
import { FormGroupTypeSafe, FormBuilderTypeSafe } from 'form-type-safe';
import { NG_VALUE_ACCESSOR } from '@angular/forms';
import { TextActionData } from '@Dashboard/features/script/interfaces';

@Component({
    selector: 'script-text-action-form',
    templateUrl: './text-action-form.component.html',
    styleUrls: ['./text-action-form.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
    providers: [
        {
            provide: NG_VALUE_ACCESSOR,
            useExisting: forwardRef(() => TextActionFormComponent),
            multi: true
        }
    ]
})
export class TextActionFormComponent implements OnInit, OnDestroy {

    public inputData: TextActionData;

    public formGroup: FormGroupTypeSafe<TextActionData>;

    constructor (
        private readonly fb: FormBuilderTypeSafe
    ) { }

    public ngOnInit (): void {}

    public ngOnDestroy (): void {}

    public formGroupInit (inputData?: TextActionData): void {
        this.formGroup = this.fb.group<TextActionData>({
            messageText: this.fb.control(inputData || '')
        });
    }

    public registerOnChange (fn: Function): void {
        this.onChange = (value) => { fn(value); };
        this.formGroupInit(this.inputData);
    }

    public registerOnTouched (): void {}

    public writeValue (value: any): void {
        if (value !== undefined && value !== null) {
            this.inputData = value;
        }
    }

    public onChange = (_: any) => {};

}
