import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TextActionFormComponent } from './text-action-form.component';

describe('TextActionFormComponent', () => {
  let component: TextActionFormComponent;
  let fixture: ComponentFixture<TextActionFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TextActionFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TextActionFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
