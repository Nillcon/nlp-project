import { Component, OnInit, forwardRef, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';

import { ActionMap } from '../../../data';
import { ActionEnum } from '../../../enums';

@Component({
    selector: 'script-action-selector',
    templateUrl: './action-selector.component.html',
    styleUrls: ['./action-selector.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
    providers: [
        {
            provide: NG_VALUE_ACCESSOR,
            useExisting: forwardRef(() => ActionSelectorComponent),
            multi: true
        }
    ]
})
export class ActionSelectorComponent implements OnInit, ControlValueAccessor {

    public ActionMap = ActionMap;

    // The fix for fu**ing PrimeNG
    public actions = [...this.ActionMap.values()].map(action => {
        return {
            label: action.name,
            value: action.id
        };
    });

    public selectedAction: ActionEnum = ActionEnum.Text;

    constructor (
        private readonly changeDetectorRef: ChangeDetectorRef
    ) { }

    public ngOnInit (): void {
    }

    public registerOnChange (fn: Function): void {
        this.onChange = (value) => { fn(value); };
    }

    public registerOnTouched (): void {}

    public writeValue (value: any): void {
        if (value !== undefined && value !== null) {
            this.selectedAction = value;
            this.changeDetectorRef.markForCheck();
        }
    }

    public onChange = (_: any) => {};

    public onActionSelectorChange (action: {value: number}): void {
       this.onChange(action.value);
    }

}
