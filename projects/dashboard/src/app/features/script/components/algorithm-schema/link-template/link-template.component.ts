import { Component, OnInit, Input, ChangeDetectionStrategy } from '@angular/core';

@Component({
    selector: 'g[script-link-template]',
    templateUrl: './link-template.component.html',
    styleUrls: ['./link-template.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class LinkTemplateComponent implements OnInit {

    @Input()
    public link: any;

    constructor () { }

    public ngOnInit (): void {
    }

}
