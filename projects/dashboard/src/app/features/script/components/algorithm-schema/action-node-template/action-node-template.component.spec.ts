import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ActionNodeTemplateComponent } from './action-node-template.component';

describe('ActionNodeTemplateComponent', () => {
  let component: ActionNodeTemplateComponent;
  let fixture: ComponentFixture<ActionNodeTemplateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ActionNodeTemplateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ActionNodeTemplateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
