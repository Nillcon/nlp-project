import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConditionNodeTemplateComponent } from './condition-node-template.component';

describe('ConditionNodeTemplateComponent', () => {
  let component: ConditionNodeTemplateComponent;
  let fixture: ComponentFixture<ConditionNodeTemplateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConditionNodeTemplateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConditionNodeTemplateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
