import { Component, OnInit, Input, ChangeDetectionStrategy, Output, EventEmitter, ViewChild } from '@angular/core';
import * as shape from 'd3-shape';
import { MenuItem } from 'primeng-lts/api';
import { ContextMenu } from 'primeng-lts/contextmenu';
import { Edge } from '@swimlane/ngx-graph';
import { GraphNode, TemplateContextMenuEvent, Link } from '../../interfaces';
import { NodeTypeEnum } from '../../enums';
import { OverlayPanel } from 'primeng-lts/overlaypanel';

@Component({
    selector: 'script-algorithm-schema',
    templateUrl: './algorithm-schema.component.html',
    styleUrls: ['./algorithm-schema.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class AlgorithmSchemaComponent implements OnInit {

    @Input()
    public nodes: GraphNode[];

    @Input()
    public edges: Edge[];

    @Input()
    public selectedNode: GraphNode;

    @Input()
    public draggingEnabled: boolean = false;

    @Output()
    public OnSelectedNode: EventEmitter<GraphNode> = new EventEmitter();

    @Output()
    public OnCreateLinkButtonClick: EventEmitter<Partial<Link>> = new EventEmitter();

    @ViewChild(ContextMenu, {static: false})
    public contextMenu: ContextMenu;

    @ViewChild(OverlayPanel, {static: false})
    public overlayPanel: OverlayPanel;

    public contextItems: MenuItem[] = [];

    public readonly layoutSettings = {
        orientation: 'TB'
    };

    public readonly curve: any = shape.curveNatural;

    public contextSelectedNode: GraphNode;

    public NodeTypeEnum = NodeTypeEnum;

    constructor () { }

    public ngOnInit (): void {
    }

    public onNodeClick (node: GraphNode): void {
        this.selectedNode = node;
        this.OnSelectedNode.emit(node);
    }

    public onNodeRightClick (data: TemplateContextMenuEvent, node: GraphNode): void {
        this.contextItems = data.items;
        this.contextSelectedNode = node;
        data.event.stopPropagation();

        this.contextMenu.toggle(data.event);
    }

    public onContextHide (): void {
        this.contextSelectedNode = null;
    }

    public onContextMenuClose (): void {
        this.contextMenu.hide();
    }

    public onContextMenuSelect (link: Link, sourceNode: GraphNode): void {
        this.OnCreateLinkButtonClick.emit({
            ...link,
            sourceNodeId: sourceNode.id
        });
    }
}
