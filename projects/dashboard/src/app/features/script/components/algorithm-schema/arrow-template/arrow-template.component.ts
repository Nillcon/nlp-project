import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';

@Component({
    selector: 'g[script-arrow-template]',
    templateUrl: './arrow-template.component.html',
    styleUrls: ['./arrow-template.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class ArrowTemplateComponent implements OnInit {

    constructor () { }

    public ngOnInit (): void {
    }

}
