import { Component, OnInit, ChangeDetectionStrategy, Input, Output, EventEmitter } from '@angular/core';
import { MenuItem } from 'primeng-lts/api';
import { TemplateContextMenuEvent, Link } from '../../../interfaces';

@Component({
    selector: 'g[script-action-template]',
    templateUrl: './action-node-template.component.html',
    styleUrls: ['./action-node-template.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class ActionNodeTemplateComponent implements OnInit {

    @Input()
    public isSelected: boolean;

    @Input()
    public isContextSelected: boolean;

    @Input()
    public label: string;

    @Output()
    public OnNodeClick: EventEmitter<MouseEvent> = new EventEmitter();

    @Output()
    public OnContextMenuClick: EventEmitter<TemplateContextMenuEvent> = new EventEmitter();

    @Output()
    public OnContextMenuSelect: EventEmitter<Partial<Link>> = new EventEmitter();

    public readonly contextMenuItems: MenuItem[] = [
        {
            label: 'Add node',
            command: () => {
                this.OnContextMenuSelect.emit({});
            }
        }
    ];

    constructor () { }

    public ngOnInit (): void {
    }

    public onNodeClick (event: MouseEvent): void {
        this.OnNodeClick.emit(event);
    }

    public onContextMenuClick (event: MouseEvent): void {
        this.OnContextMenuClick.emit({
            event: event,
            items: this.contextMenuItems
        });
    }

}
