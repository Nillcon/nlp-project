import { Component, OnInit, Input, Output, EventEmitter, ChangeDetectionStrategy } from '@angular/core';
import { MenuItem } from 'primeng-lts/api';
import { GraphNode, TemplateContextMenuEvent, Link } from '../../../interfaces';
import { ConditionTypeEnum } from '@Dashboard/features/script/enums';

@Component({
    selector: 'g[script-condition-template]',
    templateUrl: './condition-node-template.component.html',
    styleUrls: ['./condition-node-template.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class ConditionNodeTemplateComponent implements OnInit {

    @Input()
    public isSelected: boolean;

    @Input()
    public isContextSelected: boolean;

    @Input()
    public label: string;

    @Output()
    public OnNodeClick: EventEmitter<MouseEvent> = new EventEmitter();

    @Output()
    public OnContextMenuClick: EventEmitter<TemplateContextMenuEvent> = new EventEmitter();

    @Output()
    public OnContextMenuSelect: EventEmitter<Partial<Link>> = new EventEmitter();

    public readonly contextMenuItems: MenuItem[] = [
        {
            label: 'Add True',
            command: () => {
                this.OnContextMenuSelect.emit({
                    conditionType: ConditionTypeEnum.TRUE
                });
            }
        },
        {
            label: 'Add False',
            command: () => {
                this.OnContextMenuSelect.emit({
                    conditionType: ConditionTypeEnum.FALSE
                });
            }
        }
    ];

    constructor () { }

    public ngOnInit (): void {
    }

    public onNodeClick (event: MouseEvent): void {
        this.OnNodeClick.emit(event);
    }

    public onContextMenuClick (event: MouseEvent): void {
        this.OnContextMenuClick.emit({
            event: event,
            items: this.contextMenuItems
        });
    }

}
