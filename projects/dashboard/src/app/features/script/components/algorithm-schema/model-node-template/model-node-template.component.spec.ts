import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModelNodeTemplateComponent } from './model-node-template.component';

describe('ModelNodeTemplateComponent', () => {
  let component: ModelNodeTemplateComponent;
  let fixture: ComponentFixture<ModelNodeTemplateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModelNodeTemplateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModelNodeTemplateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
