import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ArrowTemplateComponent } from './arrow-template.component';

describe('ArrowTemplateComponent', () => {
  let component: ArrowTemplateComponent;
  let fixture: ComponentFixture<ArrowTemplateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ArrowTemplateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ArrowTemplateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
