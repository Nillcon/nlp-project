import { Component, OnInit, EventEmitter, Output, Input, OnDestroy } from '@angular/core';
import { Form, BaseForm } from '@Shared/features';
import { Node } from '../../interfaces';
import { FormGroupTypeSafe, FormBuilderTypeSafe } from 'form-type-safe';
import { NodeTypeEnum } from '../../enums';

@Form()
@Component({
    selector: 'node-form',
    templateUrl: './node-form.component.html',
    styleUrls: ['./node-form.component.scss']
})
export class NodeFormComponent implements OnInit, OnDestroy, BaseForm<Node> {

    @Input()
    public inputData: Node;

    @Output()
    public OnFormInit: EventEmitter<FormGroupTypeSafe<Node>> = new EventEmitter();

    public nodeType: NodeTypeEnum = NodeTypeEnum.Action;

    public formGroup: FormGroupTypeSafe<Node>;

    public NodeTypeEnum = NodeTypeEnum;

    constructor (
        private readonly fb: FormBuilderTypeSafe
    ) { }

    public ngOnInit (): void {
    }

    public ngOnDestroy (): void {}

    public formGroupInit (inputData: Node): FormGroupTypeSafe<Node> {
        this.nodeType = inputData?.type;

        return this.fb.group<Node>({
            name: this.fb.control(inputData?.name || ''),
            data: this.fb.control(inputData?.data)
        });
    }

}
