import { Component, OnInit, EventEmitter, Output, Input, OnDestroy, ViewChild, ElementRef, ChangeDetectionStrategy } from '@angular/core';
import { Form, BaseForm } from '@Shared/features';
import { FormGroupTypeSafe, FormBuilderTypeSafe } from 'form-type-safe';
import { Script } from '../../interfaces';
import { ScriptFormValidationRules } from '../../data';

@Form()
@Component({
    selector: 'script-creation-form',
    templateUrl: './creation-form.component.html',
    styleUrls: ['./creation-form.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class CreationFormComponent implements OnInit, OnDestroy, BaseForm<Script> {

    @Output()
    public OnFormInit: EventEmitter<FormGroupTypeSafe<Script>> = new EventEmitter();

    @ViewChild('nameInput', {static: true})
    public nameInput: ElementRef;

    public formGroup: FormGroupTypeSafe<Script>;

    constructor (
        private readonly fb: FormBuilderTypeSafe
    ) { }

    public ngOnInit (): void {
        this.nameInput.nativeElement.focus();
    }

    public ngOnDestroy (): void {}

    public formGroupInit (): FormGroupTypeSafe<Script> {
        return this.fb.group<Script>({
            name: this.fb.control('', ScriptFormValidationRules.get('name'))
        });
    }

}
