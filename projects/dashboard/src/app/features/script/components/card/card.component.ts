import { Component, OnInit, EventEmitter, Output, Input } from '@angular/core';
import { Script } from '../../interfaces';

@Component({
  selector: 'script-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.scss']
})
export class CardComponent implements OnInit {

    @Input()
    public script: Script;

    @Output()
    public OnCardClick: EventEmitter<MouseEvent> = new EventEmitter();

    constructor () { }

    public ngOnInit (): void {
    }

    public onCardClick (mouseEvent: MouseEvent): void {
        this.OnCardClick.emit(mouseEvent);
    }

}
