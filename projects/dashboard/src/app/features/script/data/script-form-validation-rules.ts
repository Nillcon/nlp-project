import { Validators, ValidatorFn } from '@angular/forms';
import { Script } from '../interfaces';

export const ScriptFormValidationRules = new Map<keyof Script, ValidatorFn[]>()
    .set(
        'name',
        [
            Validators.required,
        ]
    );

