import { ActionItem } from '../interfaces';
import { ActionEnum } from '../enums';

export const ActionMap = new Map<ActionEnum, ActionItem>()
    .set(
        ActionEnum.Text,
        {
            id: ActionEnum.Text,
            name: 'Text',
            iconClass: 'fas fa-align-left'
        }
    )
    .set(
        ActionEnum.Poll,
        {
            id: ActionEnum.Poll,
            name: 'Poll',
            iconClass: 'fas fa-poll-h'
        }
    )
    .set(
        ActionEnum.Image,
        {
            id: ActionEnum.Image,
            name: 'Image',
            iconClass: 'far fa-image'
        }
    )
    .set(
        ActionEnum.Buttons,
        {
            id: ActionEnum.Buttons,
            name: 'Buttons',
            iconClass: 'fas fa-th'
        }
    )
    .set(
        ActionEnum.Form,
        {
            id: ActionEnum.Form,
            name: 'Form ',
            iconClass: 'far fa-file-alt'
        }
    )
    .set(
        ActionEnum.Poll,
        {
            id: ActionEnum.Poll,
            name: 'Poll',
            iconClass: 'fas fa-poll-h'
        }
    );
