export enum ActionEnum {
    Text = 1,
    Poll,
    Image,
    Buttons,
    Form
}
