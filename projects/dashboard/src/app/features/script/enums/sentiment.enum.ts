export enum SentimentEnum {
    Positive,
    Neutral,
    Negative
}
