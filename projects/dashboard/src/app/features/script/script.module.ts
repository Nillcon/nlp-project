import { NgModule } from '@angular/core';
import { SharedModule } from '@Dashboard/app-shared.module';

import {
    AlgorithmSchemaComponent,
    ActionFormComponent,
    ActionSelectorComponent,
    TextActionFormComponent,
    CardComponent,
    CreationFormComponent,
    EditingFormComponent,
    ConditionFormComponent,
    NodeFormComponent,
    ActionNodeTemplateComponent,
    ConditionNodeTemplateComponent,
    ModelNodeTemplateComponent,
    ArrowTemplateComponent,
    LinkTemplateComponent
} from './components';

import { ModelModule } from '../model';
import { NgxGraphModule } from '@swimlane/ngx-graph';
import { NgxBlocklyModule } from 'ngx-blockly';

import { ContextMenuModule } from 'primeng-lts/contextmenu';
import { OverlayPanelModule } from 'primeng-lts/overlaypanel';


@NgModule({
    declarations: [
        AlgorithmSchemaComponent,
        // NodeFormComponent,
        ActionFormComponent,
        ActionSelectorComponent,
        TextActionFormComponent,
        CardComponent,
        CreationFormComponent,
        EditingFormComponent,
        ConditionFormComponent,
        NodeFormComponent,
        ConditionNodeTemplateComponent,
        ActionNodeTemplateComponent,
        ModelNodeTemplateComponent,
        ArrowTemplateComponent,
        LinkTemplateComponent,
    ],
    imports: [
        SharedModule,
        ModelModule,

        NgxGraphModule,
        NgxBlocklyModule,
        ContextMenuModule,
        OverlayPanelModule
    ],
    exports: [
        AlgorithmSchemaComponent,
        NodeFormComponent,
        CardComponent,
        CreationFormComponent,
        EditingFormComponent,
        ActionFormComponent,
        ConditionFormComponent
    ]
})

export class ScriptModule { }
