import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { ScriptApiService } from './script-api.service';
import { map } from 'rxjs/operators';

import { Tree, Script, Node } from '../interfaces';
import { Variable } from '@Dashboard/features/variable';

@Injectable({
    providedIn: 'root'
})
export class ScriptService {

    constructor (
        private readonly scriptApiService: ScriptApiService
    ) {}

    public getAll (): Observable<Script[]> {
        return this.scriptApiService.getAll();
    }

    public get (id: number): Observable<Script> {
        return this.scriptApiService.get(id);
    }

    public edit (script: Script): Observable<any> {
        return this.scriptApiService.edit(script);
    }

    public create (script: Script): Observable<any> {
        return this.scriptApiService.create(script);
    }

    public delete (id: number): Observable<any> {
        return this.scriptApiService.delete(id);
    }

    public getTree (): Observable<Tree> {
        return this.scriptApiService.getTree()
            .pipe(
                map(tree => {
                    tree.edges
                        .map(edge => {
                            edge.id = `id${edge.id}`;
                        });

                    return tree;
                }),
            );
    }

    public getNode (scriptId: number, nodeId: number): Observable<Node> {
        return this.scriptApiService.getNode(scriptId, nodeId)
            .pipe(
                map(node => {
                    node.id = String(node.id);
                    return node;
                })
            );
    }

    public createNode (scriptId: number, node: Node): Observable<number> {
        return this.scriptApiService.createNode(scriptId, node);
    }

    public editNode (scriptId: number, node: Node): Observable<any> {
        return this.scriptApiService.editNode(scriptId, node);
    }

    public deleteNode (scriptId: number, nodeId: number): Observable<any> {
        return this.scriptApiService.deleteNode(scriptId, nodeId);
    }

    public getVariables (scriptId: number): Observable<Variable[]> {
        return this.scriptApiService.getVariables(scriptId);
    }

    public getVariableOfScript (scriptId: number, variableId: number): Observable<Variable> {
        return this.scriptApiService.getVariableOfScript(scriptId, variableId);
    }

    public createVariable (scriptId: number, variable: Variable): Observable<any> {
        return this.scriptApiService.createVariable(scriptId, variable);
    }


    public editVariable (scriptId: number, variable: Variable): Observable<any> {
        return this.scriptApiService.editVariable(scriptId, variable);
    }

    public deleteVariable (scriptId: number, variableId: number): Observable<any> {
        return this.scriptApiService.deleteVariable(scriptId, variableId);
    }

    public createLink (scriptId: number, link: Link): Observable<any> {
        return this.scriptApiService.createLink(scriptId, link);
    }

}
