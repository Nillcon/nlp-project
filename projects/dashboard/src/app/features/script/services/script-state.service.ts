import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { Script } from '../interfaces';
import { Variable } from '@Dashboard/features/variable';

@Injectable({
    providedIn: 'root'
})
export class ScriptStateService {

    public get scripts$ (): Observable<Script[]> {
        return this._scripts$.asObservable();
    }

    private readonly _scripts$: BehaviorSubject<Script[]> = new BehaviorSubject([]);

    constructor () {}

    public setScripts (scripts: Script[]): void {
        this._scripts$.next(scripts);
    }

    public getScripts (): Script[] {
        return this._scripts$.getValue();
    }

    public addScript (script: Script): void {
        const scripts = this.getScripts() || [];

        this._scripts$.next([...scripts, script]);
    }

    public updateScript (script: Script): void {
        const scripts = this.getScripts();
        const targetScriptId = scripts
            .findIndex((currScript) => currScript.id === script.id);

        scripts[targetScriptId] = script;

        this._scripts$.next([...scripts]);
    }

    public updateScriptId (scriptToReplace: Script, addedScriptWithId: Script): void {
        const scripts = this.getScripts();
        const targetScriptId = scripts
            .findIndex((currScript) => currScript === scriptToReplace);

        scripts[targetScriptId] = addedScriptWithId;
        this._scripts$.next([...scripts]);
    }

    public editScript (script: Partial<Script>): void {
        const scripts = this.getScripts();
        const targetScriptIndex = scripts.findIndex((currScript) => currScript.id === script.id);

        scripts[targetScriptIndex] = { ...scripts[targetScriptIndex], ...script };

        this._scripts$.next([...scripts]);
    }

    public removeScript (scriptId: number): void {
        const scripts = this.getScripts();
        const filteredScripts = scripts
            .filter((currScript) => currScript.id !== scriptId);

        this._scripts$.next(filteredScripts);
    }

    public updateVariablesOfScript (scriptId: number, variables: Variable[]): void {
        const scripts = this.getScripts();
        const targetScriptIndex = scripts.findIndex((currScript) => currScript.id === scriptId);

        scripts[targetScriptIndex] = {
            ...scripts[targetScriptIndex],
            variables: variables
        };

        this._scripts$.next([...scripts]);
    }

    public updateVariableOfScript (scriptId: number, variable: Variable, variableToReplace: Variable): void {
        const scripts = this.getScripts();
        const targetScriptIndex = scripts.findIndex((currScript) => currScript.id === scriptId);

        const targetVariableIndex = scripts[targetScriptIndex].variables
            .findIndex((currVariable) => currVariable === variable);

        scripts[targetScriptIndex].variables[targetVariableIndex] = variableToReplace;
        scripts[targetScriptIndex].variables = [...scripts[targetScriptIndex].variables];

        this._scripts$.next([...scripts]);
    }

    public getVariableOfScript (scriptId: number, variableId: number): Variable {
        const scripts = this.getScripts();
        const targetScriptIndex = scripts.findIndex((currScript) => currScript.id === scriptId);

        const targetVariableIndex = scripts[targetScriptIndex].variables
            .findIndex((currVariable) => currVariable.id === variableId);

        return  scripts[targetScriptIndex].variables[targetVariableIndex];
    }

    public addVariableToScript (scriptId: number, variable: Variable): void {
        const scripts = this.getScripts();
        const targetScriptIndex = scripts.findIndex((currScript) => currScript.id === scriptId);

        scripts[targetScriptIndex].variables = [...scripts[targetScriptIndex].variables, variable];

        this._scripts$.next([...scripts]);
    }

    public deleteVariableOfScript (scriptId: number, variableId: number): Variable {
        const scripts = this.getScripts();
        const targetScriptIndex = scripts.findIndex((currScript) => currScript.id === scriptId);

        const removedVariable = scripts[targetScriptIndex].variables
            .find(variable => variable.id === variableId);

        scripts[targetScriptIndex].variables = scripts[targetScriptIndex].variables
            .filter(variable => variable.id !== variableId);

        this._scripts$.next([...scripts]);

        return removedVariable;
    }

}
