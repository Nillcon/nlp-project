import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpRequestService } from '@Shared/features';

import { Tree, Script, Node, Link } from '../interfaces';
import { tap } from 'rxjs/operators';
import { Variable } from '@Dashboard/features/variable';

@Injectable({
    providedIn: 'root'
})
export class ScriptApiService {

    constructor (
        private readonly httpRequestService: HttpRequestService
    ) {}

    public getAll (): Observable<Script[]> {
        return this.httpRequestService.get<Script[]>(`/Chats/Scripts`);
    }

    public get (id: number): Observable<Script> {
        return this.httpRequestService.get<Script>(`/Chats/Scripts/${id}`);
    }

    public delete (id: number): Observable<any> {
        return this.httpRequestService.delete(`/Chats/Scripts/${id}`);
    }

    public edit (script: Script): Observable<any> {
        return this.httpRequestService.put(`/Chats/Scripts/${script.id}`, script);
    }

    public create (script: Script): Observable<any> {
        return this.httpRequestService.post<Script[]>(`/Chats/Scripts`, script)
            .pipe(
                tap(console.log)
            );
    }

    public getNode (scriptId: number, nodeId: number): Observable<Node> {
        return this.httpRequestService.get(`/Chats/Scripts/${scriptId}/Nodes/${nodeId}`);
    }

    public createNode (scriptId: number, node: Node): Observable<number> {
        return this.httpRequestService.post(`/Chats/Scripts/${scriptId}/Nodes`, node);
    }

    public editNode (scriptId: number, node: Node): Observable<any> {
        return this.httpRequestService.put(`/Chats/Scripts/${scriptId}/Nodes/${node.id}`, node);
    }

    public deleteNode (scriptId: number, nodeId: number): Observable<any> {
        return this.httpRequestService.delete(`/Chats/Scripts/${scriptId}/Nodes/${nodeId}`);
    }

    public getTree (): Observable<Tree> {
        // FIXME: var id
        return this.httpRequestService.get<Tree>(`/Chats/Scripts/1/Nodes`);
    }

    public getVariables (scriptId: number): Observable<Variable[]> {
        return this.httpRequestService.get<Variable[]>(`/Chats/Scripts/${scriptId}/Variables`);
    }

    public getVariableOfScript (scriptId: number, variableId: number): Observable<Variable> {
        return this.httpRequestService.get<Variable>(`/Chats/Scripts/${scriptId}/Variables/${variableId}`);
    }

    public createVariable (scriptId: number, variable: Variable): Observable<any> {
        return this.httpRequestService.post<Variable[]>(`/Chats/Scripts/${scriptId}/Variables`, variable);
    }

    public editVariable (scriptId: number, variable: Variable): Observable<any> {
        return this.httpRequestService.put<Variable[]>(`/Chats/Scripts/${scriptId}/Variables/${variable.id}`, variable);
    }

    public deleteVariable (scriptId: number, variableId: number): Observable<any> {
        return this.httpRequestService.delete(`/Chats/Scripts/${scriptId}/Variables/${variableId}`);
    }

    public createLink (scriptId: number, link: Link): Observable<any> {
        return this.httpRequestService.post(`/Chats/Scripts/${scriptId}/Nodes/${link.sourceNodeId}`, link);
    }

}
