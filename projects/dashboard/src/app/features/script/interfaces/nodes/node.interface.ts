import { NodeTypeEnum } from '../../enums';

export interface Node {
    id?: string;
    type?: NodeTypeEnum;
    name?: string;
    data?: any;
}
