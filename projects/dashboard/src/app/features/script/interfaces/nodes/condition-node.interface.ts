import { Node } from './node.interface';

export interface ContidionNode extends Node {
    data: ConditionNodeData;
}

export interface ConditionNodeData {
    logicExpression: string;
}
