import { Node } from '@swimlane/ngx-graph';
import { NodeTypeEnum } from '../../enums';

export interface GraphNode extends Node {
    type: NodeTypeEnum;
}
