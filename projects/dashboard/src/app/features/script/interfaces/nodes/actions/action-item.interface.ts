import { ActionEnum } from '../../../enums';

export interface ActionItem {
    id: ActionEnum;
    name: string;
    iconClass: string;
}
