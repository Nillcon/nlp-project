import { ActionEnum } from '../../../enums';
import { Node } from '../node.interface';

export interface ActionNode extends Node {
    data: ActionNodeData;
}

export interface ActionNodeData {
    text: string;
    type: ActionEnum;
    data: any;
}
