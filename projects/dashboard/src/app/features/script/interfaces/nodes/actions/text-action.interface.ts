import { ActionNodeData } from './action.interface';

export interface TextAction extends ActionNodeData {
    data: TextActionData;
}

export interface TextActionData {}
