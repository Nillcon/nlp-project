import { MenuItem } from 'primeng-lts/api';

export interface TemplateContextMenuEvent {
    event: MouseEvent;
    items: MenuItem[];
}
