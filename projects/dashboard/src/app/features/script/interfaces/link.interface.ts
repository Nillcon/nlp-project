import { ConditionTypeEnum } from '../enums';

export interface Link {
    conditionType: ConditionTypeEnum;
    targetNodeId: string;
    sourceNodeId: string;
}
