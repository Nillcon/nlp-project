import { Edge } from '@swimlane/ngx-graph';
import { GraphNode } from './nodes';

export interface Tree {
    nodes: GraphNode[];
    edges: Edge[];
}
