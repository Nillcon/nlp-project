import { Variable } from '@Dashboard/features/variable';

export interface Script {
    id?: number;
    name: string;
    variables?: Variable[];
}
