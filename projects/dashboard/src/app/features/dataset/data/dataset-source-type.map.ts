import { DatasetSourceTypeEnum } from '../enums/dataset-source-type.enum';
import { DatasetSourceType } from '../interfaces/dataset-source-type.interface';

export const DatasetSourceTypeMap = new Map<DatasetSourceTypeEnum, DatasetSourceType>()
    .set(
        DatasetSourceTypeEnum.Excel,
        {
            id: DatasetSourceTypeEnum.Excel,
            name: 'Excel',
            iconLink: 'assets/icons/excel-icon.png',
            acceptFileTypes: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel, .xls, .xlsx',
            isHasDelimiterForColumns: false
        }
    )
    .set(
        DatasetSourceTypeEnum.Csv,
        {
            id: DatasetSourceTypeEnum.Csv,
            name: 'CSV',
            iconLink: 'assets/icons/csv-icon.png',
            acceptFileTypes: '.csv',
            isHasDelimiterForColumns: true
        }
    )
    .set(
        DatasetSourceTypeEnum.Zip,
        {
            id: DatasetSourceTypeEnum.Zip,
            name: 'ZIP',
            iconLink: 'assets/icons/zip-icon.png',
            acceptFileTypes: '.zip',
            isHasDelimiterForColumns: false
        }
    )
    .set(
        DatasetSourceTypeEnum.ExistModel,
        {
            id: DatasetSourceTypeEnum.ExistModel,
            name: 'Exist model',
            iconLink: 'assets/icons/exist_model-icon.png',
            acceptFileTypes: '*',
            isHasDelimiterForColumns: false
        }
    );
