import { DatasetColumnTypeEnum } from '../enums';
import { DatasetColumnType } from '../interfaces';

export const DatasetColumnTypeMap = new Map<DatasetColumnTypeEnum, DatasetColumnType>()
    .set(
        DatasetColumnTypeEnum.None,
        {
            id: DatasetColumnTypeEnum.None,
            name: '-Unselected-',
            isUnique: false,
            isNeedAssociation: false
        }
    )
    .set(
        DatasetColumnTypeEnum.Value,
        {
            id: DatasetColumnTypeEnum.Value,
            name: 'Value',
            isUnique: false,
            isNeedAssociation: false
        }
    )
    .set(
        DatasetColumnTypeEnum.SentimentLabel,
        {
            id: DatasetColumnTypeEnum.SentimentLabel,
            name: 'Label',
            isUnique: true,
            isNeedAssociation: true,
        }
    );
