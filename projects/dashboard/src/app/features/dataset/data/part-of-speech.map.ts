import { PartOfSpeechEnum } from '../enums/part-of-speech.enum';
import { PartOfSpeech } from '../interfaces/part-of-speech.interface';

export const PartOfSpeechMap = new Map<PartOfSpeechEnum, PartOfSpeech>()
    .set(
        PartOfSpeechEnum.Noun,
        {
            id: PartOfSpeechEnum.Noun,
            name: 'Noun',
            color: '#f5ad42'
        }
    )
    .set(
        PartOfSpeechEnum.Verb,
        {
            id: PartOfSpeechEnum.Verb,
            name: 'Verb',
            color: '#427bf5'
        }
    )
    .set(
        PartOfSpeechEnum.Adjective,
        {
            id: PartOfSpeechEnum.Adjective,
            name: 'Adjective',
            color: '#28b586'
        }
    );
