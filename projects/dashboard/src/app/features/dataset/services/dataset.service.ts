import { Injectable } from '@angular/core';
import { DatasetApiService } from './dataset-api.service';
import { Observable } from 'rxjs';
import { DatasetAnalytics } from '../interfaces';

@Injectable({
    providedIn: 'root'
})
export class DatasetService {

    constructor (
        private readonly datasetApiService: DatasetApiService
    ) {}

    public analyzeText (text: string): Observable<DatasetAnalytics> {
        return this.datasetApiService.analyzeText(text);
    }

}
