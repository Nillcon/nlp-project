import { TestBed, async } from '@angular/core/testing';
import { DatasetExtractorService } from './dataset-extractor.service';
import { Tag } from '@Dashboard/features/tag';

describe('DatasetExtractorService', () => {
    const debugSentence: string = 'How do I turn on line numbers by default in TextWrangler on the Mac?';
    const debugSportTag: Partial<Tag> = { id: 1, name: 'Sport', color: '#348feb' };
    const debugTechTag: Partial<Tag> = { id: 2, name: 'Tech', color: '#fcba03' };

    let extractorService: DatasetExtractorService;

    beforeEach(() => TestBed.configureTestingModule({
        providers: [
            DatasetExtractorService
        ]
    }));

    beforeEach(() => {
        extractorService = TestBed.get(DatasetExtractorService);
        extractorService.setSentence(debugSentence);
    });

    it('should be created', () => {
        expect(extractorService).toBeTruthy();
    });

    it('should parse sentence on tokens', async(() => {
        const tokens = extractorService.tokens;
        const debugSentenceTokensLength: number = debugSentence.split(' ').length;

        expect(tokens).toBeTruthy();
        expect(tokens.length).toEqual(debugSentenceTokensLength);
    }));

    it('should add SPORT tag for third token, than for second and than for fourth one. Than shoud remove tag and return three tokens instead one', () => {
        extractorService.setTagForToken(2, debugSportTag.id);

        const thirdToken = extractorService.getTokenByIndex(2);

        expect(thirdToken.tagId).toBeTruthy();
        expect(thirdToken.tagId).toEqual(debugSportTag.id);
        expect(thirdToken.value).toEqual('I');


        extractorService.setTagForToken(1, debugSportTag.id);

        const secondToken = extractorService.getTokenByIndex(1);

        expect(secondToken.tagId).toBeTruthy();
        expect(secondToken.tagId).toEqual(debugSportTag.id);
        expect(secondToken.value).toEqual('do I');


        extractorService.setTagForToken(2, debugSportTag.id);

        const secondTokenAgain = extractorService.getTokenByIndex(1);

        expect(secondTokenAgain.tagId).toBeTruthy();
        expect(secondTokenAgain.tagId).toEqual(debugSportTag.id);
        expect(secondTokenAgain.value).toEqual('do I turn');


        extractorService.removeTagFromToken(1);

        expect(extractorService.getTokenByIndex(1).value).toEqual('do');
        expect(extractorService.getTokenByIndex(1).tagId).toBeNull();

        expect(extractorService.getTokenByIndex(2).value).toEqual('I');
        expect(extractorService.getTokenByIndex(2).tagId).toBeNull();

        expect(extractorService.getTokenByIndex(3).value).toEqual('turn');
        expect(extractorService.getTokenByIndex(3).tagId).toBeNull();
    });

    it('should add SPORT tag for first token and TECH tag for second token', () => {
        extractorService.setTagForToken(0, debugSportTag.id);
        extractorService.setTagForToken(1, debugTechTag.id);

        const firstToken = extractorService.getTokenByIndex(0);
        const secondToken = extractorService.getTokenByIndex(1);

        expect(firstToken.tagId).toBeTruthy();
        expect(firstToken.tagId).toEqual(debugSportTag.id);

        expect(secondToken.tagId).toBeTruthy();
        expect(secondToken.tagId).toEqual(debugTechTag.id);
    });

});
