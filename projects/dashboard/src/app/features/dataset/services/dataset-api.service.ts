import { Injectable } from '@angular/core';
import { HttpRequestService } from '@Shared/features';
import { Observable } from 'rxjs';
import { DatasetAnalytics } from '../interfaces';

@Injectable({
    providedIn: 'root'
})
export class DatasetApiService {

    constructor (
        private readonly httpService: HttpRequestService
    ) {}

    public analyzeText (text: string): Observable<DatasetAnalytics> {
        return this.httpService.post(
            `/Dataset/TextAnalysis`,
            { value: text }
        );
    }

}
