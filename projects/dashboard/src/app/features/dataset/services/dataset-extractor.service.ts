import { Injectable } from '@angular/core';
import { DatasetExtractorToken } from '../interfaces/extractor-token.interface';

@Injectable({
    providedIn: 'root'
})
export class DatasetExtractorService {

    public get tokens (): DatasetExtractorToken[] {
        return this._tokens;
    }

    private _tokens: DatasetExtractorToken[] = [];

    constructor () {}

    public setSentence (sentence: string): void {
        this._tokens = this.parseSentenceOnTokens(sentence);
    }

    public setTokens (tokens: DatasetExtractorToken[]): void {
        this._tokens = tokens;
    }

    public parseSentenceOnTokens (sentence: string): DatasetExtractorToken[] {
        return sentence.split(' ')
            .map((currWord) => <DatasetExtractorToken>{
                value: currWord,
                tagId: null
            });
    }

    public getTokenByIndex (tokenIndex: number): DatasetExtractorToken {
        return this._tokens[tokenIndex];
    }

    public setTagForToken (targetTokenIndex: number, tagId: number): void {
        const targetToken = this.getTokenByIndex(targetTokenIndex);
        targetToken.tagId = tagId;

        this.combineTokenWithRightOne(targetTokenIndex);
        this.combineTokenWithLeftOne(targetTokenIndex);
    }

    public removeTagFromToken (targetTokenIndex: number): void {
        const targetToken = this.getTokenByIndex(targetTokenIndex);
        const targetTokenValueAsTokens = this.parseSentenceOnTokens(targetToken.value);

        this._tokens.splice(targetTokenIndex, 1, ...targetTokenValueAsTokens);
    }

    public removeTagFromTokens (tagId: number): void {
        for (let i = this.tokens.length - 1; i >= 0; i--) {
            if (this.tokens[i].tagId === tagId) {
                const tokensFromToken = this.parseSentenceOnTokens(this.tokens[i].value);
                this.tokens.splice(i, 1, ...tokensFromToken);
            }
        }
    }

    public combineTokenWithLeftOne (targetTokenIndex: number): void {
        const targetToken = this.getTokenByIndex(targetTokenIndex);
        const isLeftTokenHasSameTag = this.checkIsTokenFromLeftWithSameTag(targetTokenIndex);

        if (isLeftTokenHasSameTag) {
            targetToken.value = `${this._tokens[targetTokenIndex - 1].value} ${targetToken.value}`;

            this._tokens.splice(targetTokenIndex - 1, 1);
        }
    }

    public combineTokenWithRightOne (targetTokenIndex: number): void {
        const targetToken = this.getTokenByIndex(targetTokenIndex);
        const isRightTokenHasSameTag = this.checkIsTokenFromRightWithSameTag(targetTokenIndex);

        if (isRightTokenHasSameTag) {
            targetToken.value += ` ${this._tokens[targetTokenIndex + 1].value}`;

            this._tokens.splice(targetTokenIndex + 1, 1);
        }
    }

    public checkIsTokenFromLeftWithSameTag (tokenIndex: number): boolean {
        const leftTokenId: number = this._tokens[tokenIndex - 1]?.tagId;
        const targetTokenId: number = this._tokens[tokenIndex]?.tagId;

        return  Number.isInteger(leftTokenId)   &&
                Number.isInteger(targetTokenId) &&
                (leftTokenId === targetTokenId);
    }

    public checkIsTokenFromRightWithSameTag (tokenIndex: number): boolean {
        const rightTokenId: number = this._tokens[tokenIndex + 1]?.tagId;
        const targetTokenId: number = this._tokens[tokenIndex]?.tagId;

        return  Number.isInteger(rightTokenId)  &&
                Number.isInteger(targetTokenId) &&
                (rightTokenId === targetTokenId);
    }

}
