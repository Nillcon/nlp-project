import { NgModule } from '@angular/core';
import { SharedModule } from '@Dashboard/app-shared.module';

import {
    ExtractorFieldComponent,
    ExtractorBlockComponent,
    TypeBlockComponent,
    UploaderComponent,
    ColumnsSelectorComponent,
    ModelWordsTableComponent,
    LemmatizationTableActionsComponent,
    ManualLemmaFieldComponent,
    ColumnSelectorSelectionDropdownComponent,
    ColumnSelectorSheetButtonComponent,
    ColumnAssociationTableComponent
} from './components';

@NgModule({
    declarations: [
        UploaderComponent,
        TypeBlockComponent,
        ExtractorFieldComponent,
        ExtractorBlockComponent,
        ColumnsSelectorComponent,
        ModelWordsTableComponent,
        LemmatizationTableActionsComponent,
        ManualLemmaFieldComponent,
        ColumnSelectorSelectionDropdownComponent,
        ColumnSelectorSheetButtonComponent,
        ColumnAssociationTableComponent
    ],
    imports: [
        SharedModule
    ],
    exports: [
        UploaderComponent,
        TypeBlockComponent,
        ExtractorFieldComponent,
        ExtractorBlockComponent,
        ColumnsSelectorComponent,
        ModelWordsTableComponent,
        ManualLemmaFieldComponent,
        ColumnAssociationTableComponent
    ]
})
export class DatasetModule {}
