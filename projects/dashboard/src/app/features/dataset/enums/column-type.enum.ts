export enum DatasetColumnTypeEnum {
    None,
    Value,
    SentimentLabel
}
