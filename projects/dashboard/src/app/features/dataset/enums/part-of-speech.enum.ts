export enum PartOfSpeechEnum {
    Noun = 1,
    Verb,
    Adjective
}
