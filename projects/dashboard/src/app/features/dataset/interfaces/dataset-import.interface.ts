import { DatasetSelectedColumn } from './dataset-selected-column.interface';

export interface DatasetImportData {
    worksheetIndex: number;
    selectedColumns: DatasetSelectedColumn[];
}
