import { DatasetColumnTypeEnum } from '../enums';

export interface DatasetColumnType {
    id: DatasetColumnTypeEnum;
    name: string;
    isUnique: boolean;
    isNeedAssociation: boolean;
}
