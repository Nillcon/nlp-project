export interface DatasetExtractorToken {
    value: string;
    tagId: number;
}
