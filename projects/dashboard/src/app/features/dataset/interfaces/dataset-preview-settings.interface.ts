export interface DatasetPreviewSettings {
    delimiter: string;
    hasHeader: boolean;
    isLabelSelectionEnabled: boolean;
}
