import { DatasetSourceTypeEnum } from '../enums/dataset-source-type.enum';

export interface DatasetSourceType {
    id: DatasetSourceTypeEnum;
    name: string;
    iconLink: string;
    acceptFileTypes: string;
    isHasDelimiterForColumns: boolean;
}
