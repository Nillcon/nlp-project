import { DatasetColumnTypeEnum } from '../enums';
import { DatasetAssociationRow } from './association-row.interface';

export interface DatasetAssociationColumn {
    type: DatasetColumnTypeEnum;
    sheetIndex: number;
    uniqueRows: DatasetAssociationRow[];
}
