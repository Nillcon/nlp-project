export interface ModelWord {
    id: number;
    value: string;
    lemma: string;
    isStopWord: boolean;
}
