export interface DatasetAssociationRow {
    value: string;
    association: any;
}
