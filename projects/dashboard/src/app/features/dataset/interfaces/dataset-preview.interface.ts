import { DatasetSourceTypeEnum } from '../enums';
import { DatasetPreviewSettings, DatasetSheetData } from './';

export interface DatasetPreview extends DatasetPreviewSettings {
    data: Map<number, DatasetSheetData>;
    worksheets: string[];
    type: DatasetSourceTypeEnum;
}
