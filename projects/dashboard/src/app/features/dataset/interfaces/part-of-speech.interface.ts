import { PartOfSpeechEnum } from '../enums/part-of-speech.enum';

export interface PartOfSpeech {
    id: PartOfSpeechEnum;
    name: string;
    color: string;
}
