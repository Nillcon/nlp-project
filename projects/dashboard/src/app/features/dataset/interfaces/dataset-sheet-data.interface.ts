export interface DatasetSheetData {
    rows: string[][];
    totalRowCount: number;
}
