import { DatasetColumnTypeEnum } from '../enums';

export interface DatasetSelectedColumn {
    index: number;
    type: DatasetColumnTypeEnum;
}
