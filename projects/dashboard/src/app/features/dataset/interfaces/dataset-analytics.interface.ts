import { DatasetExtractorToken } from '@Dashboard/features/dataset';
import { AnalyticTag } from '@Dashboard/features/tag/interfaces/analytic-tag.interface';

export interface DatasetAnalytics {
    tokens: DatasetExtractorToken[];
    tags: AnalyticTag[];
}
