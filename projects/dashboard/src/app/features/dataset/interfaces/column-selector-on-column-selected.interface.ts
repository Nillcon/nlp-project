import { DatasetColumnTypeEnum } from '../enums';

export interface DatasetColumnSelectorOnColumnSelected {
    sheetIndex: number;
    columnIndex: number;
    columnTypeId: DatasetColumnTypeEnum;
}
