import { Component, OnInit, ChangeDetectionStrategy, Input, Output, EventEmitter, ViewChild, ElementRef, AfterViewInit } from '@angular/core';
import { IsString, IsBoolean } from 'class-validator';
import { ClassValidation } from '@Shared/decorators';

@ClassValidation()
@Component({
    selector: 'dataset-manual-lemma-field',
    templateUrl: './manual-lemma-field.component.html',
    styleUrls: ['./manual-lemma-field.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class ManualLemmaFieldComponent implements OnInit, AfterViewInit {

    @Input()
    @IsString()
    public value: string = '';

    @Input()
    @IsBoolean()
    public isAutoFocused: boolean = false;

    @Output()
    public OnKeyUp = new EventEmitter<KeyboardEvent>();

    @Output()
    public OnValueChanged = new EventEmitter<string>();

    @ViewChild('Input', { static: true })
    private readonly _inputElementRef: ElementRef<HTMLInputElement>;

    constructor () {}

    public ngOnInit (): void {}

    public ngAfterViewInit (): void {
        if (this.isAutoFocused) {
            this.focusInputElement();
        }
    }

    private focusInputElement (): void {
        this._inputElementRef.nativeElement.focus();
    }

}
