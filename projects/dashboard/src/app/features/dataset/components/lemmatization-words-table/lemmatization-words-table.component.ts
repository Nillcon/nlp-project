import { Component, OnInit, Input, ChangeDetectionStrategy, ViewChild, Output, EventEmitter, ChangeDetectorRef } from '@angular/core';
import { IsArray, IsBoolean } from 'class-validator';
import { ClassValidation } from '@Shared/decorators';
import { ModelWord } from '../../interfaces/model-word.interface';
import { Table } from 'primeng-lts/table';

@ClassValidation()
@Component({
    selector: 'dataset-lemmatization-words-table',
    templateUrl: './lemmatization-words-table.component.html',
    styleUrls: ['./lemmatization-words-table.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class ModelWordsTableComponent implements OnInit {

    @Input()
    @IsArray()
    public words: ModelWord[];

    @Input()
    @IsBoolean()
    public isSelectionEnabled: boolean = true;

    @Input()
    @IsBoolean()
    public isActionToolbarDisabled: boolean = true;

    @Output()
    public OnSelectionChanged = new EventEmitter<ModelWord[]>();

    @Output()
    public OnSelected = new EventEmitter<ModelWord>();

    @Output()
    public OnUnselected = new EventEmitter<ModelWord>();

    @Output()
    public OnStopWordButtonClick = new EventEmitter<ModelWord>();

    @Output()
    public OnLemmatizeButtonClick = new EventEmitter<ModelWord>();

    @Output()
    public OnPartOfSpeechButtonClick = new EventEmitter<ModelWord>();

    public filteringFields: Array<keyof ModelWord> = ['value'];

    public selectedWords: ModelWord[] = [];

    @ViewChild(Table, { static: true })
    private readonly _tableComponent: Table;

    constructor (
        private readonly changeDetector: ChangeDetectorRef
    ) {}

    public ngOnInit (): void {}

    public deselectAllRows (): void {
        this.selectedWords = [];

        this.changeDetector.markForCheck();
    }

    public onSelectionChanged (): void {
        this.OnSelectionChanged.emit(this.selectedWords);
    }

}
