import { Component, OnInit, ChangeDetectionStrategy, Output, EventEmitter, Input } from '@angular/core';
import { IsBoolean } from 'class-validator';
import { ClassValidation } from '@Shared/decorators';

@ClassValidation()
@Component({
    selector: 'dataset-lemmatization-table-actions',
    templateUrl: './lemmatization-table-actions.component.html',
    styleUrls: ['./lemmatization-table-actions.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class LemmatizationTableActionsComponent implements OnInit {

    @Input()
    @IsBoolean()
    public isDisabled: boolean = true;

    @Output()
    public OnStopWordButtonClick = new EventEmitter<MouseEvent>();

    @Output()
    public OnLemmatizeButtonClick = new EventEmitter<MouseEvent>();

    constructor () {}

    public ngOnInit (): void {}

}
