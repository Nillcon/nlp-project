import { Component, OnInit, ChangeDetectionStrategy, Input, Output, EventEmitter } from '@angular/core';
import { ClassValidation } from '@Shared/decorators';
import { DatasetSourceType } from '../../interfaces/dataset-source-type.interface';
import { IsObject, IsBoolean } from 'class-validator';

@ClassValidation()
@Component({
    selector: 'dataset-type-block',
    templateUrl: './type-block.component.html',
    styleUrls: ['./type-block.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class TypeBlockComponent implements OnInit {

    @Input()
    @IsObject()
    public readonly sourceType: DatasetSourceType;

    @Input()
    @IsBoolean()
    public readonly isSelectable: boolean = false;

    @Output()
    public OnClick: EventEmitter<MouseEvent> = new EventEmitter();

    constructor () {}

    public ngOnInit (): void {}

}
