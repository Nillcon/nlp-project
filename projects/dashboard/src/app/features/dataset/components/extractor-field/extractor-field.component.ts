import { Component, OnInit, Input, ChangeDetectionStrategy, ChangeDetectorRef, ContentChild, TemplateRef } from '@angular/core';
import { DatasetExtractorToken } from '../../interfaces/extractor-token.interface';
import { ClassValidation } from '@Shared/decorators';
import { IsArray, IsBoolean, IsString } from 'class-validator';
import { DatasetExtractorService } from '../../services/dataset-extractor.service';
import { Tag } from '@Dashboard/features/tag';

@ClassValidation()
@Component({
    selector: 'dataset-extractor-field',
    templateUrl: './extractor-field.component.html',
    styleUrls: ['./extractor-field.component.scss'],
    providers: [DatasetExtractorService],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class ExtractorFieldComponent implements OnInit {

    @Input()
    public set sentence (value: string) {
        this.extractorService.setSentence(value);
    }

    @Input()
    public set tokens (tokens: DatasetExtractorToken[]) {
        this.extractorService.setTokens(tokens);
    }

    @Input()
    @IsArray()
    public readonly tags: Tag[];

    @Input()
    @IsBoolean()
    public isReadonly: boolean = false;

    @Input()
    public set targetTagId (id: number) {
        this._targetTagId = id;
        this.targetTag    = this.getTagById(id);
    }

    public get targetTagId (): number {
        return this._targetTagId;
    }

    public targetTag: Tag;

    public get tokens (): DatasetExtractorToken[] {
        return this.extractorService.tokens;
    }

    @ContentChild('TagChipTemplate', { static: true })
    public readonly tagChipTemplate: TemplateRef<any>;

    private _targetTagId: number;

    constructor (
        private readonly extractorService: DatasetExtractorService,
        private readonly changeDetector: ChangeDetectorRef
    ) {}

    public ngOnInit (): void {}

    public getTagById (id: number): Tag {
        let targetTag: Tag = null;

        if (id) {
            targetTag = this.tags.find((currTag) => currTag.id === id);
        }

        return targetTag;
    }

    public removeTagFromTokens (tag: Tag): void {
        this.extractorService.removeTagFromTokens(tag.id);
    }

    public onTokenClick (tokenIndex: number): void {
        if (!this.isReadonly) {
            const targetToken: DatasetExtractorToken = this.extractorService.getTokenByIndex(tokenIndex);

            if (targetToken.tagId && targetToken.tagId === this.targetTagId) {
                this.extractorService.removeTagFromToken(tokenIndex);
            } else if (targetToken.tagId && targetToken.tagId !== this.targetTagId) {
                this.extractorService.setTagForToken(tokenIndex, this.targetTagId);
            } else {
                this.extractorService.setTagForToken(tokenIndex, this.targetTagId);
            }

            this.changeDetector.markForCheck();
        }
    }

    public trackById (tagIndex: number, tag: Tag): number {
        return tag.id;
    }

}
