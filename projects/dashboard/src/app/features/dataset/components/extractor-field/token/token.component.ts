import { Component, OnInit, Input, ChangeDetectionStrategy, Output, EventEmitter, ViewChild, ElementRef, TemplateRef } from '@angular/core';
import { Tag } from '@Dashboard/features/tag';

@Component({
    selector: 'dataset-token',
    templateUrl: './token.component.html',
    styleUrls: ['./token.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class ExtractorBlockComponent implements OnInit {

    @Input()
    public readonly tagChipTemplate: TemplateRef<any>;

    @Input()
    public readonly data: string;

    @Input()
    public readonly tokenIndex;

    @Input()
    public set tag (tag: Tag) {
        this._tag = tag;

        this.onTagChanged();
    }

    public get tag (): Tag {
        return this._tag;
    }

    @Input()
    public readonly targetTag: Tag;

    @Input()
    public isReadonly: boolean = false;

    @Output()
    public OnClick: EventEmitter<MouseEvent> = new EventEmitter();

    @ViewChild('WrapperElement', { static: true })
    private readonly _wrapperElementRef: ElementRef<HTMLDivElement>;

    private _tag: Tag;

    constructor () {}

    public ngOnInit (): void {}

    public onMouseOverToken (): void {
        if (!this.isReadonly && !this._tag) {
            this.addSelectionStyles(this.targetTag);
        }
    }

    public onMouseOutToken (): void {
        if (!this.isReadonly && !this._tag) {
            this.removeSelectionStyles();
        }
    }

    private onTagChanged (): void {
        if (this._tag) {
            this.addSelectionStyles(this._tag);
        } else {
            this.removeSelectionStyles();
        }
    }

    private addSelectionStyles (tag: Tag): void {
        this._wrapperElementRef.nativeElement.style.border     = `solid 1px ${tag.color}`;
        this._wrapperElementRef.nativeElement.style.background = `${tag.color}10`;
    }

    private removeSelectionStyles (): void {
        this._wrapperElementRef.nativeElement.style.border     = '';
        this._wrapperElementRef.nativeElement.style.background = '';
    }

}
