import { Component, OnInit, ChangeDetectionStrategy, Input, ViewChild, Output, EventEmitter } from '@angular/core';
import { IsString } from 'class-validator';
import { ClassValidation } from '@Shared/decorators';

@ClassValidation()
@Component({
    selector: 'dataset-uploader',
    templateUrl: './uploader.component.html',
    styleUrls: ['./uploader.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class UploaderComponent implements OnInit {

    @Input()
    @IsString()
    public acceptFiles: string;

    @Output()
    public OnFilesSelected = new EventEmitter<Blob[]>();

    constructor () {}

    public ngOnInit (): void {}

    public onUploadCustomHandler (e: any): void {
        this.OnFilesSelected.emit(e.files);
    }

}
