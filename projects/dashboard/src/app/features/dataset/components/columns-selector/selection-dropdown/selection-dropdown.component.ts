import { Component, OnInit, ChangeDetectionStrategy, Input, Output, EventEmitter } from '@angular/core';
import { SelectItem } from 'primeng-lts/api';

import {
    DatasetColumnType
} from '../../../interfaces';

import {
    DatasetColumnTypeEnum
} from '../../../enums';

import {
    DatasetColumnTypeMap
} from '../../../data';

@Component({
    selector: 'selection-dropdown',
    templateUrl: './selection-dropdown.component.html',
    styleUrls: ['./selection-dropdown.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class ColumnSelectorSelectionDropdownComponent implements OnInit {

    @Input()
    public selectionId: DatasetColumnTypeEnum;

    @Input()
    public columnTypes: DatasetColumnTypeEnum[];

    @Output()
    public OnSelection = new EventEmitter<number>();

    public datasetColumnTypeEnum = DatasetColumnTypeEnum;

    public selectboxColumnTypeItems: SelectItem[];

    constructor () {}

    public ngOnInit (): void {
        this.initColumnTypeArray();
    }

    public getSelectionTypeById (id: DatasetColumnTypeEnum): DatasetColumnType {
        return DatasetColumnTypeMap.get(id);
    }

    private initColumnTypeArray (): void {
        const items: SelectItem[] = [];
        const itemsToAdd  = [DatasetColumnTypeEnum.None, ...this.columnTypes];

        for (const datasetColumnType of itemsToAdd) {
            const item = this.getSelectionTypeById(datasetColumnType);

            items.push({
                label: item.name,
                value: item.id
            });
        }

        this.selectboxColumnTypeItems = items;
    }

}
