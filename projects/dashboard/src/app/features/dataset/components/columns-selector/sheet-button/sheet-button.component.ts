import { Component, OnInit, ChangeDetectionStrategy, Input, Output, EventEmitter } from '@angular/core';

@Component({
    selector: 'sheet-button',
    templateUrl: './sheet-button.component.html',
    styleUrls: ['./sheet-button.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class ColumnSelectorSheetButtonComponent implements OnInit {

    @Input()
    public selectedSheetIndex: number;

    @Input()
    public isSelected: boolean;

    @Input()
    public sheetName: string;

    @Output()
    public OnSheetButtonClick = new EventEmitter<MouseEvent>();

    constructor () {}

    public ngOnInit (): void {}

}
