import { Component, OnInit, ChangeDetectionStrategy, Input, Output, EventEmitter, ChangeDetectorRef } from '@angular/core';
import { ClassValidation } from '@Shared/decorators/class-validation.decorator';
import { IsArray, IsObject, IsBoolean } from 'class-validator';

import {
    DatasetColumnTypeEnum
} from '../../enums';

import {
    DatasetSheetData,
    DatasetColumnSelectorOnColumnSelected as OnColumnSelectedData
} from '../../interfaces';

import {
    DatasetColumnTypeMap
} from '../../data';

@ClassValidation()
@Component({
    selector: 'dataset-columns-selector',
    templateUrl: './columns-selector.component.html',
    styleUrls: ['./columns-selector.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class ColumnsSelectorComponent implements OnInit {

    @Input()
    public maxRowCountToShow: number = 6;

    @Input()
    @IsArray()
    public sheets: string[];

    @Input()
    @IsObject()
    public sheetDataMap: Map<number, DatasetSheetData>;

    @Input()
    @IsBoolean()
    public isDiscardFirstRow: boolean;

    @Input()
    @IsArray()
    public columnTypes: DatasetColumnTypeEnum[];

    @Output()
    public OnColumnSelected = new EventEmitter<OnColumnSelectedData>();

    @Output()
    public OnSelectionBetweenMultipleSheets = new EventEmitter<Map<number, Map<number, DatasetColumnTypeEnum>>>();

    @Output()
    public OnSelectionInActiveSheet = new EventEmitter<Map<number, DatasetColumnTypeEnum>>();

    @Output()
    public OnSheetSelected = new EventEmitter<number>();

    public datasetColumnTypeEnum = DatasetColumnTypeEnum;

    public selectedSheetIndex: number = 0;

    public selectedColumnsPerSheet: Map<number, Map<number, DatasetColumnTypeEnum>>;
    public totalRowCountPerSheet = new Map<number, number>();

    constructor (
        private readonly changeDetection: ChangeDetectorRef
    ) {}

    public ngOnInit (): void {
        this.initSelectedColumns();
        this.initTotalRowCount();
        this.emitSelectionData();
    }

    public onColumnSelection (columnIndex: number, columnTypeId: DatasetColumnTypeEnum): void {
        const columnType = DatasetColumnTypeMap.get(columnTypeId);

        if (columnType.isUnique) {
            this.clearColumnsSelectionWithType(this.selectedSheetIndex, columnTypeId);
        }

        this.selectedColumnsPerSheet
            .get(this.selectedSheetIndex)
            .set(columnIndex, columnTypeId);

        this.emitSelectionData();

        this.OnColumnSelected.emit({
            sheetIndex: this.selectedSheetIndex,
            columnIndex,
            columnTypeId
        });
    }

    public onSheetButtonClick (sheetIndex: number): void {
        this.selectedSheetIndex = sheetIndex;

        this.OnSheetSelected.emit(sheetIndex);
    }

    public getColumnSelection (columnIndex: number): DatasetColumnTypeEnum {
        return this.selectedColumnsPerSheet
            .get(this.selectedSheetIndex)
            .get(columnIndex);
    }

    public getSheetRows (sheetIndex: number): string[][] {
        return this.sheetDataMap.get(sheetIndex).rows;
    }

    /** Method-helper for ngFor that helps generate tr elements in count as array length */
    public getRowCountArrayToShow (sheetIndex: number): any[] {
        const rowCount = this.getRowCountToShow(sheetIndex);

        return [].constructor(rowCount);
    }

    public getRowCountToShow (sheetIndex: number): number {
        const sheetRowCount = this.totalRowCountPerSheet.get(sheetIndex);

        return (sheetRowCount > this.maxRowCountToShow) ? this.maxRowCountToShow : sheetRowCount;
    }

    public clearColumnsSelectionWithType (sheetIndex: number, columnTypeId: DatasetColumnTypeEnum): void {
        const selectionData = this.selectedColumnsPerSheet.get(sheetIndex);

        for (const [columnIndex, columnSelection] of selectionData.entries()) {
            if (columnSelection === columnTypeId) {
                this.selectedColumnsPerSheet
                    .get(sheetIndex)
                    .set(columnIndex, DatasetColumnTypeEnum.None);
            }
        }

        this.changeDetection.markForCheck();
    }

    private emitSelectionData (): void {
        this.OnSelectionBetweenMultipleSheets.emit(this.selectedColumnsPerSheet);
        this.OnSelectionInActiveSheet.emit(this.selectedColumnsPerSheet.get(this.selectedSheetIndex));
    }

    private initSelectedColumns (): void {
        this.selectedColumnsPerSheet = new Map();

        for (const [sheetIndex, sheetData] of this.sheetDataMap.entries()) {
            const selectedColumnsMap = new Map<number, DatasetColumnTypeEnum>();

            sheetData.rows[0].forEach((_, currColumnIndex) => {
                selectedColumnsMap.set(currColumnIndex, DatasetColumnTypeEnum.None);
            });

            this.selectedColumnsPerSheet.set(sheetIndex, selectedColumnsMap);
        }
    }

    private initTotalRowCount (): void {
        for (const [sheetIndex, sheetData] of this.sheetDataMap.entries()) {
            this.totalRowCountPerSheet.set(+sheetIndex, sheetData.totalRowCount);
        }
    }

}
