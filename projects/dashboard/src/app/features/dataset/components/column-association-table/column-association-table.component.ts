import { Component, OnInit, ChangeDetectionStrategy, Input, Output, EventEmitter } from '@angular/core';
import { DatasetAssociationColumn } from '../../interfaces';
import { IsObject, IsArray } from 'class-validator';
import { ClassValidation } from '@Shared/decorators';
import { SelectItem } from 'primeng-lts/api';

@ClassValidation()
@Component({
    selector: 'dataset-column-association-table',
    templateUrl: './column-association-table.component.html',
    styleUrls: ['./column-association-table.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class ColumnAssociationTableComponent implements OnInit {

    @Input()
    @IsObject()
    public associationColumn: DatasetAssociationColumn;

    @Input()
    @IsArray()
    public associationDropdownItems: SelectItem[];

    @Output()
    public OnAssociated = new EventEmitter<any>();

    constructor () {}

    public ngOnInit (): void {}

}
