import { NgModule } from '@angular/core';
import { SharedModule } from '@Dashboard/app-shared.module';

import {
    CreationFieldComponent,
    ChipComponent
} from './components';

@NgModule({
    declarations: [
        CreationFieldComponent,
        ChipComponent
    ],
    imports: [
        SharedModule
    ],
    exports: [
        CreationFieldComponent,
        ChipComponent
    ]
})
export class TagModule {}
