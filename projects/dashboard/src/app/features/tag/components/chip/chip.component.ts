import { Component, OnInit, ChangeDetectionStrategy, Input, Output, EventEmitter } from '@angular/core';
import { Tag } from '../../interfaces';
import { IsObject, IsEnum, IsBoolean, IsString } from 'class-validator';
import { ClassValidation } from '@Shared/decorators';
import { ModelStateEnum } from '@Shared/enums';

@ClassValidation()
@Component({
    selector: 'tag-chip',
    templateUrl: './chip.component.html',
    styleUrls: ['./chip.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class ChipComponent implements OnInit {

    @Input()
    public set data (tag: Tag) {
        this._data = tag;

        this.initEnteredTagName(tag.name);
    }

    public get data (): Tag {
        return this._data;
    }

    @Input()
    @IsEnum(ModelStateEnum)
    public modelState: ModelStateEnum = ModelStateEnum.Created;

    @Input()
    @IsBoolean()
    public isEditable: boolean = false;

    @Input()
    @IsBoolean()
    public isCloseButtonVisible: boolean = true;

    @Input()
    @IsString()
    public readonly maxWidth: string = '100%';

    @Output()
    public OnCloseButtonClick: EventEmitter<MouseEvent> = new EventEmitter();

    @Output()
    public OnTagNameEdited: EventEmitter<string> = new EventEmitter();

    public enteredTagName: string;

    public readonly ModelStateEnumEnum: typeof ModelStateEnum = ModelStateEnum;

    @IsObject()
    private _data: Tag;

    constructor () {}

    public ngOnInit (): void {}

    public onTagInputKeyUp (event: KeyboardEvent): void {
        const inputValue: string = (event.target as HTMLInputElement).value;
        this.enteredTagName      = inputValue;
    }

    private initEnteredTagName (tagName: string): void {
        this.enteredTagName = tagName;
    }

}
