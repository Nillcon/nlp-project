import { Component, OnInit, Output, ChangeDetectionStrategy, Input, ViewChild, AfterViewInit, EventEmitter, ElementRef, OnDestroy, ChangeDetectorRef } from '@angular/core';
import { IsBoolean } from 'class-validator';
import { ClassValidation } from '@Shared/decorators';
import { Tag } from '../../interfaces';
import { Form, BaseForm } from '@Shared/features';
import { FormGroupTypeSafe, FormBuilderTypeSafe, FormControlTypeSafe } from 'form-type-safe';
import { Validators } from '@angular/forms';

@Form()
@ClassValidation()
@Component({
    selector: 'tag-creation-field',
    templateUrl: './creation-field.component.html',
    styleUrls: ['./creation-field.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class CreationFieldComponent implements OnInit, OnDestroy, AfterViewInit, BaseForm<Partial<Tag>> {

    @Input()
    @IsBoolean()
    public readonly isAutoFocused: boolean = false;

    @Output()
    public OnFormInit = new EventEmitter<FormGroupTypeSafe<Partial<Tag>>>();

    @Output()
    public OnEnter = new EventEmitter<string>();

    public formGroup: FormGroupTypeSafe<Partial<Tag>>;

    @ViewChild('InputField', { static: true })
    private readonly _inputElementRef: ElementRef<HTMLInputElement>;

    constructor (
        private readonly formBuilder: FormBuilderTypeSafe
    ) {}

    public ngOnInit (): void {}

    public ngOnDestroy (): void {}

    public ngAfterViewInit (): void {
        if (this.isAutoFocused) {
            this.focusInputElement();
        }
    }

    public formGroupInit (): FormGroupTypeSafe<Partial<Tag>> {
        return this.formBuilder.group<Partial<Tag>>({
            name: new FormControlTypeSafe('', [Validators.required])
        });
    }

    public focusInputElement (): void {
        this._inputElementRef.nativeElement.focus();
    }

    public onKeyUp (event: KeyboardEvent): void {
        if (event.code === 'Enter') {
            const enteredTagName = this.formGroup.controls.name.value;

            this.OnEnter.emit(enteredTagName);
        }
    }

}
