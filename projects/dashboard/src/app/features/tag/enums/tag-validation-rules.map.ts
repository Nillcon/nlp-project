import { Tag } from '../interfaces';
import { ValidatorFn, Validators } from '@angular/forms';

export const TagValidationRules = new Map<keyof Tag, ValidatorFn[]>()
    .set(
        'name',
        [
            Validators.required,
            Validators.minLength(3),
            Validators.maxLength(6)
        ]
    )
    .set(
        'color',
        [
            Validators.required,
            Validators.minLength(7),
            Validators.maxLength(7)
        ]
    );
