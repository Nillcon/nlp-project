import { Tag } from '@Dashboard/features/tag';
import { SentimentEnum } from '@Dashboard/features/model';

export interface AnalyticTag extends Tag {
    sentiment: SentimentEnum;
    confidencePercent: number;
}
