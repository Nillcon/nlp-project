import { Injectable } from "@angular/core";
import { HttpRequestService } from '@Shared/features';
import { Observable, of, timer, throwError } from 'rxjs';
import { Tag } from '../interfaces';
import { mapTo, tap } from 'rxjs/operators';

@Injectable({
    providedIn: 'root'
})
export class TagApiService {

    constructor (
        private readonly httpService: HttpRequestService
    ) {}

    public getAll (): Observable<Tag[]> {
        return of([
            { id: 1, name: 'Sport', color: '#4287f5' },
            { id: 2, name: 'Test', color: '#e3a02d' },
            { id: 3, name: 'Incredible', color: '#e3672d' }
        ]);
    }

    public get (id: number): Observable<Tag> {
        return timer(1000)
            .pipe(
                mapTo({ id: 4, name: 'Sport', color: '#4287f5' })
            );
    }

    public create (tagName: string): Observable<number> {
        return of(4);
    }

    public edit (id: number, tag: Partial<Tag>): Observable<any> {
        return timer(500)
            .pipe(
                tap(() => {
                    if (tag.name.length === 0) {
                        throw new Error('Tag name can\'t be empty!');
                    }
                }),
                mapTo(true)
            );
    }

    public delete (id: number): Observable<any> {
        return of(null);
    }

}
