import { Injectable } from "@angular/core";
import { TagApiService } from './tag-api.service';
import { Observable } from 'rxjs';
import { Tag } from '../interfaces';

@Injectable({
    providedIn: 'root'
})
export class TagService {

    constructor (
        private readonly tagApiService: TagApiService
    ) {}

    public getAll (): Observable<Tag[]> {
        return this.tagApiService.getAll();
    }

    public get (id: number): Observable<Tag> {
        return this.tagApiService.get(id);
    }

    public create (tagName: string): Observable<number> {
        return this.tagApiService.create(tagName);
    }

    public edit (id: number, tag: Partial<Tag>): Observable<any> {
        return this.tagApiService.edit(id, tag);
    }

    public delete (id: number): Observable<any> {
        return this.tagApiService.delete(id);
    }

}
