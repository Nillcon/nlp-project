import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { Tag } from '../interfaces';

@Injectable({
    providedIn: 'root'
})
export class TagStateService {

    private readonly _tags$: BehaviorSubject<Tag[]> = new BehaviorSubject(null);

    constructor () {}

    public getTags$ (): Observable<Tag[]> {
        return this._tags$.asObservable();
    }

    public getTags (): Tag[] {
        return this._tags$.getValue();
    }

    public setTags (tags: Tag[]): void {
        this._tags$.next(tags);
    }

    public updateTag (tag: Tag): void {
        const tags = this.getTags();
        const targetTagId = tags
            .findIndex((currTag) => currTag.id === tag.id);

        tags[targetTagId] = tag;

        this._tags$.next([...tags]);
    }

    public updateTagId (tagToReplace: Tag, addedTagWithId: Tag): void {
        const tags = this.getTags();
        const targetTagIndex = tags
            .findIndex((currTag) => currTag === tagToReplace);

        tags[targetTagIndex] = addedTagWithId;

        this._tags$.next([...tags]);
    }

    public addTag (tag: Partial<Tag>): void {
        const tags = this.getTags();

        this._tags$.next([...tags, tag as Tag]);
    }

    public editTag (tag: Partial<Tag>): void {
        const tags = this.getTags();
        const targetTagIndex = tags.findIndex((currTag) => currTag.id === tag.id);

        tags[targetTagIndex] = { ...tags[targetTagIndex], ...tag };

        this._tags$.next([...tags]);
    }

    public removeTag (tagId: number): void {
        const tags = this.getTags();
        const filteredTags = tags
            .filter((currTag) => currTag.id !== tagId);

        this._tags$.next(filteredTags);
    }

}
