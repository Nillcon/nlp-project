import { Component, ChangeDetectionStrategy, OnInit, OnDestroy } from '@angular/core';
import { HttpInterceptorFacade } from '@Dashboard/view-features';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class AppComponent implements OnInit, OnDestroy {

    constructor (
        private readonly httpInterceptorFacade: HttpInterceptorFacade
    ) {}

    public ngOnInit (): void {
        this.httpInterceptorFacade.subscribe();
    }

    public ngOnDestroy (): void {}

}
