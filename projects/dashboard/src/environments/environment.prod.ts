export const environment = {
    production: true,
    host: location.origin,
    apiRoute: '/api',
    landingAppPath: '/'
};
