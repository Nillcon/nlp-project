
import { PipeTransform, Pipe } from '@angular/core';

@Pipe({
    name: 'sliceWithSymbols'
})
export class SliceWithSymbolsPipe implements PipeTransform {

    constructor () {}

    public transform (text: string, maxLength: number, symbols: string = '...'): any {
        let result: string = text;

        if (text.length > maxLength) {
            result = result.slice(0, maxLength) + symbols;
        }

        return result;
    }

}
