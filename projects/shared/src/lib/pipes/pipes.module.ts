import { NgModule } from '@angular/core';
import { SafeHtmlPipe } from './safe-html.pipe';
import { SafeUrlPipe } from './safe-url.pipe';
import { RandomPipe } from './random.pipe';
import { ToFixedPipe } from './to-fixed.pipe';
import { SliceWithSymbolsPipe } from './slice-with-symbols.pipe';

@NgModule({
    declarations: [
        SafeHtmlPipe,
        SafeUrlPipe,
        RandomPipe,
        ToFixedPipe,
        SliceWithSymbolsPipe
    ],
    exports: [
        SafeHtmlPipe,
        SafeUrlPipe,
        RandomPipe,
        ToFixedPipe,
        SliceWithSymbolsPipe
    ]
})
export class PipesModule {}
