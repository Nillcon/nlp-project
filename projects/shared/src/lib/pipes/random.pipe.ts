
import { PipeTransform, Pipe } from '@angular/core';

@Pipe({
    name: 'random'
})
export class RandomPipe implements PipeTransform {

    constructor () {}

    public transform (items: any[]): any {
        const randomIndex = Math.floor(Math.random() * items.length);

        return items[randomIndex];
    }

}
