
import { PipeTransform, Pipe } from '@angular/core';

@Pipe({
    name: 'toFixed'
})
export class ToFixedPipe implements PipeTransform {

    constructor () {}

    public transform (num: number, digitsCount: number = 2): number {
        return +(+num).toFixed(digitsCount);
    }

}
