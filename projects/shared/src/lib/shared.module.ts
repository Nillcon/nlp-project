import { NgModule } from '@angular/core';
import { ToastModule } from './features/toast';
import { ContentLoaderModule } from './modules/content-loader/content-loader.module';
import { ConfirmationWindowModule } from './modules/confirmation-window/confirmation-window.module';
import { PipesModule } from './pipes/pipes.module';
import { ButtonLoaderDirective } from './directives/button-loader.directive';
import { ButtonLoaderModule } from './modules/button-loader/button-loader.module';
import { LoadingWindowModule } from './modules/loading-window';
import { MarkAsForbiddenClickDirective } from './features';

@NgModule({
    declarations: [
        MarkAsForbiddenClickDirective,
        ButtonLoaderDirective
    ],
    imports: [
        PipesModule,
        ContentLoaderModule,
        ToastModule,
        ConfirmationWindowModule,
        LoadingWindowModule,
        ButtonLoaderModule
    ],
    exports: [
        MarkAsForbiddenClickDirective,
        ButtonLoaderDirective,

        PipesModule,
        ContentLoaderModule,
        ToastModule,
        ConfirmationWindowModule,
        LoadingWindowModule,
        ButtonLoaderModule
    ]
})
export class SharedModule {}
