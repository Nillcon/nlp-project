import { Component, OnInit, Input, OnDestroy, ChangeDetectionStrategy, Inject } from '@angular/core';
import { ChatService } from './services';

@Component({
    selector: 'chat',
    templateUrl: './chat.component.html',
    styleUrls: ['./chat.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class ChatComponent implements OnInit, OnDestroy {

    @Input()
    public roomId: string;

    constructor (
        private readonly chatService: ChatService
    ) {}

    public ngOnInit (): void {
        this.chatService.enterToRoom(this.roomId)
            .subscribe();
    }

    public ngOnDestroy (): void {
        this.chatService.leaveRoom(this.roomId);
    }

}
