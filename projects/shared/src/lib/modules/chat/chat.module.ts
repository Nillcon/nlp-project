import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { ChatModule as KendoChatModule } from '@progress/kendo-angular-conversational-ui';
import { InputsModule } from '@progress/kendo-angular-inputs';
import { ButtonsModule } from '@progress/kendo-angular-buttons';

import { ChatComponent } from './chat.component';

import {
    ConversationalBlockComponent,
    CheckboxComponent
} from './components';

import { ChatService } from './services';


@NgModule({
    declarations: [
      ChatComponent,
      ConversationalBlockComponent,
      CheckboxComponent
    ],
    imports: [
        FormsModule,
        CommonModule,
        KendoChatModule,
        InputsModule,
        ButtonsModule
    ],
    providers: [
        ChatService
    ],
    exports: [
        ChatComponent
    ]
})
export class ChatModule { }
