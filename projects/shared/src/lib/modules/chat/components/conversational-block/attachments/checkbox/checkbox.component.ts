import { Component, OnInit, Input, HostBinding, Output, EventEmitter, ChangeDetectionStrategy } from '@angular/core';

import { CheckboxAttachement, Checkbox } from '../../../../interfaces/checkbox';

@Component({
    selector: 'checkbox',
    templateUrl: './checkbox.component.html',
    styleUrls: ['./checkbox.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class CheckboxComponent implements OnInit {

    @Input()
    public attachment: CheckboxAttachement;

    @Output()
    public OnSubmit: EventEmitter<string> = new EventEmitter();

    @HostBinding('class.k-card')
    public cssClass = true;

    public blockIsVisible: boolean = true;

    constructor () { }

    public ngOnInit (): void {
    }

    public onSubmitButtonClick (): void {
        const checkedItems = this.getCheckedItems(this.attachment.content);
        const result = this.getValueFromCheckboxes(checkedItems);

        this.OnSubmit.emit(result);

        this.hideBlock();
    }

    private getValueFromCheckboxes (checkboxes: Checkbox[]): string {
        const value = checkboxes
            .map(checkbox => checkbox.value)
            .join(', ');

        return value;
    }

    private getCheckedItems (checkboxes: Checkbox[]): Checkbox[] {
        const checkedItems = checkboxes
            .filter(checkbox => checkbox.isCheck);

        return checkedItems;
    }

    private hideBlock (): void {
        this.blockIsVisible = false;
    }

    private showBlock (): void {
        this.blockIsVisible = true;
    }


}
