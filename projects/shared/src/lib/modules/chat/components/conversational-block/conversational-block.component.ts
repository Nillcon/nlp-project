import { Component, OnInit, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { User, SendMessageEvent, Message, ExecuteActionEvent } from '@progress/kendo-angular-conversational-ui';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { ChatService } from '../../services/chat.service';

@Component({
    selector: 'conversational-block',
    templateUrl: './conversational-block.component.html',
    styleUrls: ['./conversational-block.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class ConversationalBlockComponent implements OnInit {

    public messages$: Observable<Message[]>;

    public readonly user: User = {
        id: 0
    };

    constructor (
        private readonly chatService: ChatService,
        private readonly changeDetectorRef: ChangeDetectorRef
    ) { }

    public ngOnInit (): void {
        this.chatOwnerIdObserver();

        this.messageObserver();
    }

    public onSendButtonClick (messageEvent: SendMessageEvent): void {
        this.send(messageEvent.message);
    }

    public onExecuteAction (messageEvent: ExecuteActionEvent): void {
    }

    public onSubmitAction (result: string): void {
        const message: Message = {
            author: this.user,
            text: result,
            timestamp: new Date()
        };

        this.send(message);
    }

    private send (message: Message): void {
        this.chatService.send(message)
            .subscribe();
    }

    private chatOwnerIdObserver (): void {
        this.chatService._chatOwnerId$
            .subscribe((chatOwnerId: number) => {
                this.user.id = chatOwnerId;
            });
    }

    private messageObserver (): void {
        this.messages$ = this.chatService.messages$
            .pipe(
                tap(() => {
                    setTimeout(() => {
                        this.changeDetectorRef.detectChanges();
                    });
                })
            );
    }
}
