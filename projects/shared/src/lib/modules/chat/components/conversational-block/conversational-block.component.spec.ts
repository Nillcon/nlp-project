import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConversationalBlockComponent } from './conversational-block.component';

describe('ConversationalBlockComponent', () => {
  let component: ConversationalBlockComponent;
  let fixture: ComponentFixture<ConversationalBlockComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConversationalBlockComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConversationalBlockComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
