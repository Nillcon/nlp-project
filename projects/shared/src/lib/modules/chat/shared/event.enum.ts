export enum EventEnum {
    InitializeChat = 'InitializeChat',
    NewMessage = 'NewMessage',
    History = 'history'
}
