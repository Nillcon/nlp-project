export enum InvokesEnum {
    RoomEnter = 'OnRoomEnter',
    RoomExit = 'OnRoomExit',
    Send = 'send'
}
