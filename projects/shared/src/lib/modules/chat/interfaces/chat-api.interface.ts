import { Observable } from 'rxjs';
import { Message } from '@progress/kendo-angular-conversational-ui';

export interface ChatApi {
    chatOwnerId$: Observable<number>;
    previusMessages$: Observable<Message[]>;
    newMessage$: Observable<Message>;

    initStream: () => void;
    startListen: () => void;
    enterToRoom: (id: string) => Observable<any>;
    leaveRoom: (id: string) => Observable<any>;
    send: (message: Message) => Observable<any>;
    onInitializeChat: () => void;
}
