export interface Checkbox {
    title: string;
    value: string;
    isCheck?: boolean;
}
