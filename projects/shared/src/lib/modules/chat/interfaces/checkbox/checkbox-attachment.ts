import { Attachment } from "@progress/kendo-angular-conversational-ui";
import { Checkbox } from './checkbox';

export interface CheckboxAttachement extends Attachment {
    content: Checkbox[];
}
