import { InjectionToken } from '@angular/core';
import { ChatApi } from '../interfaces';

export const CHAT_API = new InjectionToken<ChatApi>('CHAT_API');
