import { Injectable, Inject } from '@angular/core';
// import { ChatApiService } from './chat-api.service';
import { Observable, BehaviorSubject } from 'rxjs';
import { Message } from '@progress/kendo-angular-conversational-ui';
import { CHAT_API } from '../config';
import { ChatApi } from '../interfaces';

@Injectable()
export class ChatService {

    public _chatOwnerId$: Observable<number>;

    public get messages$ (): Observable<Message[]> {
        return this._messages$.asObservable();
    }

    private readonly _messages: Message[] = [];
    private readonly _messages$: BehaviorSubject<Message[]> = new BehaviorSubject(this._messages);

    constructor (
        @Inject(CHAT_API) private readonly chatApiService: ChatApi
    ) {
        this.connect();
    }

    public connect (): void {
        this.chatApiService.initStream();
        this.chatApiService.startListen();

        this.chatOwnerObserver();
        this.newMessageObserver();
        this.previusMessagesObserver();
    }

    public send (message: Message): Observable<any> {
        this.addMessage(message);
        console.log(message);

        return this.chatApiService.send(message);
    }

    public enterToRoom (id: string): Observable<any> {
        return this.chatApiService.enterToRoom(id);
    }

    public leaveRoom (id: string): Observable<any>  {
        return this.chatApiService.leaveRoom(id);
    }

    private chatOwnerObserver (): void {
        this._chatOwnerId$ = this.chatApiService.chatOwnerId$;
    }

    private newMessageObserver (): void {
        this.chatApiService.newMessage$
            .subscribe((message: Message) => {
                this.addMessage(message);
            });
    }

    private previusMessagesObserver (): void {
        this.chatApiService.previusMessages$
            .subscribe((messages: Message[]) => {
                this.addMessages(messages);
            });
    }

    private addMessage (message: Message): void {
        message.timestamp = new Date(message.timestamp);

        this._messages.push(message);
        this._messages$.next([...this._messages]);
    }

    private addMessages (messages: Message[]): void {
        messages.forEach(message => {
            this.addMessage(message);
        });
    }
}
