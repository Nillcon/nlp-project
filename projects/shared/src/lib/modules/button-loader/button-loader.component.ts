import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';

@Component({
    selector: 'button-loader',
    templateUrl: './button-loader.component.html',
    styleUrls: ['../../styles/button-loader.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class ButtonLoaderComponent implements OnInit {

    constructor () {}

    public ngOnInit (): void {}

}
