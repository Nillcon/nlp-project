import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ContentLoaderComponent } from './content-loader.component';
import { ProgressSpinnerModule } from 'primeng-lts/progressspinner';

@NgModule({
    declarations: [
        ContentLoaderComponent
    ],
    imports: [
        CommonModule,
        ProgressSpinnerModule
    ],
    exports: [
        ContentLoaderComponent
    ]
})
export class ContentLoaderModule {}
