import { Component, OnInit, ContentChild, Input, OnDestroy, TemplateRef, ElementRef, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { Observable, Subscription, throwError } from 'rxjs';
import { AutoUnsubscribe } from 'ngx-auto-unsubscribe';
import { catchError } from 'rxjs/operators';

@AutoUnsubscribe()
@Component({
    selector: 'content-loader',
    templateUrl: './content-loader.component.html',
    styleUrls: ['./content-loader.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class ContentLoaderComponent implements OnInit, OnDestroy {

    @ContentChild(TemplateRef, { static: true })
    public contentTemplate: TemplateRef<any>;

    @Input()
    public set showIf (data: any) {
        this.isShow = !!data;
        this.data   = data;

        this.changeDetector.markForCheck();
    }

    @Input()
    public set showIfAsync (_observable: Observable<any>) {
        if (this._showIsSubscription) {
            this._showIsSubscription.unsubscribe();
        }

        this._showIsSubscription = _observable
            .pipe(
                catchError((error) => {
                    this.isErrorInObservable = true;
                    this.changeDetector.markForCheck();

                    return throwError(error);
                })
            )
            .subscribe((data) => {
                this.isShow = !!data;
                this.data   = data;

                this.changeDetector.markForCheck();
            });
    }

    @Input()
    public set waitingForObservable (_observable: Observable<any>) {
        if (this._showIsSubscription) {
            this._showIsSubscription.unsubscribe();
        }

        this._showIsSubscription = _observable
            .pipe(
                catchError((error) => {
                    this.isErrorInObservable = true;
                    this.changeDetector.markForCheck();

                    return throwError(error);
                })
            )
            .subscribe((data) => {
                this.isShow = true;
                this.data   = data;

                this.changeDetector.markForCheck();
            });
    }

    public data: any;

    public isShow: boolean = false;
    public isErrorInObservable = false;

    private _showIsSubscription: Subscription;

    constructor (
        private readonly changeDetector: ChangeDetectorRef
    ) {}

    public ngOnInit (): void {}

    public ngOnDestroy (): void {}

}
