import { Injectable } from '@angular/core';
import { DialogService, DynamicDialogRef } from 'primeng-lts/api';
import { LoadingWindowComponent } from '../loading-window.component';

@Injectable({
    providedIn: 'root'
})
export class LoadingWindowService {

    constructor (
        private readonly dialogService: DialogService
    ) {}

    public show (): DynamicDialogRef {
        return this.dialogService.open(LoadingWindowComponent, {
            data: null,
            closable: false,
            closeOnEscape: false,
            showHeader: false
        });
    }

}
