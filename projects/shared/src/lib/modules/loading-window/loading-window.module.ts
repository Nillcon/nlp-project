import { NgModule } from '@angular/core';
import { ProgressSpinnerModule } from 'primeng-lts/progressspinner';
import { LoadingWindowComponent } from './loading-window.component';

@NgModule({
    declarations: [
        LoadingWindowComponent
    ],
    imports: [
        ProgressSpinnerModule
    ],
    exports: [
        LoadingWindowComponent
    ],
    entryComponents: [
        LoadingWindowComponent
    ]
})
export class LoadingWindowModule {}
