import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { DynamicDialogRef, DynamicDialogConfig } from 'primeng-lts/api';

@Component({
    selector: 'app-loading-window',
    templateUrl: './loading-window.component.html',
    styleUrls: ['./loading-window.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class LoadingWindowComponent implements OnInit {

    constructor (
        private readonly dialogRef: DynamicDialogRef,
        private readonly config: DynamicDialogConfig
    ) {}

    public ngOnInit (): void {}

}
