import { NgModule } from '@angular/core';
import { ConfirmationWindowComponent } from './confirmation-window.component';
import { ConfirmationWindowService } from './services/confirmation-window.service';
import { PipesModule } from '@Shared/pipes/pipes.module';

import { DialogService } from 'primeng-lts/api';
import { DynamicDialogModule } from 'primeng-lts/dynamicdialog';
import { DialogModule } from 'primeng-lts/dialog';
import { ButtonModule } from 'primeng-lts/button';

@NgModule({
    declarations: [
        ConfirmationWindowComponent
    ],
    imports: [
        PipesModule,
        DialogModule,
        ButtonModule,
        DynamicDialogModule
    ],
    entryComponents: [
        ConfirmationWindowComponent
    ],
    providers: [
        DialogService,
        ConfirmationWindowService
    ]
})
export class ConfirmationWindowModule {}
