import { Injectable } from "@angular/core";
import { ConfirmationWindowComponent } from "../confirmation-window.component";
import { Observable } from "rxjs";
import { DialogService } from 'primeng-lts/api';

@Injectable()
export class ConfirmationWindowService {

    constructor (
        private readonly dialogService: DialogService
    ) {}

    public show (
        text: string = 'Are you sure you want to do it?',
        title: string = 'Confirm action'
    ): Observable<boolean> {
        const window = this.dialogService.open(
            ConfirmationWindowComponent,
            {
                width: '350px',
                header: title,
                data: {
                    title: title,
                    message: text
                }
            }
        );

        return window.onClose;
    }

}
