import { Component, OnInit } from '@angular/core';
import { IsNotEmptyObject } from 'class-validator';
import { ClassValidation } from '@Shared/decorators';
import { DynamicDialogRef, DynamicDialogConfig } from 'primeng-lts/api';
import { ConfirmationData } from './interfaces/confirmation-data.interface';

@ClassValidation()
@Component({
    selector: 'app-confirmation-window',
    templateUrl: './confirmation-window.component.html',
    styleUrls: [
        './confirmation-window.component.scss'
    ]
})
export class ConfirmationWindowComponent implements OnInit {

    @IsNotEmptyObject()
    public data: ConfirmationData;

    constructor (
        private readonly dialogRef: DynamicDialogRef,
        private readonly config: DynamicDialogConfig
    ) {
        this.data = this.config.data;
    }

    public ngOnInit (): void {}

    public closeWithResult (result: boolean): void {
        this.dialogRef.close(result);
    }

}
