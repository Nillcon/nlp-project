import { BaseClassForCustomDecorator } from '@Shared/interfaces';
import { ConfirmationWindowService } from '../services/confirmation-window.service';

/** Shows confirmation window before call method.
 * ## Required field in your class:
 * `injector: Injector`
 */
export function NeedsConfirmation (description?: string, title?: string): any {

    return function (targetClass: BaseClassForCustomDecorator, method: string, decorator: PropertyDescriptor): any {
        if (!targetClass['_maPatched']) {
            targetClass['_maPatched']   = true;
            targetClass['_maEventsMap'] = [...(targetClass['_maEventsMap'] || [])];
        }

        let confirmationWindowService: ConfirmationWindowService;

        const ngOnInitUnpatched = targetClass.ngOnInit;
        const originalMethod    = decorator.value;

        targetClass.ngOnInit = function (this: BaseClassForCustomDecorator): any {
            confirmationWindowService = this.injector.get(ConfirmationWindowService);

            if (ngOnInitUnpatched) {
                return ngOnInitUnpatched.call(this);
            }
        };

        decorator.value = function (...args): any {
            confirmationWindowService.show(description, title)
                .subscribe(confirmationResult => {
                    if (confirmationResult) {
                        return originalMethod.apply(this, args);
                    } else {
                        return;
                    }
                });
        };

        return decorator;
    };

}
