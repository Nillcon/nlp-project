import { ElementRef, Directive, TemplateRef, ViewContainerRef, OnDestroy, Input, OnInit } from "@angular/core";
import { Subscription } from "rxjs";
import { AutoUnsubscribe } from "ngx-auto-unsubscribe";
import { ElementRenderableFeatures } from '@Shared/helpers/element-renderable-features.helper';

/** ## Required in your styles import:
 * `@Shared/styles/button-loader.scss`
 */
@AutoUnsubscribe()
@Directive({
    selector: "[ButtonLoader]"
})
export class ButtonLoaderDirective extends ElementRenderableFeatures implements OnInit, OnDestroy {

    @Input('ButtonLoader') public set subscription (subscription: Subscription) {
        if (subscription) {
            this._subscription = subscription;
            this.processElement();
        }
    }

    private _subscription: Subscription;
    private _isLoading: boolean = false;
    private _loadingElement: HTMLDivElement;
    private readonly _defaultHtml: string;

    constructor (
        private readonly element: ElementRef,
        private readonly templateRef: TemplateRef<any>,
        private readonly viewContainer: ViewContainerRef
    ) {
        super(element, templateRef, viewContainer);
    }

    public ngOnInit (): void {
        this.showElement();
    }

    public ngOnDestroy (): void {}

    private processElement (): void {
        if (!this._isLoading) {
            this._isLoading = true;

            this.createLoaderForButton();

            const subscriptionResolver: Promise<any> = new Promise((resolve, reject) => {
                this._subscription.add(resolve);
            });

            subscriptionResolver.then((data) => {
                this.removeLoaderFromButton();
                this._isLoading = false;
            });
        }
    }

    private createLoaderForButton (): void {
        const elementWithDirective: HTMLElement = this.element.nativeElement.nextElementSibling;
        const targetElem: HTMLElement = this.getButtonElement(elementWithDirective);

        this.setVisibilityForElements(targetElem.children, 'collapse');

        this.createLoader();

        targetElem.insertBefore(this._loadingElement, targetElem.firstElementChild);
    }

    private removeLoaderFromButton (): void {
        const elementWithDirective: HTMLElement = this.element.nativeElement.nextElementSibling;
        const targetElem: HTMLElement = this.getButtonElement(elementWithDirective);

        this.setVisibilityForElements(targetElem.children, '');

        this.removeLoader();
    }

    private getButtonElement (elementWithDirective: HTMLElement): HTMLElement {
        let targetElem: HTMLElement;

        if (elementWithDirective.tagName === 'BUTTON') {
            targetElem = elementWithDirective;
        } else {
            targetElem = elementWithDirective.querySelector('button');
        }

        if (!targetElem) {
            throw new Error('ButtonLoader directive works only with <button> elements!');
        }

        return targetElem;
    }

    private createLoader (): void {
        this._loadingElement = document.createElement('div');
        this._loadingElement.classList.add('button-loader');
        this._loadingElement.innerHTML = '<div></div><div></div><div></div><div></div>';
    }

    private removeLoader (): void {
        if (this._loadingElement) {
            this._loadingElement.remove();
        }
    }

    private setVisibilityForElements (elements: HTMLCollection, visibilityVal: string): void {
        for (let i = 0; i < elements.length; i++) {
            const targetElem = elements[i] as HTMLElement;

            if (targetElem.style) {
                targetElem.style.visibility = visibilityVal;
            }
        }
    }

}
