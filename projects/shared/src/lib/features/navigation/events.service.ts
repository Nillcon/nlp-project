import { Injectable } from '@angular/core';
import { Router, NavigationEnd, Event } from '@angular/router';
import { BehaviorSubject, Observable } from 'rxjs';
import { filter } from 'rxjs/operators';

@Injectable({
    providedIn: 'root'
})
export class NavigationEventsService {

    public get navigationEnd$ (): Observable<Event> {
        return this._navigationEnd$.asObservable();
    }

    private readonly _navigationEnd$ = new BehaviorSubject<Event>(null);

    constructor (
        private readonly router: Router
    ) {
        this.initNavigationEndObserver();
    }

    private initNavigationEndObserver (): void {
        this.router.events
            .pipe(
                filter((event) => event instanceof NavigationEnd)
            )
            .subscribe((event) => this._navigationEnd$.next(event));
    }

}
