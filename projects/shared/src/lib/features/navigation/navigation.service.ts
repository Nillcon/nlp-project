import { Injectable } from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router";
import { UrlService } from '../url/url.service';

@Injectable()
export class NavigationService {

    constructor (
        private readonly router: Router,
        private readonly activatedRoute: ActivatedRoute,
        private readonly urlService: UrlService
    ) {}

    public getCurrentUrl (): string {
        return this.router.url;
    }

    public navigate ({
        subUrl = '/',
        relativeToCurrentPath = true,
        params = null,
        mergeQueryParams = false
    }): Promise<boolean> {
        return this.router.navigate(
            [subUrl],
            {
                relativeTo: (relativeToCurrentPath) ? this.activatedRoute : null,
                queryParams: params || this.urlService.getParameters(),
                queryParamsHandling: (mergeQueryParams) ? 'merge' : 'preserve'
            }
        );
    }

}
