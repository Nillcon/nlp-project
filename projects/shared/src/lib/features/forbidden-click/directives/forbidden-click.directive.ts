import { Directive, OnInit, ElementRef } from '@angular/core';

@Directive({
    selector: "[MarkAsForbiddenClick]"
})
export class MarkAsForbiddenClickDirective implements OnInit {

    constructor (
        private readonly element: ElementRef
    ) {}

    public ngOnInit (): void {}

}
