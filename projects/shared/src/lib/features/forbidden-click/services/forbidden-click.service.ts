import { Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root'
})
export class ForbiddenClickService {

    constructor () {}

    public checkIsMarkAsForbiddenClick (event: MouseEvent): boolean {
        const bubbledElements: HTMLElement[] = event.composedPath() as any;

        return bubbledElements
            .some((element) => (element.hasAttribute) ? element.hasAttribute('markasforbiddenclick') : false);
    }

}
