import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { tap, map } from 'rxjs/operators';
import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { HttpOptions } from './interfaces/http-options.interface';
import { HttpRequestService } from './http-request.service';

@Injectable({
    providedIn: 'root'
})
export class HttpFileService {

    constructor (
        private readonly httpService: HttpClient,
        private readonly httpRequestService: HttpRequestService
    ) {}

    public downloadFile (way: string): Observable<Blob> {
        const httpOptions: HttpOptions = this.httpRequestService.httpOptions;
        httpOptions.responseType        = 'blob'     as any;
        httpOptions.observe             = 'response' as any;

        const url: string = this.httpRequestService.makeCorrectUrl(way);

        return this.httpService.get(url, httpOptions)
            .pipe(
                tap((response: HttpResponse<Blob>) => {
                    const fileName = this.getFileNameFromHeaders(response.headers);
                    const fileType = this.getFileTypeFromHeaders(response.headers);

                    this.downloadFileInBrowser(response.body, fileName, fileType);
                }),
                map(response => response.body)
            );
    }

    public uploadFile<T> (way: string, files: Blob | Blob[]): Observable<T> {
        const filesToUpload      = (Array.isArray(files)) ? files : [files];
        const formData: FormData = new FormData();
        const url: string        = this.httpRequestService.makeCorrectUrl(way);
        const httpOptions        = this.httpRequestService.httpOptions;

        filesToUpload.forEach((file, index) => {
            formData.append(`file-${index}`, file);
        });

        return this.httpService.post<T>(url, formData, httpOptions);
    }

    public downloadFileInBrowser (file: Blob, fileName: string, fileType: string): void {
        if (typeof(window.navigator.msSaveOrOpenBlob) === 'function') {
            // File download in Edge (Maybe also in IE)
            window.navigator.msSaveOrOpenBlob(file, `${fileName}.${fileType}`);
        } else {
            // File download in Chrome, Firefox and other browsers
            const urlAddresponses = window.URL.createObjectURL(file);
            const a = document.createElement('a');

            document.body.appendChild(a);
            a.setAttribute('style', 'display: none');
            a.href = urlAddresponses;
            a.download = `${fileName}.${fileType}`;
            a.click();
            window.URL.revokeObjectURL(urlAddresponses);
            a.remove();
        }
    }

    private getFileNameFromHeaders (headers: HttpHeaders): string {
        const fileName: string = headers.get('x-filename') || 'File';

        return fileName;
    }

    private getFileTypeFromHeaders (headers: HttpHeaders): string {
        const fileTypesDictionary = {
            'text/comma-separated-values': 'csv',
            'text/xml': 'xml',
            'application/pdf': 'pdf',
            'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet': 'xlsx'
        };

        const fileTypeFromHeaders: string = headers.get('Content-Type');

        return fileTypesDictionary[fileTypeFromHeaders];
    }

}
