import { InjectionToken } from '@angular/core';

export const HTTP_HOST                  = new InjectionToken<string>('HTTP_HOST');
export const HTTP_API_ROUTE             = new InjectionToken<string>('HTTP_API_ROUTE');
export const DEBUG_HOST_STORAGE_KEY     = new InjectionToken<string>('DEBUG_HOST_STORAGE_KEY');
export const ACCESS_TOKEN_STORAGE_KEY   = new InjectionToken<string>('ACCESS_TOKEN_STORAGE_KEY');
