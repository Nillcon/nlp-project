import { Injectable, Inject } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { LocalStorageService } from '../storage/local-storage.service';
import { HttpOptions } from './interfaces/http-options.interface';
import { HTTP_HOST, HTTP_API_ROUTE, DEBUG_HOST_STORAGE_KEY, ACCESS_TOKEN_STORAGE_KEY } from './config';

@Injectable({
    providedIn: 'root'
})
export class HttpRequestService {

    constructor (
        @Inject(HTTP_HOST) private readonly _host: string,
        @Inject(HTTP_API_ROUTE) private readonly _apiRoute: string,
        @Inject(DEBUG_HOST_STORAGE_KEY) private readonly _debugHostStorageKey: string,
        @Inject(ACCESS_TOKEN_STORAGE_KEY) private readonly _accessTokenStorageKey: string,

        private readonly httpService: HttpClient,
        private readonly storageService: LocalStorageService
    ) {
        console.log(`HOST: ${this.host}`);
    }

    public get<T> (way: string): Observable<T> {
        return this.httpService.get<T>(
            this.makeCorrectUrl(way),
            this.httpOptions
        );
    }

    public post<T> (way: string, data: object = {}): Observable<T> {
        return this.httpService.post<T>(
            this.makeCorrectUrl(way),
            data,
            this.httpOptions
        );
    }

    public put<T> (way: string, data: object = {}): Observable<T> {
        return this.httpService.put<T>(
            this.makeCorrectUrl(way),
            data,
            this.httpOptions
        );
    }

    public patch<T> (way: string, data: object = {}): Observable<T> {
        return this.httpService.patch<T>(
            this.makeCorrectUrl(way),
            data,
            this.httpOptions
        );
    }

    public delete (way: string): Observable<boolean> {
        return this.httpService.delete<boolean>(
            this.makeCorrectUrl(way),
            this.httpOptions
        );
    }

    public get host (): string {
        const debugHost: string = this.storageService.get<string>(this._debugHostStorageKey);
        const host: string   = debugHost || this._host || location.origin;

        return host;
    }

    public get httpOptions (): HttpOptions {
        return {
            headers: {
                Authorization: this.token
            }
        };
    }

    public get token (): string {
        const accessToken = this.storageService.get(this._accessTokenStorageKey);

        return accessToken ? `Bearer ${accessToken}` : '';
    }

    public makeCorrectUrl (urlOrSubUrl: string): string {
        const baseUrl: string                   = this.host;
        const isWayIncludesProtocol: boolean    = this.checkProtocolInWay(urlOrSubUrl);

        return (isWayIncludesProtocol) ? urlOrSubUrl : `${baseUrl}${this._apiRoute}${urlOrSubUrl}`;
    }

    public checkProtocolInWay (way: string): boolean {
        const protocols: string[] = ['http://', 'https://'];

        return protocols.some((currProtocol) => way.includes(currProtocol));
    }

}
