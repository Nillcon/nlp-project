import { Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root'
})
export class DateService {

    constructor () {}

    public minusDays (date: Date, days: number): Date {
        const _date = new Date(date);
        return new Date(_date.setDate(_date.getDate() - days));
    }

    public plusDays (date: Date, days: number): Date {
        const _date = new Date(date);
        return new Date(_date.setDate(_date.getDate() + days));
    }

    public getLocalTime (date: Date): Date {
        const _date = new Date(date);
        const minutes = date.getMinutes();
        const offset = date.getTimezoneOffset();

        _date.setMinutes(minutes - offset);
        return _date;
    }

    public getLocalTimeString (date: Date): string {
        return this.getLocalTime(date).toISOString();
    }

    public getLocalJsonTimeString (date: Date): string {
        const _date = new Date(date);
        const minutes = date.getMinutes();
        const offset = date.getTimezoneOffset();

        _date.setMinutes(minutes - offset);
        return _date.toJSON().slice(0, -1);
    }

    public isToday (date: Date): boolean {
        const _date = new Date(date);
        const dateNow = new Date();

        return (_date.toDateString() === dateNow.toDateString());
    }

    public getDateWithoutTime (date: Date): Date {
        const _date = new Date(date);
        _date.setHours(0, 0, 0, 0);

        return _date;
    }

}
