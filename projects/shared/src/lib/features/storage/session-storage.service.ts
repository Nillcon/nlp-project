import { Injectable, Inject } from '@angular/core';

@Injectable({
    providedIn: 'root'
})
export class SessionStorageService {

    constructor () {}

    public get<T> (key: string): T {
        const data = sessionStorage.getItem(key);

        return (data) ? JSON.parse(data) : null;
    }

    public set (key: string, data: any): void {
        const body = JSON.stringify(data);
        sessionStorage.setItem(key, body);
    }

    public remove (key: string): void {
        sessionStorage.removeItem(key);
    }

    public clear (): void {
        sessionStorage.clear();
    }

}
