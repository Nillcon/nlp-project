import { Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root'
})
export class LocalStorageService {

    constructor () {}

    public get<T> (key: string): T {
        const data = localStorage.getItem(key);

        return (data) ? JSON.parse(data) : null;
    }

    public set (key: string, data: any): void {
        const body = JSON.stringify(data);
        localStorage.setItem(key, body);
    }

    public remove (key: string): void {
        localStorage.removeItem(key);
    }

    public clear (): void {
        localStorage.clear();
    }

}
