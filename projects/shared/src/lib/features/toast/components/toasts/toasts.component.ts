import { Component, OnInit, Input, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { ClassValidation } from '@Shared/decorators';
import { IsString, IsNumber } from 'class-validator';
import { ToastService } from '../../services';

@ClassValidation()
@Component({
    selector: 'toasts-container',
    templateUrl: './toasts.component.html',
    styleUrls: ['./toasts.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class ToastsComponent implements OnInit {

    @Input()
    @IsNumber()
    public marginTop: number = 25;

    @Input()
    @IsString()
    public position: string = 'top-right';

    constructor (
        private readonly changeDetector: ChangeDetectorRef,
        private readonly toastService: ToastService
    ) {}

    public ngOnInit (): void {
        this.onNewToastObserver();
    }

    private onNewToastObserver (): void {
        this.toastService.toastsCreated$
            .subscribe(() => {
                this.changeDetector.markForCheck();
            });
    }

}
