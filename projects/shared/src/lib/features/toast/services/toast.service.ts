import { Injectable } from '@angular/core';
import { MessageService, Message } from 'primeng-lts/api';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Toast } from '../interfaces';

@Injectable({
    providedIn: 'root'
})
export class ToastService {

    public toastsCreated$: Observable<Message[]>;

    private readonly _defaultDuration: number = 5000;

    constructor (
        private readonly messageService: MessageService
    ) {
        this.initToastEvent();
    }

    public info (toast: Partial<Toast> = {
        title: 'Info',
        description: '',
        duration: this._defaultDuration
    }): void {
        this.messageService.add({
            severity: 'info',
            summary: toast.title,
            detail: toast.description,
            life: toast.duration || this._defaultDuration
        });
    }

    public success (toast: Partial<Toast> = {
        title: 'Success',
        description: '',
        duration: this._defaultDuration
    }): void {
        this.messageService.add({
            severity: 'success',
            summary: toast.title,
            detail: toast.description,
            life: toast.duration || this._defaultDuration
        });
    }

    public warn (toast: Partial<Toast> = {
        title: 'Warn',
        description: '',
        duration: this._defaultDuration
    }): void {
        this.messageService.add({
            severity: 'warn',
            summary: toast.title,
            detail: toast.description,
            life: toast.duration || this._defaultDuration
        });
    }

    public error (toast: Partial<Toast> = {
        title: 'Error',
        description: '',
        duration: this._defaultDuration
    }): void {
        this.messageService.add({
            severity: 'error',
            summary: toast.title,
            detail: toast.description,
            life: toast.duration || this._defaultDuration
        });
    }

    private initToastEvent (): void {
        this.toastsCreated$ = this.messageService.messageObserver
            .pipe(
                map((data) => Array.isArray(data) ? data : [data])
            );
    }

}
