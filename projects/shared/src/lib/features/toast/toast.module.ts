import { NgModule } from '@angular/core';
import { ToastModule as PrimeToastModule } from 'primeng-lts/toast';
import { ToastService } from './services/toast.service';
import { MessageService } from 'primeng-lts/api';
import { ToastsComponent } from './components/toasts/toasts.component';

@NgModule({
    declarations: [
        ToastsComponent
    ],
    imports: [
        PrimeToastModule
    ],
    exports: [
        ToastsComponent,

        PrimeToastModule
    ],
    providers: [
        MessageService
    ]
})
export class ToastModule {}
