export interface Toast {
    title: string;
    description: string;
    duration: number;
}
