import { BaseForm } from '../interfaces/base-form.interface';

export function Form (): any {

    return function (targetClass: any): any {
        const originalNgOnInitMethod      = (targetClass.prototype as BaseForm<any>).ngOnInit;
        const originalNgOnDestroyMethod   = (targetClass.prototype as BaseForm<any>)['ngOnDestroy'];
        const originalFormGroupInitMethod = (targetClass.prototype as BaseForm<any>).formGroupInit;
        let isInputDataIsset: boolean     = false;

        targetClass.prototype.ngOnInit = function (...args): any {
            const _this: BaseForm<any> = this;

            if (!isInputDataIsset) {
                _this.formGroupInit();
            } else {
                isInputDataIsset = false;
            }

            originalNgOnInitMethod.apply(_this, args);
        };

        targetClass.prototype.__defineSetter__('inputData', function (value: any): any {
            const _this: BaseForm<any> = this;
            if (value) {
                _this.formGroupInit(value);
                isInputDataIsset = true;
            }
        });

        (targetClass.prototype as BaseForm<any>).formGroupInit = function (...args): any {
            const _this: BaseForm<any>     = this;
            const originalMethodResult: any = originalFormGroupInitMethod.apply(_this, args);

            _this.formGroup = originalMethodResult;

            if (!_this.formGroup) {
                console.error(
                    `%c⚠️BaseFormError \n\n%cformGroupInit must create FormGroup instance for formGroup property!`,
                    'font-size: 20px; font-weight: 600;',
                    'background: #9c1e1e; color: white; font-size: 16px; font-weight: 600; border-radius: 6px; padding: 2px 7px;'
                );
            }

            _this.OnFormInit.emit(_this.formGroup);

            return originalMethodResult;
        };

        (targetClass.prototype as BaseForm<any>)['ngOnDestroy'] = function (...args): any {
            const _this: BaseForm<any> = this;
            isInputDataIsset = false;
            originalNgOnDestroyMethod.apply(_this, args);
        };

    };

}
