import { EventEmitter, OnInit, OnDestroy } from '@angular/core';
import { FormGroupTypeSafe } from 'form-type-safe';

export interface BaseForm<T> extends OnInit, OnDestroy {
    OnFormInit: EventEmitter<FormGroupTypeSafe<T>>;

    inputData?: T;
    formGroup: FormGroupTypeSafe<T>;

    formGroupInit (inputValue?: T): FormGroupTypeSafe<T>;
}
