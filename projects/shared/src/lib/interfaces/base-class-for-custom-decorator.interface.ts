import { Injector, OnInit, OnDestroy } from "@angular/core";

export interface BaseClassForCustomDecorator extends OnInit, OnDestroy {
    injector: Injector;
}
